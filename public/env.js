window.env = {
    pmsApiUrl: '#{pmsApiUrl}', //http://pms-api-mainone.test.vggdev.com
    staffApiUrl: '#{staffApiUrl}', // 'https://staff-api.test.vggdev.com',
    postLogoutRedirectUri: '#{postLogoutRedirectUri}', //'pms-ui.test.vggdev.com',
    redirectUri: '#{redirectUri}', //'https://pms-ui.test.vggdev.com/redirect',
    ssoUrl: '#{ssoUrl}' ,  //'https://sso.test.vggdev.com/identity'
    testVar: '#{testVar}',
}