import axios from "axios";
import Oidc from "oidc-client";

// import {
//   SSO_REDIRECT_PATH,
//   SSO_URL,
//   REDIRECT_URI,
//   POST_LOGOUT_REDIRECT_URI,
//   CLIENT_ID
// } from "./appConstants";

// const settings = {
//   client_id: CLIENT_ID,
//   authority: SSO_URL,
//   redirect_uri: REDIRECT_URI,
//   response_type: "id_token token",
//   scope: "openid profile identity-server-api",
//   post_logout_redirect_uri: POST_LOGOUT_REDIRECT_URI
// };

//  const client = new Oidc.UserManager(settings);


export const createRequest = (url = '', config, token = '') => {
    const validMethods = ['GET', 'POST', 'HEAD', 'PUT', 'DELETE', 'PATCH'];
    const defaultconfig = {
        mode: 'cors',
        cache: 'default',
        credentials: 'same-origin',
    }
    const defaultHeaders = new Headers();
    defaultHeaders.set('Content-Type', 'application/json');
    if (token) {
        defaultHeaders.set('Authorization', `Bearer ${token}`);
    }
    defaultHeaders.set('Accept', `application/json`);
    if (typeof config.method !== 'string') {
        throw new TypeError("config method property must be a string.");
    }
    if (validMethods.indexOf(config.method.toUpperCase()) === -1) {
        throw Error("config method property value most be one of ['GET','POST','HEAD','PUT','DELETE']");
    }
    config.headers = config.headers || defaultHeaders;
    if (config.headers && !config.headers instanceof Headers) {
        throw new TypeError("config headers property must be of type Headers.");
    }
    const requestConfig = {
        ...defaultconfig,
        ...config
    };
    return new Request(url, requestConfig);
}


export const createRequestSansHeader = (url = '', config, token = '') => {
    const validMethods = ['GET', 'POST', 'HEAD', 'PUT', 'DELETE', 'PATCH'];
    const defaultconfig = {
        mode: 'cors',
        cache: 'default',
        credentials: 'same-origin',
    }
    const defaultHeaders = new Headers();
    // defaultHeaders.set('Content-Type', 'application/json');
    if (token) {
        defaultHeaders.set('Authorization', `Bearer ${token}`);
    }
    // defaultHeaders.set('Accept', `application/json`);
    if (typeof config.method !== 'string') {
        throw new TypeError("config method property must be a string.");
    }
    if (validMethods.indexOf(config.method.toUpperCase()) === -1) {
        throw Error("config method property value most be one of ['GET','POST','HEAD','PUT','DELETE']");
    }
    config.headers = config.headers || defaultHeaders;
    if (config.headers && !config.headers instanceof Headers) {
        throw new TypeError("config headers property must be of type Headers.");
    }
    const requestConfig = {
        ...defaultconfig,
        ...config
    };
    return new Request(url, requestConfig);
}


 const Level = [
  "Level 0",
  "Level 1",
  "Level 2",
  "Level 3",
  "Level 3.5",
  "Level 4",
  "Level 5",
  "Level 6",
  "Level 7",
  "Level 8"
];
export default Level;


// const setAuthorizationToken = token => {
//   if (token) {
//     // eslint-disable-next-line no-return-assign
//     return (axios.defaults.headers.common.Authorization = `Bearer ${token}`);
//   } else {
//     delete axios.defaults.headers.common.Authorization;
//   }
// };
// export default setAuthorizationToken;
//const user =  client.getUser();

// client.getUser().then(user =>{
//     console.log(user,"THIS IS THE LOGGED IN USER");
// axios.defaults.headers.common.Authorization = `Bearer ${user}`;

// });

//helper function to sort keyresults according to order before posting
export const sortFunctionTwo = (a,b) => {
    if(a.order < b.order) {
        return -1
    } else if(a.order > b.order) {
        return 1
    } else {
        return 0
    }
}

//userIDs sort function
export const sortFunctionOne = (a, b) => {
    // const { userObjectives } = props
    if (a.order < b.order) {
        return -1
    } else if (a.order > b.order) {
        return 1
    } else {
        return 0
    }
}


//All possible error codes
export const errorCodes =  [ 401, 402, 404, 503 ]