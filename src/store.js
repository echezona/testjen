import { createStore, applyMiddleware, compose } from "redux"
import rootReducer from './reducer'
import createSagaMiddleware from 'redux-saga'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import hardSet from 'redux-persist/lib/stateReconciler/hardSet'
import rootSaga from './sagas'
import { composeWithDevTools } from 'redux-devtools-extension'

const persistConfig = {
    key: 'root',
    storage,
    stateReconciler: hardSet,
    whitelist: ['requestingStaffDetails'],
                // 'requestingLineManagerStaffAppraisals'
                // ['requestingStaffDetails', 'lmPendingKeyResults', 'requestingStaffRole', 
                // 'requestingActiveTab', 'requestingActiveMiniTab', 'userData', 'userDataLm',
                // 'category', 'sbuObjectives'],
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

// export default () => {
//     let store = createStore(persistedReducer)
//     let persistor = persistStore(store)
//     return { store, persistor }
// }
///

const sagaMiddleware = createSagaMiddleware();
const store = createStore(
    persistedReducer,
    composeWithDevTools(applyMiddleware(sagaMiddleware))
)
const persistor = persistStore(store)


sagaMiddleware.run(rootSaga);


export default { store, persistor }
