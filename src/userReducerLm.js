import { combineReducers } from "redux"
import { SET_OKRs, GET_LINE_MANAGER_OKRS, GET_LMPO_OKRS } from "./constants";


const entities = (state = null, { type, entities }) => {
    switch (type) {
        case GET_LMPO_OKRS:
            return entities || []
        default:
            return state
    }
}

const objectivesById = (state = [], { type, ids }) => {
    switch (type) {
        case GET_LMPO_OKRS:
            return ids || []
        default:
            return state
    }
}

export default combineReducers({
    objectivesById,
    entities
})