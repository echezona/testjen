import { combineReducers } from "redux";
import { SET_USER, SET_APPRAISAL_OBJECTIVE_KEY_RESULTS, IS_REQUESTING_SAVE_RESULTS, REQUEST_SUCCESS, REQUEST_ERROR, SET_OKRs, IS_DONE_REQUESTING_SAVE_RESULTS, SAVE_SUCCESSFUL, SAVE_FAILED, SAVE_DONE, GET_SBU_OBJECTIVES, GET_CATEGORY, GET_APPRAISAL_CYCLE, GET_STAFF_APPRAISAL_ID, GET_STAFF_APPRAISAL, GET_APPRAISAL_OBJECTIVES, GET_OKRs_NOT_NORMALIZED, GET_DECODED_TOKEN, SBU_REQUEST_SUCCESS, SBU_REQUEST_FAILURE, SAVE_SUCCESSFUL_MESSAGE_VERSION, SAVE_FAILED_MESSAGE_VERSION, SAVE_DONE_MESSAGE_VERSION, GET_LINE_MANAGER_STAFF_APPRAISALS, GET_STAFF_DETAILS, GET_PEOPLE_OPS_STAFF_APPRAISALS, STAFF_APPRAISAL_EXISTS, STORE_PENDING_KEY_RESULTS, SUBMITTING_OKRS, DONE_SUBMITTING_OKRS, FETCHING_STAFF_APPRAISALS, DONE_FETCHING_STAFF_APPRAISALS, FETCHING_LMPO_OKRS, DONE_FETCHING_LMPO_OKRS, GET_STAFF_ROLE, GET_SUPER_LINE_MANAGER_STAFF_APPRAISALS, STORE_ACTIVE_TAB, STORE_ACTIVE_MINI_TAB, GET_BEHAVIORAL_INDICATORS, CREATE_STAFF_APPRAISAL_ERROR, BEHAVIORAL_INDICATORS_SAVE_SUCCESSFUL, SAVE_BEHAVIORAL_ASSESSMENT_IS_LOADING, SAVE_BEHAVIORAL_ASSESSMENT_HAS_STOPPED_LOADING, FETCH_BEHAVIORAL_ASSESSMENT_IS_LOADING, FETCH_BEHAVIORAL_ASSESSMENT_HAS_STOPPED_LOADING, FETCH_OBJECTIVES_AND_KEY_RESULTS_IS_LOADING, FETCH_OBJECTIVES_AND_KEY_RESULTS_HAS_STOPPED_LOADING, FETCH_CATEGORY_IS_LOADING, FETCH_CATEGORY_HAS_STOPPED_LOADING, FETCH_SBU_OBJECTIVES_IS_LOADING, FETCH_SBU_OBJECTIVES_HAS_STOPPED_LOADING, SAVE_KEY_RESULTS_IS_LOADING, SAVE_KEY_RESULTS_HAS_STOPPED_LOADING, GET_RECOMMENDATION_TYPES, GET_RECOMMENDATION, MODIFY_RECOMMENDATION_AFTER_SUBMIT, MODIFY_RECOMMENDATION_AFTER_SAVE, MODIFY_RECOMMENDATION_AFTER_UPDATE, GET_SUPER_RECOMMENDATION, MODIFY_SUPER_RECOMMENDATION_AFTER_UPDATE, MODIFY_APPRAISAL_CYCLE_AFTER_UPDATE, GET_PEOPLE_OPS_COUNT, GET_ALL_BUSINESS_UNITS, GET_ALL_SBU_OBJECTIVES, MODIFY_ALL_SBU_OBJECTIVES_AFTER_SUBMIT, REPLACE_APPRAISAL_CYCLE_AFTER_CREATION, SAVE_STAFF_APPRAISAL_IS_LOADING, SAVE_STAFF_APPRAISAL_HAS_STOPPED_LOADING, GET_BEHAVIOURS, FETCH_BEHAVIOURS_IS_LOADING, FETCH_BEHAVIOURS_HAS_STOPPED_LOADING, UPLOAD_BEHAVIOURS_IS_LOADING, UPLOAD_BEHAVIOURS_HAS_STOPPED_LOADING, CREATE_SBU_OBJECTIVES_IS_LOADING, CREATE_SBU_OBJECTIVES_HAS_STOPPED_LOADING, LOCK_APPRAISAL_PERIOD_IS_LOADING, LOCK_APPRAISAL_PERIOD_HAS_STOPPED_LOADING, SBU_OBJECTIVES_SAVE_SUCCESSFUL, SBU_OBJECTIVES_SAVE_FAILED, SBU_OBJECTIVES_SAVE_DONE, LOCK_APPRAISAL_CYCLE_SUCCESSFUL, LOCK_APPRAISAL_CYCLE_FAILED, LOCK_APPRAISAL_CYCLE_DONE, UPLOAD_BEHAVIOUR_SUCCESSFUL, UPLOAD_BEHAVIOUR_FAILED, UPLOAD_BEHAVIOUR_DONE, FETCH_LMPO_OKRS_ERROR, FETCH_STAFF_DETAILS_IS_LOADING, FETCH_STAFF_DETAILS_HAS_STOPPED_LOADING, MODIFY_LINE_MANAGER_STAFF_APPRAISALS_ON_OKR_SUBMIT, SUBMIT_OKRS_SUCCESSFUL, SUBMIT_OKRS_FAILED, SUBMIT_OKRS_DONE, MODIFY_SUPER_LINE_MANAGER_STAFF_APPRAISALS_ON_OKR_SUBMIT, MODIFY_PEOPLE_OPS_STAFF_APPRAISALS_ON_OKR_SUBMIT, SUBMIT_PEOPLE_OPS_OKRS_IS_LOADING, SUBMIT_PEOPLE_OPS_OKRS_HAS_STOPPED_LOADING, UPDATE_KEY_RESULTS_IS_LOADING, UPDATE_KEY_RESULTS_HAS_STOPPED_LOADING, UPDATE_KEY_RESULTS_SUCCESSFUL, UPDATE_KEY_RESULTS_FAILED, UPDATE_KEY_RESULTS_DONE, MODIFY_STAFF_APPRAISALS_ON_KEY_RESULTS_UPDATE, GET_CONTINUE_ENABLED_OBJECTIVES, GET_CONTINUE_ENABLED_KEY_RESULTS, MODIFY_CONTINUE_ENABLED_OBJECTIVES_ON_SAVE, MODIFY_CONTINUE_ENABLED_KEY_RESULTS_ON_SAVE, ONBOARDING_USER_IS_LOADING, ONBOARDING_USER_HAS_STOPPED_LOADING, ONBOARDING_USER_SUCCESSFUL, ONBOARDING_USER_FAILED, ONBOARDING_USER_DONE, CHANGE_PASSWORD_IS_LOADING, CHANGE_PASSWORD_HAS_STOPPED_LOADING, CHANGING_PASSWORD_SUCCESSFUL, CHANGE_PASSWORD_SUCCESSFUL, CHANGE_PASSWORD_FAILED, CHANGE_PASSWORD_DONE, FETCH_STAFF_DETAILS_RESPONSE_STATUS, FETCH_STAFF_APPRAISAL_IS_LOADING, FETCH_STAFF_APPRAISAL_HAS_STOPPED_LOADING, SUBMIT_LINE_MANAGER_OKRS_IS_LOADING, SUBMIT_LINE_MANAGER_OKRS_HAS_STOPPED_LOADING, LINE_MANAGER_UPDATE_KEY_RESULTS_SUCCESSFUL, LINE_MANAGER_UPDATE_KEY_RESULTS_FAILED, LINE_MANAGER_UPDATE_KEY_RESULTS_DONE, PEOPLE_OPS_UPDATE_KEY_RESULTS_SUCCESSFUL, PEOPLE_OPS_UPDATE_KEY_RESULTS_FAILED, PEOPLE_OPS_UPDATE_KEY_RESULTS_DONE, UPDATE_SBU_OBJECTIVE_IS_LOADING, UPDATE_SBU_OBJECTIVE_HAS_STOPPED_LOADING, DELETE_SBU_OBJECTIVE_IS_LOADING, DELETE_SBU_OBJECTIVE_HAS_STOPPED_LOADING, UNLOCK_APPRAISAL_PERIOD_IS_LOADING, UNLOCK_APPRAISAL_PERIOD_HAS_STOPPED_LOADING, UNLOCK_APPRAISAL_CYCLE_SUCCESSFUL, UNLOCK_APPRAISAL_CYCLE_FAILED, UNLOCK_APPRAISAL_CYCLE_DONE, FETCH_APPRAISAL_CYCLE_IS_LOADING, FETCH_APPRAISAL_CYCLE_HAS_STOPPED_LOADING, MODIFY_ALL_SBU_OBJECTIVES_AFTER_UPDATE, MODIFY_ALL_SBU_OBJECTIVES_AFTER_DELETE, FETCH_ALL_BUSINESS_UNITS_IS_LOADING, FETCH_ALL_BUSINESS_UNITS_HAS_STOPPED_LOADING, SAVE_BEHAVIOUR_SUCCESSFUL, SAVE_BEHAVIOUR_FAILED, SAVE_BEHAVIOUR_DONE, UPDATE_RECOMMENDATION_SUCCESSFUL, UPDATE_RECOMMENDATION_FAILED, UPDATE_RECOMMENDATION_DONE, UPDATE_RECOMMENDATION_IS_LOADING, UPDATE_RECOMMENDATION_HAS_STOPPED_LOADING, FETCH_STAFF_APPRAISAL_SCORE_IS_LOADING, FETCH_STAFF_APPRAISAL_SCORE_HAS_STOPPED_LOADING, CALCULATE_STAFF_APPRAISAL_SCORE_SUCCESSFUL, CALCULATE_STAFF_APPRAISAL_SCORE_FAILED, CALCULATE_STAFF_APPRAISAL_SCORE_DONE, CALCULATE_STAFF_APPRAISAL_SCORE_IS_LOADING, CALCULATE_STAFF_APPRAISAL_SCORE_HAS_STOPPED_LOADING, GET_STAFF_APPRAISAL_SCORE, MODIFY_STAFF_APPRAISAL_SCORE_AFTER_CALCULATION, SUBMIT_RECOMMENDATION_SUCCESSFUL, SUBMIT_RECOMMENDATION_FAILED, SUBMIT_RECOMMENDATION_DONE, SUBMIT_RECOMMENDATION_HAS_STOPPED_LOADING, SUBMIT_RECOMMENDATION_IS_LOADING, SUBMIT_LINE_MANAGER_ASSESSMENT_SUCCESSFUL, SUBMIT_LINE_MANAGER_ASSESSMENT_FAILED, SUBMIT_LINE_MANAGER_ASSESSMENT_DONE, SUBMIT_LINE_MANAGER_ASSESSMENT_IS_LOADING, SUBMIT_LINE_MANAGER_ASSESSMENT_HAS_STOPPED_LOADING, MODIFY_LINE_MANAGER_STAFF_APPRAISALS_ON_ASSESS_COMPLETE, MODIFY_SUPER_LINE_MANAGER_STAFF_APPRAISALS_ON_ASSESS_COMPLETE, SAVE_APPRAISAL_CYCLE_IS_LOADING, SAVE_APPRAISAL_CYCLE_HAS_STOPPED_LOADING, SAVE_APPRAISAL_CYCLE_SUCCESSFUL, SAVE_APPRAISAL_CYCLE_FAILED, SAVE_APPRAISAL_CYCLE_DONE, DELETE_APPRAISAL_OBJECTIVE_IS_LOADING, DELETE_APPRAISAL_OBJECTIVE_HAS_STOPPED_LOADING, ADD_KEY_RESULT_IS_LOADING, ADD_KEY_RESULT_HAS_STOPPED_LOADING, DELETE_KEY_RESULT_IS_LOADING, DELETE_KEY_RESULT_HAS_STOPPED_LOADING, GET_ALL_LINE_MANAGERS, FETCH_ALL_LINE_MANAGERS, FETCH_ALL_LINE_MANAGERS_HAS_STOPPED_LOADING, FETCH_ALL_LINE_MANAGERS_IS_LOADING, GET_ALL_DEPARTMENTS, FETCH_ALL_DEPARTMENTS_IS_LOADING, FETCH_ALL_DEPARTMENTS_HAS_STOPPED_LOADING, GET_STAFF_DETAILS_BY_ADMIN, FETCH_STAFF_DETAILS_BY_ADMIN_IS_LOADING, FETCH_STAFF_DETAILS_BY_ADMIN_HAS_STOPPED_LOADING, UPDATE_STAFF_DETAILS, UPDATE_STAFF_DETAILS_IS_LOADING, UPDATE_STAFF_DETAILS_HAS_STOPPED_LOADING, GET_STAFF_CLAIMS, FETCH_STAFF_CLAIMS_IS_LOADING, FETCH_STAFF_CLAIMS_HAS_STOPPED_LOADING, UPDATE_USER_CLAIMS_IS_LOADING, UPDATE_USER_CLAIMS_HAS_STOPPED_LOADING, GET_STAFF_OKR_PERIOD, FETCH_STAFF_OKR_PERIOD_IS_LOADING, FETCH_STAFF_OKR_PERIOD_HAS_STOPPED_LOADING, CREATE_STAFF_OKR_PERIOD_IS_LOADING, CREATE_STAFF_OKR_PERIOD_HAS_STOPPED_LOADING, UPDATE_STAFF_OKR_PERIOD_IS_LOADING, UPDATE_STAFF_OKR_PERIOD_HAS_STOPPED_LOADING, GET_ALL_STAFF_OKR_PERIOD, FETCH_ALL_STAFF_OKR_PERIOD_IS_LOADING, FETCH_ALL_STAFF_OKR_PERIOD_HAS_STOPPED_LOADING, GET_ALL_LM_STAFF_OKR_PERIOD, FETCH_ALL_LM_STAFF_OKR_PERIOD_IS_LOADING, FETCH_ALL_LM_STAFF_OKR_PERIOD_HAS_STOPPED_LOADING} from './constants';
import userReducer from "./userReducer";
import userReducerLm from "./userReducerLm";

export const userSelector = (state) => {
    return state.user ? state.user : null
}

export const userProfileSelector = state => {
    return state.user ? state.user.profile : null
}

export const userEmailSelector = state => {
    return state.user.profile ? state.user.profile.email : null
}

export const userCategoriesSelector = (state) => {
    return state.userData.entities ? state.userData.entities.categories : null
}

export const userKeyResultsSelector = state => {
    return state.userData.entities ? state.userData.entities.keyResults : null
}

export const userObjectivesSelector = state => {
    return state.userData.entities ? state.userData.entities.Objectives : null
}

export const userIDsSelector = state => {
    return state.userData ? state.userData.objectivesById : null
}


export const lmCategoriesSelector = (state) => {
    return state.userDataLm.entities ? state.userDataLm.entities.categories : null
}

export const lmKeyResultsSelector = state => {
    return state.userDataLm.entities ? state.userDataLm.entities.keyResults : null
}

export const lmObjectivesSelector = state => {
    return state.userDataLm.entities ? state.userDataLm.entities.Objectives : null
}

export const lmSbuObjectivesSelector = state => {
    return state.userDataLm.entities ? state.userDataLm.entities.sbuObjectives : null
}

// export const lmStaffAppraisalsSelector = state => {
//     return state.userDataLm.entities ? state.userDataLm.entities.staffAppraisals : null
// }

export const lmIDsSelector = state => {
    return state.userDataLm ? state.userDataLm.objectivesById : null
}


export const sbuObjectivesSelector = state => {
    return state.sbuObjectives ? state.sbuObjectives : null
}

export const categorySelector = state => {
    return state.category ? state.category : null
}

export const appraisalCycleIdSelector = state => {
    return state.requestingAppraisalCycle ? state.requestingAppraisalCycle.id : null
}

export const staffAppraisalIdSelector = state => {
    return state.requestingStaffAppraisal ? state.requestingStaffAppraisal.id : null
}

export const staffAppraisalSelector = state => {
    return state.requestingStaffAppraisal ? state.requestingStaffAppraisal : null
}

export const appraisalObjectivesSelector = state => {
    return state.requestingAppraisalObjectives ? state.requestingAppraisalObjectives : null
}

export const notNormalizedOkrsSelector = state => {
    return state.requestingNotNormalizedOkrs ? state.requestingNotNormalizedOkrs : null
}

export const decodedTokenRoleSelector = state => {
    return state.requestingDecodedToken["pmsapi.role"] ? state.requestingDecodedToken["pmsapi.role"] : null
}

export const lmStaffAppraisalsSelector = state => {
    return state.requestingLineManagerStaffAppraisals ? state.requestingLineManagerStaffAppraisals : null
}

export const poStaffAppraisalsSelector = state => {
    return state.requestingPeopleOpsStaffAppraisals ? state.requestingPeopleOpsStaffAppraisals : null
}

export const staffIdSelector = state => {
    return state.requestingStaffDetails ? state.requestingStaffDetails.staffId : null
}

export const sbuNameSelector = state => {
    return state.requestingStaffDetails.businessUnit ? state.requestingStaffDetails.businessUnit.name : null
}

export const sbuIdSelector = state => {
    return state.requestingStaffDetails ? state.requestingStaffDetails.sbuId : null
}



const sbuObjectives = (state = [], { type, payload }) => {
    switch (type) {
        case GET_SBU_OBJECTIVES:
            return payload || []
        default:
            return state
    }
}

const REQUESTING_SAVE_KEY_RESULTS = (state = false, { type }) => {
    switch (type) {
        case IS_REQUESTING_SAVE_RESULTS:
            return true
        case IS_DONE_REQUESTING_SAVE_RESULTS:
            return false
        default:
            return state
    }
}

const SAVING_OBJECTIVE_STATUS = (state = null, { type }) => {
    switch(type) {
        case SAVE_SUCCESSFUL:
            return true
        case SAVE_FAILED:
            return false
        case SAVE_DONE:
            return null
        default:
            return state
    }
}

const SAVING_SBU_OBJECTIVES_STATUS = (state = null, { type }) => {
    switch(type) {
        case SBU_OBJECTIVES_SAVE_SUCCESSFUL:
            return true
        case SBU_OBJECTIVES_SAVE_FAILED:
            return false
        case SBU_OBJECTIVES_SAVE_DONE:
            return null
        default:
            return state
    }
}

const LOCK_APPRAISAL_CYCLE_STATUS = (state = null, { type }) => {
    switch(type) {
        case LOCK_APPRAISAL_CYCLE_SUCCESSFUL:
            return true
        case LOCK_APPRAISAL_CYCLE_FAILED:
            return false
        case LOCK_APPRAISAL_CYCLE_DONE:
            return null
        default:
            return state
    }
}

const UNLOCK_APPRAISAL_CYCLE_STATUS = (state = null, { type }) => {
    switch(type) {
        case UNLOCK_APPRAISAL_CYCLE_SUCCESSFUL:
            return true
        case UNLOCK_APPRAISAL_CYCLE_FAILED:
            return false
        case UNLOCK_APPRAISAL_CYCLE_DONE:
            return null
        default:
            return state
    }
}

const SUBMIT_OKRS_STATUS = (state = null, { type }) => {
    switch(type) {
        case SUBMIT_OKRS_SUCCESSFUL:
            return true
        case SUBMIT_OKRS_FAILED:
            return false
        case SUBMIT_OKRS_DONE:
            return null
        default:
            return state
    }
}

const UPLOAD_BEHAVIOUR_STATUS = (state = null, { type }) => {
    switch(type) {
        case UPLOAD_BEHAVIOUR_SUCCESSFUL:
            return true
        case UPLOAD_BEHAVIOUR_FAILED:
            return false
        case UPLOAD_BEHAVIOUR_DONE:
            return null
        default:
            return state
    }
}

const SAVE_BEHAVIOUR_STATUS = (state = null, { type }) => {
    switch(type) {
        case SAVE_BEHAVIOUR_SUCCESSFUL:
            return true
        case SAVE_BEHAVIOUR_FAILED:
            return false
        case SAVE_BEHAVIOUR_DONE:
            return null
        default:
            return state
    }
}

const UPDATE_RECOMMENDATION_STATUS = (state = null, { type }) => {
    switch(type) {
        case UPDATE_RECOMMENDATION_SUCCESSFUL:
            return true
        case UPDATE_RECOMMENDATION_FAILED:
            return false
        case UPDATE_RECOMMENDATION_DONE:
            return null
        default:
            return state
    }
}

const SUBMIT_RECOMMENDATION_STATUS = (state = null, { type }) => {
    switch(type) {
        case SUBMIT_RECOMMENDATION_SUCCESSFUL:
            return true
        case SUBMIT_RECOMMENDATION_FAILED:
            return false
        case SUBMIT_RECOMMENDATION_DONE:
            return null
        default:
            return state
    }
}

const UPDATE_KEY_RESULTS_STATUS = (state = null, { type }) => {
    switch(type) {
        case UPDATE_KEY_RESULTS_SUCCESSFUL:
            return true
        case UPDATE_KEY_RESULTS_FAILED:
            return false
        case UPDATE_KEY_RESULTS_DONE:
            return null
        default:
            return state
    }
}

const CALCULATE_STAFF_APPRAISAL_SCORE_STATUS = (state = null, { type }) => {
    switch(type) {
        case CALCULATE_STAFF_APPRAISAL_SCORE_SUCCESSFUL:
            return true
        case CALCULATE_STAFF_APPRAISAL_SCORE_FAILED:
            return false
        case CALCULATE_STAFF_APPRAISAL_SCORE_DONE:
            return null
        default:
            return state
    }
}

const SUBMIT_LINE_MANAGER_ASSESSMENT_STATUS = (state = null, { type }) => {
    switch(type) {
        case SUBMIT_LINE_MANAGER_ASSESSMENT_SUCCESSFUL:
            return true
        case SUBMIT_LINE_MANAGER_ASSESSMENT_FAILED:
            return false
        case SUBMIT_LINE_MANAGER_ASSESSMENT_DONE:
            return null
        default:
            return state
    }
}

const SAVE_APPRAISAL_CYCLE_STATUS = (state = null, { type }) => {
    switch(type) {
        case SAVE_APPRAISAL_CYCLE_SUCCESSFUL:
            return true
        case SAVE_APPRAISAL_CYCLE_FAILED:
            return false
        case SAVE_APPRAISAL_CYCLE_DONE:
            return null
        default:
            return state
    }
}


const ONBOARDING_USER_STATUS = (state = null, { type, message }) => {
    switch(type) {
        case ONBOARDING_USER_SUCCESSFUL:
            return { status: true, message }
        case ONBOARDING_USER_FAILED:
            return { status: false, message }
        case ONBOARDING_USER_DONE:
            return null
        default:
            return state
    }
}

const CHANGE_PASSWORD_STATUS = (state = null, { type, message }) => {
    switch(type) {
        case CHANGE_PASSWORD_SUCCESSFUL:
            return { status: true, message }
        case CHANGE_PASSWORD_FAILED:
            return { status: false, message }
        case CHANGE_PASSWORD_DONE:
            return null
        default:
            return state
    }
}

const LINE_MANAGER_UPDATE_KEY_RESULTS_STATUS = (state = null, { type, message }) => {
    switch(type) {
        case LINE_MANAGER_UPDATE_KEY_RESULTS_SUCCESSFUL:
            return { status: true, message }
        case LINE_MANAGER_UPDATE_KEY_RESULTS_FAILED:
            return { status: false, message }
        case LINE_MANAGER_UPDATE_KEY_RESULTS_DONE:
            return null
        default:
            return state
    }
}

const PEOPLE_OPS_UPDATE_KEY_RESULTS_STATUS = (state = null, { type, message }) => {
    switch(type) {
        case PEOPLE_OPS_UPDATE_KEY_RESULTS_SUCCESSFUL:
            return { status: true, message }
        case PEOPLE_OPS_UPDATE_KEY_RESULTS_FAILED:
            return { status: false, message }
        case PEOPLE_OPS_UPDATE_KEY_RESULTS_DONE:
            return null
        default:
            return state
    }
}

const SAVING_OBJECTIVE_STATUS_MESSAGE_VERSION = (state = null, { type }) => {
    switch(type) {
        case SAVE_SUCCESSFUL_MESSAGE_VERSION:
            return true
        case SAVE_FAILED_MESSAGE_VERSION:
            return false
        case SAVE_DONE_MESSAGE_VERSION:
            return null
        default:
            return state
    }
}



const user = (state = {}, { type, user }) => {
    switch (type) {
        case SET_USER:
            return user
        default:
            return state
    }
}

const appraisalObjectiveKeyResults = (state = {}, { type, payload }) => {
    switch (type) {
        case SET_APPRAISAL_OBJECTIVE_KEY_RESULTS:
            return payload || {}
        default:
            return state
    }
}

const setOKRs = (state = {}, {type, payload}) => {
    switch(type){
        case SET_OKRs: 
            return payload || {}
        default:
            return state
    }
}

const requestingNotNormalizedOkrs = (state = {}, {type,data}) => {
    switch(type){
        case GET_OKRs_NOT_NORMALIZED:
            return data || {}
        default:
            return state
    }
}

///
const requestingOKRs = (state = true, { type }) => {
    switch (type) {
        case REQUEST_SUCCESS:
            return false
        case REQUEST_ERROR:
            return true
        default:
            return state
    }
}

const requestingSBUs = (state = true, { type }) => {
    switch (type) {
        case SBU_REQUEST_SUCCESS:
            return false
        case SBU_REQUEST_FAILURE:
            return true
        default:
            return state
    }
}

const category = (state = [], { type,payload }) => {
    switch (type) {
        case GET_CATEGORY:
            return payload || []
        default:
            return state
    }
}

const requestingAppraisalCycle = (state = {}, { type,payload , data}) => {
    switch(type) {
        case GET_APPRAISAL_CYCLE: 
            return payload || {}
        case MODIFY_APPRAISAL_CYCLE_AFTER_UPDATE: 
            return { ...state, ...data }
        case REPLACE_APPRAISAL_CYCLE_AFTER_CREATION: 
            return { ...data }
        default:
            return state
    }
}

const requestingStaffAppraisalId = (state = '', { type,payload }) => {
    switch(type) {
        case GET_STAFF_APPRAISAL_ID: 
            return payload || ''
        default:
            return state
    }
}

const requestingStaffAppraisal = (state = {}, { type,payload,error ,staffSubmitted }) => {
    switch(type) {
        case GET_STAFF_APPRAISAL: 
            return payload || {}
        case CREATE_STAFF_APPRAISAL_ERROR:
            return {...state, error}
        case MODIFY_STAFF_APPRAISALS_ON_KEY_RESULTS_UPDATE:
            return { ...state, ...staffSubmitted }
        default:
            return state
    }
}

const requestingStaffDetails = (state = {}, { type,payload }) => {
    switch(type) {
        case GET_STAFF_DETAILS:
            return payload || {}
        default:
            return state
    }
}

const requestingStaffRole = (state = "", { type,payload }) => {
    switch(type) {
        case GET_STAFF_ROLE:
            return payload || ""
        default:
            return state
    }
}

const requestingAppraisalObjectives = (state = [], { type,payload }) => {
    switch(type) {
        case GET_APPRAISAL_OBJECTIVES:
            return payload || []
        default:
            return state
    }
}

const requestingDecodedToken = (state = '' , { type, token }) => {
    switch(type) {
        case GET_DECODED_TOKEN:
            return token
        default:
            return state
    }
}

const requestingLineManagerStaffAppraisals = (state = [], { type,payload, data,lineManagerStatus, superLineManagerStatus }) => {
    switch(type) {
        case GET_LINE_MANAGER_STAFF_APPRAISALS: 
            return payload || []
        case MODIFY_LINE_MANAGER_STAFF_APPRAISALS_ON_OKR_SUBMIT: 
            return state.map((item, index) => {
                // console.log("found you", state)
                if(item.staffId !== data.staffId) {
                    
                    //this is not the item we want, keep it as is
                    return item
                }
                // Otherwise, this is the one we want - return an updated value
                
                return {
                    ...item,
                    lineManagerStatus,
                    superLineManagerStatus,
                    ...data //spread in the data object to update the "firstApproved" property
                  }
            })
        case MODIFY_LINE_MANAGER_STAFF_APPRAISALS_ON_ASSESS_COMPLETE:
            return state.map(item => {
                if(item.staffId !== data.staffId) {
                    return item
                }
                return {
                    ...item,
                    ...data
                }
            })
        default:
            return state
    }
}

const requestingSuperLineManagerStaffAppraisals = (state = [], { type,payload , data,superLineManagerStatus}) => {
    switch(type) {
        case GET_SUPER_LINE_MANAGER_STAFF_APPRAISALS: 
            return payload || []
        case MODIFY_SUPER_LINE_MANAGER_STAFF_APPRAISALS_ON_OKR_SUBMIT: 
            return state.map((item, index) => {
                if(item.staffId !== data.staffId) {
                    
                    //this is not the item we want, keep it as is
                    return item
                }
                // Otherwise, this is the one we want - return an updated value
                
                return {
                    ...item,
                    ...superLineManagerStatus
                }
            })
        case MODIFY_SUPER_LINE_MANAGER_STAFF_APPRAISALS_ON_ASSESS_COMPLETE:
                return state.map(item => {
                    if(item.staffId !== data.staffId) {
                        return item
                    }
                    return {
                        ...item,
                        ...data
                    }
                })
        default:
            return state
    }
}

const requestingPeopleOpsStaffAppraisals = (state = [], { type,payload , data,peopleOpsStatus }) => {
    switch(type) {
        case GET_PEOPLE_OPS_STAFF_APPRAISALS: 
            return payload || []
        case MODIFY_PEOPLE_OPS_STAFF_APPRAISALS_ON_OKR_SUBMIT: 
            return state.map((item, index) => {
                if(item.staffId !== data.staffId) {
                    //this is not the item we want, keep it as is
                    return item
                }
                // Otherwise, this is the one we want - return an updated value
                return {
                    ...item,
                    ...peopleOpsStatus
                }
            })
        default:
            return state
    }
}

const requestingRecommendationTypes = (state = [], { type,payload }) => {
    switch(type) {
        case GET_RECOMMENDATION_TYPES: 
            return payload || []
        default:
            return state
    }
}

const requestingRecommendation = (state = {}, { type,payload ,id,recommendation}) => {
    switch(type) {
        case GET_RECOMMENDATION: 
            return payload || {}
        case MODIFY_RECOMMENDATION_AFTER_SUBMIT:
            return { ...state, id }
        case MODIFY_RECOMMENDATION_AFTER_UPDATE:
            return { ...state, ...recommendation }
        default:
            return state
    }
}

const requestingSuperRecommendation = (state = {}, { type,payload ,id,recommendation}) => {
    switch(type) {
        case GET_SUPER_RECOMMENDATION: 
            return payload || {}
        // case MODIFY_SUPER_RECOMMENDATION_AFTER_SUBMIT:
        //     return { ...state, id }
        case MODIFY_SUPER_RECOMMENDATION_AFTER_UPDATE:
            return { ...state, ...recommendation }
        default:
            return state
    }
}

const requestingStaffAppraisalExistence = (state = false, {type}) => {
    switch(type){
        case STAFF_APPRAISAL_EXISTS:
            return true
        default:
            return state
    }
}

const requestingPeopleOpsCount = (state = {}, {type, payload}) => {
    switch(type){
        case GET_PEOPLE_OPS_COUNT:
            return payload || {}
        default:
            return state
    }
}

const requestingAllBusinessUnits = (state = [], {type, payload}) => {
    switch(type){
        case GET_ALL_BUSINESS_UNITS:
            return payload || []
        default:
            return state
    }
}

const requestingAllSbuObjectives = (state = [], {type, payload, data, id}) => {
    switch(type){
        case GET_ALL_SBU_OBJECTIVES:
            return payload || []
        case MODIFY_ALL_SBU_OBJECTIVES_AFTER_SUBMIT:
            return [ ...state,  data ]
        case MODIFY_ALL_SBU_OBJECTIVES_AFTER_UPDATE: //using an object to update an object in an array
            return state.map((item, index) => {
                if(item.id !== data.id) {
                    //this is not the item we want, keep it as is
                    return item
                }
                // Otherwise, this is the one we want - return an updated value
                return {
                    ...item,
                    ...data
                }
            })
        case MODIFY_ALL_SBU_OBJECTIVES_AFTER_DELETE:
            return state.filter((item) => item.id !== id)
        default:
            return state
    }
}

const requestingStaffAppraisalScore = (state={}, {type, payload, data}) => {
    switch(type){
        case GET_STAFF_APPRAISAL_SCORE:
            return payload || {}
        case MODIFY_STAFF_APPRAISAL_SCORE_AFTER_CALCULATION:
            return {...state, ...data}
        default:
            return state
    }
}

const requestingBehaviours = (state = [], {type, payload}) => {
    switch(type){
        case GET_BEHAVIOURS:
            return payload || []
        default:
            return state
    }
}

const lmPendingKeyResults = (state = [], {type, keyResults}) => {
    switch(type){
        case STORE_PENDING_KEY_RESULTS: 
            return keyResults
        default: 
            return state
    }
}

const isSubmittingOKRs = (state = false, {type}) => {
    switch(type){
        case SUBMITTING_OKRS:
            return true
        case DONE_SUBMITTING_OKRS:
            return false
        default:
            return state
    }
}

const isFetchingStaffAppraisals = (state = false, {type}) => {
    switch(type){
        case FETCHING_STAFF_APPRAISALS:
            return true
        case DONE_FETCHING_STAFF_APPRAISALS: 
            return false
        default:
            return state 
    }
}

const isFetchingLMPOOKRs = (state = false, {type}) => {
    switch(type){
        case FETCHING_LMPO_OKRS:
            return true
        case DONE_FETCHING_LMPO_OKRS:
            return false
        default:
            return state
    }
}

const requestingActiveTab = (state = null, {type, data}) => {
    switch(type){
        case STORE_ACTIVE_TAB:
            return data
        default:
            return state
    }
}

const requestingActiveMiniTab = (state = null, {type, data}) => {
    switch(type){
        case STORE_ACTIVE_MINI_TAB:
            return data
        default:
            return state
    }
}

const requestingBehavioralIndicators = (state = [], { type,payload, data, objIndex }) => {
    switch(type) {
        //fetch and store behavioral indicators
        case GET_BEHAVIORAL_INDICATORS: 
            return payload || []
        //update behavioral indicators after each save
        case BEHAVIORAL_INDICATORS_SAVE_SUCCESSFUL: 
            return state.map((item, index) => {
                if(index !== objIndex) {
                    //this is not the item we want, keep it as is
                    return item
                }
                // Otherwise, this is the one we want - return an updated value
                return {
                    ...item,
                    ...data
                  }
            })
        default:
            return state
    }
}

const saveLoadingReducer = (state = {}, {type}) => {
    switch(type) {
        // pages/BehaviorlAssessment/results.js
        case SAVE_BEHAVIORAL_ASSESSMENT_IS_LOADING:
            return { ...state, SAVE_BEHAVIORAL_ASSESSMENT_LOADING_STATUS: true }
        case SAVE_BEHAVIORAL_ASSESSMENT_HAS_STOPPED_LOADING:
            return { ...state, SAVE_BEHAVIORAL_ASSESSMENT_LOADING_STATUS: false }
        case SAVE_KEY_RESULTS_IS_LOADING:
            return { ...state, SAVE_KEY_RESULTS_LOADING_STATUS: true }
        case SAVE_KEY_RESULTS_HAS_STOPPED_LOADING:
            return { ...state, SAVE_KEY_RESULTS_LOADING_STATUS: false }
        case SAVE_STAFF_APPRAISAL_IS_LOADING:
            return { ...state, SAVE_STAFF_APPRAISAL_LOADING_STATUS: true }
        case SAVE_STAFF_APPRAISAL_HAS_STOPPED_LOADING:
            return { ...state, SAVE_STAFF_APPRAISAL_LOADING_STATUS: false }
        case UPLOAD_BEHAVIOURS_IS_LOADING:
            return { ...state, UPLOAD_BEHAVIOURS_LOADING_STATUS: true }
        case UPLOAD_BEHAVIOURS_HAS_STOPPED_LOADING:
            return { ...state, UPLOAD_BEHAVIOURS_LOADING_STATUS: false }
        case CREATE_SBU_OBJECTIVES_IS_LOADING:
            return { ...state, CREATE_SBU_OBJECTIVES_LOADING_STATUS: true }
        case CREATE_SBU_OBJECTIVES_HAS_STOPPED_LOADING:
            return { ...state, CREATE_SBU_OBJECTIVES_LOADING_STATUS: false }
        case LOCK_APPRAISAL_PERIOD_IS_LOADING:
            return { ...state, LOCK_APPRAISAL_PERIOD_LOADING_STATUS: true }
        case LOCK_APPRAISAL_PERIOD_HAS_STOPPED_LOADING:
            return { ...state, LOCK_APPRAISAL_PERIOD_LOADING_STATUS: false }
        case UNLOCK_APPRAISAL_PERIOD_IS_LOADING:
            return { ...state, UNLOCK_APPRAISAL_PERIOD_LOADING_STATUS: true }
        case UNLOCK_APPRAISAL_PERIOD_HAS_STOPPED_LOADING:
            return { ...state, UNLOCK_APPRAISAL_PERIOD_LOADING_STATUS: false }
        case SUBMIT_PEOPLE_OPS_OKRS_IS_LOADING:
            return { ...state, SUBMIT_PEOPLE_OPS_OKRS_LOADING_STATUS: true }
        case SUBMIT_PEOPLE_OPS_OKRS_HAS_STOPPED_LOADING:
            return { ...state, SUBMIT_PEOPLE_OPS_OKRS_LOADING_STATUS: false }
        case UPDATE_KEY_RESULTS_IS_LOADING:
            return { ...state, UPDATE_KEY_RESULTS_LOADING_STATUS: true }
        case UPDATE_KEY_RESULTS_HAS_STOPPED_LOADING:
            return { ...state, UPDATE_KEY_RESULTS_LOADING_STATUS: false }
        case ONBOARDING_USER_IS_LOADING:
            return { ...state, ONBOARDING_USER_LOADING_STATUS: true }
        case ONBOARDING_USER_HAS_STOPPED_LOADING:
            return { ...state, ONBOARDING_USER_LOADING_STATUS: false }
        case CHANGE_PASSWORD_IS_LOADING:
            return { ...state, CHANGE_PASSWORD_LOADING_STATUS: true }
        case CHANGE_PASSWORD_HAS_STOPPED_LOADING:
            return { ...state, CHANGE_PASSWORD_LOADING_STATUS: false }
        case SUBMIT_LINE_MANAGER_OKRS_IS_LOADING:
            return { ...state, SUBMIT_LINE_MANAGER_OKRS_LOADING_STATUS: true }
        case SUBMIT_LINE_MANAGER_OKRS_HAS_STOPPED_LOADING:
            return { ...state, SUBMIT_LINE_MANAGER_OKRS_LOADING_STATUS: false }
        case UPDATE_SBU_OBJECTIVE_IS_LOADING:
            return { ...state, UPDATE_SBU_OBJECTIVE_LOADING_STATUS: true }
        case UPDATE_SBU_OBJECTIVE_HAS_STOPPED_LOADING:
            return { ...state, UPDATE_SBU_OBJECTIVE_LOADING_STATUS: false }
        case DELETE_SBU_OBJECTIVE_IS_LOADING:
            return { ...state, DELETE_SBU_OBJECTIVE_LOADING_STATUS: true }
        case DELETE_SBU_OBJECTIVE_HAS_STOPPED_LOADING:
            return { ...state, DELETE_SBU_OBJECTIVE_LOADING_STATUS: false }
        case UPDATE_RECOMMENDATION_IS_LOADING:
            return { ...state, UPDATE_RECOMMENDATION_LOADING_STATUS: true }
        case UPDATE_RECOMMENDATION_HAS_STOPPED_LOADING:
            return { ...state, UPDATE_RECOMMENDATION_LOADING_STATUS: false }
        case SUBMIT_RECOMMENDATION_IS_LOADING:
            return { ...state, SUBMIT_RECOMMENDATION_LOADING_STATUS: true }
        case SUBMIT_RECOMMENDATION_HAS_STOPPED_LOADING:
            return { ...state, SUBMIT_RECOMMENDATION_LOADING_STATUS: false }
        case CALCULATE_STAFF_APPRAISAL_SCORE_IS_LOADING:
            return { ...state, CALCULATE_STAFF_APPRAISAL_SCORE_LOADING_STATUS: true }
        case CALCULATE_STAFF_APPRAISAL_SCORE_HAS_STOPPED_LOADING:
            return { ...state, CALCULATE_STAFF_APPRAISAL_SCORE_LOADING_STATUS: false }
        case SUBMIT_LINE_MANAGER_ASSESSMENT_IS_LOADING:
            return { ...state, SUBMIT_LINE_MANAGER_ASSESSMENT_LOADING_STATUS: true }
        case SUBMIT_LINE_MANAGER_ASSESSMENT_HAS_STOPPED_LOADING:
            return { ...state, SUBMIT_LINE_MANAGER_ASSESSMENT_LOADING_STATUS: false }
        case SAVE_APPRAISAL_CYCLE_IS_LOADING:
            return { ...state, SAVE_APPRAISAL_CYCLE_LOADING_STATUS: true }
        case SAVE_APPRAISAL_CYCLE_HAS_STOPPED_LOADING:
            return { ...state, SAVE_APPRAISAL_CYCLE_LOADING_STATUS: false }
        case DELETE_APPRAISAL_OBJECTIVE_IS_LOADING:
            return { ...state, DELETE_APPRAISAL_OBJECTIVE_LOADING_STATUS: true }
        case DELETE_APPRAISAL_OBJECTIVE_HAS_STOPPED_LOADING:
            return { ...state, DELETE_APPRAISAL_OBJECTIVE_LOADING_STATUS: false }
        case DELETE_KEY_RESULT_IS_LOADING:
            return { ...state, DELETE_KEY_RESULT_LOADING_STATUS: true }
        case DELETE_KEY_RESULT_HAS_STOPPED_LOADING:
            return { ...state, DELETE_KEY_RESULT_LOADING_STATUS: false }
        case ADD_KEY_RESULT_IS_LOADING:
            return { ...state, ADD_KEY_RESULT_LOADING_STATUS: true }
        case ADD_KEY_RESULT_HAS_STOPPED_LOADING:
            return { ...state, ADD_KEY_RESULT_LOADING_STATUS: false }
        case UPDATE_STAFF_DETAILS_IS_LOADING:
            return { ...state, UPDATE_STAFF_DETAILS_LOADING_STATUS: true }
        case UPDATE_STAFF_DETAILS_HAS_STOPPED_LOADING:
            return { ...state, UPDATE_STAFF_DETAILS_LOADING_STATUS: false }
        case UPDATE_USER_CLAIMS_IS_LOADING:
            return { ...state, UPDATE_USER_CLAIMS_LOADING_STATUS: true }
        case UPDATE_USER_CLAIMS_HAS_STOPPED_LOADING:
            return { ...state, UPDATE_USER_CLAIMS_LOADING_STATUS: false }
        case CREATE_STAFF_OKR_PERIOD_IS_LOADING:
            return { ...state, CREATE_STAFF_OKR_PERIOD_LOADING_STATUS: true }
        case CREATE_STAFF_OKR_PERIOD_HAS_STOPPED_LOADING:
            return { ...state, CREATE_STAFF_OKR_PERIOD_LOADING_STATUS: false }
        case UPDATE_STAFF_OKR_PERIOD_IS_LOADING:
            return { ...state, UPDATE_STAFF_OKR_PERIOD_LOADING_STATUS: true }
        case UPDATE_STAFF_OKR_PERIOD_HAS_STOPPED_LOADING:
            return { ...state, UPDATE_STAFF_OKR_PERIOD_LOADING_STATUS: false }
        default:
            return state
    }
}

const fetchLoadingReducer = (state = {}, {type}) => {
    switch(type) {
        // pages/BehaviorlAssessment/results.js
        case FETCH_BEHAVIORAL_ASSESSMENT_IS_LOADING:
            return { ...state, FETCH_BEHAVIORAL_ASSESSMENT_LOADING_STATUS: true }
        case FETCH_BEHAVIORAL_ASSESSMENT_HAS_STOPPED_LOADING:
            return { ...state, FETCH_BEHAVIORAL_ASSESSMENT_LOADING_STATUS: false }
        case FETCH_OBJECTIVES_AND_KEY_RESULTS_IS_LOADING:
            return { ...state, FETCH_OBJECTIVES_AND_KEY_RESULTS_LOADING_STATUS: true }
        case FETCH_OBJECTIVES_AND_KEY_RESULTS_HAS_STOPPED_LOADING:
            return { ...state, FETCH_OBJECTIVES_AND_KEY_RESULTS_LOADING_STATUS: false }
        case FETCH_CATEGORY_IS_LOADING:
            return { ...state, FETCH_CATEGORY_LOADING_STATUS: true }
        case FETCH_CATEGORY_HAS_STOPPED_LOADING:
            return { ...state, FETCH_CATEGORY_LOADING_STATUS: false }
        case FETCH_SBU_OBJECTIVES_IS_LOADING:
            return { ...state, FETCH_SBU_OBJECTIVES_LOADING_STATUS: true }
        case FETCH_SBU_OBJECTIVES_HAS_STOPPED_LOADING:
            return { ...state, FETCH_SBU_OBJECTIVES_LOADING_STATUS: false }
        case FETCH_BEHAVIOURS_IS_LOADING:
            return { ...state, FETCH_BEHAVIOURS_LOADING_STATUS: true }
        case FETCH_BEHAVIOURS_HAS_STOPPED_LOADING:
            return { ...state, FETCH_BEHAVIOURS_LOADING_STATUS: false }
        case FETCH_STAFF_DETAILS_IS_LOADING:
            return { ...state, FETCH_STAFF_DETAILS_LOADING_STATUS: true }
        case FETCH_STAFF_DETAILS_HAS_STOPPED_LOADING:
            return { ...state, FETCH_STAFF_DETAILS_LOADING_STATUS: false }
        case FETCH_STAFF_APPRAISAL_IS_LOADING:
            return { ...state, FETCH_STAFF_APPRAISAL_LOADING_STATUS: true }
        case FETCH_STAFF_APPRAISAL_HAS_STOPPED_LOADING:
            return { ...state, FETCH_STAFF_APPRAISAL_LOADING_STATUS: false }
        case FETCH_APPRAISAL_CYCLE_IS_LOADING:
            return { ...state, FETCH_APPRAISAL_CYCLE_LOADING_STATUS: true }
        case FETCH_APPRAISAL_CYCLE_HAS_STOPPED_LOADING:
            return { ...state, FETCH_APPRAISAL_CYCLE_LOADING_STATUS: false }
        case FETCH_ALL_BUSINESS_UNITS_IS_LOADING:
            return { ...state, FETCH_ALL_BUSINESS_UNITS_LOADING_STATUS: true }
        case FETCH_ALL_BUSINESS_UNITS_HAS_STOPPED_LOADING:
            return { ...state, FETCH_ALL_BUSINESS_UNITS_LOADING_STATUS: false }
        case FETCH_STAFF_APPRAISAL_SCORE_IS_LOADING:
            return { ...state, FETCH_STAFF_APPRAISAL_SCORE_LOADING_STATUS: true }
        case FETCH_STAFF_APPRAISAL_SCORE_HAS_STOPPED_LOADING:
            return { ...state, FETCH_STAFF_APPRAISAL_SCORE_LOADING_STATUS: false }
        default:
            return state
    }
}

// const actionStatusReducer = ( state={}, { type } ) => {
//     switch(type){
//         case UPDATE_KEY_RESULTS_SUCCESSFUL:
//             return { ...state, UPDATE_KEY_RESULTS_ACTION_STATUS: true }
//         case SAVE_FAILED:
//             return false
//         case SAVE_DONE:
//             return null
//         default:
//             return state

//     }
// }


const fetchErrorReducer = ( state={}, {type, error} ) => {
    switch(type){
        case FETCH_LMPO_OKRS_ERROR:
            return { ...state, FETCH_LMPO_OKRS_ERROR: error }
        default:
            return state
    }
}


const responseStatusReducer = ( state={}, {type, statusCode} ) => {
    switch(type){
        case FETCH_STAFF_DETAILS_RESPONSE_STATUS:
            return { ...state, FETCH_STAFF_DETAILS_RESPONSE_STATUS: statusCode }
        default:
            return state
    }
}


const continueEnabledLineManagerObjectives = ( state=[], {type,payload , data} ) => {
    switch(type){
        case GET_CONTINUE_ENABLED_OBJECTIVES:
            return payload || []
        case MODIFY_CONTINUE_ENABLED_OBJECTIVES_ON_SAVE: //using an object to update an object in an array
            return state.map((item, index) => {
                if(item.id !== data.id) {
                    //this is not the item we want, keep it as is
                    return item
                }
                // Otherwise, this is the one we want - return an updated value
                return {
                    ...item,
                    ...data
                }
            })
        default:
            return state
    }
}

const continueEnabledLineManagerKeyResults = ( state=[], {type,payload , data} ) => {
    switch(type){
        case GET_CONTINUE_ENABLED_KEY_RESULTS:
            return [ ...state, ...payload ]
        case MODIFY_CONTINUE_ENABLED_KEY_RESULTS_ON_SAVE: //using an array to update an object in an array
            // keep this code but keep an eye on it to see if it works
            return state.map((item, index) => {
                const matchObj = data.find(item => item.id === data.id)
                if(matchObj && item.id !== matchObj.id) {
                    //this is not the item we want, keep it as is
                    return item
                }
                // Otherwise, this is the one we want - return an updated value
                else if (matchObj && item.id === matchObj.id) {
                    return {
                        ...item,
                        ...matchObj
                    }
                } else {
                    return item
                }
            })
        default:
            return state
    }
}

const staffManagerReducer = (state={ loading: false, error: null, data: [] }, { type, payload }) => {
    switch(type) {
        case 'GET_ALL_STAFF':
            return {...state, data: payload.data};
        case 'FETCH_ALL_STAFF_IS_LOADING':
            return {...state, loading: true};
        case 'FETCH_ALL_STAFF_HAS_STOPPED_LOADING':
            return {...state, loading: false};
        default:
            return state;
    }
}

const allLineManagersReducer = (state = { loading: false, error: null, data: [] }, { type, payload }) => {
    switch(type) {
        case GET_ALL_LINE_MANAGERS:
            return { ...state, data: payload.data };
        case FETCH_ALL_LINE_MANAGERS_IS_LOADING:
            return { ...state, loading: true };
        case FETCH_ALL_LINE_MANAGERS_HAS_STOPPED_LOADING:
            return { ...state, loading: false };
        default:
            return state;
    }
}

const allDepartmentsReducer = (state = { loading: false, error: null, data: [] }, { type, payload }) => {
    switch(type) {
        case GET_ALL_DEPARTMENTS:
            return { ...state, data: payload.data };
        case FETCH_ALL_DEPARTMENTS_IS_LOADING:
            return { ...state, loading: true };
        case FETCH_ALL_DEPARTMENTS_HAS_STOPPED_LOADING:
            return { ...state, loading: false };
        default:
            return state;
    }
}

const staffDetailsByAdminReducer = (state = { loading: false, error: null, data: [] }, { type, payload }) => {
    switch(type) {
        case GET_STAFF_DETAILS_BY_ADMIN:
            return { ...state, data: payload.data };
        case FETCH_STAFF_DETAILS_BY_ADMIN_IS_LOADING:
            return { ...state, loading: true };
        case FETCH_STAFF_DETAILS_BY_ADMIN_HAS_STOPPED_LOADING:
            return { ...state, loading: false };
        default:
            return state;
    }
}

const staffClaimsReducer = (state = { loading: false, error: null, data: [] }, { type, payload }) => {
    switch(type) {
        case GET_STAFF_CLAIMS:
            console.log('unedit', payload.data)
            return { 
                ...state,
                data: payload.data.filter(item => (
                    item.type.includes('vggpmsclient')
                    || item.type.includes('pmsapi')
                    || item.value.includes('vggpmsclient')
                    || item.value.includes('pmsapi')
                ))
            };
        case FETCH_STAFF_CLAIMS_IS_LOADING:
            return { ...state, loading: true };
        case FETCH_STAFF_CLAIMS_HAS_STOPPED_LOADING:
            return { ...state, loading: false };
        default:
            return state;
    }
}

const staffOkrPeriodReducer = (state = { loading: false, error: null, data: null }, { type, payload }) => {
    switch(type) {
        case GET_STAFF_OKR_PERIOD:
            return { ...state, data: payload.data };
        case FETCH_STAFF_OKR_PERIOD_IS_LOADING:
            return { ...state, loading: true };
        case FETCH_STAFF_OKR_PERIOD_HAS_STOPPED_LOADING:
            return { ...state, loading: false };
        default:
            return state;
    }
}

const allStaffOkrPeriodReducer = (state = { loading: false, error: null, data: null }, { type, payload }) => {
    switch(type) {
        case GET_ALL_STAFF_OKR_PERIOD:
            return { ...state, data: payload.data };
        case FETCH_ALL_STAFF_OKR_PERIOD_IS_LOADING:
            return { ...state, loading: true };
        case FETCH_ALL_STAFF_OKR_PERIOD_HAS_STOPPED_LOADING:
            return { ...state, loading: false };
        default:
            return state;
    }
}

const allLmStaffOkrPeriodReducer = (state = { loading: false, error: null, data: null }, { type, payload }) => {
    switch(type) {
        case GET_ALL_LM_STAFF_OKR_PERIOD:
            return { ...state, data: payload.data };
        case FETCH_ALL_LM_STAFF_OKR_PERIOD_IS_LOADING:
            return { ...state, loading: true };
        case FETCH_ALL_LM_STAFF_OKR_PERIOD_HAS_STOPPED_LOADING:
            return { ...state, loading: false };
        default:
            return state;
    }
}

export default combineReducers({
    sbuObjectives,
    user,
    appraisalObjectiveKeyResults,
    REQUESTING_SAVE_KEY_RESULTS,
    requestingOKRs,
    requestingSBUs,
    setOKRs,
    userData: userReducer,
    userDataLm: userReducerLm,
    SAVING_OBJECTIVE_STATUS,
    SAVING_OBJECTIVE_STATUS_MESSAGE_VERSION,
    category,
    requestingAppraisalCycle,
    requestingStaffAppraisalId,
    requestingStaffAppraisal,
    requestingStaffDetails,
    requestingAppraisalObjectives,
    requestingNotNormalizedOkrs,
    requestingDecodedToken,
    requestingLineManagerStaffAppraisals,
    requestingPeopleOpsStaffAppraisals,
    requestingStaffAppraisalExistence,
    lmPendingKeyResults,
    isSubmittingOKRs,
    isFetchingStaffAppraisals,
    isFetchingLMPOOKRs,
    requestingStaffRole,
    requestingSuperLineManagerStaffAppraisals,
    requestingActiveTab,
    requestingActiveMiniTab,
    requestingBehavioralIndicators,
    saveLoadingReducer,
    fetchLoadingReducer,
    fetchErrorReducer,
    requestingRecommendationTypes,
    requestingRecommendation,
    requestingSuperRecommendation,
    requestingPeopleOpsCount,
    requestingAllBusinessUnits,
    requestingAllSbuObjectives,
    requestingBehaviours,
    requestingStaffAppraisalScore,
    SAVING_SBU_OBJECTIVES_STATUS,
    LOCK_APPRAISAL_CYCLE_STATUS,
    UNLOCK_APPRAISAL_CYCLE_STATUS,
    UPLOAD_BEHAVIOUR_STATUS,
    SAVE_BEHAVIOUR_STATUS,
    UPDATE_RECOMMENDATION_STATUS,
    SUBMIT_OKRS_STATUS,
    UPDATE_KEY_RESULTS_STATUS,
    ONBOARDING_USER_STATUS,
    CHANGE_PASSWORD_STATUS,
    LINE_MANAGER_UPDATE_KEY_RESULTS_STATUS,
    PEOPLE_OPS_UPDATE_KEY_RESULTS_STATUS,
    CALCULATE_STAFF_APPRAISAL_SCORE_STATUS,
    continueEnabledLineManagerObjectives,
    continueEnabledLineManagerKeyResults,
    responseStatusReducer,
    SUBMIT_RECOMMENDATION_STATUS,
    SUBMIT_LINE_MANAGER_ASSESSMENT_STATUS,
    SAVE_APPRAISAL_CYCLE_STATUS,
    staffManagerReducer,
    allLineManagersReducer,
    allDepartmentsReducer,
    staffDetailsByAdminReducer,
    staffClaimsReducer,
    staffOkrPeriodReducer,
    allStaffOkrPeriodReducer,
    allLmStaffOkrPeriodReducer
})