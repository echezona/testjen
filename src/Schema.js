import { schema } from 'normalizr'

const category = new schema.Entity('categories');

const sbuObjective = new schema.Entity('sbuObjectives'); 

const appraisalObjective = new schema.Entity('appraisalObjectives'); 

const appraisal = new schema.Entity('appraisals');

const staffAppraisal = new schema.Entity('staffAppraisals', {
    appraisal: appraisal
  });

const supportingDocument = new schema.Entity('supportingDocuments');

const keyResult = new schema.Entity('keyResults', {
    appraisalObjective: appraisalObjective,
    sbuObjective: sbuObjective,
    staffAppraisal: staffAppraisal,
    supportingDocuments: [supportingDocument]
  });

 const objective = new schema.Entity('Objectives', {
     category:category,
     sbuObjective: sbuObjective,
     staffAppraisal: staffAppraisal,
     keyResults:[keyResult],
 });

export const objectivesSchema = [objective]
