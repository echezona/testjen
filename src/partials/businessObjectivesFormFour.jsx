import React, { Fragment } from 'react'
import { Form, Message, Segment, Button, Header, Label } from 'semantic-ui-react'
import { connect } from 'react-redux'
import lifecycle from 'react-pure-lifecycle';

import { sbuObjectivesSelector, staffAppraisalIdSelector, appraisalObjectivesSelector, userIDsSelector, userKeyResultsSelector, userObjectivesSelector, notNormalizedOkrsSelector, sbuNameSelector } from '../reducer'
import { CREATE_OKRs, FETCH_APPRAISAL_OBJECTIVES, IS_REQUESTING_SAVE_RESULTS, DELETE_APPRAISAL_OBJECTIVE } from '../constants';
import { sortFunctionTwo } from '../helper';

import {cloneDeep} from 'lodash';
import OKRForm from './OKRForm';


const BusinessObjectivesFormFour = (props) => {
    const {
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
        isValid,
        setFieldTouched,
        dispatch,
        formName,
        order,
        categoryId,
        sbuObjs,
        staffAppraisalId,
        appraisalObjectives,
        userIDs,
        userKeyResults,
        userObjectives,
        okrs,
        REQUESTING_SAVE_KEY_RESULTS,
        SAVING_OBJECTIVE_STATUS,
        getOrder,
        IS_REQUESTING_OKRs,
        isDeletingAppraisalObjective,
    } = props


    const handleInputChange = (name, ...r) => {
        handleChange(...r)
        setFieldTouched(name, true, false)
    }

    const handleFormSubmit = () => {
        getOrder(order)

        const arr = [values[`one${order}`], values[`two${order}`], values[`three${order}`], values[`four${order}`], values[`five${order}`]]
            .filter((item) => item !== "")
            .map((item, index) => ({ description: item, order: index + 1 + "", status: "pending", lineManagerStatus: "pending", peopleOpsStatus: "pending" }))
            .sort(sortFunctionTwo)

        const data = {
            description: values[`objective${order}`],
            categoryId,
            sbuObjectiveId: values[`sbuObjectives${order}`],
            lineManagerStatus: "pending",
            peopleOpsStatus: "pending",
            order,
            staffAppraisalId,
            keyResults: [...arr]
        }

        //The first two validations are checking whether anything has been posted in the DB previously (i.e whether the fetch response is still an object)
        //The third validation serves to validate the if statement inside this block
        if (typeof okrs === "object" && okrs.constructor === Array && okrs.find(item => item.order === order) !== undefined) {
            //This checks whether an objective with the same "order" property already exists in the DB. If so, then it can access the block,
            //else it's meant to be POST not PUT and as such can't access the block.    
            if (okrs.find(item => item.order === order).order === order) {
                let objId

                appraisalObjectives.forEach((item) => {
                    if (item.order === order) {
                        objId = item.id
                        return data.id = item.id
                    }
                })

                arr.forEach((local) => {
                    local.appraisalObjectiveId = objId;
                    const dbValId = userObjectives[objId].keyResults.find((db) => {
                        return userKeyResults[db].order === local.order
                    });
                    if (dbValId !== undefined) {
                        local.id = dbValId
                        console.log("Whyyyy!", local.id)
                    }

                })


                // Value declarations for getting and posting the array of deleted IDs
                let postArray = [...arr]
                let fetchedArray = []
                let finalArray = []
                //for deletedIDs
                userObjectives[objId].keyResults.forEach((db) => fetchedArray.push(userKeyResults[db]))

                const postArrayIds = postArray.map(item => item.id)
                const fetchedArrayIds = fetchedArray.map(item => item.id)

                console.log("to be posted", postArrayIds)
                console.log("to be fetched", fetchedArrayIds)

                if (postArrayIds.length < fetchedArrayIds.length) {
                    fetchedArrayIds.forEach((e1) => {
                        const val = postArrayIds.includes(e1)
                        console.log("val", val)
                        if (val === false) {
                            finalArray.push(e1)
                            console.log("finalfinal", finalArray)
                        }
                    })
                    data.deletedIds = finalArray
                }
            }
        }

        dispatch({ type: CREATE_OKRs, data })
    }

    //userIDs sort function
    const sortFunctionOne = (a, b) => {
        const { userObjectives } = props
        if (userObjectives[a].order < userObjectives[b].order) {
            return -1
        } else if (userObjectives[a].order > userObjectives[b].order) {
            return 1
        } else {
            return 0
        }
    }

    //objective id of this objective (objective 1)
    const currId = userIDs && userIDs.length > 0 && userObjectives && Object.keys(userObjectives).length > 0 ? userIDs.sort(sortFunctionOne).map(item => userObjectives[item]).filter(item => item.order === order)[0]?.id : undefined;
    
    const handleObjectiveDelete = () => {
        let orderBase = 1
        const OKRs = userIDs.sort(sortFunctionOne).map(item => userObjectives[item])
                            .filter(item => item.order !== order)
                            .reduce((acc, curr) => {
                                if(curr.order != 'Personal') {
                                    curr.order = orderBase.toString();
                                    orderBase++;
                                }
                                acc.push(curr)
                                return acc
                            }, [])

        const OBJs = cloneDeep(OKRs)
                        .reduce((acc, curr) => {
                            curr.peopleOpsStatus = "pending";
                            curr.lineManagerStatus = "pending";
                            delete curr.keyResults;
                            delete curr.sbuObjective;
                            delete curr.sbuObjectiveId;
                            acc.push(curr)
                            return acc
                        }, [])

        const KRs = cloneDeep(OKRs)
                        .flatMap(item => item.keyResults)
                        .map(item => userKeyResults[item])
                        .reduce((acc, curr) => {
                            curr.peopleOpsStatus = "pending";
                            curr.lineManagerStatus = "pending";
                            acc.push(curr)
                            return acc
                        }, [])

        const okrDeleteFlow = {history: props.history}

        const forUpdateKeyResults = {KRs, OBJs, id: staffAppraisalId, okrDeleteFlow}

        // callBack();
        dispatch({ type: DELETE_APPRAISAL_OBJECTIVE, id: currId, forUpdateKeyResults })
    }


    return (
        <OKRForm
            order={order} currId={currId} isDeletingAppraisalObjective={isDeletingAppraisalObjective}
            handleObjectiveDelete={handleObjectiveDelete} IS_REQUESTING_OKRs={IS_REQUESTING_OKRs}
            values={values} handleInputChange={handleInputChange} touched={touched} errors={errors}
            sbuObjs={sbuObjs} handleChange={handleChange} SAVING_OBJECTIVE_STATUS={SAVING_OBJECTIVE_STATUS}
            REQUESTING_SAVE_KEY_RESULTS={REQUESTING_SAVE_KEY_RESULTS} isValid={isValid}
            handleFormSubmit={handleFormSubmit}
        />
    )
}

function mapStateToProps(state) {
    return {
        sbuObjs: sbuObjectivesSelector(state),
        staffAppraisalId: staffAppraisalIdSelector(state),
        appraisalObjectives: appraisalObjectivesSelector(state),
        userIDs: userIDsSelector(state),
        userKeyResults: userKeyResultsSelector(state),
        userObjectives: userObjectivesSelector(state),
        okrs: notNormalizedOkrsSelector(state),
        REQUESTING_SAVE_KEY_RESULTS: state.REQUESTING_SAVE_KEY_RESULTS,
        SAVING_OBJECTIVE_STATUS: state.SAVING_OBJECTIVE_STATUS,
        IS_REQUESTING_OKRs: state.requestingOKRs,
        sbuName: sbuNameSelector(state),
        isDeletingAppraisalObjective: state.saveLoadingReducer.DELETE_APPRAISAL_OBJECTIVE_LOADING_STATUS,
    }
}

const methods = {
    componentWillUnmount(props) {
        const {
            values,
            isValid,
            setFieldTouched,
            dispatch,
            formName,
            order,
            categoryId,
            sbuObjs,
            staffAppraisalId,
            appraisalObjectives,
            userIDs,
            userKeyResults,
            userObjectives,
            okrs,
            getOrder,
        } = props
        const handleFormSubmit = () => {
            getOrder(order)
    
            const arr = [values[`one${order}`], values[`two${order}`], values[`three${order}`], values[`four${order}`], values[`five${order}`]]
                .filter((item) => item !== "")
                .map((item, index) => ({ description: item, order: index + 1 + "", status: "pending", lineManagerStatus: "pending", peopleOpsStatus: "pending" }))
                .sort(sortFunctionTwo)
    
            const data = {
                description: values[`objective${order}`],
                categoryId,
                sbuObjectiveId: values[`sbuObjectives${order}`],
                lineManagerStatus: "pending",
                peopleOpsStatus: "pending",
                order,
                staffAppraisalId,
                keyResults: [...arr]
            }
    
            //The first two validations are checking whether anything has been posted in the DB previously (i.e whether the fetch response is still an object)
            //The third validation serves to validate the if statement inside this block
            if (typeof okrs === "object" && okrs.constructor === Array && okrs.find(item => item.order === order) !== undefined) {
                //This checks whether an objective with the same "order" property already exists in the DB. If so, then it can access the block,
                //else it's meant to be POST not PUT and as such can't access the block.    
                if (okrs.find(item => item.order === order).order === order) {
                    let objId
    
                    appraisalObjectives.forEach((item) => {
                        if (item.order === order) {
                            objId = item.id
                            return data.id = item.id
                        }
                    })
    
                    arr.forEach((local) => {
                        local.appraisalObjectiveId = objId;
                        const dbValId = userObjectives[objId].keyResults.find((db) => {
                            return userKeyResults[db].order === local.order
                        });
                        if (dbValId !== undefined) {
                            local.id = dbValId
                            console.log("Whyyyy!", local.id)
                        }
    
                    })
    
    
                    // Value declarations for getting and posting the array of deleted IDs
                    let postArray = [...arr]
                    let fetchedArray = []
                    let finalArray = []
                    //for deletedIDs
                    userObjectives[objId].keyResults.forEach((db) => fetchedArray.push(userKeyResults[db]))
    
                    const postArrayIds = postArray.map(item => item.id)
                    const fetchedArrayIds = fetchedArray.map(item => item.id)
    
                    console.log("to be posted", postArrayIds)
                    console.log("to be fetched", fetchedArrayIds)
    
                    if (postArrayIds.length < fetchedArrayIds.length) {
                        fetchedArrayIds.forEach((e1) => {
                            const val = postArrayIds.includes(e1)
                            console.log("val", val)
                            if (val === false) {
                                finalArray.push(e1)
                                console.log("finalfinal", finalArray)
                            }
                        })
                        data.deletedIds = finalArray
                    }
                }
            }
    
            dispatch({ type: CREATE_OKRs, data })
        }
        if(isValid){
            handleFormSubmit()
        }
        console.log('Now unmounting from objective ', order);
    }
};

export default connect(mapStateToProps)(lifecycle(methods)(BusinessObjectivesFormFour))