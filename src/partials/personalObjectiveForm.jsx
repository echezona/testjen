import React,{Fragment, useState} from 'react'
import { Form, Label, Segment, Button, Header, Divider } from 'semantic-ui-react'
import {connect} from 'react-redux'
import lifecycle from 'react-pure-lifecycle';

import { sbuObjectivesSelector, staffAppraisalIdSelector, appraisalObjectivesSelector, userKeyResultsSelector, userObjectivesSelector, notNormalizedOkrsSelector, userIDsSelector } from '../reducer'
import { CREATE_OKRs, IS_REQUESTING_SAVE_RESULTS, DELETE_APPRAISAL_OBJECTIVE } from '../constants';
import { sortFunctionTwo } from '../helper';

import {cloneDeep} from 'lodash';
import { ToastContainer } from 'react-toastify';
import ReactModal from 'react-modal';

const PersonalObjectiveForm = (props) => {
    const {
        values: { one, two, three, four, five, objective },
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
        isValid,
        setFieldTouched,
        dispatch,
        formName,
        order,
        categoryId,
        staffAppraisalId,
        appraisalObjectives,
        userKeyResults,
        userIDs,
        userObjectives,
        okrs,
        REQUESTING_SAVE_KEY_RESULTS,
        SAVING_OBJECTIVE_STATUS,
        getOrder,
        IS_REQUESTING_OKRs,
        isDeletingAppraisalObjective,
    } = props

    const [deleteModalOpen, setDeleteModalOpen] = useState(false)
    const handleModalToggle = () => {
        setDeleteModalOpen(!deleteModalOpen)
    }

    const handleInputChange = (name, ...r) => {
        handleChange(...r)
        setFieldTouched(name, true, false)
    }

    const handleFormSubmit = () => {
        getOrder(order)

        const arr = [one, two, three, four, five]
            .filter((item) => item !== "")
            .map((item, index) => ({ description: item, order: index + 1 + "", status: "pending", lineManagerStatus: "pending", peopleOpsStatus: "pending" }))
            .sort(sortFunctionTwo)

        const data = {
            description: objective,
            categoryId,
            lineManagerStatus: "pending",
            peopleOpsStatus: "pending",
            order,
            staffAppraisalId,
            keyResults: [...arr]
        }

        //The first two validations are checking whether anything has been posted in the DB previously (i.e whether the fetch response is still an object)
        //The third validation serves to validate the if statement inside this block
        if (typeof okrs === "object" && okrs.constructor === Array && okrs.find(item => item.order === order) !== undefined) {
            //This checks whether an objective with the same "order" property already exists in the DB. If so, then it can access the block,
            //else it's meant to be POST not PUT and as such can't access the block.    
            if (okrs.find(item => item.order === order).order === order) {
                let objId

                appraisalObjectives.forEach((item) => {
                    if (item.order === order) {
                        objId = item.id
                        return data.id = item.id
                    }
                })

                arr.forEach((local) => {
                    local.appraisalObjectiveId = objId;
                    const dbValId = userObjectives[objId].keyResults.find((db) => {
                        return userKeyResults[db].order === local.order
                    });
                    if (dbValId !== undefined) {
                        local.id = dbValId
                        console.log("Whyyyy!", local.id)
                    }

                })


                // Value declarations for getting and posting the array of deleted IDs
                let postArray = [...arr]
                let fetchedArray = []
                let finalArray = []
                //for deletedIDs
                userObjectives[objId].keyResults.forEach((db) => fetchedArray.push(userKeyResults[db]))

                const postArrayIds = postArray.map(item => item.id)
                const fetchedArrayIds = fetchedArray.map(item => item.id)

                console.log("to be posted", postArrayIds)
                console.log("to be fetched", fetchedArrayIds)

                if (postArrayIds.length < fetchedArrayIds.length) {
                    fetchedArrayIds.forEach((e1) => {
                        const val = postArrayIds.includes(e1)
                        console.log("val", val)
                        if (val === false) {
                            finalArray.push(e1)
                            console.log("finalfinal", finalArray)
                        }
                    })
                    data.deletedIds = finalArray
                }
            }
        }

        dispatch({ type: CREATE_OKRs, data })
    }

    //userIDs sort function
    const sortFunctionOne = (a, b) => {
        const { userObjectives } = props
        if (userObjectives[a].order < userObjectives[b].order) {
            return -1
        } else if (userObjectives[a].order > userObjectives[b].order) {
            return 1
        } else {
            return 0
        }
    }

    //objective id of this objective (objective 1)
    const currId = userIDs && userIDs.length > 0 && userObjectives && Object.keys(userObjectives).length > 0 ? userIDs.sort(sortFunctionOne).map(item => userObjectives[item]).filter(item => item.order === order)[0]?.id : undefined;
    
    const handleObjectiveDelete = () => {
        let orderBase = 1
        const OKRs = userIDs.sort(sortFunctionOne).map(item => userObjectives[item])
                            .filter(item => item.order !== order)
                            .reduce((acc, curr) => {
                                if(curr.order != 'Personal') {
                                    curr.order = orderBase.toString();
                                    orderBase++;
                                }
                                acc.push(curr)
                                return acc
                            }, [])

        const OBJs = cloneDeep(OKRs)
                        .reduce((acc, curr) => {
                            curr.peopleOpsStatus = "pending";
                            curr.lineManagerStatus = "pending";
                            delete curr.keyResults;
                            delete curr.sbuObjective;
                            delete curr.sbuObjectiveId;
                            acc.push(curr)
                            return acc
                        }, [])

        const KRs = cloneDeep(OKRs)
                        .flatMap(item => item.keyResults)
                        .map(item => userKeyResults[item])
                        .reduce((acc, curr) => {
                            curr.peopleOpsStatus = "pending";
                            curr.lineManagerStatus = "pending";
                            acc.push(curr)
                            return acc
                        }, [])

        const okrDeleteFlow = {history: props.history}

        const forUpdateKeyResults = {KRs, OBJs, id: staffAppraisalId, okrDeleteFlow}

        // callBack();
        dispatch({ type: DELETE_APPRAISAL_OBJECTIVE, id: currId, forUpdateKeyResults })
    }

    return (
        <Fragment>
            <ToastContainer/>
            {/* Reset Status Modal starts here */}
            <ReactModal
                isOpen={deleteModalOpen}
                onRequestClose={handleModalToggle}
                style={{
                    overlay: {backgroundColor: 'rgba(0, 0, 0, 0.75)'},
                    content: {
                        top: '30vh',
                        left: '300px',
                        right: '300px',
                        bottom: '50vh',
                    },
                }}
            >
                <Header
                    content='Are you sure you want to do this?'
                    subheader='This action will delete this objective'
                />
                <div>
                    <Button
                        content='No, take me back!' basic
                        onClick={handleModalToggle}
                    />
                    <Button
                        content='Yes, please delete!' 
                        color='red'
                        onClick={() => { handleObjectiveDelete(); handleModalToggle(); }}
                    />
                </div>
            </ReactModal>

            {/* Main page starts here */}
            <div
                style={{
                width: "100%",
                display: "flex",
                justifyContent: "space-between",
                alignItems: "baseline",
                }}
            >
                <Header as="h4" content={`${order} Development Objective`} />
                <Button
                    disabled={!currId || isDeletingAppraisalObjective}
                    color="red"
                    circular
                    onClick={handleModalToggle}
                    icon="trash alternate"
                />
            </div>
            <Divider style={{ marginTop: 0 }} />
            <Form loading={IS_REQUESTING_OKRs}>
                <div>
                    <textarea rows="4" name="objective" value={objective} onChange={handleInputChange.bind(null,'objective')} placeholder="Enter your objective here *" />
                    {touched.objective && errors.objective && <span>{errors.objective}</span>}
                </div>
                <br/>
                <div>
                    <label><b>Key Result 1<span style={{color: "red"}}>*</span></b></label>
                    <textarea rows="2" name="one" value={one} onChange={handleChange}/>
                    {/* {touched.one && errors.one && <span>{errors.one}</span>} */}
                </div>
                <br/>
                <div>
                    <label><b>Key Result 2<span style={{color: "red"}}>*</span></b></label>
                    <textarea rows="2" name="two" value={two} onChange={handleChange} />
                    {/* {touched.two && errors.two && <span>{errors.two}</span>} */}
                </div>
                <br/>
                <div>
                    <label><b>Key Result 3<span style={{color: "red"}}>*</span></b></label>
                    <textarea rows="2" name="three" value={three} onChange={handleChange} />
                    {/* {touched.three && errors.three && <span>{errors.three}</span>} */}
                </div>
                <br/>
                <div>
                    <label><b>Key Result 4</b></label>
                    <textarea rows="2" name="four" value={four} onChange={handleChange} />
                </div>
                <br/>
                <div>
                    <label><b>Key Result 5</b></label>
                    <textarea rows="2" name="five" value={five} onChange={handleChange} />
                </div>
                <br />
                
                <Segment basic textAlign="right">
                    {SAVING_OBJECTIVE_STATUS === true &&
                        <Label pointing='right' color="green" >Save Successful</Label>
                    }
                    {SAVING_OBJECTIVE_STATUS === false &&
                        <Label pointing='right' color="red" >Save Failed. Please try again or refresh page.</Label>
                    }
                    <Button basic content="Save" loading={REQUESTING_SAVE_KEY_RESULTS} disabled={!isValid || REQUESTING_SAVE_KEY_RESULTS || IS_REQUESTING_OKRs} onClick={() => handleFormSubmit()}/>
                </Segment>
            </Form>
        </Fragment>
    )
}


function mapStateToProps(state){
    return {
        staffAppraisalId: staffAppraisalIdSelector(state),
        appraisalObjectives: appraisalObjectivesSelector(state),
        userKeyResults: userKeyResultsSelector(state),
        userIDs: userIDsSelector(state),
        userObjectives: userObjectivesSelector(state),
        okrs: notNormalizedOkrsSelector(state),
        REQUESTING_SAVE_KEY_RESULTS: state.REQUESTING_SAVE_KEY_RESULTS,
        SAVING_OBJECTIVE_STATUS: state.SAVING_OBJECTIVE_STATUS,
        IS_REQUESTING_OKRs: state.requestingOKRs,
        isDeletingAppraisalObjective: state.saveLoadingReducer.DELETE_APPRAISAL_OBJECTIVE_LOADING_STATUS,
    }
}

const methods = {
    componentWillUnmount(props) {
        const {
            values: { one, two, three, four, five, objective },
            isValid,
            setFieldTouched,
            dispatch,
            formName,
            order,
            categoryId,
            staffAppraisalId,
            appraisalObjectives,
            userKeyResults,
            userObjectives,
            okrs,
            REQUESTING_SAVE_KEY_RESULTS,
            SAVING_OBJECTIVE_STATUS,
            getOrder,
        } = props
        const handleFormSubmit = () => {
            getOrder(order)
        
            const arr = [one, two, three, four, five]
                .filter((item) => item !== "")
                .map((item, index) => ({ description: item, order: index + 1 + "", status: "pending", lineManagerStatus: "pending", peopleOpsStatus: "pending" }))
                .sort(sortFunctionTwo)
    
            const data = {
                description: objective,
                categoryId,
                lineManagerStatus: "pending",
                peopleOpsStatus: "pending",
                order,
                staffAppraisalId,
                keyResults: [...arr]
            }
    
            //The first two validations are checking whether anything has been posted in the DB previously (i.e whether the fetch response is still an object)
            //The third validation serves to validate the if statement inside this block
            if (typeof okrs === "object" && okrs.constructor === Array && okrs.find(item => item.order === order) !== undefined) {
                //This checks whether an objective with the same "order" property already exists in the DB. If so, then it can access the block,
                //else it's meant to be POST not PUT and as such can't access the block.    
                if (okrs.find(item => item.order === order).order === order) {
                    let objId
    
                    appraisalObjectives.forEach((item) => {
                        if (item.order === order) {
                            objId = item.id
                            return data.id = item.id
                        }
                    })
    
                    arr.forEach((local) => {
                        local.appraisalObjectiveId = objId;
                        const dbValId = userObjectives[objId].keyResults.find((db) => {
                            return userKeyResults[db].order === local.order
                        });
                        if (dbValId !== undefined) {
                            local.id = dbValId
                            console.log("Whyyyy!", local.id)
                        }
    
                    })
    
    
                    // Value declarations for getting and posting the array of deleted IDs
                    let postArray = [...arr]
                    let fetchedArray = []
                    let finalArray = []
                    //for deletedIDs
                    userObjectives[objId].keyResults.forEach((db) => fetchedArray.push(userKeyResults[db]))
    
                    const postArrayIds = postArray.map(item => item.id)
                    const fetchedArrayIds = fetchedArray.map(item => item.id)
    
                    console.log("to be posted", postArrayIds)
                    console.log("to be fetched", fetchedArrayIds)
    
                    if (postArrayIds.length < fetchedArrayIds.length) {
                        fetchedArrayIds.forEach((e1) => {
                            const val = postArrayIds.includes(e1)
                            console.log("val", val)
                            if (val === false) {
                                finalArray.push(e1)
                                console.log("finalfinal", finalArray)
                            }
                        })
                        data.deletedIds = finalArray
                    }
                }
            }
    
            dispatch({ type: CREATE_OKRs, data })
        }
        if(isValid){
            handleFormSubmit()
        }
        console.log(`Now unmounting from ${order} objective`);
    }
};

export default connect(mapStateToProps)(lifecycle(methods)(PersonalObjectiveForm))