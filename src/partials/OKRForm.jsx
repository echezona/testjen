import React, {Fragment, useState} from "react";
import { Form, Segment, Button, Header, Label, Divider } from 'semantic-ui-react';
import ReactModal from 'react-modal';
import { ToastContainer } from "react-toastify";


const OKRForm = ({
  order,
  currId,
  isDeletingAppraisalObjective,
  handleObjectiveDelete,
  IS_REQUESTING_OKRs,
  values,
  handleInputChange,
  touched,
  errors,
  sbuObjs,
  handleChange,
  SAVING_OBJECTIVE_STATUS,
  REQUESTING_SAVE_KEY_RESULTS,
  isValid,
  handleFormSubmit,
}) => {
  const [deleteModalOpen, setDeleteModalOpen] = useState(false)
  const handleModalToggle = () => {
    setDeleteModalOpen(!deleteModalOpen)
  }
  return (
    <Fragment>
      <ToastContainer/>
      {/* Reset Status Modal starts here */}
      <ReactModal
        isOpen={deleteModalOpen}
        onRequestClose={handleModalToggle}
        style={{
            overlay: {backgroundColor: 'rgba(0, 0, 0, 0.75)'},
            content: {
            top: '30vh',
            left: '300px',
            right: '300px',
            bottom: '50vh',
            },
        }}
      >
        <Header
            content='Are you sure you want to do this?'
            subheader='This action will delete this objective'
        />
        <div>
            <Button
              content='No, take me back!' basic
              onClick={handleModalToggle}
            />
            <Button
              content='Yes, please delete!' 
              color='red'
              onClick={() => { handleObjectiveDelete(); handleModalToggle(); }}
            />
        </div>
      </ReactModal>

      {/* Main page starts here */}
      <div
        style={{
          width: "100%",
          display: "flex",
          justifyContent: "space-between",
          alignItems: "baseline",
        }}
      >
        <Header as="h4" content={`Business Objective ${order}`} />
        <Button
          disabled={!currId || isDeletingAppraisalObjective}
          color="red"
          circular
          onClick={handleModalToggle}
          icon="trash alternate"
        />
      </div>
      <Divider style={{ marginTop: 0 }} />
      <Form loading={IS_REQUESTING_OKRs}>
        <div>
          <textarea
            rows="4"
            name={`objective${order}`}
            value={values[`objective${order}`]}
            onChange={handleInputChange.bind(null, `objective${order}`)}
            placeholder="Enter your objective here *"
          />
          {touched[`objective${order}`] && errors[`objective${order}`] && (
            <span style={{ color: "red" }}>{errors[`objective${order}`]}</span>
          )}
        </div>
        <br />
        <div>
          <select
            name={`sbuObjectives${order}`}
            value={values[`sbuObjectives${order}`]}
            onChange={handleInputChange.bind(null, `sbuObjectives${order}`)}
          >
            <option value="" disabled>
              Select the SBU objective that the objective above helps achieve
            </option>
            {sbuObjs.map((item, index) => (
              <option value={item.id} key={item.id}>{`SBU Objective ${
                index + 1
              }`}</option>
            ))}
          </select>
          {touched[`sbuObjectives${order}`] &&
            errors[`sbuObjectives${order}`] && (
              <span style={{ color: "red" }}>
                {errors[`sbuObjectives${order}`]}
              </span>
            )}
        </div>
        <br />
        <div>
          <label>
            <b>
              Key Result 1<span style={{ color: "red" }}>*</span>
            </b>
          </label>
          <textarea
            rows="2"
            name={`one${order}`}
            value={values[`one${order}`]}
            onChange={handleChange}
          />
          {/* {touched.one && errors.one && <span style={{ color: "red" }}>{errors.one}</span>} */}
        </div>
        <br />
        <div>
          <label>
            <b>
              Key Result 2<span style={{ color: "red" }}>*</span>
            </b>
          </label>
          <textarea
            rows="2"
            name={`two${order}`}
            value={values[`two${order}`]}
            onChange={handleChange}
          />
          {/* {touched.two && errors.two && <span style={{ color: "red" }}>{errors.two}</span>} */}
        </div>
        <br />
        <div>
          <label>
            <b>
              Key Result 3<span style={{ color: "red" }}>*</span>
            </b>
          </label>
          <textarea
            rows="2"
            name={`three${order}`}
            value={values[`three${order}`]}
            onChange={handleChange}
          />
          {/* {touched.three && errors.three && <span style={{ color: "red" }}>{errors.three}</span>} */}
        </div>
        <br />
        <div>
          <label>
            <b>Key Result 4</b>
          </label>
          <textarea
            rows="2"
            name={`four${order}`}
            value={values[`four${order}`]}
            onChange={handleChange}
          />
        </div>
        <br />
        <div>
          <label>
            <b>Key Result 5</b>
          </label>
          <textarea
            rows="2"
            name={`five${order}`}
            value={values[`five${order}`]}
            onChange={handleChange}
          />
        </div>
        <br />
        <Segment basic textAlign="right">
          {SAVING_OBJECTIVE_STATUS === true && (
            <Label pointing="right" color="green">
              Save Successful
            </Label>
          )}
          {SAVING_OBJECTIVE_STATUS === false && (
            <Label pointing="right" color="red">
              Save Failed. Please try again or refresh page.
            </Label>
          )}
          <Button
            basic
            content="Save"
            loading={REQUESTING_SAVE_KEY_RESULTS}
            disabled={
              !isValid || REQUESTING_SAVE_KEY_RESULTS || IS_REQUESTING_OKRs
            }
            onClick={() => handleFormSubmit()}
          />
        </Segment>
      </Form>
    </Fragment>
  );
};


export default OKRForm;