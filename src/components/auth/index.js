import React, { Component, Fragment } from 'react'
import { Loader, Segment } from 'semantic-ui-react'
import Oidc from 'oidc-client'
import App from '../../App'
import { withRouter, Route } from 'react-router-dom'
import { SSO_REDIRECT_PATH, SSO_URL, REDIRECT_URI, POST_LOGOUT_REDIRECT_URI, CLIENT_ID } from '../../appConstants'
import { connect } from "react-redux"
import { SET_USER, FETCH_OBJECTIVES_AND_KEY_RESULTS } from '../../constants'
import { userSelector } from '../../reducer'
import ErrorBoundary from '../../pages/ErrorBoundary';
import Login from '../../pages/Login';
import Onboarding from '../../pages/Onboarding';

const settings = {
  client_id: CLIENT_ID,
  authority: SSO_URL,
  redirect_uri: REDIRECT_URI,
  response_type: 'id_token token',
  scope: 'openid profile identity-server-api',
  post_logout_redirect_uri: POST_LOGOUT_REDIRECT_URI,
}

const client = new Oidc.UserManager(settings)

const authStyle = {
  minHeight: '100vh',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: '#000000'
}

class Authentication extends Component {
  constructor(props) {
    super(props)
    this.state = {
      gettingUser: true
    }
  }

  componentDidMount() {

    const {
      location: { pathname },
      history,
      dispatch
    } = this.props

    client.getUser().then(user => {
      if (user) {
        console.log('user present')
        //switch to app
        this.setState({
          gettingUser: false
        })
        dispatch({ type: SET_USER, user })
      } else if (pathname === SSO_REDIRECT_PATH) {
        //redirect to sso
        console.log('here') // setTimeout(() => client.signoutRedirect(), 3600000)
        client.signinRedirectCallback().then(() => {
          this.setState({
            gettingUser: false
          })
          history.replace('/businessobjectives')
          window.location.reload()
          //set the time in localstorage to the current time whenever someone logs in from SSO
          window.localStorage.setItem("initialTime", Date.now())
        })
      } else {
        // client.signinRedirect()
        this.setState({
          gettingUser: false
        })
        if(history) history.replace('/login')
      }
    })

    dispatch({ type: FETCH_OBJECTIVES_AND_KEY_RESULTS })
  }


  render() {
    if(this.state.gettingUser){
      return (
        <Segment basic style={authStyle}>
          <Loader active />
        </Segment>
      )
    } else if (Object.keys(this.props.user).length < 1) {
      return(
        <Fragment>
          <Route render={props => <Login {...props} client={client}/>} path="/login" />
          <Route render={props => <Onboarding {...props} client={client}/>} path="/onboarding" />
        </Fragment>
      )
    } else {
      return (
        <App client={client} history={this.props.history} location={this.props.location} />
      )
    }
  }


  // render() {
  //   return (this.state.gettingUser || !this.props.user) ? (
  //     <Segment basic style={authStyle}>
  //       <Loader active />
  //     </Segment>
  //   ) : (
  //       <App client={client} history={this.props.history} location={this.props.location} />
  //     )
  // }
  
}

function mapStateToProps(state) {
  return {
    user: userSelector(state)
  }
}

export default withRouter(connect(mapStateToProps)(Authentication))

