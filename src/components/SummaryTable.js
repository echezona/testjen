import React, {Fragment} from 'react';
import { Table, Label, Segment } from 'semantic-ui-react';
// import Indicator from '../pages/BehavioralAssessment/Indicator';

const SummaryTable = ({ variant, objectives, keyResults, indicators }) => {

    let appraisalIds;
    if(variant == 'performance') {
        appraisalIds = keyResults.reduce((acc, curr) => {
            if(!acc.find(item => item == curr.appraisalObjectiveId)) {
                acc.push(curr.appraisalObjectiveId)
            } 

            return acc
        }, [])
    }

    if (variant == 'performance') {
        return (
            <Table celled structured striped>
                <Table.Header color='blue' >
                    <Table.Row>
                        <Table.HeaderCell rowSpan='2' textAlign='center'>Objectives</Table.HeaderCell>
                        <Table.HeaderCell rowSpan='2' textAlign='center'>Key Results</Table.HeaderCell>
                        <Table.HeaderCell colSpan='2' textAlign='center'>Staff</Table.HeaderCell>
                        <Table.HeaderCell colSpan='2' textAlign='center'>Line Manager</Table.HeaderCell>
                    </Table.Row>
                    <Table.Row>
                        <Table.HeaderCell textAlign='center'>Rating</Table.HeaderCell>
                        <Table.HeaderCell textAlign='center'>Comment</Table.HeaderCell>
                        <Table.HeaderCell textAlign='center'>Rating</Table.HeaderCell>
                        <Table.HeaderCell textAlign='center'>Comment</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {
                        keyResults.map((item, index) => {
                            return (
                                <Table.Row key={index}>

                                    { appraisalIds.map((id, index) => {

                                        if(item.appraisalObjectiveId == id && item.order == '1') {
                                            return (
                                                <Table.Cell 
                                                    key={id}
                                                    verticalAlign='top'
                                                    rowSpan={objectives.find((objItem) => objItem.id == id).keyResults.length}
                                                >
                                                    <Label color='blue' size='mini' circular>
                                                        {objectives.find((objItem) => objItem.id == id).order}
                                                    </Label>
                                                    <div style={{ marginTop: '10px' }}>
                                                        {objectives.find((objItem) => objItem.id == id).description}
                                                    </div>
                                                </Table.Cell>
                                            )
                                        }
                                    }) 

                                        
                                    }
                                    <Table.Cell>
                                        <Label color='blue' size='mini' circular style={{ marginRight: '10px' }}>
                                            {item.order}
                                        </Label>
                                        {item.description}
                                    </Table.Cell>
                                    <Table.Cell >{item.staffRating}</Table.Cell>
                                    <Table.Cell >{item.staffComment}</Table.Cell>
                                    <Table.Cell >{item.lineManagerRating}</Table.Cell>
                                    <Table.Cell >{item.lineManagerComment}</Table.Cell>
                                    
                                </Table.Row>
                            )
                        })
                    }
                </Table.Body>
            </Table>
        )
    } else if (variant == 'behavior') {
        return (
            <Table celled structured striped>
                <Table.Header color='blue' >
                    <Table.Row>
                        <Table.HeaderCell rowSpan='2' textAlign='center'>Behavioral Indicators</Table.HeaderCell>
                        <Table.HeaderCell colSpan='2' textAlign='center'>Staff</Table.HeaderCell>
                        <Table.HeaderCell colSpan='2' textAlign='center'>Line Manager</Table.HeaderCell>
                    </Table.Row>
                    <Table.Row>
                        <Table.HeaderCell textAlign='center'>Rating</Table.HeaderCell>
                        <Table.HeaderCell textAlign='center'>Comment</Table.HeaderCell>
                        <Table.HeaderCell textAlign='center'>Rating</Table.HeaderCell>
                        <Table.HeaderCell textAlign='center'>Comment</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {
                        indicators.map((item, index) => {
                            return (
                                <Table.Row key={index}>
                                    <Table.Cell>
                                        <Label color='blue' size='mini' circular style={{ marginRight: '10px' }}>
                                            {index + 1}
                                        </Label>
                                        {item.behaviour.description}
                                    </Table.Cell>
                                    <Table.Cell >{item.staffRating}</Table.Cell>
                                    <Table.Cell >{item.staffComment}</Table.Cell>
                                    <Table.Cell >{item.lineManagerRating}</Table.Cell>
                                    <Table.Cell >{item.lineManagerComment}</Table.Cell>
                                </Table.Row>
                            )
                        })
                    }
                </Table.Body>
            </Table>
        )
    } else {
        return (
            <Segment>Please pass a "variant" prop of either "performance" or "behavior" into SummaryTable</Segment>
        )
    }
}

export default SummaryTable;