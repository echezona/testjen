import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import Header from './header'
import LeftNav from './leftNav'
import { decodedTokenRoleSelector } from '../../reducer';

class Layout extends Component {
  constructor(props) {
    super(props)
    this.state = {
      activateMiniTabs: true,
      user: null,
      leftNavOpen: false,
      // rightNavOpen: true,
      updated: false,
      loggedOut: false,
      menu: [
        // {
        //   name: 'Home Feed',
        //   className: 'waves-effect',
        //   icon: 'home',
        //   active: false,
        //   path: '/Home',
        //   changePath: true
        // },
        {
          name: 'My OKRs',
          className: 'waves-effect',
          icon: 'tasks',
          active: false,
          path: '/businessobjectivesparent',
          changePath: false,  // the changepath property is true for all side menu items w/o children and false for those that have children
          values: [
            {
              name: 'Set OKRs',
              className: 'waves-effect',
              icon: 'tasks',
              active: false,
              path: '/businessobjectives',
            },
            {
              name: 'Behavioral indicators',
              className: 'waves-effect',
              icon: 'tasks',
              active: false,
              path: '/behavioralindicators',
            },
          ]
        },
        {
          name: 'Self Assessment',
          className: 'waves-effect',
          icon: 'address card',
          active: false,
          path: '/selfassessmentparent',
          changePath: false,  // the changepath property is true for all side menu items w/o children and false for those that have children
          values: [
            {
              name: 'Performance Assessment',
              className: 'waves-effect',
              icon: 'tasks',
              active: false,
              path: '/PerformanceAssessment',
            },
            {
              name: 'Behavioral Assessment',
              className: 'waves-effect',
              icon: 'tasks',
              active: false,
              path: '/BehavioralAssessment',
            },
            {
              name: 'Assessment Scores',
              className: 'waves-effect',
              icon: 'tasks',
              active: false,
              path: '/assessmentscores',
            },
          ]
        },
        {
          name: "One on One - Personal",
          className: "waves-effect",
          icon: "male",
          active: false,
          path: "/oneonone-personal",
          changePath: true
        }
      ]
    }
  }

  // componentWillMount(){
  //   const {roles, dispatch} = this.props
  //   // dispatch({type: FETCH_OBJECTIVES_AND_KEY_RESULTS})
  //   // console.log("roles",roles)
  // }

  componentDidUpdate() {
    const { roles, primaryRole } = this.props
    const { menu, updated } = this.state
    console.log("roles", roles)
    console.log("primaryRole", primaryRole)

    let newMenuItems = []
    if (roles !== null && primaryRole !== "" && roles.includes("LineManager")) {
      newMenuItems.push(
        {
          
            name: 'One on One - Direct Reports',
            className: 'waves-effect',
            icon: 'group',
            active: false,
            path: '/oneonone-direct-report',
            changePath: true,  // the changepath property is true for all side menu items w/o children and false for those that have children
         
        },
      )
    }
    // if (roles !== null && primaryRole !== "" && roles.includes("LineManager")) {
    //   newMenuItems.push(
    //     {
    //       name: 'One on One',
    //       className: 'waves-effect',
    //       icon: 'address card',
    //       active: false,
    //       path: '/oneononeparent',
    //       changePath: false,  // the changepath property is true for all side menu items w/o children and false for those that have children
    //       values: [
    //         {
    //           name: 'Personal',
    //           className: 'waves-effect',
    //           icon: 'tasks',
    //           active: false,
    //           path: '/oneonone-personal',
    //         },
    //         {
    //           name: 'Direct Reports',
    //           className: 'waves-effect',
    //           icon: 'tasks',
    //           active: false,
    //           path: '/oneonone-direct-report',
    //         }
    //       ]
    //     },
    //   )
    // }
     /*else {
      newMenuItems.push(
        {
          name: "One on One",
          className: "waves-effect",
          icon: "pencil",
          active: false,
          path: "/oneonone-personal",
          changePath: true
        },
      )
    }*/

    if (roles !== null && primaryRole !== "" && primaryRole !== "SuperLineManager" && roles.includes("LineManager")) {
      newMenuItems.push(
        {
          name: "Manage OKRs",
          className: "waves-effect",
          icon: "pencil",
          active: false,
          path: "/ManageOKRs",
          changePath: true
        },
        {
          name: "Ward Assessment",
          className: "waves-effect",
          icon: "address book",
          active: false,
          path: "/manageassessment",
          changePath: true // the changepath property is true for all side menu items w/o children and false for those that have children
        },
      )
    }

    if (roles !== null && primaryRole !== "" && primaryRole === "SuperLineManager" && roles.includes("LineManager")) {
      newMenuItems.push(
        {
          name: 'Manage OKRs',
          className: 'waves-effect',
          icon: 'pencil',
          active: false,
          path: '/ManageOKRsparent',
          changePath: false,
          values: [
            {
              name: 'Direct reports',
              className: 'waves-effect',
              icon: 'pencil',
              active: false,
              path: '/ManageOKRs',
            },
            {
              name: 'N-2s',
              className: 'waves-effect',
              icon: 'pencil',
              active: false,
              path: '/superManageOKRs',
            },
          ]
        },
        {
          name: 'Ward Assessment',
          className: 'waves-effect',
          icon: 'address book',
          active: false,
          path: '/manageassessmentparent',
          changePath: false,  // the changepath property is true for all side menu items w/o children and false for those that have children
          values: [
            {
              name: 'Direct reports',
              className: 'waves-effect',
              icon: 'pencil',
              active: false,
              path: '/manageassessment',
            },
            {
              name: 'N-2s',
              className: 'waves-effect',
              icon: 'pencil',
              active: false,
              path: '/supermanageassessment',
            },
          ]
        },
      )
    }

   

    if (roles !== null && roles.includes("PeopleOps")) {
      newMenuItems.push(
        {
          name: "People-Ops",
          className: "waves-effect",
          icon: "chess queen",
          active: false,
          path: "/adminmanageokrsparent",
          changePath: false,
          values: [
            {
              name: "SSO Staff Management",
              className: "waves-effect",
              icon: "pencil",
              active: false,
              path: "/staff-management"
            },
            {
              name: "Manage Staff OKRs",
              className: "waves-effect",
              icon: "pencil",
              active: false,
              path: "/adminmanageokrs"
            },
            {
              name: "Manage Staff Assessment",
              className: "waves-effect",
              icon: "pencil",
              active: false,
              path: "/adminmanageassessment"
            },
            {
              name: "Set Behavioral",
              className: "waves-effect",
              icon: "pencil",
              active: false,
              path: "/behavioralsetting"
            },
            {
              name: "Set Appraisal Cycle",
              className: "waves-effect",
              icon: "pencil",
              active: false,
              path: "/appraisalcyclesetting"
            },
            {
              name: "Set SBU Objectives",
              className: "waves-effect",
              icon: "pencil",
              active: false,
              path: "/sbuobjectivesetting"
            },
            {
              name: "Set Staff List",
              className: "waves-effect",
              icon: "pencil",
              active: false,
              path: "/setstafflist"
            }
          ]
        }
      )
    }

    console.log('pree', roles, primaryRole)

    if (roles && primaryRole && newMenuItems.length > 0 && primaryRole !== "Staff" && this.state.menu.length < 4) {
      this.setState(prevState => ({ menu: [ ...prevState.menu, ...newMenuItems ] }))
    }

  }

  //   componentWillMount() {
  //     const user = JSON.parse(localStorage.getItem('user'))
  //     if (!user) {
  //       this.props.history.replace('/signin')
  //     } else {
  //       this.setState({ user })
  //     }
  //   }
  handleToggle() {
    if (this.state.leftNavOpen) {
      this.setState({
        leftNavOpen: false
      })
    } else {
      this.setState({
        leftNavOpen: true
      })
    }
  }
  handleRightNavToggle() {
    this.setState({ rightNavOpen: !this.state.rightNavOpen, activateMiniTabs: !this.state.activateMiniTabs })
  }
  handleLogOut = history => {
    this.setState({
      loggedOut: true,
    })
    window.localStorage.clear()
    // history.replace({
    //   pathname: '/landing'
    // })
    this.props.client.signoutRedirect(this.props.client)
  }

  render() {
    const { history } = this.props
    return (
      <Fragment>
        {/* <LeftNav
          visible={this.state.leftNavOpen}
          history={history}
          menu={this.state.menu}
          requestingActiveTab={this.props.requestingActiveTab}
          requestingActiveMiniTab={this.props.requestingActiveMiniTab}
          dispatch={this.props.dispatch}
          activateMiniTabs={this.state.activateMiniTabs}
          leftNavOpen={this.state.leftNavOpen}
          className='leftNav'
        > */}
          <Header
            visible={this.state.leftNavOpen}
            history={history}
            menu={this.state.menu}
            requestingActiveTab={this.props.requestingActiveTab}
            requestingActiveMiniTab={this.props.requestingActiveMiniTab}
            activateMiniTabs={this.state.activateMiniTabs}
            onToggle={() => this.handleToggle()}
            leftNavOpen={this.state.leftNavOpen}
            rightNavToggle={() => this.handleRightNavToggle()}
            logoutAction={() => this.handleLogOut()}
            loggedOut={this.state.loggedOut} //loggedOut became necassary to implicitly state that the user has logged out and the localstorage should be wiped out.
            {...this.props}
          />
          <div>
            {this.props.children}
          </div>
        {/* </LeftNav> */}
      </Fragment>
    )
  }
}

Layout.propTypes = {
  children: PropTypes.any
}

function mapStateToProps(state) {
  return {
    roles: decodedTokenRoleSelector(state),
    primaryRole: state.requestingStaffRole,
    requestingActiveTab: state.requestingActiveTab,
    requestingActiveMiniTab: state.requestingActiveMiniTab,
  }
}

export default connect(mapStateToProps)(Layout)






// menu: [
//   {
//     name: 'Home Feed',
//     className: 'waves-effect',
//     icon: 'home',
//     active: false,
//     path: '/Home'
//   },
//   {
//     name: 'Approve Recommendations',
//     className: 'waves-effect',
//     icon: 'file alternate',
//     active: false,
//     path: '/ApproveRecommendation'
//   },
//   // {
//   //   name: 'Performance Assessment',
//   //   className: 'hand point up outline',
//   //   icon: 'camera',
//   //   active: false,
//   //   path: '/'
//   // },
//   {
//     name: 'Behaviour Dashboard',
//     className: 'waves-effect',
//     icon: 'users',
//     active: false,
//     path: '/BehaviorDashboard'
//   },
//   // {
//   //   name: 'Staff Page',
//   //   className: 'waves-effect',
//   //   icon: 'user',
//   //   active: false,
//   //   path: '/Staff'
//   // },
//   {
//     name: 'Business Objectives',
//     className: 'waves-effect',
//     icon: 'tasks',
//     active: false,
//     path: '/businessobjectives'
//   },
//   {
//     name: 'Personal Objectives',
//     className: 'waves-effect',
//     icon: 'pencil',
//     active: false,
//     path: '/PersonalObjectives'
//   },
//   {
//     name: 'Performance Assessment',
//     className: 'waves-effect',
//     icon: 'pencil',
//     active: false,
//     path: '/PerformanceAssessment'
//   },
//   {
//     name: 'Behavioral Assessment',
//     className: 'waves-effect',
//     icon: 'pencil',
//     active: false,
//     path: '/BehavioralAssessment'
//   },
//   {
//     name: 'Manage OKRs',
//     className: 'waves-effect',
//     icon: 'pencil',
//     active: false,
//     path: '/ManageOKRs'
//   },
//   {
//     name: 'N1 Performance Assessment',
//     className: 'waves-effect',
//     icon: 'pencil',
//     active: false,
//     path: '/N1PerformanceAssessment'
//   },
//   {
//     name: 'N1 Behavioral Assessment',
//     className: 'waves-effect',
//     icon: 'pencil',
//     active: false,
//     path: '/N1BehavioralAssessment'
//   },
// ]


// {
        //   name: 'test dropdown',
        //   className: 'waves-effect',
        //   icon: 'money',
        //   active: false,
        //   path: "/businessobjectivesere",
        //   changePath: false,
        //   values: [{
        //     name: 'September',
        //     className: 'waves-effect',
        //     icon: 'tasks',
        //     active: false,
        //     path: '/september'
        //   },
        //   {
        //     name: 'November',
        //     className: 'waves-effect',
        //     icon: 'tasks',
        //     active: false,
        //     path: '/november'
        //   },]
        // },

//prev stable menu updater in componentDidUpdate

 // if(roles !== null && primaryRole !== "" && primaryRole === "SuperLineManager" && roles.includes("PeopleOps") && roles.includes("LineManager") && !updated) {
    //   this.setState(prevState => ({
    //     menu: prevState.menu.concat([
    //       {
    //         name: 'Manage OKRs',
    //         className: 'waves-effect',
    //         icon: 'pencil',
    //         active: false,
    //         path: '/ManageOKRsparent',
    //         changePath: false,
    //         values: [
    //           {
    //             name: 'Direct reports',
    //             className: 'waves-effect',
    //             icon: 'pencil',
    //             active: false,
    //             path: '/ManageOKRs',
    //           },
    //           {
    //             name: 'N-2s',
    //             className: 'waves-effect',
    //             icon: 'pencil',
    //             active: false,
    //             path: '/superManageOKRs',
    //           },
    //         ]
    //       },
    //       {
    //         name: 'Ward Assessment',
    //         className: 'waves-effect',
    //         icon: 'address book',
    //         active: false,
    //         path: '/manageassessmentparent',
    //         changePath: false,  // the changepath property is true for all side menu items w/o children and false for those that have children
    //         values: [
    //           {
    //             name: 'Direct reports',
    //             className: 'waves-effect',
    //             icon: 'pencil',
    //             active: false,
    //             path: '/manageassessment',
    //           },
    //           {
    //             name: 'N-2s',
    //             className: 'waves-effect',
    //             icon: 'pencil',
    //             active: false,
    //             path: '/supermanageassessment',
    //           },
    //         ]
    //       },
    //       {
    //         name: "People-Ops",
    //         className: 'waves-effect',
    //         icon: 'chess queen',
    //         active: false,
    //         path: '/adminmanageokrsparent',
    //         changePath: false,
    //         values: [
    //           {
    //             name: 'Manage Staff OKRs',
    //             className: 'waves-effect',
    //             icon: 'pencil',
    //             active: false,
    //             path: '/adminmanageokrs',
    //           },
    //           {
    //             name: 'Manage Staff Assessment',
    //             className: 'waves-effect',
    //             icon: 'pencil',
    //             active: false,
    //             path: '/adminmanageassessment'
    //           },
    //           {
    //             name: 'Set Behavioral',
    //             className: 'waves-effect',
    //             icon: 'pencil',
    //             active: false,
    //             path: '/behavioralsetting'
    //           },
    //           {
    //             name: 'Set Appraisal Cycle',
    //             className: 'waves-effect',
    //             icon: 'pencil',
    //             active: false,
    //             path: '/appraisalcyclesetting'
    //           },
    //           {
    //             name: 'Set SBU Objectives',
    //             className: 'waves-effect',
    //             icon: 'pencil',
    //             active: false,
    //             path: '/sbuobjectivesetting'
    //           },
    //         ]
    //       },
    //     ]),
    //     updated: true,
    //   }))
    // }else if (roles !== null && primaryRole !== "" && primaryRole === "SuperLineManager" && roles.includes("LineManager") && !updated) {
    //   this.setState(prevState => ({
    //     menu: prevState.menu.concat([
    //       {
    //         name: 'Manage OKRs',
    //         className: 'waves-effect',
    //         icon: 'pencil',
    //         active: false,
    //         path: '/ManageOKRsparent',
    //         changePath: false,
    //         values: [
    //           {
    //             name: 'Direct reports',
    //             className: 'waves-effect',
    //             icon: 'pencil',
    //             active: false,
    //             path: '/ManageOKRs',
    //           },
    //           {
    //             name: 'N-2s',
    //             className: 'waves-effect',
    //             icon: 'pencil',
    //             active: false,
    //             path: '/superManageOKRs',
    //           },
    //         ]
    //       },
    //       {
    //         name: 'Ward Assessment',
    //         className: 'waves-effect',
    //         icon: 'address book',
    //         active: false,
    //         path: '/manageassessmentparent',
    //         changePath: false,  // the changepath property is true for all side menu items w/o children and false for those that have children
    //         values: [
    //           {
    //             name: 'Direct reports',
    //             className: 'waves-effect',
    //             icon: 'pencil',
    //             active: false,
    //             path: '/manageassessment',
    //           },
    //           {
    //             name: 'N-2s',
    //             className: 'waves-effect',
    //             icon: 'pencil',
    //             active: false,
    //             path: '/supermanageassessment',
    //           },
    //         ]
    //       },
    //       // {
    //       //   name: 'Super Manage OKRs',
    //       //   className: 'waves-effect',
    //       //   icon: 'pencil',
    //       //   active: false,
    //       //   path: '/superManageOKRs',
    //       //   // changePath: true
    //       // },
    //     ]),
    //     updated: true,
    //   }))
    // } else if (roles !== null && primaryRole !== "" && roles.includes("LineManager") && roles.includes("PeopleOps") && !updated) {
    //   this.setState(prevState => ({
    //     menu: prevState.menu.concat([
    //       {
    //         name: "Manage OKRs",
    //         className: "waves-effect",
    //         icon: "pencil",
    //         active: false,
    //         path: "/ManageOKRs",
    //         changePath: true
    //       },
    //       {
    //         name: "Ward Assessment",
    //         className: "waves-effect",
    //         icon: "address book",
    //         active: false,
    //         path: "/manageassessment",
    //         changePath: true // the changepath property is true for all side menu items w/o children and false for those that have children
    //       },
    //       {
    //         name: "People-Ops",
    //         className: "waves-effect",
    //         icon: "chess queen",
    //         active: false,
    //         path: "/adminmanageokrsparent",
    //         changePath: false,
    //         values: [
    //           {
    //             name: "SSO Staff Management",
    //             className: "waves-effect",
    //             icon: "pencil",
    //             active: false,
    //             path: "/staff-management"
    //           },
    //           {
    //             name: "Manage Staff OKRs",
    //             className: "waves-effect",
    //             icon: "pencil",
    //             active: false,
    //             path: "/adminmanageokrs"
    //           },
    //           {
    //             name: "Manage Staff Assessment",
    //             className: "waves-effect",
    //             icon: "pencil",
    //             active: false,
    //             path: "/adminmanageassessment"
    //           },
    //           {
    //             name: "Set Behavioral",
    //             className: "waves-effect",
    //             icon: "pencil",
    //             active: false,
    //             path: "/behavioralsetting"
    //           },
    //           {
    //             name: "Set Appraisal Cycle",
    //             className: "waves-effect",
    //             icon: "pencil",
    //             active: false,
    //             path: "/appraisalcyclesetting"
    //           },
    //           {
    //             name: "Set SBU Objectives",
    //             className: "waves-effect",
    //             icon: "pencil",
    //             active: false,
    //             path: "/sbuobjectivesetting"
    //           },
    //           {
    //             name: "Set Staff List",
    //             className: "waves-effect",
    //             icon: "pencil",
    //             active: false,
    //             path: "/setstafflist"
    //           }
    //         ]
    //       }
    //       // {
    //       //   name: 'Set Behavioral',
    //       //   className: 'waves-effect',
    //       //   icon: 'pencil',
    //       //   active: false,
    //       //   path: '/behavioralsetting'
    //       // },
    //     ]),
    //     updated: true
    //   }));
    // } else if (roles !== null && primaryRole !== "" && roles.includes("LineManager") && !updated) {
    //   this.setState(prevState => ({
    //     menu: prevState.menu.concat([
    //       {
    //         name: 'Manage OKRs',
    //         className: 'waves-effect',
    //         icon: 'pencil',
    //         active: false,
    //         path: '/ManageOKRs',
    //         changePath: true,
    //       },
    //       {
    //         name: 'Ward Assessment',
    //         className: 'waves-effect',
    //         icon: 'address book',
    //         active: false,
    //         path: '/manageassessment',
    //         changePath: true,  // the changepath property is true for all side menu items w/o children and false for those that have children
    //       },
    //     ]),
    //     updated: true,
    //   }))
    // } else if (roles !== null && primaryRole !== "" && roles.includes("PeopleOps") && !updated) {
    //   this.setState(prevState => ({
    //     menu: prevState.menu.concat([
    //       {
    //         name: "People-Ops",
    //         className: "waves-effect",
    //         icon: "chess queen",
    //         active: false,
    //         path: "/adminmanageokrsparent",
    //         changePath: false,
    //         values: [
    //           {
    //             name: "Manage Staff OKRs",
    //             className: "waves-effect",
    //             icon: "pencil",
    //             active: false,
    //             path: "/adminmanageokrs"
    //           },
    //           {
    //             name: "Manage Staff Assessment",
    //             className: "waves-effect",
    //             icon: "pencil",
    //             active: false,
    //             path: "/adminmanageassessment"
    //           },
    //           {
    //             name: "Set Behavioral",
    //             className: "waves-effect",
    //             icon: "pencil",
    //             active: false,
    //             path: "/behavioralsetting"
    //           },
    //           {
    //             name: "Set Appraisal Cycle",
    //             className: "waves-effect",
    //             icon: "pencil",
    //             active: false,
    //             path: "/appraisalcyclesetting"
    //           },
    //           {
    //             name: "Set SBU Objectives",
    //             className: "waves-effect",
    //             icon: "pencil",
    //             active: false,
    //             path: "/sbuobjectivesetting"
    //           },
    //           {
    //             name: "Set Staff List",
    //             className: "waves-effect",
    //             icon: "pencil",
    //             active: false,
    //             path: "/setstafflist"
    //           }
    //         ]
    //       }
    //     ]),
    //     updated: true
    //   }));
    // }