import React from 'react'
import PropTypes from 'prop-types'
import { Menu, Dropdown, Image, Icon, Popup, Button, Container, Grid, Divider } from 'semantic-ui-react'
import { connect } from 'react-redux'
import { userProfileSelector } from '../../reducer';
import logo from '../../assets/logo1.png'
import Dock from 'react-dock';

const linkNav = (history, path) => {
  history.push({
    pathname: path
  })
}

const navigationStyle = {
  // backgroundColor: '#013042', 
  background: "linear-gradient(#013042, #024058)",
  padding: '0 0px'
}

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0,
      counter: null,
      hours: 0,
      minutes: 0,
      seconds: 0,
      activeTab: '',
    }
  }

  componentDidMount() {
    this.watch()
  }

  componentDidUpdate(){
    if(this.props.loggedOut){
      window.localStorage.clear()
    }
  }

  goToChangePassword = () => {
    const {history} = this.props;
    // console.log("blad", history)
    history.push('/changepassword')
  }

  watch = () => {
    let initialTime = Date.now()
    let counter = setInterval(() => timer(this.props), 1000)

    if(localStorage.getItem('initialTime') === null){
      initialTime = setLocalStorage("initialTime", initialTime)
    } else {
      initialTime = getLocalStorage("initialTime")
    }

    function setLocalStorage(key, val) {
      if (window.localStorage) {
        window.localStorage.setItem(key, val);
      }
      return val;
    }

    function getLocalStorage(key) {
      return window.localStorage ? window.localStorage.getItem(key) : '';
    }

    var that = this
    function timer(props) {
      let currentTime = Date.now()
      let timeDiff = currentTime - initialTime
      // console.log("fairydust", timeDiff )
      if (timeDiff >= 3600000) {
        clearInterval(counter);
        props.logoutAction()
        return;
      }

      // var seconds = timeDiff % 60;
      // var minutes = Math.floor(timeDiff / 60);
      // var hours = Math.floor(minutes / 60);
      // minutes %= 60;
      // hours %= 60;

      // //Here i'm checking for any instance where any of hour, minutes, or seconds is a single digit number
      // //if it is then i am adding a 0 to the front of it to give it a clock format
      // seconds = /^\d$/.test(seconds) ? `0${seconds}` : seconds + ""
      // minutes = /^\d$/.test(minutes) ? `0${minutes}` : minutes + ""
      // hours = /^\d$/.test(hours) ? `0${hours}` : hours + ""

      // that.setState({
      //   seconds: seconds,
      //   minutes: minutes,
      //   hours: hours,
      // })
    }
  }

  handleTabSelection = (path, changePath) => {
    const { history, dispatch } = this.props;
    this.setState({
      activeTab: path,
      activeMiniTab: "",
    })
    // dispatch({ type: STORE_ACTIVE_TAB, data: path })
    if (changePath) {
      linkNav(history, path)

      this.props.onToggle();
    }
  }

  handleMiniTabSelection = (path) => {
    const { history, dispatch } = this.props;
    this.setState({
      activeMiniTab: path,
      activeTab: ""
    })
    // dispatch({ type: STORE_ACTIVE_MINI_TAB, data: path })
    linkNav(history, path)

    this.props.onToggle();
  }


  render() {
    const { onToggle, leftNavOpen, rightNavToggle, logoutAction, user, history, menu } = this.props
    const { hours, minutes, seconds, activeTab } = this.state
    const username = user ? `${user.family_name ? user.family_name : ""} ${user.given_name ? user.given_name : ""}` : ''
    const items = [{divider: true, label: 'Main navigation', value: 'main-nav'}]
    return (
      <Menu
        stackable
        secondary
        className="ph3 pa2 flex menu secondary stackable ui h3"
        style={{
          border: '1px solid rgba(34,36,38,.15)',
          marginBottom: '0px',
          backgroundColor: 'white'
        }}
      >
        <Menu.Item onClick={onToggle}>
          <Icon name="bars" fitted />
        </Menu.Item>
        {/* <Menu.Menu position="right"> */}
           {/* <Menu.Item>
        <Input icon="search" placeholder="Search..." />
      </Menu.Item> */}
      <Menu.Menu position="right">
        {/*<Menu.Item onClick={rightNavToggle}>
          <Icon name="grid layout" fitted />
        </Menu.Item>
        <Dropdown text="" icon="bell outline" simple className="icon">
          <Dropdown.Menu>
            <Dropdown.Header>Notifications</Dropdown.Header>
            <Dropdown.Divider />
            <Dropdown.Item
              label={{ color: 'red', empty: true, circular: true }}
              text="Important"
            />
            <Dropdown.Item
              label={{ color: 'blue', empty: true, circular: true }}
              text="Announcement"
            />
            <Dropdown.Item
              label={{ color: 'black', empty: true, circular: true }}
              text="Discussion"
            />
          </Dropdown.Menu>
        </Dropdown> */}
        
          <Dropdown
            text={username}
            icon="user circle outline"
            simple
            className="icon"
          >
            <Dropdown.Menu>
              <Dropdown.Header>Options</Dropdown.Header>
            <Dropdown.Divider />
            {/* <Dropdown.Item>View Profile</Dropdown.Item> */}
            <Dropdown.Item onClick={() => this.goToChangePassword()}>Change Password</Dropdown.Item>
            <Dropdown.Divider />
              {/* <Menu.Item className="red" >{`You will be logged out in - ${hours}:${minutes}:${seconds}`}</Menu.Item> */}
              {/* <Menu.Item className="red"> */}
                {/* <Grid>
                  <Grid.Column columns={1} textAlign='center'>
                    <Button onClick={()=>logoutAction()} content="LOGOUT" icon='power off' color='red' size='small' />
                  </Grid.Column>
                </Grid> */}
              {/* </Menu.Item> */}
            </Dropdown.Menu>
          </Dropdown>
        </Menu.Menu> 
        
        {/* <Menu.Item>
          <Divider vertical/>
        </Menu.Item> */}

        <Menu.Item>
          <Popup trigger={<Button circular onClick={()=>logoutAction()} color='red' icon='power'/>} content={`Logout`}/>
        </Menu.Item>


        <Dock isVisible={leftNavOpen} onVisibleChange={onToggle} size='200px' zIndex={leftNavOpen ? 1000 : -1000} duration={0}>
          {/* <div className='pusher '>
            <div className='full height'>
              <div className='toc noprint'> */}

                <Menu inverted secondary className='left fixed vertical' style={navigationStyle}>
                  <Menu.Item style={{ margin: '5px' }}>
                    <div>
                      <Image src={logo} size="medium" centered />
                    </div>
                    <Divider />
                  </Menu.Item>
                  <div>
                  {menu.map((link, index) => {
                    if (!link.values) {
                      return (
                        <Menu.Item key={index} name={link.name} active={activeTab === link.path ? true : false || history.location.pathname.includes(link.path)} onClick={() => this.handleTabSelection(link.path, link.changePath)}>
                          <Icon className='left' name={link.icon} size='large' style={{ marginBottom: '5px' }} />
                          <span className='f5 ml3'>{link.name}</span>
                        </Menu.Item>
                      )
                    }
                    else if (link.values) {
                      return (
                        
                        <Menu.Item key={index} name={link.name} active={history.location.pathname.includes(link.path) || activeTab.includes(link.path) || link.values.some(item => history.location.pathname.includes(item.path))} >
                          <Icon className='left' name={link.icon} size='large' style={{ marginBottom: '5px' }} />
                          <Dropdown style={{ display: 'inline-block' }} item text={link.name}>
                            <Dropdown.Menu style={{ backgroundColor: '#013042' }}>
                              {link.values.map((item, index) => (
                                <Dropdown.Item key={index} name={item.name} onClick={() => this.handleMiniTabSelection(item.path)}>
                                  <Icon name='circle' color='white' />
                                  <span className='white'>{item.name}</span>
                                </Dropdown.Item>
                              ))
                              }
                            </Dropdown.Menu>
                          </Dropdown>
                        </Menu.Item>
                        
                        
                      )
                    }
                  })
                  }
                  </div>
                </Menu>
              {/* </div>
            </div>
          </div> */}
        </Dock>
      </Menu>
    )
  }
}

Header.propTypes = {
  onToggle: PropTypes.func,
  rightNavToggle: PropTypes.func,
  logoutAction: PropTypes.func
}

function mapStateToProps(state) {
  return {
    user: userProfileSelector(state)
  }
}
export default connect(mapStateToProps)(Header)


/* <Popup
            trigger={<Icon name={`down arrow`} />}
            header={'Session expiry'}
            // content={`Your session expires in ...`}
            hoverable
          >
            {`Your session expires in ${hours}:${minutes}:${seconds}`}
            <Container><Button>LogOut Now</Button></Container>
          </Popup> */


// THe code below would be ideal for converting milliseconds to hours, minutes and seconds
      // //Get hours from milliseconds
      // var hours = count / (1000*60*60);
      // var absoluteHours = Math.floor(hours);
      // var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;

      // //Get remainder from hours and convert to minutes
      // var minutes = (hours - absoluteHours) * 60;
      // var absoluteMinutes = Math.floor(minutes);
      // var m = absoluteMinutes > 9 ? absoluteMinutes : '0' +  absoluteMinutes;

      // //Get remainder from minutes and convert to seconds
      // var seconds = (minutes - absoluteMinutes) * 60;
      // var absoluteSeconds = Math.floor(seconds);
      // var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;

// const Header = ({ onToggle, rightNavToggle, logoutAction, user }) => {
  //   const username = user ? `${user.family_name} ${user.given_name}` : ''
  //   return (
  //     <Menu
  //       stackable
  //       secondary
  //       className="ph3 pa2 flex menu secondary stackable ui h3"
  //       style={{
  //         border: '1px solid rgba(34,36,38,.15)',
  //         marginBottom: '0px',
  //         backgroundColor: 'white'
  //       }}
  //     >
  //       <Menu.Item onClick={onToggle}>
  //         <Icon name="bars" fitted />
  //       </Menu.Item>
  //       <Menu.Menu position="right">
  //         {/* <Menu.Item>
  //         <Input icon="search" placeholder="Search..." />
  //       </Menu.Item>
  //       <Menu.Menu position="right">
  //         <Menu.Item onClick={rightNavToggle}>
  //           <Icon name="grid layout" fitted />
  //         </Menu.Item>
  //         <Dropdown text="" icon="bell outline" simple className="icon">
  //           <Dropdown.Menu>
  //             <Dropdown.Header>Notifications</Dropdown.Header>
  //             <Dropdown.Divider />
  //             <Dropdown.Item
  //               label={{ color: 'red', empty: true, circular: true }}
  //               text="Important"
  //             />
  //             <Dropdown.Item
  //               label={{ color: 'blue', empty: true, circular: true }}
  //               text="Announcement"
  //             />
  //             <Dropdown.Item
  //               label={{ color: 'black', empty: true, circular: true }}
  //               text="Discussion"
  //             />
  //           </Dropdown.Menu>
  //         </Dropdown> */}
  //         <Popup
  //           trigger={<Icon name={`down arrow`} />}
  //           header={'Session expiry'}
  //           // content={`Your session expires in ...`}
  //           hoverable
  //         >
  //           {`Your session expires in ...`}
  //           <Button>LogOut Now</Button>
  //         </Popup>
  //         <Dropdown
  //           text={username}
  //           icon="user circle outline"
  //           simple
  //           className="icon"
  //         >
  //           <Dropdown.Menu>
  //             {/* <Dropdown.Header>Options</Dropdown.Header>
  //             <Dropdown.Divider />
  //             <Dropdown.Item>View Profile</Dropdown.Item>
  //             <Dropdown.Item>Change Password</Dropdown.Item>
  //             <Dropdown.Divider /> */}
  //             <Menu.Item className="red" onClick={logoutAction} name="logout" />
  //           </Dropdown.Menu>
  //         </Dropdown>
  //       </Menu.Menu>
  //     </Menu>
  //   )
  // }






  // timepiece = () => {
  //   var count = 0;
  //   var counter = null;

  //   function initCounter() {
  //     // get count from localStorage, or set to initial value of 1000
  //     console.log("the count", getLocalStorage('count'))
  //     count = getLocalStorage('count') < 1 ? 3300 : getLocalStorage('count'); //3300 seconds = 55 minutes
  //     counter = setInterval(timer, 1000); //1000 will  run it every 1 second
  //   }

  //   function setLocalStorage(key, val) {
  //     if (window.localStorage) {
  //       window.localStorage.setItem(key, val);
  //     }
  //     return val;
  //   }

  //   function getLocalStorage(key) {
  //     return window.localStorage ? window.localStorage.getItem(key) : '';
  //   }

  //   const timer = () => {
  //     count = setLocalStorage('count', count - 1);
  //     if (count == -1) {
  //       clearInterval(counter);
  //       this.props.logoutAction()
  //       return;
  //     }

  //     var seconds = count % 60;
  //     var minutes = Math.floor(count / 60);
  //     var hours = Math.floor(minutes / 60);
  //     minutes %= 60;
  //     hours %= 60;

  //     // seconds = seconds === 0 ? "00" : seconds + ""
  //     // minutes = minutes === 0 ? "00" : minutes + ""
  //     // hours = hours === 0 ? "00" : hours + ""

  //     //Here i'm checking for any instance where any of hour, minutes, or seconds is a single digit number
  //     //if it is then i am adding a 0 to the front of it to give it a clock format
  //     seconds = /^\d$/.test(seconds) ? `0${seconds}` : seconds + ""
  //     minutes = /^\d$/.test(minutes) ? `0${minutes}` : minutes + ""
  //     hours = /^\d$/.test(hours) ? `0${hours}` : hours + ""

  //     this.setState({
  //       seconds: seconds,
  //       minutes: minutes,
  //       hours: hours,
  //     })

  //   }

  //   return initCounter
      
  // }