import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'

import { Container, Menu, Icon, Image, Dropdown, Segment, Divider } from 'semantic-ui-react'
import './layout.css'

import logo from '../../assets/logo1.png'
import logoMin from '../../assets/logo_icon.png'
import { STORE_ACTIVE_TAB, STORE_ACTIVE_MINI_TAB } from '../../constants';

const leftNavBg = '#153a5f'
const linkNav = (history, path) => {
  history.push({
    pathname: path
  })
}
const navigationStyle = {
  // backgroundColor: '#013042', 
  background: "linear-gradient(#013042, #024058)",
  padding: '0 0px'
}

class LeftNav extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: "",
      activeMiniTab: "",
      activeTabUpdated: false,
      activeMiniTabUpdated: false,
    }
  }

  componentDidUpdate() {
    const { activeTab, activeMiniTab, activeTabUpdated, activeMiniTabUpdated } = this.state;
    const { requestingActiveTab, requestingActiveMiniTab } = this.props;
    // console.log("activeTab", activeTab)
    // console.log("activeMiniTab", activeMiniTab)
    // console.log("activeTabUpdatedUpdated", activeTabUpdated)
    // console.log("activeMiniTabUpdated", activeMiniTabUpdated)
    if (requestingActiveTab && !activeTabUpdated) {
      this.setState({
        activeTab: requestingActiveTab,
        activeTabUpdated: true,
      })
    }
    if (requestingActiveMiniTab && !activeMiniTabUpdated) {
      this.setState({
        activeTab: requestingActiveMiniTab,
        activeMiniTabUpdated: true,
      })
    }
  }

  handleTabSelection = (path, changePath) => {
    const { history, dispatch } = this.props;
    this.setState({
      activeTab: path,
      activeMiniTab: "",
    })
    dispatch({ type: STORE_ACTIVE_TAB, data: path })
    if (changePath) {
      linkNav(history, path)
    }
  }

  handleMiniTabSelection = (path) => {
    const { history, dispatch } = this.props;
    this.setState({
      activeMiniTab: path,
      activeTab: ""
    })
    dispatch({ type: STORE_ACTIVE_MINI_TAB, data: path })
    linkNav(history, path)
  }

  render() {
    const { visible, children, history, menu, activateMiniTabs, leftNavOpen } = this.props
    const { activeTab, activeMiniTab } = this.state
    // console.log("the left", leftNavOpen) 
    // console.log("will you open", (activeTab.includes(link.path) || link.values.some(item => history.location.pathname.includes(item.path))) && leftNavOpen === true)
    // console.log("activeTab", activeTab) 
    return (
      <div className='pusher '>
        <div className='full height'>
          <div className='toc noprint'>

            <Menu inverted secondary className='left fixed vertical' id="menupms" style={navigationStyle}>
              <Menu.Item style={{ margin: '5px' }}>
                <div>
                  <Container>
                  <Image src={logo} size="medium" centered />
                  </Container>
                </div>
                <Divider />
              </Menu.Item>
              <div>
              {menu.map((link, index) => {
                if (!link.values) {
                  return (
                    <Menu.Item key={index} name={link.name} active={activeTab === link.path ? true : false || history.location.pathname.includes(link.path)} onClick={() => this.handleTabSelection(link.path, link.changePath)}>
                      <Icon className='left' name={link.icon} size='large' style={{ marginBottom: '5px' }} />
                      <span className='f5 ml3'>{link.name}</span>
                    </Menu.Item>
                  )
                }
                else if (link.values) {
                  return (
                    
                    <Menu.Item key={index} name={link.name} active={history.location.pathname.includes(link.path) || activeTab.includes(link.path) || link.values.some(item => history.location.pathname.includes(item.path))} >
                      <Icon className='left' name={link.icon} size='large' style={{ marginBottom: '5px' }} />
                      <Dropdown style={{ display: 'inline-block' }} item text={link.name}>
                        <Dropdown.Menu style={{ backgroundColor: '#013042' }}>
                          {link.values.map((item, index) => (
                            <Dropdown.Item key={index} name={item.name} onClick={() => this.handleMiniTabSelection(item.path)}>
                              <Icon name='circle' color='white' />
                              <span className='white'>{item.name}</span>
                            </Dropdown.Item>
                          ))
                          }
                        </Dropdown.Menu>
                      </Dropdown>
                    </Menu.Item>
                    
                    
                  )
                }
              })
              }
              </div>
            </Menu>
          </div>
          <div className="cot">
            {this.props.children}
          </div>
        </div>
      </div>
    )
  }
}




  //     <Sidebar.Pushable
  //       as={'div'}
  //       className="bg-near-white"
  //       style={{ marginTop: '0px' }}
  //     >
  //       <Sidebar
  //         as={Menu}
  //         animation="push"
  //         visible={visible}
  //         vertical
  //         inverted
  //         style={{ backgroundColor: leftNavBg, overflowX: 'visible', width: '500px' }}
  //       >
  //         <div className="h3 mb4" style={{ background: 'rgba(0, 0, 0, 0.25)' }}>
  //           <Image src={logo} className="center ui" />
  //           <Image src={logoMin} className="center pv2 w2" />
  //         </div>
  //         {menu.map((link, index) => {
  //           if (!link.values) {
  //             return (
  //               <Menu.Item
  //                 name={link.name}
  //                 className={link.className}
  //                 onClick={() => this.handleTabSelection(link.path, link.changePath)}
  //                 active={activeTab === link.path ? true : false || history.location.pathname.includes(link.path)}
  //                 key={index}
  //               >
  //                 <Icon
  //                   name={link.icon}
  //                   style={{
  //                     float: 'left',
  //                     marginLeft: '0em',
  //                     marginRight: '1em'
  //                   }}
  //                 />
  //                 <span className="hide-menu ttc">{link.name}</span>
  //               </Menu.Item>
  //             )
  //           }
  //           //test dropdown starts here
  //           else if (link.values) {
  //             return (
  //               <Fragment>
  //                 <Menu.Item
  //                   name={link.name}
  //                   className={link.className}
  //                   onClick={() => this.handleTabSelection(link.path, link.changePath)}
  //                   active={history.location.pathname.includes(link.path) || activeTab.includes(link.path) || link.values.some(item => history.location.pathname.includes(item.path))}
  //                   key={index}
  //                   style={{ overflow: "auto" }}
  //                 >
  //                   <Icon
  //                     name={link.icon}
  //                     style={{
  //                       float: 'left',
  //                       marginLeft: '0em',
  //                       marginRight: '1em',
  //                     }}
  //                   />
  //                   {/* <span className="hide-menu ttc">{link.name}</span> */}
  //                   {/* {((activeTab.includes(link.path) || link.values.some(item => history.location.pathname.includes(item.path))) && leftNavOpen === true) && */}
  //                   <Dropdown item text={link.name} style={{ display: "inline", float: "left" }}>
  //                     <Dropdown.Menu>
  //                       {link.values.map((item, index) => (
  //                         <Dropdown.Item
  //                           name={item.name}
  //                           onClick={() => this.handleMiniTabSelection(item.path)}
  //                           active={history.location.pathname === item.path ? true : false}
  //                           key={index}
  //                         // className='miniTab'
  //                         // icon='circle'
  //                         // position='right'
  //                         >
  //                           <Icon
  //                             name={`circle`}
  //                             style={{
  //                               // float: 'left',
  //                               // marginLeft: "4em",
  //                               // marginLeft: '0em',
  //                               // marginRight: '1em',
  //                               marginTop: '1em',
  //                             }}
  //                             size='tiny'
  //                           />
  //                           <span className="hide-menu ttc" style={{ fontSize: '1.3em' }}>{item.name}</span>
  //                         </Dropdown.Item>
  //                       ))}
  //                     </Dropdown.Menu>
  //                   </Dropdown>
  //                   {/* } */}
  //                 </Menu.Item>

  //                 {/* {((activeTab.includes(link.path) || link.values.some(item => history.location.pathname.includes(item.path))) && leftNavOpen === true) &&
  //                   <Menu style={{ marginTop: "0em", background: "transparent", }}
  //                     vertical
  //                     compact
  //                     width='thin'
  //                     borderless
  //                   >
  //                     {link.values.map((item, index) => (
  //                       <Menu.Item
  //                         name={item.name}
  //                         onClick={() => this.handleMiniTabSelection(item.path)}
  //                         active={history.location.pathname === item.path ? true : false}
  //                         key={index}
  //                         // className='miniTab'
  //                       // icon='circle'
  //                       // position='right'
  //                       >
  //                         <Icon
  //                           name={`circle`}
  //                           style={{
  //                             float: 'left',
  //                             // marginLeft: "4em",
  //                             // marginLeft: '0em',
  //                             // marginRight: '1em',
  //                             marginTop: '1em',
  //                           }}
  //                           size='tiny'
  //                         />
  //                         <span className="hide-menu ttc" style={{ fontSize: '1.3em' }}>{item.name}</span>
  //                       </Menu.Item>
  //                     ))}
  //                   </Menu>
  //                 } */}
  //               </Fragment>

  //               // <Dropdown item text={link.name} className={`link item hide-menu`} pointing="left" style={{}}>
  //               //   <Dropdown.Menu>
  //               //     {link.values.map((valItem, valIndex) => (
  //               //       <Menu.Item
  //               //         name={valItem.name}
  //               //         className={`${valItem.className}`}
  //               //         onClick={() => linkNav(history, link.path)}
  //               //         active={history.location.pathname === link.path ? true : false}
  //               //         key={valIndex}>
  //               //         {/* {valItem.name} */}
  //               //       </Menu.Item>
  //               //     ))}
  //               //   </Dropdown.Menu>
  //               // </Dropdown>
  //             )
  //           }
  //         })}

  //       </Sidebar>
  //       <Sidebar.Pusher>
  //         <div className="min-vh-100">{children}</div>
  //       </Sidebar.Pusher>
  //     </Sidebar.Pushable>
  //   )
  // }
// }

// const LeftNav = () => ()

LeftNav.propTypes = {
  visible: PropTypes.bool.isRequired,
  menu: PropTypes.array.isRequired,
  children: PropTypes.any
}

export default LeftNav














// import React, { Fragment } from 'react'
  // import PropTypes from 'prop-types'

  // import { Sidebar, Menu, Icon, Image, Dropdown, Segment } from 'semantic-ui-react'
  // import logo from '../../assets/logo1.png'
  // import logoMin from '../../assets/logo_icon.png'

  // const leftNavBg = '#153a5f'
  // const linkNav = (history, path) => {
  //   history.push({
  //     pathname: path
  //   })
  // }

  // class LeftNav extends React.Component {
  //   constructor(props){
  //     super(props);
  //     this.state = {
  //       activeTab: "",
  //       activeMiniTab: ""
  //     }
  //   }

  //   handleTabSelection = (path) => {
  //     const {history} = this.props;
  //     this.setState({
  //       activeTab: path
  //     })
  //     linkNav(history, path)
  //   }

  //   handleMiniTabSelection = (path) => {
  //     const {history} = this.props;
  //     this.setState({
  //       activeMiniTab: path
  //     })
  //     linkNav(history, path)
  //   }

  //   render() {
  //     const { visible, children, history, menu } = this.props

  //     return (
  //       <Sidebar.Pushable
  //         as={'div'}
  //         className="bg-near-white"
  //         style={{ marginTop: '0px'}}
  //       >
  //         <Sidebar
  //           as={Menu}
  //           animation="push"
  //           visible={visible}
  //           vertical
  //           inverted
  //           style={{ backgroundColor: leftNavBg }}
  //         >
  //           <div className="h3 mb4" style={{ background: 'rgba(0, 0, 0, 0.25)' }}>
  //             <Image src={logo} className="center ui" />
  //             <Image src={logoMin} className="center pv2 w2" />
  //           </div>
  //           {menu.map((link, index) => {
  //             if (!link.values) {
  //               return (
  //                 <Menu.Item
  //                   name={link.name}
  //                   className={link.className}
  //                   onClick={() => this.handleTabSelection(link.path)}
  //                   active={history.location.pathname === link.path ? true : false}
  //                   key={index}
  //                 >
  //                   <Icon
  //                     name={link.icon}
  //                     style={{
  //                       float: 'left',
  //                       marginLeft: '0em',
  //                       marginRight: '1em'
  //                     }}
  //                   />
  //                   <span className="hide-menu ttc">{link.name}</span>
  //                 </Menu.Item>
  //               )
  //             }
  //             //test dropdown starts here
  //             else if (link.values) {
  //               return (
  //                 <div>
  //                   <Menu.Item
  //                     name={link.name}
  //                     className={link.className}
  //                     onClick={() => linkNav(history, link.path)}
  //                     active={history.location.pathname.includes(link.path) || link.values.some(item => history.location.pathname.includes(item.path))}
  //                     key={index}
  //                   >
  //                     <Icon
  //                       name={link.icon}
  //                       style={{
  //                         float: 'left',
  //                         marginLeft: '0em',
  //                         marginRight: '1em'
  //                       }}
  //                     />
  //                     <span className="hide-menu ttc">{link.name}</span>
  //                     {/* {history.location.pathname.includes(link.path) && */}

  //                     {/* } */}
  //                   </Menu.Item>
  //                   {(history.location.pathname.includes(link.path) || link.values.some(item => history.location.pathname.includes(item.path))) &&
  //                     <Menu style={{ marginLeft: "4em", marginTop: "0em", background: "transparent", width: "10000px" }}
  //                       vertical
  //                       compact
  //                     >
  //                       {link.values.map((item, index) => (
  //                         <Menu.Item
  //                           name={item.name}
  //                           onClick={() => this.handleSelection(item.path)}
  //                           active={history.location.pathname === item.path ? true : false}
  //                           key={index}
  //                           // icon='circle'
  //                           // position='right'
  //                         >
  //                           <Icon
  //                             name={`circle`}
  //                             style={{
  //                               float: 'left',
  //                               marginLeft: '0em',
  //                               marginRight: '1em'
  //                             }}
  //                           />
  //                           <span className="hide-menu ttc">{item.name}</span>
  //                         </Menu.Item>
  //                       ))}
  //                     </Menu>
  //                   }
  //                 </div>

  //                 // <Dropdown item text={link.name} className={`link item hide-menu`} pointing="left" style={{}}>
  //                 //   <Dropdown.Menu>
  //                 //     {link.values.map((valItem, valIndex) => (
  //                 //       <Menu.Item
  //                 //         name={valItem.name}
  //                 //         className={`${valItem.className}`}
  //                 //         onClick={() => linkNav(history, link.path)}
  //                 //         active={history.location.pathname === link.path ? true : false}
  //                 //         key={valIndex}>
  //                 //         {/* {valItem.name} */}
  //                 //       </Menu.Item>
  //                 //     ))}
  //                 //   </Dropdown.Menu>
  //                 // </Dropdown>
  //               )
  //             }
  //           })}

  //         </Sidebar>
  //         <Sidebar.Pusher>
  //           <div className="min-vh-100">{children}</div>
  //         </Sidebar.Pusher>
  //       </Sidebar.Pushable>
  //     )
  //   }
  // }

  // // const LeftNav = () => ()

  // LeftNav.propTypes = {
  //   visible: PropTypes.bool.isRequired,
  //   menu: PropTypes.array.isRequired,
  //   children: PropTypes.any
  // }

  // export default LeftNav
