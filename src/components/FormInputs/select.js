import React from 'react'
import PropTypes from 'prop-types'

const renderOption = option => (
  <option key={option.value} value={option.value}>{option.label}</option>
)

const FormInputs = ({ field, form, label, options, ...rest }) => (
  <div className="pb2">
    {label && (
      <label htmlFor={field.name} className="f5 db mb2 black-30">
        {label}
      </label>
    )}
    <select
      className="w-100 input-reset ba b--black-10 black-60 bg-white pa2 mb2 db br0"
      {...field}
      {...rest}
    >
      {options.map(option => renderOption(option))}
    </select>
    {form.errors[field.name] &&
      form.touched[field.name] && (
        <small className="f6 dark-red db mb2">{form.errors[field.name]}</small>
      )}
  </div>
)

FormInputs.propTypes = {
  field: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,
  label: PropTypes.string
}

export default FormInputs
