import React from 'react'
import PropTypes from 'prop-types'

import Dropzone from 'react-dropzone'

// import request from 'superagent'

const onDrop = acceptedFiles => {
  // const req = request.post('/upload')
    acceptedFiles.forEach(file => {
      const reader = new FileReader()
      reader.onload = () => {
        const dataUrl = reader.result

        // req
        //   .set('Content-Type', 'image/png)
        //   .set('Content-Disposition', 'attachment; filename='+file.fileName)
        //   .send(filePath)
        //   .end()
    }

    reader.readAsDataURL(file)
  })
}

const FormInputs = ({ field, form, label, ...rest }) => (
  <div className="pb2">
    {label && (
      <label htmlFor={field.name} className="f5 db mb2 black-30">
        {label}
      </label>
    )}
    <Dropzone
      accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel text/csv"
      onDrop={onDrop}
      {...rest}
      className="br0 w-100 h4 ba b--light-gray"
      activeClassName="bg-light-green"
      rejectClassName="bg-light-red"
    >
      {({ isDragActive, isDragReject, acceptedFiles, rejectedFiles }) => {
        return acceptedFiles.length || rejectedFiles.length
          ? `Accepted ${acceptedFiles.length}, rejected ${
              rejectedFiles.length
            } files`
          : 'Drag and drop your accounts inventory files here (supports excel and csv)yarn build.'
      }}
    </Dropzone>
    {form.errors[field.name] &&
      form.touched[field.name] && (
        <small className="f6 dark-red db mb2">{form.errors[field.name]}</small>
      )}
  </div>
)

FormInputs.propTypes = {
  field: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,
  label: PropTypes.string
}

export default FormInputs
