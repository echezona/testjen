import React from 'react'
import PropTypes from 'prop-types'

const FormInputs = ({ field, form, label, ...rest }) => (
  <div className="pb2">
    {label && (
      <label htmlFor={field.name} className="f5 db mb2 black-30">
        {label}
      </label>
    )}
    <input
      {...field}
      {...rest}
      className="input-reset ba b--black-10 black-60 pa2 mb2 db w-100"
    />
    {form.errors[field.name] &&
      form.touched[field.name] && (
        <small className="f6 dark-red db mb2">{form.errors[field.name]}</small>
      )}
  </div>
)

FormInputs.propTypes = {
  field: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,
  label: PropTypes.string
}

export default FormInputs
