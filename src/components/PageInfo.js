import React, { Component } from 'react'
import { Accordion, Icon } from 'semantic-ui-react'

export default class PageInfo extends Component {
  state = { activeIndex: 0 }

  handleClick = (e, titleProps) => {
    const { index } = titleProps
    const { activeIndex } = this.state
    const newIndex = activeIndex === index ? -1 : index

    this.setState({ activeIndex: newIndex })
  }

  render() {
    const { activeIndex } = this.state
    const { title, content } = this.props

    return (
      <Accordion fluid styled>
        <Accordion.Title active={activeIndex === 0} index={0} onClick={this.handleClick}>
          <Icon name='info circle' />
          {title}
        </Accordion.Title>
        <Accordion.Content active={activeIndex === 0}>
          {content}
        </Accordion.Content>
      </Accordion>
    )
  }
}
