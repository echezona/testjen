import React from 'react'
import { Route, Switch } from 'react-router-dom'
import DashboardPage from './pages/Dashboard'
import Authentication from './components/auth'
import BehavioralDashboard from './pages/BehavioralDashboard'
import SalaryReview from './pages/SalaryReview';
import StaffPage from './pages/Staff'
import BusinessObjectives from './pages/BusinessObjectives'
import PersonalObjectives from './pages/PersonalObjectives'
import PerformanceAssessment from './pages/PerformanceAssessment'
import BehavioralAssessment from './pages/BehavioralAssessment'
import ManageOKRs from './pages/ManageOKRs'
import N1PerformanceAssessment from './pages/N1PerformanceAssessment'
import N1BehavioralAssessment from './pages/N1BehavioralAssessment'
import OkrObjectives from './pages/okrObjectives'

export default (
  <Switch>
    <Route exact component={Authentication} path="/" />
    <Route component={StaffPage} path="/Home" />
    <Route component={BehavioralDashboard} path="/BehaviorDashboard" />
    <Route component={SalaryReview} path="/ApproveRecommendation" />
    <Route component={BusinessObjectives} path="/BusinessObjectives" />
    <Route component={PersonalObjectives} path="/PersonalObjectives" />
    <Route component={PerformanceAssessment} path="/PerformanceAssessment"/>
    <Route component={BehavioralAssessment} path="/BehavioralAssessment"/>
    <Route component={ManageOKRs} path="/ManageOKRs"/>
    <Route component={N1PerformanceAssessment} path="/N1PerformanceAssessment"/>
    <Route component={N1BehavioralAssessment} path="/N1BehavioralAssessment"/>
    <Route component={OkrObjectives} path="/OkrObjectives"/>
  </Switch>
)
