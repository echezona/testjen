import {all,call,takeLatest} from 'redux-saga/effects'
import {FETCH_APPRAISAL_OBJECTIVE} from './constants'
import { createRequest } from './helper'

function* fetchAppraisalObjective(){
    try {
        const token = ''
        const req = yield call(createRequest, "https://pms-api.test.vggdev.com/api/AppraisalObjective", { method: 'GET' }, token)
        const res = yield call(fetch, req)
        console.log(res)

        /**
         * check that response header content type value is application/json
         */

        // yield call(checkContentType, JSON_CONTENT_TYPE, res)

        // const resJson = yield call(res.json.bind(res))

    } catch (error) {  
        
    }
}

function* fetchAppraisalObjectiveWatcher() {
    yield takeLatest(FETCH_APPRAISAL_OBJECTIVE,fetchAppraisalObjective)
}

//my sorry attempt
// function* fetchObjectivesandKeyResults(){
//     try {
//         const token = ''
//         const req = yield call(createRequest, "", { method: 'GET' }, token)
//         const res = yield call(fetch, req)
//         console.log(res)

//         /**
//          * check that response header content type value is application/json
//          */

//         // yield call(checkContentType, JSON_CONTENT_TYPE, res)

//         // const resJson = yield call(res.json.bind(res))

//     } catch (error) {  
        
//     }
// }


// function* fetchObjectivesandKeyResultsWatcher() {
//     yield takeLatest(FETCH_OBJECTIVES_AND_KEY_RESULTS,fetchAppraisalObjective)
// }




export default function* rootSaga(){
    yield all([
        fetchAppraisalObjectiveWatcher(),
    
    ])
}

