import React, { Component } from 'react'
import './App.css'
import Layout from './components/Layout'

import { userSelector, staffIdSelector, userEmailSelector } from './reducer'
import { connect } from "react-redux"
import { Route, Switch } from 'react-router-dom'
import Authentication from './components/auth'
import StaffPage from './pages/Staff'
import BusinessObjectives from './pages/BusinessObjectives'
import PerformanceAssessment from './pages/PerformanceAssessment'
import BehavioralAssessment from './pages/BehavioralAssessment'
import ManageOKRs from './pages/ManageOKRs'
import OkrObjectives from './pages/okrObjectives'
import OkrObjectiveStaff from './pages/OkrObjectiveStaff'
import Summary from './pages/Summary'
import AdminManageOKRs from './pages/AdminManageOKRs'
import AdminApproveOkrs from './pages/AdminApproveOkrs'
import SuperApproveOKRs from './pages/superApproveOkrs'
import SuperManageOKRs from './pages/superManageOKRs'
import BehavioralSetting from './pages/BehavioralSetting'
// import PerformanceScore from './pages/AssessmentScore/Performance.js'
// import BehaviourScore from './pages/AssessmentScore/Behaviour.js'
import SetStaffList from "./pages/SetStaffList";
import UploadLineManager from "./pages/Upload-Line-Manager";
import UploadStaff from "./pages/Upload-Staff";
import Department from "./pages/Department";
import BusinessUnit from "./pages/BusinessUnit";
import { FETCH_STAFF_DETAILS, FETCH_OBJECTIVES_AND_KEY_RESULTS, FETCH_CATEGORY, FETCH_APPRAISAL_CYCLE, FETCH_STAFF_ROLE } from './constants';
import BehavioralIndicators from './pages/BehavioralIndicators';
import AppraisalCycleSetting from './pages/AppraisalCycleSetting';
import ManageAssessment from './pages/ManageAssessment';
import AssessOKRs from './pages/AssessOKRs';
import NoMatch from './pages/NoMatch';
import SuperManageAssessment from './pages/SuperManageAssessment';
import SuperAssessOKRs from './pages/SuperAssessOKRs';
import SbuObjectiveSetting from './pages/SbuObjectiveSetting';
import ErrorBoundary from './pages/ErrorBoundary';
import ChangePassword from './pages/ChangePassword';
import AdminManageAssessment from './pages/AdminManageAssessment';
import AdminAssessOkrs from './pages/AdminAssessOkrs';
import Scores from './pages/Scores';
import AdminStaffManager from './pages/AdminStaffManager'
import StaffManagerDetails from './pages/AdminStaffManager/StaffManagerDetails'
import OneOnOneManager from './pages/OneOnOneManager'
import OneOnOneStaff from './pages/OneOnOneStaff'
import OneOnOneManagerDetails from './pages/OneOnOneManager/OneOnOneManagerDetails'



class App extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentDidMount() {
    const { user, dispatch, userEmail } = this.props
    // console.log("user object", user.profile)
    dispatch({ type: FETCH_STAFF_DETAILS, userEmail: userEmail })
    dispatch({ type: FETCH_STAFF_ROLE, userEmail:  userEmail })
    // dispatch({ type: FETCH_STAFF_DETAILS, userEmail: user.profile.email })
    // dispatch({ type: FETCH_STAFF_ROLE, userEmail:  user.profile.email })
    dispatch({ type: FETCH_APPRAISAL_CYCLE })
    dispatch({ type: FETCH_OBJECTIVES_AND_KEY_RESULTS })
    dispatch({ type: FETCH_CATEGORY })
    // dispatch({ type: FETCH_ALL_SBU_OBJECTIVES })

  }

  // dispatch({ type: FETCH_STAFF_APPRAISAL })

  render() {
    const { history, client, user, staffRole } = this.props
    return (
      <Layout history={history} client={client}>
        <ErrorBoundary history={history}>
          <Switch>
            <Route exact render={props => <Authentication {...props} /> } path="/" />
            <Route render={props => <ChangePassword {...props} /> } path="/changepassword" />
            <Route render={props => <StaffPage  {...props} /> } path="/Home" />
            <Route render={props => <BusinessObjectives {...props} user={user}/> } path="/businessobjectives" />
            <Route render={props => <PerformanceAssessment {...props} /> } path="/PerformanceAssessment" />
            <Route render={props => <BehavioralAssessment {...props} /> } path="/BehavioralAssessment" />
            <Route render={props => <SetStaffList {...props} /> } path="/setstafflist" />
            <Route render={props => <UploadLineManager {...props} /> } path="/upload-line-manager" />
            <Route render={props => <UploadStaff {...props} /> } path="/upload-staff" />
            <Route render={props => <BusinessUnit {...props} /> } path="/upload-business-unit" />
            <Route render={props => <Department {...props} /> } path="/upload-department" />
            <Route render={props => <ManageOKRs {...props} /> } path="/ManageOKRs" />
            <Route render={props => <Summary {...props} user={user}/> } path="/summary" />
            <Route render={props => <AdminManageOKRs {...props} /> } path="/adminmanageokrs" />
            <Route render={props => <AdminApproveOkrs {...props} /> } path="/adminapproveokrs" />
            <Route render={props => <OkrObjectives {...props} /> } path="/OkrObjectives" />
            <Route render={props => <OkrObjectiveStaff {...props} /> } path="/OkrObjectivestaff" />
            <Route render={props => <AssessOKRs {...props} /> } path="/assessokrs" />
            <Route render={props => <SuperAssessOKRs {...props} /> } path="/superassessokrs" />
            <Route render={props => <SuperManageOKRs {...props} /> } path="/superManageOKRs" />
            <Route render={props => <SuperApproveOKRs {...props} /> } path="/superApproveOKRs" />
            <Route render={props => <BehavioralSetting {...props} /> } path="/behavioralsetting" />
            <Route render={props => <BehavioralIndicators {...props} /> } path="/behavioralindicators" />
            <Route render={props => <AppraisalCycleSetting {...props} /> } path="/appraisalcyclesetting" />
            <Route render={props => <SbuObjectiveSetting {...props} /> } path="/sbuobjectivesetting" />
            <Route render={props => <ManageAssessment {...props} /> } path="/manageassessment" />
            <Route render={props => <SuperManageAssessment {...props} /> } path="/supermanageassessment" />
            <Route render={props => <AdminManageAssessment {...props} /> } path="/adminmanageassessment" />
            <Route render={props => <AdminAssessOkrs {...props} /> } path="/adminassessokrs" />
            <Route render={props => <Scores {...props} /> } path="/assessmentscores" />
            <Route render={props => <AdminStaffManager {...props} /> } path="/staff-management" />
            <Route render={props => <StaffManagerDetails {...props} /> } path="/staff-management-details" />
            <Route render={props => <OneOnOneManager {...props} /> } path="/oneonone-direct-report" />
            <Route render={props => <OneOnOneManagerDetails {...props} /> } path="/oneonone-direct-report-details" />
            <Route render={props => <OneOnOneStaff {...props} /> } path="/oneonone-personal" />
            <Route component={NoMatch}/>
          </Switch>
        </ErrorBoundary>
      </Layout>
    )

  }
}

function mapStateToProps(state) {
  return {
    user: userSelector(state),
    userEmail: userEmailSelector(state),
    staffId: staffIdSelector(state),
    staffRole: state.requestingStaffRole,
  }
}

export default connect(mapStateToProps)(App);
