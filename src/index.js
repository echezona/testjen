import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";
import { createBrowserHistory } from "history";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import storeAndPersistorObject from "./store";
import Auth from "./components/auth/index";

//import routes from './routes'
import "semantic-ui-css/semantic.min.css";
import "react-datepicker/dist/react-datepicker.css";
import "react-datepicker/dist/react-datepicker-cssmodules.css";
import './pages/AppraisalCycleSetting/main.css';
import "./index.css";
const { store, persistor } = storeAndPersistorObject;



ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <Router>
        <Auth />
      </Router>
    </PersistGate>
  </Provider>,
  document.getElementById("root")
);
