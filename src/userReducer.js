import { combineReducers } from "redux"
import { SET_OKRs } from "./constants";


const entities = (state = null, { type, entities }) => {
    switch (type) {
        case SET_OKRs:
            console.log('entities', entities)
            return entities || []
        default:
            return state
    }
}
const objectivesById = (state = [], { type, ids }) => {
    switch (type) {
        case SET_OKRs:
            return ids || []
        default:
            return state
    }
}

export default combineReducers({
    objectivesById,
    entities
})