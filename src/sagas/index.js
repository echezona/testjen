import { all, call, takeLatest, select, put, take } from 'redux-saga/effects'
import { FETCH_APPRAISAL_OBJECTIVES, FETCH_OBJECTIVES_AND_KEY_RESULTS, SAVE_KEY_RESULTS, REQUEST_SUCCESS,
    REQUEST_ERROR, SET_OKRs, IS_REQUESTING_SAVE_RESULTS, IS_DONE_REQUESTING_SAVE_RESULTS, SAVE_SUCCESSFUL,
    SAVE_FAILED, SAVE_DONE, FETCH_SBU_OBJECTIVES, CREATE_OKRs, GET_SBU_OBJECTIVES, FETCH_CATEGORY, GET_CATEGORY,
    FETCH_APPRAISAL_CYCLE, GET_APPRAISAL_CYCLE, FETCH_STAFF_APPRAISAL, GET_STAFF_APPRAISAL, GET_STAFF_APPRAISAL_ID,
    CREATE_STAFF_APPRAISAL, GET_APPRAISAL_OBJECTIVES, FETCH_STAFF_DETAILS, GET_STAFF_DETAILS, GET_OKRs_NOT_NORMALIZED,
    GET_DECODED_TOKEN, SBU_REQUEST_SUCCESS, SAVE_SUCCESSFUL_MESSAGE_VERSION, SAVE_FAILED_MESSAGE_VERSION, FETCH_LINE_MANAGER_OKRS,
    GET_LINE_MANAGER_OKRS, GET_LINE_MANAGER_STAFF_APPRAISALS, FETCH_LINE_MANAGER_STAFF_APPRAISALS, SUBMIT_OKRS, SUBMIT_LINE_MANAGER_OKRS,
    UPDATE_LINE_MANAGER_STATUS, UPDATE_KEY_RESULTS, GET_PEOPLE_OPS_STAFF_APPRAISALS, FETCH_PEOPLE_OPS_STAFF_APPRAISALS,
    STAFF_APPRAISAL_EXISTS, UPDATE_PEOPLE_OPS_STATUS, FETCH_LMPO_OKRS, GET_LMPO_OKRS, SUBMIT_PEOPLE_OPS_OKRS, UPDATE_OBJECTIVES,
    DONE_SUBMITTING_OKRS, SUBMITTING_OKRS, FETCHING_STAFF_APPRAISALS, DONE_FETCHING_STAFF_APPRAISALS, FETCHING_LMPO_OKRS,
    DONE_FETCHING_LMPO_OKRS, GET_STAFF_ROLE, FETCH_STAFF_ROLE, FETCH_SUPER_LINE_MANAGER_STAFF_APPRAISALS,
    GET_SUPER_LINE_MANAGER_STAFF_APPRAISALS, SUBMIT_SUPER_LINE_MANAGER_OKRS, UPLOAD_BEHAVIOUR, GET_BEHAVIORAL_INDICATORS,
    FETCH_BEHAVIORAL_INDICATORS, SAVE_APPRAISAL_CYCLE, CREATE_STAFF_APPRAISAL_ERROR, SAVE_BEHAVIORAL_INDICATORS,
    BEHAVIORAL_INDICATORS_SAVE_SUCCESSFUL, SAVE_BEHAVIORAL_ASSESSMENT_HAS_STOPPED_LOADING, SAVE_BEHAVIORAL_ASSESSMENT_IS_LOADING,
    FETCH_BEHAVIORAL_ASSESSMENT_HAS_STOPPED_LOADING, FETCH_BEHAVIORAL_ASSESSMENT_IS_LOADING, FETCH_OBJECTIVES_AND_KEY_RESULTS_HAS_STOPPED_LOADING,
    FETCH_OBJECTIVES_AND_KEY_RESULTS_IS_LOADING, FETCH_CATEGORY_IS_LOADING, FETCH_CATEGORY_HAS_STOPPED_LOADING, SAVE_KEY_RESULTS_IS_LOADING,
    SAVE_KEY_RESULTS_HAS_STOPPED_LOADING, KEY_RESULTS_SAVE_SUCCESSFUL, GET_RECOMMENDATION_TYPES, FETCH_RECOMMENDATION_TYPES, SAVE_RECOMMENDATION,
    GET_RECOMMENDATION, FETCH_RECOMMENDATION, SUBMIT_LINE_MANAGER_ASSESSMENT, SUBMIT_RECOMMENDATION, UPDATE_RECOMMENDATION, 
    MODIFY_RECOMMENDATION_AFTER_UPDATE, MODIFY_RECOMMENDATION_AFTER_SUBMIT, GET_SUPER_RECOMMENDATION, FETCH_SUPER_RECOMMENDATION,
    MODIFY_SUPER_RECOMMENDATION_AFTER_UPDATE, UPDATE_SUPER_RECOMMENDATION, SUBMIT_SUPER_LINE_MANAGER_ASSESSMENT, LOCK_APPRAISAL_PERIOD,
    MODIFY_APPRAISAL_CYCLE_AFTER_UPDATE, FETCH_PEOPLE_OPS_COUNT, GET_PEOPLE_OPS_COUNT, FETCH_ALL_BUSINESS_UNITS, GET_ALL_BUSINESS_UNITS,
    FETCH_ALL_SBU_OBJECTIVES, GET_ALL_SBU_OBJECTIVES, CREATE_SBU_OBJECTIVE, MODIFY_ALL_SBU_OBJECTIVES_AFTER_SUBMIT,
    REPLACE_APPRAISAL_CYCLE_AFTER_CREATION, SAVE_STAFF_APPRAISAL_IS_LOADING, SAVE_STAFF_APPRAISAL_HAS_STOPPED_LOADING, GET_BEHAVIOURS,
    FETCH_BEHAVIOURS_IS_LOADING, FETCH_BEHAVIOURS_HAS_STOPPED_LOADING, FETCH_BEHAVIOURS, UPLOAD_BEHAVIOURS_IS_LOADING,
    UPLOAD_BEHAVIOURS_HAS_STOPPED_LOADING, CREATE_SBU_OBJECTIVES_IS_LOADING, CREATE_SBU_OBJECTIVES_HAS_STOPPED_LOADING, LOCK_APPRAISAL_PERIOD_IS_LOADING,
    LOCK_APPRAISAL_PERIOD_HAS_STOPPED_LOADING, SBU_OBJECTIVES_SAVE_DONE, SBU_OBJECTIVES_SAVE_FAILED, SBU_OBJECTIVES_SAVE_SUCCESSFUL,
    LOCK_APPRAISAL_CYCLE_FAILED, LOCK_APPRAISAL_CYCLE_DONE, LOCK_APPRAISAL_CYCLE_SUCCESSFUL, UPLOAD_BEHAVIOUR_SUCCESSFUL, UPLOAD_BEHAVIOUR_DONE,
    UPLOAD_BEHAVIOUR_FAILED, FETCH_STAFF_DETAILS_IS_LOADING, FETCH_STAFF_DETAILS_HAS_STOPPED_LOADING, MODIFY_LINE_MANAGER_STAFF_APPRAISALS_ON_OKR_SUBMIT,
    SUBMIT_OKRS_SUCCESSFUL, SUBMIT_OKRS_DONE, SUBMIT_OKRS_FAILED, MODIFY_SUPER_LINE_MANAGER_STAFF_APPRAISALS_ON_OKR_SUBMIT,
    MODIFY_PEOPLE_OPS_STAFF_APPRAISALS_ON_OKR_SUBMIT, SUBMIT_PEOPLE_OPS_OKRS_IS_LOADING, SUBMIT_PEOPLE_OPS_OKRS_HAS_STOPPED_LOADING,
    UPDATE_KEY_RESULTS_IS_LOADING, UPDATE_KEY_RESULTS_HAS_STOPPED_LOADING, UPDATE_KEY_RESULTS_SUCCESSFUL, UPDATE_KEY_RESULTS_DONE,
    UPDATE_KEY_RESULTS_FAILED, MODIFY_STAFF_APPRAISALS_ON_KEY_RESULTS_UPDATE, ONBOARD_USER, ONBOARDING_USER_IS_LOADING, ONBOARDING_USER_HAS_STOPPED_LOADING,
    ONBOARDING_USER_SUCCESSFUL, ONBOARDING_USER_DONE, ONBOARDING_USER_FAILED, CHANGE_PASSWORD, CHANGE_PASSWORD_IS_LOADING,
    CHANGE_PASSWORD_HAS_STOPPED_LOADING, CHANGE_PASSWORD_SUCCESSFUL, CHANGE_PASSWORD_DONE, CHANGE_PASSWORD_FAILED, FETCH_STAFF_DETAILS_RESPONSE_STATUS,
    FETCH_STAFF_APPRAISAL_IS_LOADING, FETCH_STAFF_APPRAISAL_HAS_STOPPED_LOADING, LINE_MANAGER_UPDATE_KEY_RESULTS, SUBMIT_LINE_MANAGER_OKRS_IS_LOADING,
    SUBMIT_LINE_MANAGER_OKRS_HAS_STOPPED_LOADING, LINE_MANAGER_UPDATE_KEY_RESULTS_FAILED, LINE_MANAGER_UPDATE_KEY_RESULTS_DONE,
    PEOPLE_OPS_UPDATE_KEY_RESULTS, PEOPLE_OPS_UPDATE_KEY_RESULTS_FAILED, PEOPLE_OPS_UPDATE_KEY_RESULTS_DONE, UPDATE_SBU_OBJECTIVE_SUCCESSFUL,
    UPDATE_SBU_OBJECTIVE_DONE, UPDATE_SBU_OBJECTIVE_FAILED, UPDATE_SBU_OBJECTIVE, UPDATE_SBU_OBJECTIVE_HAS_STOPPED_LOADING, UPDATE_SBU_OBJECTIVE_IS_LOADING,
    DELETE_SBU_OBJECTIVE_IS_LOADING, DELETE_SBU_OBJECTIVE_HAS_STOPPED_LOADING, DELETE_SBU_OBJECTIVE, UNLOCK_APPRAISAL_PERIOD,
    UNLOCK_APPRAISAL_PERIOD_IS_LOADING, UNLOCK_APPRAISAL_CYCLE_SUCCESSFUL, UNLOCK_APPRAISAL_CYCLE_DONE, UNLOCK_APPRAISAL_CYCLE_FAILED,
    UNLOCK_APPRAISAL_PERIOD_HAS_STOPPED_LOADING, FETCH_APPRAISAL_CYCLE_IS_LOADING, FETCH_APPRAISAL_CYCLE_HAS_STOPPED_LOADING,
    MODIFY_ALL_SBU_OBJECTIVES_AFTER_UPDATE, MODIFY_ALL_SBU_OBJECTIVES_AFTER_DELETE, FETCH_ALL_BUSINESS_UNITS_IS_LOADING,
    FETCH_ALL_BUSINESS_UNITS_HAS_STOPPED_LOADING, SAVE_BEHAVIOUR_SUCCESSFUL, SAVE_BEHAVIOUR_FAILED, SAVE_BEHAVIOUR_DONE, UPDATE_RECOMMENDATION_SUCCESSFUL,
    UPDATE_RECOMMENDATION_FAILED, UPDATE_RECOMMENDATION_DONE, FETCH_STAFF_APPRAISAL_SCORE, FETCH_STAFF_APPRAISAL_SCORE_IS_LOADING,
    GET_STAFF_APPRAISAL_SCORE, FETCH_STAFF_APPRAISAL_SCORE_HAS_STOPPED_LOADING, CALCULATE_STAFF_APPRAISAL_SCORE, CALCULATE_STAFF_APPRAISAL_SCORE_IS_LOADING,
    CALCULATE_STAFF_APPRAISAL_SCORE_HAS_STOPPED_LOADING, CALCULATE_STAFF_APPRAISAL_SCORE_SUCCESSFUL, CALCULATE_STAFF_APPRAISAL_SCORE_DONE,
    CALCULATE_STAFF_APPRAISAL_SCORE_FAILED, MODIFY_STAFF_APPRAISAL_SCORE_AFTER_CALCULATION, UPDATE_RECOMMENDATION_HAS_STOPPED_LOADING,
    UPDATE_RECOMMENDATION_IS_LOADING, SUBMIT_SUPER_LINE_MANAGER_OKRS_IS_LOADING, SUBMIT_RECOMMENDATION_SUCCESSFUL, SUBMIT_RECOMMENDATION_HAS_STOPPED_LOADING,
    SUBMIT_RECOMMENDATION_DONE, SUBMIT_RECOMMENDATION_FAILED, SUBMIT_RECOMMENDATION_IS_LOADING, SUBMIT_LINE_MANAGER_ASSESSMENT_HAS_STOPPED_LOADING,
    SUBMIT_LINE_MANAGER_ASSESSMENT_IS_LOADING, SUBMIT_LINE_MANAGER_ASSESSMENT_DONE, SUBMIT_LINE_MANAGER_ASSESSMENT_FAILED,
    SUBMIT_LINE_MANAGER_ASSESSMENT_SUCCESSFUL, MODIFY_LINE_MANAGER_STAFF_APPRAISALS_ON_ASSESS_COMPLETE,
    MODIFY_SUPER_LINE_MANAGER_STAFF_APPRAISALS_ON_ASSESS_COMPLETE, SAVE_APPRAISAL_CYCLE_IS_LOADING, SAVE_APPRAISAL_CYCLE_HAS_STOPPED_LOADING,
    SAVE_APPRAISAL_CYCLE_SUCCESSFUL, SAVE_APPRAISAL_CYCLE_DONE, SAVE_APPRAISAL_CYCLE_FAILED, DELETE_APPRAISAL_OBJECTIVE, DELETE_APPRAISAL_OBJECTIVE_IS_LOADING,
    DELETE_APPRAISAL_OBJECTIVE_HAS_STOPPED_LOADING, DELETE_KEY_RESULT, DELETE_KEY_RESULT_IS_LOADING, DELETE_KEY_RESULT_HAS_STOPPED_LOADING, ADD_KEY_RESULT,
    ADD_KEY_RESULT_IS_LOADING, ADD_KEY_RESULT_HAS_STOPPED_LOADING, FETCH_ALL_STAFF, GET_ALL_STAFF, FETCH_ALL_STAFF_HAS_STOPPED_LOADING,
    FETCH_ALL_STAFF_IS_LOADING, FETCH_ALL_LINE_MANAGERS, FETCH_ALL_LINE_MANAGERS_IS_LOADING, FETCH_ALL_LINE_MANAGERS_HAS_STOPPED_LOADING, GET_ALL_LINE_MANAGERS,
    FETCH_ALL_DEPARTMENTS, FETCH_ALL_DEPARTMENTS_IS_LOADING, GET_ALL_DEPARTMENTS, FETCH_ALL_DEPARTMENTS_HAS_STOPPED_LOADING, FETCH_STAFF_DETAILS_BY_MANAGER,
    FETCH_STAFF_DETAILS_BY_MANAGER_IS_LOADING, GET_STAFF_DETAILS_BY_MANAGER, FETCH_STAFF_DETAILS_BY_MANAGER_HAS_STOPPED_LOADING, FETCH_STAFF_DETAILS_BY_ADMIN,
    FETCH_STAFF_DETAILS_BY_ADMIN_HAS_STOPPED_LOADING, FETCH_STAFF_DETAILS_BY_ADMIN_IS_LOADING, GET_STAFF_DETAILS_BY_ADMIN, UPDATE_STAFF_DETAILS_IS_LOADING,
    UPDATE_STAFF_DETAILS_HAS_STOPPED_LOADING, UPDATE_STAFF_DETAILS, FETCH_STAFF_CLAIMS, GET_STAFF_CLAIMS, FETCH_STAFF_CLAIMS_IS_LOADING, FETCH_STAFF_CLAIMS_HAS_STOPPED_LOADING, UPDATE_USER_CLAIMS, UPDATE_USER_CLAIMS_HAS_STOPPED_LOADING, UPDATE_USER_CLAIMS_IS_LOADING, FETCH_STAFF_OKR_PERIOD_IS_LOADING, FETCH_STAFF_OKR_PERIOD_HAS_STOPPED_LOADING, GET_STAFF_OKR_PERIOD, FETCH_STAFF_OKR_PERIOD, CREATE_STAFF_OKR_PERIOD, CREATE_STAFF_OKR_PERIOD_IS_LOADING, CREATE_STAFF_OKR_PERIOD_HAS_STOPPED_LOADING, UPDATE_STAFF_OKR_PERIOD_IS_LOADING, UPDATE_STAFF_OKR_PERIOD_HAS_STOPPED_LOADING, UPDATE_STAFF_OKR_PERIOD, FETCH_ALL_STAFF_OKR_PERIOD_IS_LOADING, GET_ALL_STAFF_OKR_PERIOD, FETCH_ALL_STAFF_OKR_PERIOD_HAS_STOPPED_LOADING, FETCH_ALL_STAFF_OKR_PERIOD, FETCH_ALL_LM_STAFF_OKR_PERIOD, FETCH_ALL_LM_STAFF_OKR_PERIOD_IS_LOADING, GET_ALL_LM_STAFF_OKR_PERIOD, FETCH_ALL_LM_STAFF_OKR_PERIOD_HAS_STOPPED_LOADING
} from '../constants'
import { createRequest, createRequestSansHeader } from '../helper'
import { normalize } from 'normalizr'
import { objectivesSchema } from '../Schema';
import { delay } from '../../node_modules/redux-saga';
import { appraisalCycleIdSelector, staffIdSelector, staffAppraisalIdSelector, sbuNameSelector } from "../reducer"
import { PMS_API_URL, STAFF_API_URL } from '../appConstants'
import { ToastContainer, toast } from "react-toastify";


///The JWT decoding is in fetchObjectivesandKeyResults
let b64DecodeUnicode = str =>
    decodeURIComponent(
        Array.prototype.map.call(atob(str), c =>
            '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
        ).join(''))

let parseJwt = token =>
    JSON.parse(
        b64DecodeUnicode(
            token.split('.')[1].replace('-', '+').replace('_', '/')
        )
    )


function* onboardUser({ email }) {
    // yield put({type:IS_REQUESTING_SAVE_RESULTS})
    console.log("see the data you're posting oo", email)
    try {
        yield put({ type: ONBOARDING_USER_IS_LOADING })
        
        const req = yield call(createRequest, `${PMS_API_URL}/api/User/registeruser?email=${email}`, { method: 'POST' } )
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        console.log("onboardUser response", resJson)
        // yield put({type:GET_STAFF_APPRAISAL_ID, payload: resJson})

        yield put({ type: ONBOARDING_USER_HAS_STOPPED_LOADING })

        yield put({ type: ONBOARDING_USER_SUCCESSFUL, message: resJson })
        yield call(delay, 4000)
        yield put({ type: ONBOARDING_USER_DONE })

    } catch (error) {
        console.log(error)
        yield put({ type: ONBOARDING_USER_HAS_STOPPED_LOADING })

        yield put({ type: ONBOARDING_USER_FAILED, message: error })
        yield call(delay, 4000)
        yield put({ type: ONBOARDING_USER_DONE })
        
        // yield put({ type: CREATE_STAFF_APPRAISAL_ERROR, error })
    }
}
    
function* onboardUsersWatcher() {
    yield takeLatest(ONBOARD_USER, onboardUser)
}



function* changePassword({ currentPassword, newPassword }) {
    // yield put({type:IS_REQUESTING_SAVE_RESULTS})
    console.log("see the data you're posting oo", currentPassword, newPassword)
    try {
        yield put({ type: CHANGE_PASSWORD_IS_LOADING })
        // yield call(delay, 4000)
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        // console.log(token)
        const passwords = { currentPassword, newPassword }
        const req = yield call(createRequest, `${PMS_API_URL}/api/User/changepassword`, { method: 'POST', body: JSON.stringify(passwords) }, token )
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        console.log("changePassword response", resJson)

        yield put({ type: CHANGE_PASSWORD_HAS_STOPPED_LOADING })

        yield put({ type: CHANGE_PASSWORD_SUCCESSFUL, message: resJson })
        yield call(delay, 4000)
        yield put({ type: CHANGE_PASSWORD_DONE })

    } catch (error) {
        console.log(error)
        yield put({ type: CHANGE_PASSWORD_HAS_STOPPED_LOADING })

        yield put({ type: CHANGE_PASSWORD_FAILED, message: error })
        yield call(delay, 4000)
        yield put({ type: CHANGE_PASSWORD_DONE })
        
        // yield put({ type: CREATE_STAFF_APPRAISAL_ERROR, error })
    }
}
    
function* changePasswordWatcher() {
    yield takeLatest(CHANGE_PASSWORD, changePassword)
}



function* fetchAppraisalObjectives() {
    try {
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        console.log(token)
        const req = yield call(createRequest, `${PMS_API_URL}/api/AppraisalObjective`, { method: 'GET' }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        yield put({ type: GET_APPRAISAL_OBJECTIVES, payload: resJson })

        /**
         * check that response header content type value is application/json
         */

        // yield call(checkContentType, JSON_CONTENT_TYPE, res)
    } catch (error) {
        console.log(error)
    }
}

function* fetchAppraisalObjectivesWatcher() {
    yield takeLatest(FETCH_APPRAISAL_OBJECTIVES, fetchAppraisalObjectives)
}

//my sorry attempt
function* fetchObjectivesandKeyResults() {
    try {
        //start fetch loader
        yield put({ type: FETCH_OBJECTIVES_AND_KEY_RESULTS_IS_LOADING })
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        // console.log(token)
        const decodedToken = parseJwt(token)
        console.log(decodedToken)
        yield put({ type: GET_DECODED_TOKEN, token: decodedToken })
        const req = yield call(createRequest, `${PMS_API_URL}/api/AppraisalObjectiveKeyResults`, { method: 'GET' }, token)
        const res = yield call(fetch, req)

        console.log("watch me NENE", res.status)

        const resJson = yield call(res.json.bind(res))
        console.log("look at the resJson: ", resJson)
        yield put({ type: REQUEST_SUCCESS })
        const normalizedData = yield call(normalize, resJson, objectivesSchema)
        console.log("normalized", normalizedData)
        yield put({ type: SET_OKRs, entities: normalizedData.entities, ids: normalizedData.result })
        yield put({ type: GET_OKRs_NOT_NORMALIZED, data: resJson })
        //stop fetch loader
        yield put({ type: FETCH_OBJECTIVES_AND_KEY_RESULTS_HAS_STOPPED_LOADING })

        /**
         * check that response header content type value is application/json
         */

        // yield call(checkContentType, JSON_CONTENT_TYPE, res)

    } catch (error) {
        //stop fetch loader
        yield put({ type: FETCH_OBJECTIVES_AND_KEY_RESULTS_HAS_STOPPED_LOADING })
        //change this stupid thing
        yield put({ type: REQUEST_SUCCESS })
        console.log(error)
        console.log(error.name)
        console.log(error.message)
        if(error.response){
            console.log("watch me", error.response.status)
        }
    }
}

function* fetchObjectivesandKeyResultsWatcher() {
    yield takeLatest(FETCH_OBJECTIVES_AND_KEY_RESULTS, fetchObjectivesandKeyResults)
}


function* saveKeyResults({ data, staffId }) {
    yield put({ type: IS_REQUESTING_SAVE_RESULTS })
    console.log("see the data you're posting oo", data)
    try {
        //start the button loader
        yield put({ type: SAVE_KEY_RESULTS_IS_LOADING })

        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        const req = yield call(createRequest, `${PMS_API_URL}/api/AppraisalObjectiveKeyResults/${data.id}`, { method: 'PUT', body: JSON.stringify(data) }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        console.log("saveKeyResults response", resJson)
        if(resJson === true) {
            //update the state to match info in DB
            yield put({ type: KEY_RESULTS_SAVE_SUCCESSFUL, data })
            //only show save successful message if it returns true
            yield put({ type: SAVE_SUCCESSFUL })
        } else {
            yield put({ type: SAVE_FAILED })
        }
        //stop the button loader
        yield put({ type: SAVE_KEY_RESULTS_HAS_STOPPED_LOADING })

        // yield put({ type: SAVE_SUCCESSFUL })
        yield put({ type: IS_DONE_REQUESTING_SAVE_RESULTS })
        yield call(delay, 4000)
        yield put({ type: SAVE_DONE })

    } catch (error) {
        //stop the button loader
        yield put({ type: SAVE_KEY_RESULTS_HAS_STOPPED_LOADING })

        yield put({ type: SAVE_FAILED })
        yield put({ type: IS_DONE_REQUESTING_SAVE_RESULTS })
        yield call(delay, 4000)
        yield put({ type: SAVE_DONE })
        console.log(error)
    }
}

function* saveKeyResultsWatcher() {
    yield takeLatest(SAVE_KEY_RESULTS, saveKeyResults)
}


function* createOKRs({ data }) {
    yield put({ type: IS_REQUESTING_SAVE_RESULTS })
    console.log("see the data you're posting oo", data)
    try {
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        const putOrPost = data.id ? 'PUT' : 'POST'
        const requestURL = data.id ? `${PMS_API_URL}/api/AppraisalObjectiveKeyResults/${data.id}` : `${PMS_API_URL}/api/AppraisalObjectiveKeyResults`
        console.log(putOrPost)
        console.log(requestURL)
        // console.log("data", data)
        const req = yield call(createRequest, requestURL, { method: putOrPost, body: JSON.stringify(data) }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        console.log("response after post", resJson)
        if(resJson===true || (Object.prototype.toString.call(resJson) === '[object String]') && resJson.includes("-")){
            yield put({ type: SAVE_SUCCESSFUL })
            yield put({ type: SAVE_SUCCESSFUL_MESSAGE_VERSION })
            yield put({ type: IS_DONE_REQUESTING_SAVE_RESULTS })
            yield put({ type: REQUEST_ERROR }) // this is to start up the form loader on the setOKRs page
            yield put({ type: FETCH_OBJECTIVES_AND_KEY_RESULTS })
            yield put({ type: FETCH_APPRAISAL_OBJECTIVES })
            yield call(delay, 6000)
            yield put({ type: SAVE_DONE })
        } else {
            yield put({ type: REQUEST_SUCCESS })
            yield put({ type: SAVE_FAILED })
            yield put({ type: SAVE_FAILED_MESSAGE_VERSION })
            yield put({ type: IS_DONE_REQUESTING_SAVE_RESULTS })
            yield call(delay, 7000)
            yield put({ type: SAVE_DONE })
        }

    } catch (error) {
        yield put({ type: REQUEST_SUCCESS })
        yield put({ type: SAVE_FAILED })
        yield put({ type: SAVE_FAILED_MESSAGE_VERSION })
        yield put({ type: IS_DONE_REQUESTING_SAVE_RESULTS })
        yield call(delay, 7000)
        yield put({ type: SAVE_DONE })
        console.log(error)
    }
}

function* createOKRsWatcher() {
    yield takeLatest(CREATE_OKRs, createOKRs)
}


function* fetchSbuObjectives({sbuNameFromStaffDetailsCall}) {
    try {
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        const sbuName = yield select((state) => sbuNameSelector(state))
        const useThis = sbuName || sbuNameFromStaffDetailsCall
        // console.log(token)
        const req = yield call(createRequest, `${PMS_API_URL}/api/SBUObjective/SBUName/${useThis}`, { method: 'GET' }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        yield put({ type: GET_SBU_OBJECTIVES, payload: resJson })
        yield put({ type: SBU_REQUEST_SUCCESS })

    } catch (error) {
        // yield put({type:REQUEST_ERROR})
        console.log(error)
    }
}

function* fetchSbuObjectivesWatcher() {
    yield takeLatest(FETCH_SBU_OBJECTIVES, fetchSbuObjectives)
}


function* fetchCategory() {
    try {
        yield put({ type: FETCH_CATEGORY_IS_LOADING })
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        // console.log(token)
        const req = yield call(createRequest, `${PMS_API_URL}/api/Category`, { method: 'GET' }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        console.log("categoryResponse", resJson)
        yield put({ type: GET_CATEGORY, payload: resJson })
        yield put({ type: FETCH_CATEGORY_HAS_STOPPED_LOADING })

    } catch (error) {
        yield put({ type: FETCH_CATEGORY_HAS_STOPPED_LOADING })
        // yield put({type:REQUEST_ERROR})
        console.log(error)
    }
}

function* fetchCategoryWatcher() {
    yield takeLatest(FETCH_CATEGORY, fetchCategory)
}


function* fetchAppraisalCycle() {
    yield put({ type: FETCH_APPRAISAL_CYCLE_IS_LOADING })
    try {
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        // console.log(token)
        const req = yield call(createRequest, `${PMS_API_URL}/api/AppraisalCycle/Current`, { method: 'GET' }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        // console.log("Appraisal Cycle object:", resJson)
        yield put({ type: GET_APPRAISAL_CYCLE, payload: resJson })
        yield put({ type: FETCH_APPRAISAL_CYCLE_HAS_STOPPED_LOADING })

    } catch (error) {
        // yield put({type:REQUEST_ERROR})
        yield put({ type: FETCH_APPRAISAL_CYCLE_HAS_STOPPED_LOADING })
        console.log(error)
    }
}

function* fetchAppraisalCycleWatcher() {
    yield takeLatest(FETCH_APPRAISAL_CYCLE, fetchAppraisalCycle)
}


// function* fetchStaffAppraisal() {
//     try {
//         console.log(9999)
//         const token = yield select((state) => {
//             return state.user ? state.user.access_token : ''
//         })
//         // console.log(token)
//         const req = yield call(createRequest, `${PMS_API_URL}/api/StaffAppraisal/Current`, { method: 'GET' }, token)
//         const res = yield call(fetch, req)
//         const resJson = yield call(res.json.bind(res))
//         //iF the staffAppraisal is not an empty object then put it in the store else if it is an empty obhject create a new
//         //one by posting to the endpoint
//         console.log("staff appraisal:", resJson);
//         if (resJson.id) {
//             // yield put({type: GET_STAFF_APPRAISAL_ID, payload: resJson.id})
//             console.log("staff appraisal:", resJson);
//             yield put({ type: GET_STAFF_APPRAISAL, payload: resJson })
//             yield put({ type: STAFF_APPRAISAL_EXISTS })
//         } else if (resJson.data === null) {
//             const staffId = yield select((state) => staffIdSelector(state))
//             const appraisalId = yield select((state) => appraisalCycleIdSelector(state))
//             const payload = { staffId, appraisalId }
//             console.log("now creating a new staff appraisal")
//             yield put({ type: CREATE_STAFF_APPRAISAL, payload })
//         } else {
//             console.log("Edgecase detected: please investigate further")
//         }

//     } catch (error) {
//         // yield put({type:REQUEST_ERROR})
//         console.log(error)
//     }
// }

function* fetchStaffAppraisal() {
    try {
        yield put({ type: FETCH_STAFF_APPRAISAL_IS_LOADING })

        console.log(9999)
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        // console.log(token)
        const req = yield call(createRequest, `${PMS_API_URL}/api/StaffAppraisal/Current`, { method: 'GET' }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        //iF the staffAppraisal is not an empty object then put it in the store else if it is an empty obhject create a new
        //one by posting to the endpoint
        console.log("staff appraisal:", resJson);
        // yield put({type: GET_STAFF_APPRAISAL_ID, payload: resJson.id})
        // console.log("staff appraisal:", resJson);
        yield put({ type: GET_STAFF_APPRAISAL, payload: resJson })
        yield put({ type: STAFF_APPRAISAL_EXISTS })
        yield put({ type: FETCH_STAFF_APPRAISAL_HAS_STOPPED_LOADING })
        
    } catch (error) {
        // yield put({type:REQUEST_ERROR})
        console.log(error)
        yield put({ type: FETCH_STAFF_APPRAISAL_HAS_STOPPED_LOADING })
    }
}

function* fetchStaffAppraisalWatcher() {
    yield takeLatest(FETCH_STAFF_APPRAISAL, fetchStaffAppraisal)
}


function* createStaffAppraisal({ payload }) {
    // yield put({type:IS_REQUESTING_SAVE_RESULTS})
    console.log("see the data you're posting oo", payload)
    try {
        yield put({type:SAVE_STAFF_APPRAISAL_IS_LOADING})
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        const req = yield call(createRequest, `${PMS_API_URL}/api/StaffAppraisal`, { method: 'POST', body: JSON.stringify(payload) }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        console.log("createStaffAppraisal response", resJson)
        // yield put({type:GET_STAFF_APPRAISAL_ID, payload: resJson})
        yield put({ type: FETCH_STAFF_APPRAISAL })
        // yield put({ type: SAVE_STAFF_APPRAISAL_HAS_STOPPED_LOADING })

        // yield put({type: SAVE_SUCCESSFUL})
        // yield put({type: IS_DONE_REQUESTING_SAVE_RESULTS})
        // yield call(delay, 4000)
        // yield put({type: SAVE_DONE})

    } catch (error) {
        // yield put({type: SAVE_FAILED})
        // yield put({type: IS_DONE_REQUESTING_SAVE_RESULTS})
        // yield call(delay, 4000)
        // yield put({type: SAVE_DONE})
        yield put({ type: SAVE_STAFF_APPRAISAL_HAS_STOPPED_LOADING })
        console.log(error)
        yield put({ type: CREATE_STAFF_APPRAISAL_ERROR, error })
    }
}

function* createStaffAppraisalWatcher() {
    yield takeLatest(CREATE_STAFF_APPRAISAL, createStaffAppraisal)
}


function* fetchStaffDetails({ userEmail }) {
    try {
        yield put({type: FETCH_STAFF_DETAILS_IS_LOADING})

        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        console.log(userEmail)
        console.log("AAARHHGGHHFHHGHDHDHDH")
        const req = yield call(createRequest, `${PMS_API_URL}/api/Staff/Email/${userEmail}`, { method: 'GET' }, token)
        const res = yield call(fetch, req)
        console.log('response', req)

        yield put({type: FETCH_STAFF_DETAILS_RESPONSE_STATUS, statusCode: res.status})

        const resJson = yield call(res.json.bind(res))
        console.log("Staff Details:", resJson)
        if(resJson.businessUnit){
            yield put({ type: FETCH_SBU_OBJECTIVES, sbuNameFromStaffDetailsCall: resJson.businessUnit.name })
        }
        yield put({ type: GET_STAFF_DETAILS, payload: resJson })
        // yield call(delay, 2000)
        yield put({type: FETCH_STAFF_DETAILS_HAS_STOPPED_LOADING})

    } catch (error) {
        // yield put({type:REQUEST_ERROR})
        yield put({type: FETCH_STAFF_DETAILS_HAS_STOPPED_LOADING})
        console.log(error)
    }
}

function* fetchStaffDetailsWatcher() {
    yield takeLatest(FETCH_STAFF_DETAILS, fetchStaffDetails)
}


function* fetchStaffRole({ userEmail }) {
    try {
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        console.log(userEmail)
        const req = yield call(createRequest, `${PMS_API_URL}/api/User/UserType/${userEmail}`, { method: 'GET' }, token)
        const res = yield call(fetch, req)
        console.log('response', req)
        const resJson = yield call(res.json.bind(res))
        console.log("Staff Role:", resJson)
        yield put({ type: GET_STAFF_ROLE, payload: resJson })

    } catch (error) {
        // yield put({type:REQUEST_ERROR})
        console.log(error)
    }
}

function* fetchStaffRoleWatcher() {
    yield takeLatest(FETCH_STAFF_ROLE, fetchStaffRole)
}


function* fetchLMPOOkrs({ staffId }) {

    yield put({ type: FETCHING_LMPO_OKRS })

    try {
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        console.log("Gte out", staffId)
        const req = yield call(createRequest, `${PMS_API_URL}/api/AppraisalObjectiveKeyResults/LM_OP/StaffId${staffId}`, { method: 'GET' }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        console.log("look at the resJson for line mgr fetch: " + staffId, resJson)
        const normalizedData = yield call(normalize, resJson, objectivesSchema)
        yield put({ type: GET_LMPO_OKRS, entities: normalizedData.entities, ids: normalizedData.result })
        yield put({ type: DONE_FETCHING_LMPO_OKRS })
    } catch (error) {
        yield put({ type: DONE_FETCHING_LMPO_OKRS })
        // yield put({type:REQUEST_ERROR})
        console.log(error)
    }
}

function* fetchLMPOOkrsWatcher() {
    yield takeLatest(FETCH_LMPO_OKRS, fetchLMPOOkrs)
}



function* fetchLineManagerStaffAppraisals() {

    yield put({ type: FETCHING_STAFF_APPRAISALS })

    try {
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        console.log("toks", token)
        const req = yield call(createRequest, `${PMS_API_URL}/api/StaffAppraisal/LineManager/ManageOKRs`, { method: 'GET' }, token)
        const res = yield call(fetch, req)
        console.log('response', req)
        const resJson = yield call(res.json.bind(res))
        console.log("Line Manager Staff Details:", resJson)
        yield put({ type: GET_LINE_MANAGER_STAFF_APPRAISALS, payload: resJson })
        yield put({ type: DONE_FETCHING_STAFF_APPRAISALS })

    } catch (error) {
        yield put({ type: DONE_FETCHING_STAFF_APPRAISALS })
        // yield put({type:REQUEST_ERROR})
        console.log(error)
    }
}

function* fetchLineManagerStaffAppraisalsWatcher() {
    yield takeLatest(FETCH_LINE_MANAGER_STAFF_APPRAISALS, fetchLineManagerStaffAppraisals)
}


function* fetchSuperLineManagerStaffAppraisals() {
    yield put({ type: FETCHING_STAFF_APPRAISALS })

    try {
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        console.log("toks", token)
        const req = yield call(createRequest, `${PMS_API_URL}/api/StaffAppraisal/SuperLineManager/ManageOKRs`, { method: 'GET' }, token)
        const res = yield call(fetch, req)
        console.log('response', req)
        const resJson = yield call(res.json.bind(res))
        console.log("Line Manager Staff Details:", resJson)
        yield put({ type: GET_SUPER_LINE_MANAGER_STAFF_APPRAISALS, payload: resJson })
        yield put({ type: DONE_FETCHING_STAFF_APPRAISALS })

    } catch (error) {
        yield put({ type: DONE_FETCHING_STAFF_APPRAISALS })
        // yield put({type:REQUEST_ERROR})
        console.log(error)
    }
}

function* fetchSuperLineManagerStaffAppraisalsWatcher() {
    yield takeLatest(FETCH_SUPER_LINE_MANAGER_STAFF_APPRAISALS, fetchSuperLineManagerStaffAppraisals)
}


function* fetchPeopleOpsStaffAppraisals() {

    yield put({ type: FETCHING_STAFF_APPRAISALS })

    try {
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        console.log("toks", token)
        const req = yield call(createRequest, `${PMS_API_URL}/api/StaffAppraisal/PeopleOps/ManageOKRs`, { method: 'GET' }, token)
        const res = yield call(fetch, req)
        console.log('response', req)
        const resJson = yield call(res.json.bind(res))
        console.log("People Ops Staff appraisal details:", resJson)
        yield put({ type: GET_PEOPLE_OPS_STAFF_APPRAISALS, payload: resJson })
        yield call(delay, 2000)
        yield put({ type: DONE_FETCHING_STAFF_APPRAISALS })

    } catch (error) {
        yield put({ type: DONE_FETCHING_STAFF_APPRAISALS })
        // yield put({type:REQUEST_ERROR})
        console.log(error)
    }
}

function* fetchPeopleOpsStaffAppraisalsWatcher() {
    yield takeLatest(FETCH_PEOPLE_OPS_STAFF_APPRAISALS, fetchPeopleOpsStaffAppraisals)
}


function* submitOKRs({fromAssessmentFlow}) {
    yield put({ type: SUBMITTING_OKRS })

    try {
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        const staffId = yield select((state) => staffIdSelector(state))
        const staffAppraisalId = yield select((state) => staffAppraisalIdSelector(state))

        const data = fromAssessmentFlow ? { staffAssessed: true, staffId } : { staffSubmitted: true, staffId }
        console.log("see the data you're submitting oo", data, "and you're posting to this id: ", staffAppraisalId)
        const req = yield call(createRequest, `${PMS_API_URL}/api/StaffAppraisal/${staffAppraisalId}/Submitted`, { method: 'PUT', body: JSON.stringify(data) }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        yield put({ type: DONE_SUBMITTING_OKRS })
        console.log("submit successful:", resJson)
        if(resJson){
            // console.log("message: error")
            yield put({type: SUBMIT_OKRS_SUCCESSFUL})
            yield call(delay, 4000)
            yield put({type: SUBMIT_OKRS_DONE})
            // yield put({ type: MODIFY_APPRAISAL_CYCLE_AFTER_UPDATE, data})
        }
        // yield put({type:GET_STAFF_APPRAISAL_ID, payload: resJson})
        ///
        yield put({ type: FETCH_STAFF_APPRAISAL }) // to allow the summary screen change to displaying the correct info
        yield put({ type: FETCH_OBJECTIVES_AND_KEY_RESULTS })
        ///
        // yield put({type: SAVE_SUCCESSFUL})
        // yield put({type: IS_DONE_REQUESTING_SAVE_RESULTS})
        // yield call(delay, 4000)
        // yield put({type: SAVE_DONE})

    } catch (error) {
        // console.log("message: error")
        yield put({ type: DONE_SUBMITTING_OKRS })
        // yield put({type: SAVE_FAILED})
        // yield put({type: IS_DONE_REQUESTING_SAVE_RESULTS})
        // yield call(delay, 4000)
        // yield put({type: SAVE_DONE})
        yield put({type: SUBMIT_OKRS_FAILED})
        yield call(delay, 4000)
        yield put({type: SUBMIT_OKRS_DONE})
        console.log(error)
    }
}

function* submitOKRsWatcher() {
    yield takeLatest(SUBMIT_OKRS, submitOKRs)
}


function* updateKeyResults({ KRs, OBJs, id, okrDeleteFlow, krDeleteFlow }) {
    // yield put({type:IS_REQUESTING_SAVE_RESULTS})
    try {
        yield put({ type: UPDATE_KEY_RESULTS_IS_LOADING })

        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        // const staffId = yield select((state) => staffIdSelector(state))
        // const staffAppraisalId = yield select((state) => staffAppraisalIdSelector(state))
        // const data = {staffSubmitted: true, staffId}
        // if (KRs.length > 0) {
        const staffSubmitted = okrDeleteFlow || krDeleteFlow ? {staffSubmitted: false} : {staffSubmitted: true}
        const body = { objectives: OBJs, keyResults: KRs, staffSubmitted: staffSubmitted.staffSubmitted }
        console.log("body you're posting", body)
        console.log("see the data you're opdating oo", body, id, staffSubmitted)
        const req = yield call(createRequest, `${PMS_API_URL}/api/StaffAppraisal/UpdateObjectiveKeyResults/${id}`, { method: 'PUT', body: JSON.stringify(body) }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        console.log("update successful:", resJson)

        //if the response message implies that the operation was succesful
        if(Object.prototype.toString.call(resJson) === '[object Object]' && resJson.data===true){
            //do something
            yield put({type: UPDATE_KEY_RESULTS_SUCCESSFUL})
            yield call(delay, 4000)
            yield put({type: UPDATE_KEY_RESULTS_DONE})
            if(okrDeleteFlow) {
                yield put({ type: REQUEST_ERROR })
                yield put({type: FETCH_OBJECTIVES_AND_KEY_RESULTS})
                if (okrDeleteFlow.history) {
                    okrDeleteFlow.history.push('/businessobjectives');
                }
                yield put({type:DELETE_APPRAISAL_OBJECTIVE_HAS_STOPPED_LOADING})
                toast.success('Successfully deleted appraisal objective')
            } else if (krDeleteFlow) {
                yield put({ type: REQUEST_ERROR })
                yield put({type: FETCH_OBJECTIVES_AND_KEY_RESULTS})
                yield put({type:DELETE_KEY_RESULT_HAS_STOPPED_LOADING})
                toast.success('Successfully deleted key result')
            } else {
                yield put({type: MODIFY_STAFF_APPRAISALS_ON_KEY_RESULTS_UPDATE, staffSubmitted})
            }
        } else {
            yield put({type: UPDATE_KEY_RESULTS_FAILED})
            if(okrDeleteFlow) {
                toast.error('Failed to delete appraisal objective')
            } else if(krDeleteFlow) {
                toast.error('Failed to delete key result')
            }
            yield call(delay, 4000)
            yield put({type: UPDATE_KEY_RESULTS_DONE})
        }

        yield put({type:UPDATE_KEY_RESULTS_HAS_STOPPED_LOADING})

        // }
        // yield put({type:GET_STAFF_APPRAISAL_ID, payload: resJson})
        ///

        // if (OBJs.length < 1) {
            // yield put({ type: SUBMIT_OKRS }) // to change staffSubmitted to true
        // } else if (OBJs.length > 0) {
        //     yield put({ type: UPDATE_OBJECTIVES, OBJs })
        // }


    } catch (error) {

        yield put({type:UPDATE_KEY_RESULTS_HAS_STOPPED_LOADING})
        console.log(error)
        yield put({type: UPDATE_KEY_RESULTS_FAILED})
        if(okrDeleteFlow) {
            yield put({type:DELETE_APPRAISAL_OBJECTIVE_HAS_STOPPED_LOADING})
            toast.error('Failed to delete appraisal objective')
        } else if(krDeleteFlow) {
            yield put({type:DELETE_KEY_RESULT_HAS_STOPPED_LOADING})
            toast.error('Failed to delete key result')
        }
        yield call(delay, 4000)
        yield put({type: UPDATE_KEY_RESULTS_DONE})
    }
}

function* updateKeyResultsWatcher() {
    yield takeLatest(UPDATE_KEY_RESULTS, updateKeyResults)
}



function* deleteAppraisalObjective({ id, forUpdateKeyResults }) {
    // yield put({type:IS_REQUESTING_SAVE_RESULTS})
    try {
        yield put({ type: DELETE_APPRAISAL_OBJECTIVE_IS_LOADING })

        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        
        // const staffSubmitted = {staffSubmitted: true}
        // const body = { objectives: OBJs, keyResults: KRs }
        // console.log("body you're posting", body)
        // console.log("see the data you're opdating oo", body, id, staffSubmitted)
        const req = yield call(createRequest, `${PMS_API_URL}/api/AppraisalObjectiveKeyResults/${id}`, { method: 'DELETE' }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        console.log("delete successful:", resJson)

        //if the response message implies that the operation was succesful
        if(resJson===true){
        //     //do something
            yield put({ type: UPDATE_KEY_RESULTS, ...forUpdateKeyResults })
        //     if(okrDeleteFlow) {
        //         yield put({ type: REQUEST_ERROR })
        //         yield put({type: FETCH_OBJECTIVES_AND_KEY_RESULTS})
        //         okrDeleteFlow.history.push('/businessobjectives');
        //     } else {
        //         yield put({type: MODIFY_STAFF_APPRAISALS_ON_KEY_RESULTS_UPDATE, staffSubmitted})
        //     }
        } else {
            yield put({type:DELETE_APPRAISAL_OBJECTIVE_HAS_STOPPED_LOADING})
            toast.error('Failed to delete appraisal objective')
        //     yield call(delay, 4000)
        //     yield put({type: UPDATE_KEY_RESULTS_DONE})
        }

        // yield put({type:UPDATE_KEY_RESULTS_HAS_STOPPED_LOADING})


    } catch (error) {

        yield put({type:DELETE_APPRAISAL_OBJECTIVE_HAS_STOPPED_LOADING})
        // console.log(error)
        // yield put({type: UPDATE_KEY_RESULTS_FAILED})
        // yield call(delay, 4000)
        // yield put({type: UPDATE_KEY_RESULTS_DONE})
    }
}

function* deleteAppraisalObjectiveWatcher() {
    yield takeLatest(DELETE_APPRAISAL_OBJECTIVE , deleteAppraisalObjective)
}



function* deleteKeyResult({ id, forUpdateKeyResults }) {
    // yield put({type:IS_REQUESTING_SAVE_RESULTS})
    try {
        yield put({ type: DELETE_KEY_RESULT_IS_LOADING })

        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        
        // const staffSubmitted = {staffSubmitted: true}
        // const body = { objectives: OBJs, keyResults: KRs }
        // console.log("body you're posting", body)
        // console.log("see the data you're opdating oo", body, id, staffSubmitted)
        const req = yield call(createRequest, `${PMS_API_URL}/api/KeyResult/${id}`, { method: 'DELETE' }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        console.log("delete successful:", resJson)

        //if the response message implies that the operation was succesful
        if(resJson===true){
        //     //do something
            yield put({ type: UPDATE_KEY_RESULTS, ...forUpdateKeyResults })
        //     if(okrDeleteFlow) {
        //         yield put({ type: REQUEST_ERROR })
        //         yield put({type: FETCH_OBJECTIVES_AND_KEY_RESULTS})
        //         okrDeleteFlow.history.push('/businessobjectives');
        //     } else {
        //         yield put({type: MODIFY_STAFF_APPRAISALS_ON_KEY_RESULTS_UPDATE, staffSubmitted})
        //     }
        } else {
            yield put({type:DELETE_KEY_RESULT_HAS_STOPPED_LOADING})
            toast.error('Failed to delete key result')
        //     yield call(delay, 4000)
        //     yield put({type: UPDATE_KEY_RESULTS_DONE})
        }

        // yield put({type:UPDATE_KEY_RESULTS_HAS_STOPPED_LOADING})


    } catch (error) {

        yield put({type:DELETE_APPRAISAL_OBJECTIVE_HAS_STOPPED_LOADING})
        // console.log(error)
        // yield put({type: UPDATE_KEY_RESULTS_FAILED})
        // yield call(delay, 4000)
        // yield put({type: UPDATE_KEY_RESULTS_DONE})
    }
}

function* deleteKeyResultWatcher() {
    yield takeLatest(DELETE_KEY_RESULT , deleteKeyResult)
}



function* addKeyResult({ data }) {
    yield put({type:ADD_KEY_RESULT_IS_LOADING})
    try {
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        const req = yield call(createRequest, `${PMS_API_URL}/api/KeyResult`, { method: 'POST', body: JSON.stringify(data) }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))

        
        if(resJson.includes('-')){ //if the response is an id
            toast.success('Successfully added key result');
            yield call(delay, 2000)
            yield put({ type: REQUEST_ERROR })
            yield put({type: FETCH_OBJECTIVES_AND_KEY_RESULTS})
        } else {
            toast.error('Failed to add key result');
        }
        
        yield put({ type: ADD_KEY_RESULT_HAS_STOPPED_LOADING })
        // yield put({ type: FETCH_STAFF_APPRAISAL })

        // yield put({type: SAVE_SUCCESSFUL})
        // yield put({type: IS_DONE_REQUESTING_SAVE_RESULTS})
        // yield call(delay, 4000)
        // yield put({type: SAVE_DONE})

    } catch (error) {
        // yield put({type: SAVE_FAILED})
        // yield put({type: IS_DONE_REQUESTING_SAVE_RESULTS})
        // yield call(delay, 4000)
        // yield put({type: SAVE_DONE})
        toast.error('Failed to add key result');
        yield put({ type: ADD_KEY_RESULT_HAS_STOPPED_LOADING })
    }
}

function* addKeyResultWatcher() {
    yield takeLatest(ADD_KEY_RESULT, addKeyResult)
}




function* lineManagerUpdateKeyResults({ KRs, OBJs, id, forSubmitLineManagerOkrs }) {
    // yield put({type:IS_REQUESTING_SAVE_RESULTS})
    try {
        yield put({ type: SUBMIT_LINE_MANAGER_OKRS_IS_LOADING })

        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        // const staffId = yield select((state) => staffIdSelector(state))
        // const staffAppraisalId = yield select((state) => staffAppraisalIdSelector(state))
        // const data = {staffSubmitted: true, staffId}
        // if (KRs.length > 0) {
        // const staffSubmitted = {staffSubmitted: true}
        const body = { objectives: OBJs, keyResults: KRs }
        console.log("body you're posting", body)
        console.log("see the data you're opdating oo", body, id)
        const req = yield call(createRequest, `${PMS_API_URL}/api/StaffAppraisal/UpdateObjectiveResults/${id}`, { method: 'PUT', body: JSON.stringify(body) }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        console.log("update successful:", resJson)

        //if the response message implies that the operation was succesful
        if(Object.prototype.toString.call(resJson) === '[object Object]' && resJson.data===true){
            //do somethingforSubmitLineManagerOkrs
            yield put({
                type: SUBMIT_LINE_MANAGER_OKRS, 
                data: forSubmitLineManagerOkrs.data, 
                history: forSubmitLineManagerOkrs.history, 
                lineManagerStatus: forSubmitLineManagerOkrs.lineManagerStatus,
                lineManagerTwoApproved: forSubmitLineManagerOkrs.lineManagerTwoApproved,
                superLineManagerStatus: forSubmitLineManagerOkrs.superLineManagerStatus,
                staffSubmitted: forSubmitLineManagerOkrs.staffSubmitted
            })
            // yield put({type: UPDATE_KEY_RESULTS_SUCCESSFUL})
            // yield call(delay, 4000)
            // yield put({type: UPDATE_KEY_RESULTS_DONE})
            // yield put({type: MODIFY_STAFF_APPRAISALS_ON_KEY_RESULTS_UPDATE, staffSubmitted})
        } else {
            yield put({type: LINE_MANAGER_UPDATE_KEY_RESULTS_FAILED})
            yield call(delay, 4000)
            yield put({ type: SUBMIT_LINE_MANAGER_OKRS_HAS_STOPPED_LOADING })
            yield put({type: LINE_MANAGER_UPDATE_KEY_RESULTS_DONE})
        }

        // yield put({type:UPDATE_KEY_RESULTS_HAS_STOPPED_LOADING})


    } catch (error) {

        // yield put({type:UPDATE_KEY_RESULTS_HAS_STOPPED_LOADING})
        yield put({ type: SUBMIT_LINE_MANAGER_OKRS_HAS_STOPPED_LOADING })
        console.log(error)
        yield put({type: LINE_MANAGER_UPDATE_KEY_RESULTS_FAILED})
        yield call(delay, 4000)
        yield put({type: LINE_MANAGER_UPDATE_KEY_RESULTS_DONE})
    }
}

function* lineManagerUpdateKeyResultsWatcher() {
    yield takeLatest(LINE_MANAGER_UPDATE_KEY_RESULTS, lineManagerUpdateKeyResults)
}



function* peopleOpsUpdateKeyResults({ KRs, OBJs, id, forSubmitPeopleOpsOkrs }) {
    // yield put({type:IS_REQUESTING_SAVE_RESULTS})
    yield put({ type: SUBMIT_PEOPLE_OPS_OKRS_IS_LOADING })
    try {

        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        // const staffId = yield select((state) => staffIdSelector(state))
        // const staffAppraisalId = yield select((state) => staffAppraisalIdSelector(state))
        // const data = {staffSubmitted: true, staffId}
        // if (KRs.length > 0) {
        // const staffSubmitted = {staffSubmitted: true}
        const body = { objectives: OBJs, keyResults: KRs }
        console.log("body you're posting", body)
        console.log("see the data you're opdating oo", body, id)
        const req = yield call(createRequest, `${PMS_API_URL}/api/StaffAppraisal/UpdateObjectiveResults/${id}`, { method: 'PUT', body: JSON.stringify(body) }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        console.log("update successful:", resJson)

        //if the response message implies that the operation was succesful
        if(Object.prototype.toString.call(resJson) === '[object Object]' && resJson.data===true){
            //do somethingforSubmitLineManagerOkrs
            yield put({type: SUBMIT_PEOPLE_OPS_OKRS, data: forSubmitPeopleOpsOkrs.data, history: forSubmitPeopleOpsOkrs.history, peopleOpsStatus: forSubmitPeopleOpsOkrs.peopleOpsStatus})
            // yield put({type: UPDATE_KEY_RESULTS_SUCCESSFUL})
            // yield call(delay, 4000)
            // yield put({type: UPDATE_KEY_RESULTS_DONE})
            // yield put({type: MODIFY_STAFF_APPRAISALS_ON_KEY_RESULTS_UPDATE, staffSubmitted})
        } else {
            yield put({type: PEOPLE_OPS_UPDATE_KEY_RESULTS_FAILED})
            yield call(delay, 4000)
            yield put({type: PEOPLE_OPS_UPDATE_KEY_RESULTS_DONE})
        }

        // yield put({type:UPDATE_KEY_RESULTS_HAS_STOPPED_LOADING})


    } catch (error) {

        // yield put({type:UPDATE_KEY_RESULTS_HAS_STOPPED_LOADING})
        yield put({ type: SUBMIT_PEOPLE_OPS_OKRS_HAS_STOPPED_LOADING })
        console.log(error)
        yield put({type: PEOPLE_OPS_UPDATE_KEY_RESULTS_FAILED})
        yield call(delay, 4000)
        yield put({type: PEOPLE_OPS_UPDATE_KEY_RESULTS_DONE})
    }
}

function* peopleOpsUpdateKeyResultsWatcher() {
    yield takeLatest(PEOPLE_OPS_UPDATE_KEY_RESULTS, peopleOpsUpdateKeyResults)
}

function* updateObjectives({ OBJs }) {
    // yield put({type:IS_REQUESTING_SAVE_RESULTS})
    try {
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        // const staffId = yield select((state) => staffIdSelector(state))
        // const staffAppraisalId = yield select((state) => staffAppraisalIdSelector(state))
        // const data = {staffSubmitted: true, staffId}
        console.log("see the data you're updating oo", OBJs)
        const req = yield call(createRequest, `${PMS_API_URL}/api/AppraisalObjective/UpdateStatus`, { method: 'PUT', body: JSON.stringify(OBJs) }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        console.log("update successful:", resJson)

        yield put({ type: SUBMIT_OKRS }) // to change staffSubmitted to true

        // yield put({type: SAVE_SUCCESSFUL})
        // yield put({type: IS_DONE_REQUESTING_SAVE_RESULTS})
        // yield call(delay, 4000)
        // yield put({type: SAVE_DONE})

    } catch (error) {
        // yield put({type: SAVE_FAILED})
        // yield put({type: IS_DONE_REQUESTING_SAVE_RESULTS})
        // yield call(delay, 4000)
        // yield put({type: SAVE_DONE})
        console.log(error)
    }
}

function* updateObjectivesWatcher() {
    yield takeLatest(UPDATE_OBJECTIVES, updateObjectives)
}


function* submitLineManagerOKRs({ data, history, lineManagerStatus, pendingKRs, lineManagerTwoApproved, superLineManagerStatus, staffSubmitted }) {
    yield put({ type: SUBMIT_LINE_MANAGER_OKRS_IS_LOADING })
    try {
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        // const staffId = yield select((state) => staffIdSelector(state))
        console.log("superHere", lineManagerTwoApproved)
        const body = { firstApproved: data.firstApproved, staffId: data.staffId, lineManagerTwoApproved, superLineManagerStatus, lineManagerStatus, staffSubmitted }
        console.log("see the data you're posting oo", body, "and you're posting to this id: ", data.staffAppraisalId)
        const req = yield call(createRequest, `${PMS_API_URL}/api/StaffAppraisal/${data.staffAppraisalId}/LineManager/Submitted`, { method: 'PUT', body: JSON.stringify(body) }, token)
        const res = yield call(fetch, req)
        console.log("pass?")
        const resJson = yield call(res.json.bind(res))
        console.log("submit successful:", resJson)
        if (resJson===true) {
            yield put({ type: MODIFY_LINE_MANAGER_STAFF_APPRAISALS_ON_OKR_SUBMIT, lineManagerStatus, superLineManagerStatus, data })
            history.replace("/ManageOKRs")
        } else {
            yield put({type: LINE_MANAGER_UPDATE_KEY_RESULTS_FAILED})
            yield call(delay, 4000)
            yield put({type: LINE_MANAGER_UPDATE_KEY_RESULTS_DONE})
        }

        yield put({ type: SUBMIT_LINE_MANAGER_OKRS_HAS_STOPPED_LOADING })
        // if(data.firstApproved === "approved"){
        //     yield put({ type: UPDATE_KEY_RESULTS, KRs: pendingKRs, history})
        // }
        // history.replace("/ManageOKRs")
        // yield put({type:GET_STAFF_APPRAISAL_ID, payload: resJson})
        ///
        // yield put({ type: FETCH_STAFF_APPRAISAL }) // to allow the summary screen change to displaying the correct info
        ///
        // yield put({type: SAVE_SUCCESSFUL})
        // yield put({type: IS_DONE_REQUESTING_SAVE_RESULTS})
        // yield call(delay, 4000)
        // yield put({type: SAVE_DONE})

    } catch (error) {
        // yield put({type: SAVE_FAILED})
        console.log(error)
        yield put({ type: SUBMIT_LINE_MANAGER_OKRS_HAS_STOPPED_LOADING })
        
        yield put({type: LINE_MANAGER_UPDATE_KEY_RESULTS_FAILED})
        yield call(delay, 4000)
        yield put({type: LINE_MANAGER_UPDATE_KEY_RESULTS_DONE})
    }
}

function* submitLineManagerOKRsWatcher() {
    yield takeLatest(SUBMIT_LINE_MANAGER_OKRS, submitLineManagerOKRs)
}


function* submitSuperLineManagerOKRs({ data, staffAppraisalId, history, superLineManagerStatus }) {
    yield put({ type: SUBMIT_LINE_MANAGER_OKRS_IS_LOADING })
    try {
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        // const staffId = yield select((state) => staffIdSelector(state))
        // const body = {firstApproved: data.firstApproved, staffId: data.staffId}
        console.log("see the data you're posting oo", data, "and you're posting to this id: ", staffAppraisalId)
        const req = yield call(createRequest, `${PMS_API_URL}/api/StaffAppraisal/${staffAppraisalId}/LineManagerTwo/Submitted`, { method: 'PUT', body: JSON.stringify(data) }, token)
        const res = yield call(fetch, req)
        console.log("pass?")
        const resJson = yield call(res.json.bind(res))
        console.log("submit successful:", resJson)
        if (resJson) {
            yield put({ type: MODIFY_SUPER_LINE_MANAGER_STAFF_APPRAISALS_ON_OKR_SUBMIT, superLineManagerStatus, data })
            history.replace("/superManageOKRs")
        }

        yield put({ type: SUBMIT_LINE_MANAGER_OKRS_HAS_STOPPED_LOADING })

        // if(data.firstApproved === "approved"){
        //     yield put({ type: UPDATE_KEY_RESULTS, KRs: pendingKRs, history})
        // }
        // history.replace("/ManageOKRs")
        // yield put({type:GET_STAFF_APPRAISAL_ID, payload: resJson})
        ///
        // yield put({ type: FETCH_STAFF_APPRAISAL }) // to allow the summary screen change to displaying the correct info
        ///
        // yield put({type: SAVE_SUCCESSFUL})
        // yield put({type: IS_DONE_REQUESTING_SAVE_RESULTS})
        // yield call(delay, 4000)
        // yield put({type: SAVE_DONE})

    } catch (error) {
        yield put({ type: SUBMIT_LINE_MANAGER_OKRS_HAS_STOPPED_LOADING })
        // yield put({type: SAVE_FAILED})
        // yield put({type: IS_DONE_REQUESTING_SAVE_RESULTS})
        // yield call(delay, 4000)
        // yield put({type: SAVE_DONE})
        console.log(error)
    }
}

function* submitSuperLineManagerOKRsWatcher() {
    yield takeLatest(SUBMIT_SUPER_LINE_MANAGER_OKRS, submitSuperLineManagerOKRs)
}



function* submitPeopleOpsOKRs({ data, staffAppraisalId, history, peopleOpsStatus }) {
    // yield put({type:IS_REQUESTING_SAVE_RESULTS})
    yield put({ type: SUBMIT_PEOPLE_OPS_OKRS_IS_LOADING })
    try {
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        // const staffId = yield select((state) => staffIdSelector(state))
        const body = { secondApproved: data.secondApproved, staffId: data.staffId }
        console.log("see the data you're posting oo", data, "and you're posting to this id: ", staffAppraisalId)
        const req = yield call(createRequest, `${PMS_API_URL}/api/StaffAppraisal/${staffAppraisalId}/PeopleOps/StaffokrEdit`, { method: 'PUT', body: JSON.stringify(data) }, token)
        const res = yield call(fetch, req)
        console.log("pass?")
        const resJson = yield call(res.json.bind(res))
        console.log("submit successful:", resJson)
        if(resJson){
            // yield put({ type: MODIFY_PEOPLE_OPS_STAFF_APPRAISALS_ON_OKR_SUBMIT, peopleOpsStatus, data })
            // history.replace("/adminmanageokrs")
            toast.success('Status reset successfully')
            yield put({ type: SUBMIT_PEOPLE_OPS_OKRS_HAS_STOPPED_LOADING })
        } else {
            toast.error('Status reset failed')
            yield put({type: PEOPLE_OPS_UPDATE_KEY_RESULTS_FAILED})
            yield call(delay, 4000)
            yield put({type: PEOPLE_OPS_UPDATE_KEY_RESULTS_DONE})
        }

        yield put({ type: SUBMIT_PEOPLE_OPS_OKRS_HAS_STOPPED_LOADING })
        
        // yield put({type:GET_STAFF_APPRAISAL_ID, payload: resJson})
        ///
        // yield put({ type: FETCH_STAFF_APPRAISAL }) // to allow the summary screen change to displaying the correct info
        ///
        // yield put({type: SAVE_SUCCESSFUL})
        // yield put({type: IS_DONE_REQUESTING_SAVE_RESULTS})
        // yield call(delay, 4000)
        // yield put({type: SAVE_DONE})

    } catch (error) {
        toast.error('Status reset failed')
        // yield put({type: SAVE_FAILED})
        // yield put({type: IS_DONE_REQUESTING_SAVE_RESULTS})
        // yield call(delay, 4000)
        // yield put({type: SAVE_DONE})
        yield put({ type: SUBMIT_PEOPLE_OPS_OKRS_HAS_STOPPED_LOADING })
        console.log(error)

        yield put({type: PEOPLE_OPS_UPDATE_KEY_RESULTS_FAILED})
        yield call(delay, 4000)
        yield put({type: PEOPLE_OPS_UPDATE_KEY_RESULTS_DONE})
    }
}

function* submitPeopleOpsOKRsWatcher() {
    yield takeLatest(SUBMIT_PEOPLE_OPS_OKRS, submitPeopleOpsOKRs)
}


function* updateLineManagerStatus({ staffAppraisalId, lineManagerStatus }) {
    // yield put({type:IS_REQUESTING_SAVE_RESULTS})
    try {
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        // const staffId = yield select((state) => staffIdSelector(state))
        // const body = {firstApproved: data.firstApproved, staffId: data.staffId}
        // console.log("see the data you're updating oo", body, "and you're posting to this id: ", body.staffAppraisalId)
        const req = yield call(createRequest, `${PMS_API_URL}/api/StaffAppraisal/${staffAppraisalId}/${lineManagerStatus}`, { method: 'PUT' }, token)
        const res = yield call(fetch, req)
        // console.log("pass?")
        const resJson = yield call(res.json.bind(res))
        console.log("status update successful:", resJson)
        // yield put({type:GET_STAFF_APPRAISAL_ID, payload: resJson})
        ///
        yield put({ type: FETCH_LINE_MANAGER_STAFF_APPRAISALS }) // to allow the summary screen change to displaying the correct info
        ///
        // yield put({type: SAVE_SUCCESSFUL})
        // yield put({type: IS_DONE_REQUESTING_SAVE_RESULTS})
        // yield call(delay, 4000)
        // yield put({type: SAVE_DONE})

    } catch (error) {
        // yield put({type: SAVE_FAILED})
        // yield put({type: IS_DONE_REQUESTING_SAVE_RESULTS})
        // yield call(delay, 4000)
        // yield put({type: SAVE_DONE})
        console.log(error)
    }
}

function* updateLineManagerStatusWatcher() {
    yield takeLatest(UPDATE_LINE_MANAGER_STATUS, updateLineManagerStatus)
}

function* updatePeopleOpsStatus({ staffAppraisalId, peopleOpsStatus }) {
    // yield put({type:IS_REQUESTING_SAVE_RESULTS})
    try {
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        // const staffId = yield select((state) => staffIdSelector(state))
        // const body = {firstApproved: data.firstApproved, staffId: data.staffId}
        // console.log("see the data you're updating oo", body, "and you're posting to this id: ", body.staffAppraisalId)
        const req = yield call(createRequest, `${PMS_API_URL}/api/StaffAppraisal/${staffAppraisalId}/${peopleOpsStatus}`, { method: 'PUT' }, token)
        const res = yield call(fetch, req)
        // console.log("pass?")
        const resJson = yield call(res.json.bind(res))
        console.log("status update successful:", resJson)
        // yield put({type:GET_STAFF_APPRAISAL_ID, payload: resJson})
        ///
        yield put({ type: FETCH_PEOPLE_OPS_STAFF_APPRAISALS }) // to allow the summary screen change to displaying the correct info
        ///
        // yield put({type: SAVE_SUCCESSFUL})
        // yield put({type: IS_DONE_REQUESTING_SAVE_RESULTS})
        // yield call(delay, 4000)
        // yield put({type: SAVE_DONE})

    } catch (error) {
        // yield put({type: SAVE_FAILED})
        // yield put({type: IS_DONE_REQUESTING_SAVE_RESULTS})
        // yield call(delay, 4000)
        // yield put({type: SAVE_DONE})
        console.log(error)
    }
}

function* updatePeopleOpsStatusWatcher() {
    yield takeLatest(UPDATE_PEOPLE_OPS_STATUS, updatePeopleOpsStatus)
}


function* uploadBehaviour({ files }) {
    // yield put({type:IS_REQUESTING_SAVE_RESULTS})
    try {
        yield put({type:UPLOAD_BEHAVIOURS_IS_LOADING})
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        // console.log("see the data you're updating oo", body, "and you're posting to this id: ", body.staffAppraisalId)
        const req = yield call(createRequestSansHeader, `${PMS_API_URL}/api/Behaviour/UploadBehaviours`, { method: 'POST', body: files }, token)
        const res = yield call(fetch, req)
        // console.log("pass?")
        const resJson = yield call(res.json.bind(res))
        console.log("status update successful:", resJson)
        if(Object.prototype.toString.call(resJson) === '[object Object]' && resJson.message.includes("fine")){
            yield put({type: UPLOAD_BEHAVIOUR_SUCCESSFUL})
            yield call(delay, 4000)
            yield put({type: UPLOAD_BEHAVIOUR_DONE})
        } else {
            yield put({type: UPLOAD_BEHAVIOUR_FAILED})
            yield call(delay, 4000)
            yield put({type: UPLOAD_BEHAVIOUR_DONE})
        }
        yield put({type:UPLOAD_BEHAVIOURS_HAS_STOPPED_LOADING})
        yield put({type:FETCH_BEHAVIOURS})
        // yield put({type: SAVE_SUCCESSFUL})
        // yield put({type: IS_DONE_REQUESTING_SAVE_RESULTS})
        // yield call(delay, 4000)
        // yield put({type: SAVE_DONE})

    } catch (error) {
        // yield put({type: SAVE_FAILED})
        // yield put({type: IS_DONE_REQUESTING_SAVE_RESULTS})
        // yield call(delay, 4000)
        // yield put({type: SAVE_DONE})
        yield put({type: UPLOAD_BEHAVIOUR_FAILED})
        yield call(delay, 4000)
        yield put({type: UPLOAD_BEHAVIOUR_DONE})
        yield put({type:UPLOAD_BEHAVIOURS_HAS_STOPPED_LOADING})
        console.log(error)
    }
}

function* uploadBehaviourWatcher() {
    yield takeLatest(UPLOAD_BEHAVIOUR, uploadBehaviour)
}

function* fetchBehavioralIndicators({LM_DR_STAFF_ID}) {
    try {
        //start fetch loader
        yield put({ type: FETCH_BEHAVIORAL_ASSESSMENT_IS_LOADING })
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        // console.log(token)
        const STAFF_STAFF_ID = yield select((state) => staffIdSelector(state))
        const staffId = LM_DR_STAFF_ID || STAFF_STAFF_ID
        console.log("what", staffId)
        const req = yield call(createRequest, `${PMS_API_URL}/api/Behaviour/StaffId?staffId=${staffId}`, { method: 'GET' }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        console.log("fresh behavioral", resJson)
        yield put({ type: GET_BEHAVIORAL_INDICATORS, payload: resJson })
        //stop fetch loader
        yield put({ type: FETCH_BEHAVIORAL_ASSESSMENT_HAS_STOPPED_LOADING })

    } catch (error) {
        //stop fetch loader
        yield put({ type: FETCH_BEHAVIORAL_ASSESSMENT_HAS_STOPPED_LOADING })
        // yield put({type:REQUEST_ERROR})
        console.log(error)
    }
}

function* fetchBehavioralIndicatorsWatcher() {
    yield takeLatest(FETCH_BEHAVIORAL_INDICATORS, fetchBehavioralIndicators)
}


function* saveBehavioralIndicators({data, objIndex}) {
    try {
        //start button loader
        yield put({ type: SAVE_BEHAVIORAL_ASSESSMENT_IS_LOADING })
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        const req = yield call(createRequest, `${PMS_API_URL}/api/Behaviour/Rating/${data.id}`, { method: 'PUT', body: JSON.stringify(data) }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        console.log("saveBehavioralIndicators response", resJson)
        if(resJson === true){
            yield put({ type: BEHAVIORAL_INDICATORS_SAVE_SUCCESSFUL, data, objIndex })
            yield put({ type: SAVE_BEHAVIOUR_SUCCESSFUL })
            yield call(delay, 2000)
            yield put({type: SAVE_BEHAVIOUR_DONE})
        }
        //stop button loader
        yield put({ type: SAVE_BEHAVIORAL_ASSESSMENT_HAS_STOPPED_LOADING })
       
    } catch (error) {
        yield put({type: SAVE_BEHAVIOUR_FAILED})
        //stop button loader
        yield put({ type: SAVE_BEHAVIORAL_ASSESSMENT_HAS_STOPPED_LOADING })
        console.log(error)
        yield call(delay, 2000)
        yield put({type: SAVE_BEHAVIOUR_DONE})
    }
}

function* saveBehavioralIndicatorsWatcher() {
    yield takeLatest(SAVE_BEHAVIORAL_INDICATORS, saveBehavioralIndicators)
}


function* saveAppraisalCycle({ dates }) {
    yield put({type:SAVE_APPRAISAL_CYCLE_IS_LOADING})
    try {
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        const req = yield call(createRequest, `${PMS_API_URL}/api/AppraisalCycle`, { method: 'POST', body: JSON.stringify(dates) }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        console.log("cycle save successful:", resJson)
        if(Object.prototype.toString.call(resJson) === '[object String]' && resJson.includes("-")){
            const data = dates
            data.id = resJson
            yield put({ type: REPLACE_APPRAISAL_CYCLE_AFTER_CREATION, data})
            yield put({type: SAVE_APPRAISAL_CYCLE_SUCCESSFUL})
            yield call(delay, 4000)
            yield put({type: SAVE_APPRAISAL_CYCLE_DONE})
        } else {
            yield put({type:SAVE_APPRAISAL_CYCLE_HAS_STOPPED_LOADING})
            yield put({type: SAVE_APPRAISAL_CYCLE_FAILED})
            yield call(delay, 4000)
            yield put({type: SAVE_APPRAISAL_CYCLE_DONE})
        }
        // yield put({type:SAVE_APPRAISAL_CYCLE_HAS_STOPPED_LOADING})
        
    } catch (error) {
        console.log(error)
        yield put({type:SAVE_APPRAISAL_CYCLE_HAS_STOPPED_LOADING})
        yield put({type: SAVE_APPRAISAL_CYCLE_FAILED})
        yield call(delay, 4000)
        yield put({type: SAVE_APPRAISAL_CYCLE_DONE})
    }
}

function* saveAppraisalCycleWatcher() {
    yield takeLatest(SAVE_APPRAISAL_CYCLE, saveAppraisalCycle)
}


function* fetchRecommendationTypes() {
    try {
        console.log(9999)
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        // console.log(token)
        const req = yield call(createRequest, `${PMS_API_URL}/api/Recommendation/Types`, { method: 'GET' }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        console.log("recommendation types:", resJson);
    
        yield put({ type: GET_RECOMMENDATION_TYPES, payload: resJson })
        // yield put({ type: STAFF_APPRAISAL_EXISTS })
        
    } catch (error) {
        // yield put({type:REQUEST_ERROR})
        console.log(error)
    }
}

function* fetchRecommendationTypesWatcher() {
    yield takeLatest(FETCH_RECOMMENDATION_TYPES, fetchRecommendationTypes)
}


function* submitRecommendation({ data }) {
    yield put({type:SUBMIT_RECOMMENDATION_IS_LOADING})
    try {
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        const req = yield call(createRequest, `${PMS_API_URL}/api/Recommendation/LineManager/Staff?staffId=${data.staffId}`, { method: 'POST', body: JSON.stringify(data) }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        console.log("recommendation save successful:", resJson)
        if (resJson.data) {
            yield put({type: MODIFY_RECOMMENDATION_AFTER_SUBMIT, id: resJson.data})
            yield put({type: SUBMIT_RECOMMENDATION_SUCCESSFUL})
            yield put({type: SUBMIT_RECOMMENDATION_HAS_STOPPED_LOADING})
            yield call(delay, 4000)
            yield put({type: SUBMIT_RECOMMENDATION_DONE})
        }
        // yield put({type: SAVE_SUCCESSFUL})
        // yield put({type: IS_DONE_REQUESTING_SAVE_RESULTS})
        // yield call(delay, 4000)
        // yield put({type: SAVE_DONE})
    } catch (error) {
        // yield put({type: SAVE_FAILED})
        // yield put({type: IS_DONE_REQUESTING_SAVE_RESULTS})
        // yield call(delay, 4000)
        // yield put({type: SAVE_DONE})
        yield put({type: SUBMIT_RECOMMENDATION_FAILED})
        yield put({type: SUBMIT_RECOMMENDATION_HAS_STOPPED_LOADING})
        yield call(delay, 4000)
        yield put({type: SUBMIT_RECOMMENDATION_DONE})
        console.log(error)
    }
}

function* submitRecommendationWatcher() {
    yield takeLatest(SUBMIT_RECOMMENDATION, submitRecommendation)
}


function* updateRecommendation({ data }) {
    yield put({type:UPDATE_RECOMMENDATION_IS_LOADING})
    try {
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        const req = yield call(createRequest, `${PMS_API_URL}/api/Recommendation/LineManager/Staff/Update?staffId=${data.staffId}`, { method: 'PUT', body: JSON.stringify(data) }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        console.log("recommendation save successful:", resJson)
        if (resJson.data) {
            yield put({type: MODIFY_RECOMMENDATION_AFTER_UPDATE, recommendation: data})
            yield put({type: UPDATE_RECOMMENDATION_SUCCESSFUL})
            yield put({type: UPDATE_RECOMMENDATION_HAS_STOPPED_LOADING})
            yield call(delay, 4000)
            yield put({type: UPDATE_RECOMMENDATION_DONE})
        }
        
    } catch (error) {
        yield put({type: UPDATE_RECOMMENDATION_FAILED})
        // yield put({type: IS_DONE_REQUESTING_SAVE_RESULTS})
        yield put({type: UPDATE_RECOMMENDATION_HAS_STOPPED_LOADING})
        yield call(delay, 4000)
        yield put({type: UPDATE_RECOMMENDATION_DONE})
        console.log(error)
    }
}

function* updateRecommendationWatcher() {
    yield takeLatest(UPDATE_RECOMMENDATION, updateRecommendation)
}


function* updateSuperRecommendation({ data }) {
    yield put({type:UPDATE_RECOMMENDATION_IS_LOADING})
    try {
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        const req = yield call(createRequest, `${PMS_API_URL}/api/Recommendation/SuperLineManager/Staff/Approval?staffId=${data.staffId}`, { method: 'PUT', body: JSON.stringify(data) }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        console.log("recommendation save successful:", resJson)
        if (resJson.data) {
            yield put({type: MODIFY_SUPER_RECOMMENDATION_AFTER_UPDATE, recommendation: data})
            yield put({type: UPDATE_RECOMMENDATION_SUCCESSFUL})
            yield put({type: UPDATE_RECOMMENDATION_HAS_STOPPED_LOADING})
            yield call(delay, 2000)
            yield put({type: UPDATE_RECOMMENDATION_DONE})
        }
        // yield put({type: SAVE_SUCCESSFUL})
        // yield put({type: IS_DONE_REQUESTING_SAVE_RESULTS})
        // yield call(delay, 4000)
        // yield put({type: SAVE_DONE})
    } catch (error) {
        // yield put({type: SAVE_FAILED})
        // yield put({type: IS_DONE_REQUESTING_SAVE_RESULTS})
        // yield call(delay, 4000)
        // yield put({type: SAVE_DONE})
        yield put({type: UPDATE_RECOMMENDATION_FAILED})
        yield put({type: UPDATE_RECOMMENDATION_HAS_STOPPED_LOADING})
        yield call(delay, 2000)
        yield put({type: UPDATE_RECOMMENDATION_DONE})
        console.log(error)
        console.log(error)
    }
}

function* updateSuperRecommendationWatcher() {
    yield takeLatest(UPDATE_SUPER_RECOMMENDATION, updateSuperRecommendation)
}


function* fetchRecommendation({staffId}) {
    try {
        console.log(9999)
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        console.log("staffId inside fetchRecommendations", staffId)
        const req = yield call(createRequest, `${PMS_API_URL}/api/Recommendation/LineManager/Staff?staffId=${staffId}`, { method: 'GET' }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        console.log("recommendation types:", resJson);
    
        yield put({ type: GET_RECOMMENDATION, payload: resJson })
        // yield put({ type: STAFF_APPRAISAL_EXISTS })
        
    } catch (error) {
        // yield put({type:REQUEST_ERROR})
        // console.log("reco-error")
        console.log(error)
    }
}

function* fetchRecommendationWatcher() {
    yield takeLatest(FETCH_RECOMMENDATION, fetchRecommendation)
}


function* fetchSuperRecommendation({staffId}) {
    try {
        console.log(9999)
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        console.log("staffId inside fetchRecommendations", staffId)
        const req = yield call(createRequest, `${PMS_API_URL}/api/Recommendation/SuperLineManager/Staff?staffId=${staffId}`, { method: 'GET' }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        console.log("recommendation object", resJson);

        yield put({ type: GET_SUPER_RECOMMENDATION, payload: resJson })
        // yield put({ type: STAFF_APPRAISAL_EXISTS })
        
    } catch (error) {
        // yield put({type:REQUEST_ERROR})
        console.log("reco-error")
        console.log(error)
    }
}

function* fetchSuperRecommendationWatcher() {
    yield takeLatest(FETCH_SUPER_RECOMMENDATION, fetchSuperRecommendation)
}


function* submitLineManagerAssessment({ data, history }) {
    yield put({type:SUBMIT_LINE_MANAGER_ASSESSMENT_IS_LOADING})
    try {
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        // const staffId = yield select((state) => staffIdSelector(state))
        const body = { firstApproved: data.firstApproved, staffId: data.staffId }
        console.log("see the data you're posting oo", body, "and you're posting to this id: ", data.staffAppraisalId)
        const req = yield call(createRequest, `${PMS_API_URL}/api/StaffAppraisal/${data.staffAppraisalId}/LineManager/Submitted`, { method: 'PUT', body: JSON.stringify(body) }, token)
        const res = yield call(fetch, req)
        console.log("pass?")
        const resJson = yield call(res.json.bind(res))
        console.log("submit successful:", resJson)
        if (resJson) {
            data.lineManagerStatus = "completed"
            yield put({type: MODIFY_LINE_MANAGER_STAFF_APPRAISALS_ON_ASSESS_COMPLETE, data})
            history.replace("/manageassessment")
        }
        
        yield put({type: SUBMIT_LINE_MANAGER_ASSESSMENT_SUCCESSFUL})
        yield put({type: SUBMIT_LINE_MANAGER_ASSESSMENT_HAS_STOPPED_LOADING})
        yield call(delay, 4000)
        yield put({type: SUBMIT_LINE_MANAGER_ASSESSMENT_DONE})

    } catch (error) {
        console.log(error)
        yield put({type: SUBMIT_LINE_MANAGER_ASSESSMENT_FAILED})
        yield put({type: SUBMIT_LINE_MANAGER_ASSESSMENT_HAS_STOPPED_LOADING})
        yield call(delay, 4000)
        yield put({type: SUBMIT_LINE_MANAGER_ASSESSMENT_DONE})
    }
}

function* submitLineManagerAssessmentWatcher() {
    yield takeLatest(SUBMIT_LINE_MANAGER_ASSESSMENT, submitLineManagerAssessment)
}


function* submitSuperLineManagerAssessment({ data, history }) {
    yield put({type:SUBMIT_LINE_MANAGER_ASSESSMENT_IS_LOADING})
    try {
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        // const staffId = yield select((state) => staffIdSelector(state))
        // const body = { firstApproved: data.firstApproved, staffId: data.staffId }
        console.log("see the data you're posting oo", data, "and you're posting to this id: ", data.staffAppraisalId)
        const req = yield call(createRequest, `${PMS_API_URL}/api/StaffAppraisal/${data.staffAppraisalId}/LineManagerTwo/Submitted`, { method: 'PUT', body: JSON.stringify(data) }, token)
        const res = yield call(fetch, req)
        console.log("pass?")
        const resJson = yield call(res.json.bind(res))
        console.log("submit successful:", resJson)
        if (resJson) {
            // data.superLineManagerStatus = "completed"
            yield put({type: MODIFY_SUPER_LINE_MANAGER_STAFF_APPRAISALS_ON_ASSESS_COMPLETE, data})
            history.replace("/supermanageassessment")
        }

        yield put({type: SUBMIT_LINE_MANAGER_ASSESSMENT_SUCCESSFUL})
        yield put({type: SUBMIT_LINE_MANAGER_ASSESSMENT_HAS_STOPPED_LOADING})
        yield call(delay, 4000)
        yield put({type: SUBMIT_LINE_MANAGER_ASSESSMENT_DONE})

        // if(data.firstApproved === "approved"){
        //     yield put({ type: UPDATE_KEY_RESULTS, KRs: pendingKRs, history})
        // }
        // history.replace("/ManageOKRs")
        // yield put({type:GET_STAFF_APPRAISAL_ID, payload: resJson})
        ///
        // yield put({ type: FETCH_STAFF_APPRAISAL }) // to allow the summary screen change to displaying the correct info
        ///
        // yield put({type: SAVE_SUCCESSFUL})
        // yield put({type: IS_DONE_REQUESTING_SAVE_RESULTS})
        // yield call(delay, 4000)
        // yield put({type: SAVE_DONE})

    } catch (error) {
        // yield put({type: SAVE_FAILED})
        // yield put({type: IS_DONE_REQUESTING_SAVE_RESULTS})
        // yield call(delay, 4000)
        // yield put({type: SAVE_DONE})
        console.log(error)
    }
}

function* submitSuperLineManagerAssessmentWatcher() {
    yield takeLatest(SUBMIT_SUPER_LINE_MANAGER_ASSESSMENT, submitSuperLineManagerAssessment)
}



function* lockAppraisalPeriod({ data }) {
    // yield put({type:IS_REQUESTING_SAVE_RESULTS})
    try {
        yield put({type:LOCK_APPRAISAL_PERIOD_IS_LOADING})
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        // const staffId = yield select((state) => staffIdSelector(state))
        // const staffAppraisalId = yield select((state) => staffAppraisalIdSelector(state))
        // const data = {staffSubmitted: true, staffId}
        // console.log("see the data you're updating oo", OBJs)
        const req = yield call(createRequest, `${PMS_API_URL}/api/AppraisalCycle/Lock/${data.id}`, { method: 'PUT', body: JSON.stringify(data) }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        console.log("locking appraisal successful:", resJson)
        if(resJson===true){
            yield put({type: LOCK_APPRAISAL_CYCLE_SUCCESSFUL})
            yield call(delay, 4000)
            yield put({type: LOCK_APPRAISAL_CYCLE_DONE})
            yield put({ type: MODIFY_APPRAISAL_CYCLE_AFTER_UPDATE, data})
        } else {
            yield put({type: LOCK_APPRAISAL_CYCLE_FAILED})
            yield call(delay, 4000)
            yield put({type: LOCK_APPRAISAL_CYCLE_DONE})
        }
        yield put({type:LOCK_APPRAISAL_PERIOD_HAS_STOPPED_LOADING})

    } catch (error) {

        yield put({type: LOCK_APPRAISAL_CYCLE_FAILED})
        yield call(delay, 4000)
        yield put({type: LOCK_APPRAISAL_CYCLE_DONE})
        yield put({type:LOCK_APPRAISAL_PERIOD_HAS_STOPPED_LOADING})
        console.log(error)
    }
}

function* lockAppraisalPeriodWatcher() {
    yield takeLatest(LOCK_APPRAISAL_PERIOD, lockAppraisalPeriod)
}



function* unlockAppraisalPeriod({ data }) {
    // yield put({type:IS_REQUESTING_SAVE_RESULTS})
    try {
        yield put({type: UNLOCK_APPRAISAL_PERIOD_IS_LOADING})
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        // const staffId = yield select((state) => staffIdSelector(state))
        // const staffAppraisalId = yield select((state) => staffAppraisalIdSelector(state))
        // const data = {staffSubmitted: true, staffId}
        // console.log("see the data you're updating oo", OBJs)
        const req = yield call(createRequest, `${PMS_API_URL}/api/AppraisalCycle/Lock/${data.id}`, { method: 'PUT', body: JSON.stringify(data) }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        console.log("locking appraisal successful:", resJson)
        if(resJson===true){
            yield put({type: UNLOCK_APPRAISAL_CYCLE_SUCCESSFUL})
            yield call(delay, 4000)
            yield put({type: UNLOCK_APPRAISAL_CYCLE_DONE})
            yield put({ type: MODIFY_APPRAISAL_CYCLE_AFTER_UPDATE, data})
        } else {
            yield put({type: UNLOCK_APPRAISAL_CYCLE_FAILED})
            yield call(delay, 4000)
            yield put({type: UNLOCK_APPRAISAL_CYCLE_DONE})
        }
        yield put({type: UNLOCK_APPRAISAL_PERIOD_HAS_STOPPED_LOADING})

    } catch (error) {

        yield put({type: UNLOCK_APPRAISAL_CYCLE_FAILED})
        yield call(delay, 4000)
        yield put({type: UNLOCK_APPRAISAL_CYCLE_DONE})
        yield put({type:UNLOCK_APPRAISAL_PERIOD_HAS_STOPPED_LOADING})
        console.log(error)
    }
}

function* unlockAppraisalPeriodWatcher() {
    yield takeLatest(UNLOCK_APPRAISAL_PERIOD, unlockAppraisalPeriod)
}



function* fetchPeopleOpsCount() {
    try {
        console.log(9999)
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        const req = yield call(createRequest, `${PMS_API_URL}/api/Dashboard/PeopleOps`, { method: 'GET' }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        console.log("people ops count", resJson);

        yield put({ type: GET_PEOPLE_OPS_COUNT, payload: resJson })
        // yield put({ type: STAFF_APPRAISAL_EXISTS })
        
    } catch (error) {
        // yield put({type:REQUEST_ERROR})
        console.log(error)
    }
}

function* fetchPeopleOpsCountWatcher() {
    yield takeLatest(FETCH_PEOPLE_OPS_COUNT, fetchPeopleOpsCount)
}



function* fetchAllBusinessUnits() {
    yield put({ type: FETCH_ALL_BUSINESS_UNITS_IS_LOADING })
    try {
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        // console.log(token)
        const req = yield call(createRequest, `${PMS_API_URL}/api/Staff/BusinessUnit`, { method: 'GET' }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        yield put({ type: GET_ALL_BUSINESS_UNITS, payload: resJson })
        // yield put({ type: SBU_REQUEST_SUCCESS })
        yield put({ type: FETCH_ALL_BUSINESS_UNITS_HAS_STOPPED_LOADING })

    } catch (error) {
        yield put({ type: FETCH_ALL_BUSINESS_UNITS_HAS_STOPPED_LOADING })
    }
}

function* fetchAllBusinessUnitsWatcher() {
    yield takeLatest(FETCH_ALL_BUSINESS_UNITS, fetchAllBusinessUnits)
}



function* fetchAllSbuObjectives() {
    try {
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        // console.log(token)
        const req = yield call(createRequest, `${PMS_API_URL}/api/SBUObjective`, { method: 'GET' }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        yield put({ type: GET_ALL_SBU_OBJECTIVES, payload: resJson })
        yield put({ type: SBU_REQUEST_SUCCESS })

    } catch (error) {
        // yield put({type:REQUEST_ERROR})
        console.log(error)
    }
}

function* fetchAllSbuObjectivesWatcher() {
    yield takeLatest(FETCH_ALL_SBU_OBJECTIVES, fetchAllSbuObjectives)
}



function* createSbuObjective({ data }) {
    // yield put({type:IS_REQUESTING_SAVE_RESULTS})
    
    try {
        yield put({type:CREATE_SBU_OBJECTIVES_IS_LOADING})
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        const req = yield call(createRequest, `${PMS_API_URL}/api/SBUObjective`, { method: 'POST', body: JSON.stringify(data) }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        console.log("sbu objective creation successful:", resJson)
        if (Object.prototype.toString.call(resJson) === '[object String]' && resJson.includes("-")) {
            data.id = resJson
            yield put({type: SBU_OBJECTIVES_SAVE_SUCCESSFUL})
            yield call(delay, 4000)
            yield put({type: SBU_OBJECTIVES_SAVE_DONE})
            yield put({type: MODIFY_ALL_SBU_OBJECTIVES_AFTER_SUBMIT, data})
        } else {
            yield put({type: SBU_OBJECTIVES_SAVE_FAILED})
            yield call(delay, 4000)
            yield put({type: SBU_OBJECTIVES_SAVE_DONE})
        }
        yield put({type:CREATE_SBU_OBJECTIVES_HAS_STOPPED_LOADING})
        // yield put({type: SAVE_SUCCESSFUL})
        // yield put({type: IS_DONE_REQUESTING_SAVE_RESULTS})
        // yield call(delay, 4000)
        // yield put({type: SAVE_DONE})
    } catch (error) {
        // yield put({type: SAVE_FAILED})
        // yield put({type: IS_DONE_REQUESTING_SAVE_RESULTS})
        // yield call(delay, 4000)
        // yield put({type: SAVE_DONE})
        yield put({type: SBU_OBJECTIVES_SAVE_FAILED})
        yield call(delay, 4000)
        yield put({type: SBU_OBJECTIVES_SAVE_DONE})
        yield put({type:CREATE_SBU_OBJECTIVES_HAS_STOPPED_LOADING})
        console.log(error)
    }
}

function* createSbuObjectiveWatcher() {
    yield takeLatest(CREATE_SBU_OBJECTIVE, createSbuObjective)
}



function* fetchBehaviours() {
    try {
        //start fetch loader
        yield put({ type: FETCH_BEHAVIOURS_IS_LOADING })
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        // console.log(token)
        // console.log("what", staffId)
        const req = yield call(createRequest, `${PMS_API_URL}/api/Behaviour`, { method: 'GET' }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        console.log("fresh behaviours", resJson)
        yield put({ type: GET_BEHAVIOURS, payload: resJson })
        //stop fetch loader
        yield put({ type: FETCH_BEHAVIOURS_HAS_STOPPED_LOADING })

    } catch (error) {
        //stop fetch loader
        yield put({ type: FETCH_BEHAVIOURS_HAS_STOPPED_LOADING })
        // yield put({type:REQUEST_ERROR})
        console.log(error)
    }
}

function* fetchBehavioursWatcher() {
    yield takeLatest(FETCH_BEHAVIOURS, fetchBehaviours)
}



function* fetchStaffAppraisalScore({ staffId }) {
    try {
        //start fetch loader
        yield put({ type: FETCH_STAFF_APPRAISAL_SCORE_IS_LOADING })
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        // console.log(token)
        // console.log("what", staffId)
        const req = yield call(createRequest, `${PMS_API_URL}/api/AppraisalScore/Staff?staffId=${staffId}`, { method: 'GET' }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        console.log("fresh Appraisal Score", resJson)
        yield put({ type: GET_STAFF_APPRAISAL_SCORE, payload: resJson })
        //stop fetch loader
        yield put({ type: FETCH_STAFF_APPRAISAL_SCORE_HAS_STOPPED_LOADING })

    } catch (error) {
        //stop fetch loader
        yield put({ type: FETCH_STAFF_APPRAISAL_SCORE_HAS_STOPPED_LOADING })
        // yield put({type:REQUEST_ERROR})
        console.log(error)
    }
}

function* fetchStaffAppraisalScoreWatcher() {
    yield takeLatest(FETCH_STAFF_APPRAISAL_SCORE, fetchStaffAppraisalScore)
}


function* updateSbuObjective({ data, id }) {
    yield put({type: UPDATE_SBU_OBJECTIVE_IS_LOADING})
    try {
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        const req = yield call(createRequest, `${PMS_API_URL}/api/SbuObjective/${id}`, { method: 'PUT', body: JSON.stringify(data) }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        console.log("recommendation save successful:", resJson)
        if (resJson===true) {
            data.id = id
            yield put({type: MODIFY_ALL_SBU_OBJECTIVES_AFTER_UPDATE, data})
            yield put({type: UPDATE_SBU_OBJECTIVE_HAS_STOPPED_LOADING})
            yield put({type: UPDATE_SBU_OBJECTIVE_SUCCESSFUL})
            // yield call(delay, 4000)
            yield put({type: UPDATE_SBU_OBJECTIVE_DONE})
        }
        
    } catch (error) {
        yield put({type: UPDATE_SBU_OBJECTIVE_HAS_STOPPED_LOADING})
        yield put({type: UPDATE_SBU_OBJECTIVE_FAILED})
        // yield call(delay, 4000)
        yield put({type: UPDATE_SBU_OBJECTIVE_DONE})
        console.log(error)
    }
}

function* updateSbuObjectiveWatcher() {
    yield takeLatest(UPDATE_SBU_OBJECTIVE, updateSbuObjective)
}



function* calculateStaffAppraisalScore({ staffId }) {
    yield put({type: CALCULATE_STAFF_APPRAISAL_SCORE_IS_LOADING})
    try {
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        const req = yield call(createRequest, `${PMS_API_URL}/api/AppraisalScore/Staff?staffId=${staffId}`, { method: 'POST'}, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        console.log("recommendation save successful:", resJson)

        //resJson is going to be an object!
        if (resJson.appraisalId) {
            // data.id = id
            yield put({type: MODIFY_STAFF_APPRAISAL_SCORE_AFTER_CALCULATION, data: resJson})
            yield put({type: CALCULATE_STAFF_APPRAISAL_SCORE_HAS_STOPPED_LOADING})
            yield put({type: CALCULATE_STAFF_APPRAISAL_SCORE_SUCCESSFUL})
            yield call(delay, 4000)
            yield put({type: CALCULATE_STAFF_APPRAISAL_SCORE_DONE})
        } else {
            yield put({type: CALCULATE_STAFF_APPRAISAL_SCORE_HAS_STOPPED_LOADING})
            yield put({type: CALCULATE_STAFF_APPRAISAL_SCORE_FAILED})
            yield call(delay, 4000)
            yield put({type: CALCULATE_STAFF_APPRAISAL_SCORE_DONE})
        }
        
    } catch (error) {
        yield put({type: CALCULATE_STAFF_APPRAISAL_SCORE_HAS_STOPPED_LOADING})
        yield put({type: CALCULATE_STAFF_APPRAISAL_SCORE_FAILED})
        yield call(delay, 4000)
        yield put({type: CALCULATE_STAFF_APPRAISAL_SCORE_DONE})
        console.log(error)
    }
}

function* calculateStaffAppraisalScoreWatcher() {
    yield takeLatest(CALCULATE_STAFF_APPRAISAL_SCORE, calculateStaffAppraisalScore)
}



function* deleteSbuObjective({ id }) {
    yield put({type: DELETE_SBU_OBJECTIVE_IS_LOADING})
    try {
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        const req = yield call(createRequest, `${PMS_API_URL}/api/SbuObjective/${id}`, { method: 'DELETE' }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        console.log("sbu objective delete successful:", resJson)
        if (resJson===true) {
            yield put({type: MODIFY_ALL_SBU_OBJECTIVES_AFTER_DELETE, id})
            yield put({type: DELETE_SBU_OBJECTIVE_HAS_STOPPED_LOADING})
            yield put({type: UPDATE_SBU_OBJECTIVE_SUCCESSFUL})
            // yield call(delay, 4000)
            yield put({type: UPDATE_SBU_OBJECTIVE_DONE})
        }
        
    } catch (error) {
        yield put({type: DELETE_SBU_OBJECTIVE_HAS_STOPPED_LOADING})
        yield put({type: UPDATE_SBU_OBJECTIVE_FAILED})
        // yield call(delay, 4000)
        yield put({type: UPDATE_SBU_OBJECTIVE_DONE})
        console.log(error)
    }
}

function* deleteSbuObjectiveWatcher() {
    yield takeLatest(DELETE_SBU_OBJECTIVE, deleteSbuObjective)
}




function* fetchAllStaff() {
    try {
        //start fetch loader
        yield put({ type: FETCH_ALL_STAFF_IS_LOADING })
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        const req = yield call(createRequest, `${STAFF_API_URL}/apimanager`, { method: 'GET' }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))

        yield put({ type: GET_ALL_STAFF , payload: {data: resJson} })
        //stop fetch loader
        yield put({ type: FETCH_ALL_STAFF_HAS_STOPPED_LOADING })

    } catch (error) {
        //stop fetch loader
        yield put({ type: FETCH_ALL_STAFF_HAS_STOPPED_LOADING })
        console.log(error)
    }
}

function* fetchAllStaffWatcher() {
    yield takeLatest(FETCH_ALL_STAFF, fetchAllStaff)
}



function* fetchAllLineManagers() {
    try {
        //start fetch loader
        yield put({ type: FETCH_ALL_LINE_MANAGERS_IS_LOADING })
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        const req = yield call(createRequest, `${STAFF_API_URL}/api/LineManager`, { method: 'GET' }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))

        yield put({ type: GET_ALL_LINE_MANAGERS , payload: {data: resJson} })
        //stop fetch loader
        yield put({ type: FETCH_ALL_LINE_MANAGERS_HAS_STOPPED_LOADING })

    } catch (error) {
        //stop fetch loader
        yield put({ type: FETCH_ALL_LINE_MANAGERS_HAS_STOPPED_LOADING })
        console.log(error)
    }
}

function* fetchAllLineManagersWatcher() {
    yield takeLatest(FETCH_ALL_LINE_MANAGERS, fetchAllLineManagers)
}



function* fetchAllDepartments() {
    try {
        //start fetch loader
        yield put({ type: FETCH_ALL_DEPARTMENTS_IS_LOADING})
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        const req = yield call(createRequest, `${STAFF_API_URL}/api/Department`, { method: 'GET' }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))

        yield put({ type: GET_ALL_DEPARTMENTS , payload: {data: resJson} })
        //stop fetch loader
        yield put({ type: FETCH_ALL_DEPARTMENTS_HAS_STOPPED_LOADING})

    } catch (error) {
        //stop fetch loader
        yield put({ type: FETCH_ALL_DEPARTMENTS_HAS_STOPPED_LOADING })
        console.log(error)
    }
}

function* fetchAllDepartmentsWatcher() {
    yield takeLatest(FETCH_ALL_DEPARTMENTS, fetchAllDepartments)
}



function* fetchStaffDetailsByAdmin({ payload: {staffId} }) {
    try {
        //start fetch loader
        yield put({ type: FETCH_STAFF_DETAILS_BY_ADMIN_IS_LOADING})
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        const req = yield call(createRequest, `${STAFF_API_URL}/apimanager/staffid?staffId=${staffId}`, { method: 'GET' }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))

        yield put({ type: GET_STAFF_DETAILS_BY_ADMIN , payload: {data: resJson} })
        //stop fetch loader
        yield put({ type: FETCH_STAFF_DETAILS_BY_ADMIN_HAS_STOPPED_LOADING})

    } catch (error) {
        //stop fetch loader
        yield put({ type: FETCH_STAFF_DETAILS_BY_ADMIN_HAS_STOPPED_LOADING })
        console.log(error)
    }
}

function* fetchStaffDetailsByAdminWatcher() {
    yield takeLatest(FETCH_STAFF_DETAILS_BY_ADMIN, fetchStaffDetailsByAdmin)
}


function* updateStaffDetails({ payload: { staffId, data } }) {
    yield put({type: UPDATE_STAFF_DETAILS_IS_LOADING})
    try {
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        const req = yield call(createRequest, `${STAFF_API_URL}/apimanager/staffId?staffId=${staffId}`, { method: 'PUT', body: JSON.stringify(data) }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        // console.log("recommendation save successful:", resJson)
        yield put({type: UPDATE_STAFF_DETAILS_HAS_STOPPED_LOADING})
        if (resJson) {
            toast.success('Staff details updated successfully!')
        }
        // if (resJson===true) {
        //     data.id = id
        //     yield put({type: MODIFY_ALL_SBU_OBJECTIVES_AFTER_UPDATE, data})
        //     yield put({type: UPDATE_SBU_OBJECTIVE_SUCCESSFUL})
        //     // yield call(delay, 4000)
        //     yield put({type: UPDATE_SBU_OBJECTIVE_DONE})
        // }
        
    } catch (error) {
        yield put({type: UPDATE_STAFF_DETAILS_HAS_STOPPED_LOADING})
        toast.success('Failed to update staff details')
        // yield put({type: UPDATE_SBU_OBJECTIVE_FAILED})
        // // yield call(delay, 4000)
        // yield put({type: UPDATE_SBU_OBJECTIVE_DONE})
        // console.log(error)
    }
}

function* updateStaffDetailsWatcher() {
    yield takeLatest(UPDATE_STAFF_DETAILS, updateStaffDetails)
}



function* fetchStaffClaims({ payload: {email} }) {
    try {
        //start fetch loader
        yield put({ type: FETCH_STAFF_CLAIMS_IS_LOADING})
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        const req = yield call(createRequest, `${PMS_API_URL}/api/sso/userclaims?email=${email}`, { method: 'GET' }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))

        if (Array.isArray(resJson)) {
            yield put({ type: GET_STAFF_CLAIMS , payload: {data: resJson} })
        } else {
            toast.error(resJson)
            yield put({ type: GET_STAFF_CLAIMS , payload: {data: []} })
        }

        //stop fetch loader
        yield put({ type: FETCH_STAFF_CLAIMS_HAS_STOPPED_LOADING})

    } catch (error) {
        //stop fetch loader
        yield put({ type: FETCH_STAFF_CLAIMS_HAS_STOPPED_LOADING })
        console.log(error)
    }
}

function* fetchStaffClaimsWatcher() {
    yield takeLatest(FETCH_STAFF_CLAIMS, fetchStaffClaims)
}



function* updateUserClaims({ payload: { email, uiClaimsToAdd, apiClaimsToAdd, claimsToRemove } }) {
    yield put({type: UPDATE_USER_CLAIMS_IS_LOADING})
    try {
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        let req, res, resJson, data
        if (uiClaimsToAdd.length) {
            data = { ClientId: 'vggpmsclient', Claims: uiClaimsToAdd }
            req = yield call(createRequest, `${PMS_API_URL}/api/sso/adduserclaim?email=${email}`, { method: 'POST', body: JSON.stringify(data) }, token)
            res = yield call(fetch, req)
            resJson = yield call(res.json.bind(res))
        }
        if (apiClaimsToAdd.length) {
            data = { ClientId: 'pmsapi', Claims: apiClaimsToAdd }
            req = yield call(createRequest, `${PMS_API_URL}/api/sso/adduserclaim?email=${email}`, { method: 'POST', body: JSON.stringify(data) }, token)
            res = yield call(fetch, req)
            resJson = yield call(res.json.bind(res))
        }
        if (claimsToRemove.length) {
            data = { Claims: claimsToRemove }
            req = yield call(createRequest, `${PMS_API_URL}/api/sso/removeuserclaim?email=${email}`, { method: 'POST', body: JSON.stringify(data) }, token)
            res = yield call(fetch, req)
            resJson = yield call(res.json.bind(res))
        }
        
        yield put({type: UPDATE_USER_CLAIMS_HAS_STOPPED_LOADING})
        // console.log('res', res)
        if (res.status == 200) {
            toast.success('User Claims Updated Successfully!')
            yield put({type: FETCH_STAFF_CLAIMS, payload: { email }})
        } else {
            toast.error('Failed to Update User Claims!')
            yield put({type: FETCH_STAFF_CLAIMS, payload: { email }})
        }

        
    } catch (error) {
        yield put({type: UPDATE_USER_CLAIMS_HAS_STOPPED_LOADING})
        toast.error('Failed to Update User Claims!')
    }
}

function* updateUserClaimsWatcher() {
    yield takeLatest(UPDATE_USER_CLAIMS, updateUserClaims)
}


function* fetchStaffOkrPeriod({ payload: {month, year} }) {
    try {
        //start fetch loader
        yield put({ type: FETCH_STAFF_OKR_PERIOD_IS_LOADING})
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        const req = yield call(createRequest, `${PMS_API_URL}/api/period/monthyear?month=${month}&year=${year}`, { method: 'GET' }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))

        console.log('resjson', resJson)

        yield put({ type: GET_STAFF_OKR_PERIOD , payload: {data: resJson.data} })
        //stop fetch loader
        yield put({ type: FETCH_STAFF_OKR_PERIOD_HAS_STOPPED_LOADING})

    } catch (error) {
        //stop fetch loader
        yield put({ type: FETCH_STAFF_OKR_PERIOD_HAS_STOPPED_LOADING })
        console.log(error)
    }
}

function* fetchStaffOkrPeriodWatcher() {
    yield takeLatest(FETCH_STAFF_OKR_PERIOD, fetchStaffOkrPeriod)
}


function* fetchAllStaffOkrPeriod() {
    try {
        //start fetch loader
        yield put({ type: FETCH_ALL_STAFF_OKR_PERIOD_IS_LOADING})
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        const req = yield call(createRequest, `${PMS_API_URL}/api/period/staff`, { method: 'GET' }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))

        console.log('resjson', resJson)

        yield put({ type: GET_ALL_STAFF_OKR_PERIOD , payload: {data: resJson} })
        //stop fetch loader
        yield put({ type: FETCH_ALL_STAFF_OKR_PERIOD_HAS_STOPPED_LOADING})

    } catch (error) {
        //stop fetch loader
        yield put({ type: FETCH_ALL_STAFF_OKR_PERIOD_HAS_STOPPED_LOADING })
        console.log(error)
    }
}

function* fetchAllStaffOkrPeriodWatcher() {
    yield takeLatest(FETCH_ALL_STAFF_OKR_PERIOD, fetchAllStaffOkrPeriod)
}



function* createStaffOkrPeriod({ payload: { data } }) {
    yield put({type: CREATE_STAFF_OKR_PERIOD_IS_LOADING})
    try {
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        const req = yield call(createRequest, `${PMS_API_URL}/api/addperiod/oneonone`, { method: 'POST', body: JSON.stringify(data) }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        // console.log("recommendation save successful:", resJson)
        yield put({type: CREATE_STAFF_OKR_PERIOD_HAS_STOPPED_LOADING})
        if (resJson) {
            toast.success('One on One Period Created Successfully!')
            yield put({ type: FETCH_STAFF_OKR_PERIOD, payload: { month: data.month, year: data.year } })
            yield put({ type: FETCH_OBJECTIVES_AND_KEY_RESULTS });
            yield put({ type: FETCH_ALL_STAFF_OKR_PERIOD });
            yield put({ type: FETCH_STAFF_APPRAISAL });
        }
        
    } catch (error) {
        yield put({type: CREATE_STAFF_OKR_PERIOD_HAS_STOPPED_LOADING})
        toast.error('Failed to Create One on One Period')
    }
}

function* createStaffOkrPeriodWatcher() {
    yield takeLatest(CREATE_STAFF_OKR_PERIOD, createStaffOkrPeriod)
}


function* updateStaffOkrPeriod({ payload: { data, refetchPeriod } }) {
    yield put({type: UPDATE_STAFF_OKR_PERIOD_IS_LOADING})
    try {
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        const req = yield call(createRequest, `${PMS_API_URL}/api/${data.id}/updateperiodoneonone`, { method: 'PUT', body: JSON.stringify(data) }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))
        // console.log("recommendation save successful:", resJson)
        yield put({type: UPDATE_STAFF_OKR_PERIOD_HAS_STOPPED_LOADING})
        if (res.status === 200) {
            toast.success('One on One Updated Successfully!')
            if( refetchPeriod ) {
                yield put({ type: FETCH_ALL_LM_STAFF_OKR_PERIOD, payload: { staffId: data.staffId } });
            }
        } else {
            toast.error('Failed to Update One on One')
        }
        
    } catch (error) {
        yield put({type: UPDATE_STAFF_OKR_PERIOD_HAS_STOPPED_LOADING})
        toast.error('Failed to Update One on One')
    }
}

function* updateStaffOkrPeriodWatcher() {
    yield takeLatest(UPDATE_STAFF_OKR_PERIOD, updateStaffOkrPeriod)
}


function* fetchAllLmStaffOkrPeriod({ payload: {staffId} }) {
    try {
        //start fetch loader
        yield put({ type: FETCH_ALL_LM_STAFF_OKR_PERIOD_IS_LOADING})
        const token = yield select((state) => {
            return state.user ? state.user.access_token : ''
        })
        const req = yield call(createRequest, `${PMS_API_URL}/api/period/getstaffperiod?staffId=${staffId}`, { method: 'GET' }, token)
        const res = yield call(fetch, req)
        const resJson = yield call(res.json.bind(res))

        console.log('resjson', resJson)

        yield put({ type: GET_ALL_LM_STAFF_OKR_PERIOD , payload: {data: resJson.data} })
        //stop fetch loader
        yield put({ type: FETCH_ALL_LM_STAFF_OKR_PERIOD_HAS_STOPPED_LOADING})

    } catch (error) {
        //stop fetch loader
        yield put({ type: FETCH_ALL_LM_STAFF_OKR_PERIOD_HAS_STOPPED_LOADING })
        console.log(error)
    }
}

function* fetchAllLmStaffOkrPeriodWatcher() {
    yield takeLatest(FETCH_ALL_LM_STAFF_OKR_PERIOD, fetchAllLmStaffOkrPeriod)
}



export default function* rootSaga() {
    yield all([
        fetchAppraisalObjectivesWatcher(),
        fetchObjectivesandKeyResultsWatcher(),
        saveKeyResultsWatcher(),
        createOKRsWatcher(),
        fetchSbuObjectivesWatcher(),
        fetchCategoryWatcher(),
        fetchAppraisalCycleWatcher(),
        fetchStaffAppraisalWatcher(),
        createStaffAppraisalWatcher(),
        fetchStaffDetailsWatcher(),
        fetchLineManagerStaffAppraisalsWatcher(),
        submitOKRsWatcher(),
        submitLineManagerOKRsWatcher(),
        submitPeopleOpsOKRsWatcher(),
        updateLineManagerStatusWatcher(),
        updateKeyResultsWatcher(),
        deleteAppraisalObjectiveWatcher(),
        deleteKeyResultWatcher(),
        addKeyResultWatcher(),
        fetchPeopleOpsStaffAppraisalsWatcher(),
        updatePeopleOpsStatusWatcher(),
        fetchLMPOOkrsWatcher(),
        updateObjectivesWatcher(),
        fetchStaffRoleWatcher(),
        fetchSuperLineManagerStaffAppraisalsWatcher(),
        submitSuperLineManagerOKRsWatcher(),
        uploadBehaviourWatcher(),
        fetchBehavioralIndicatorsWatcher(),
        saveAppraisalCycleWatcher(),
        saveBehavioralIndicatorsWatcher(),
        fetchRecommendationTypesWatcher(),
        updateRecommendationWatcher(),
        submitRecommendationWatcher(),
        fetchRecommendationWatcher(),
        submitLineManagerAssessmentWatcher(),
        fetchSuperRecommendationWatcher(),
        updateSuperRecommendationWatcher(),
        submitSuperLineManagerAssessmentWatcher(),
        lockAppraisalPeriodWatcher(),
        unlockAppraisalPeriodWatcher(),
        fetchPeopleOpsCountWatcher(),
        fetchAllBusinessUnitsWatcher(),
        fetchAllSbuObjectivesWatcher(),
        createSbuObjectiveWatcher(),
        fetchBehavioursWatcher(),
        onboardUsersWatcher(),
        changePasswordWatcher(),
        lineManagerUpdateKeyResultsWatcher(),
        peopleOpsUpdateKeyResultsWatcher(),
        updateSbuObjectiveWatcher(),
        deleteSbuObjectiveWatcher(),
        fetchStaffAppraisalScoreWatcher(),
        calculateStaffAppraisalScoreWatcher(),
        fetchAllStaffWatcher(),
        fetchAllLineManagersWatcher(),
        fetchAllDepartmentsWatcher(),
        fetchStaffDetailsByAdminWatcher(),
        updateStaffDetailsWatcher(),
        fetchStaffClaimsWatcher(),
        updateUserClaimsWatcher(),
        fetchStaffOkrPeriodWatcher(),
        fetchAllStaffOkrPeriodWatcher(),
        createStaffOkrPeriodWatcher(),
        updateStaffOkrPeriodWatcher(),
        fetchAllLmStaffOkrPeriodWatcher(),
    ])
}

