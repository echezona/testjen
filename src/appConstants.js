//Used by Oidc UserManager
export const SSO_REDIRECT_PATH = '/redirect'

export const SSO_URL = (process.env.NODE_ENV === 'development') ? 'https://sso.test.vggdev.com/identity' : window.env.ssoUrl
export const REDIRECT_URI = (process.env.NODE_ENV === 'development') ? 'http://localhost:3000/redirect' : window.env.redirectUri
export const POST_LOGOUT_REDIRECT_URI = (process.env.NODE_ENV === 'development') ? 'http://localhost:3000/landing' : window.env.postLogoutRedirectUri

export const CLIENT_ID = 'vggpmsclient'

//For PMS
export const PMS_API_URL = (process.env.NODE_ENV === 'development') ? 'https://pms-api-mainone.test.vggdev.com' : window.env.pmsApiUrl
export const STAFF_API_URL = (process.env.NODE_ENV === 'development') ? "http://staff-api.test.vggdev.com" : window.env.staffApiUrl

//Test Variable
export const TEST_VAR = (process.env.NODE_ENV === 'development') ? "You are on local..." : window.env.testVar

// 'http://pms-api.test.vggdev.com'
