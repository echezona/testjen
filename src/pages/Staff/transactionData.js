import React from 'react';
import { Card, Icon, Table } from "semantic-ui-react";

const TransactionTableComponent = () => {
    return (
        <div>
            <Card className="card-layout" fluid>
                <Card.Content>
                    <Card.Header className={'pa2'}>
                        <div className={'fl'}>
                            10 Last Transactions
                        </div>
                        <div className={'fr pointer'}>
                            <Icon name={'ellipsis vertical'}/>
                        </div>
                    </Card.Header>
                </Card.Content>
                <Card.Content>
                    <Card.Description>
                        <Table basic={"very"} striped>
                            <Table.Body>
                                <Table.Row>
                                    <Table.Cell>INV001</Table.Cell>
                                    <Table.Cell>NGN 300,000,000</Table.Cell>
                                    <Table.Cell><Icon name={'angle up'} color={'green'} size={'big'}/></Table.Cell>
                                </Table.Row>
                                <Table.Row>
                                    <Table.Cell>INV00045</Table.Cell>
                                    <Table.Cell>NGN 450,000</Table.Cell>
                                    <Table.Cell><Icon name={'angle down'} color={'red'} size={'big'}/></Table.Cell>
                                </Table.Row>
                                <Table.Row>
                                    <Table.Cell>INV001</Table.Cell>
                                    <Table.Cell>NGN 300,000</Table.Cell>
                                    <Table.Cell><Icon name={'angle up'} color={'green'} size={'big'}/></Table.Cell>
                                </Table.Row>
                                <Table.Row>
                                    <Table.Cell>INV00043</Table.Cell>
                                    <Table.Cell>NGN 450,000,000</Table.Cell>
                                    <Table.Cell><Icon name={'angle down'} color={'red'} size={'big'}/></Table.Cell>
                                </Table.Row>
                                <Table.Row>
                                    <Table.Cell>INV00045</Table.Cell>
                                    <Table.Cell>NGN 450,000</Table.Cell>
                                    <Table.Cell><Icon name={'angle down'} color={'red'} size={'big'}/></Table.Cell>
                                </Table.Row>
                                <Table.Row>
                                    <Table.Cell>INV001</Table.Cell>
                                    <Table.Cell>NGN 300,000</Table.Cell>
                                    <Table.Cell><Icon name={'angle up'} color={'green'} size={'big'}/></Table.Cell>
                                </Table.Row>
                                <Table.Row>
                                    <Table.Cell>INV00043</Table.Cell>
                                    <Table.Cell>NGN 450,000</Table.Cell>
                                    <Table.Cell><Icon name={'angle down'} color={'red'} size={'big'}/></Table.Cell>
                                </Table.Row>
                                <Table.Row>
                                    <Table.Cell>INV00045</Table.Cell>
                                    <Table.Cell>NGN 450,000</Table.Cell>
                                    <Table.Cell><Icon name={'angle down'} color={'red'} size={'big'}/></Table.Cell>
                                </Table.Row>
                                <Table.Row>
                                    <Table.Cell>INV001</Table.Cell>
                                    <Table.Cell>NGN 300,000</Table.Cell>
                                    <Table.Cell><Icon name={'angle up'} color={'green'} size={'big'}/></Table.Cell>
                                </Table.Row>
                            </Table.Body>
                        </Table>
                    </Card.Description>
                </Card.Content>
            </Card>
        </div>
    );
};

export default TransactionTableComponent;
