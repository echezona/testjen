import React from 'react'
import { Card, Icon } from 'semantic-ui-react'
import { Line } from 'react-chartjs'

const ProcessGraphComponent = ({ chartData, chartOptions }) => {
  return (
    <div>
      <Card className="card-layout" fluid>
        <Card.Content>
          <Card.Header className={'pa2'}>
            <div className={'fl'}>Total Amount Processed</div>
            <div className={'fr pointer'}>
              <Icon name={'ellipsis vertical'} />
            </div>
          </Card.Header>
        </Card.Content>
        <Card.Content>
          <Card.Description className={'pa2'}>
            <Line data={chartData} options={chartOptions} />
          </Card.Description>
        </Card.Content>
      </Card>
    </div>
  )
}

export default ProcessGraphComponent
