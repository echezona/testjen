import React from 'react';
import { Card, Icon } from "semantic-ui-react";
import { Doughnut } from "react-chartjs";

const PerformanceDataComponent = ({data}) => {
    return (
        <div>
            <Card className="card-layout" fluid>
                <Card.Content>
                    <Card.Header className={'pa2'}>
                        <div className={'fl'}>
                            Performance
                        </div>
                        <div className={'fr pointer'}>
                            <Icon name={'ellipsis vertical'}/>
                        </div>
                    </Card.Header>
                </Card.Content>
                <Card.Content>
                    <Card.Description className={'pa2'}>
                        <Doughnut data={data.data} options={data.options}/>
                    </Card.Description>
                </Card.Content>
            </Card>
        </div>
    );
};

export default PerformanceDataComponent;
