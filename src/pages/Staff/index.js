import React, { Component } from 'react'
import { AnalyticsCard, List } from 'semantic-ui-react'
import ProcessGraphComponent from './processGraph'
import TransactionTableComponent from './transactionData'
import PerformanceDataComponent from './performanceData'
import Layout from '../../components/Layout'
import './style.css'
import { Card , Image, Grid ,Segment, Divider, Icon} from 'semantic-ui-react'
//import {  } from 'semantic-ui-react'
import CardHeader from 'semantic-ui-react/dist/commonjs/views/Card/CardHeader';
import CardContent from 'semantic-ui-react/dist/commonjs/views/Card/CardContent';
import staffImage from './images/female.png';

import {TEST_VAR} from '../../appConstants'


const itemsActivities = [
  {
    header: 'Recent activites',
    description: 'Leverage agile frameworks to provide a robust synopsis for high level overviews.',
    meta: 'ROI: 30%',
  },

]



const analyticsCardData = {
  payload: [
    {
      heading: 'Total Processed',
      leftData: 'NGN 50,000,000',
      leftDataDescription: 'Total Value',
      rightData: '1000',
      rightDataDescription: 'Total Volume'
    }
  ]
}
const analyticsCardData2 = {
  payload: [
    {
      heading: 'Total Transactions',
      subHeading: '5%',
      leftData: '1,293',
      leftDataDescription: 'Total Transaction',
      rightData: '30,043',
      rightDataDescription: 'Successful Transactions'
    }
  ]
}
const analyticsCardData3 = {
  payload: [
    {
      heading: 'Channel Performance',
      subHeading: 'Webpay',
      leftData: '13,974',
      leftDataDescription: 'Total Value',
      rightData: 'NGN 15.9 BN',
      rightDataDescription: 'Total Volume'
    },
    {
      heading: 'Performance Of Vigipay',
      subHeading: 'VGN',
      leftData: '22,974',
      leftDataDescription: 'Total Value',
      rightData: 'USD 22,090 BN',
      rightDataDescription: 'Total Volume'
    }
  ]
}

class StaffPageComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      chartData: {
        labels: [
          'January',
          'February',
          'March',
          'April',
          'May',
          'June',
          'July',
          'August',
          'September',
          'October',
          'November',
          'December'
        ],
        datasets: [
          {
            label: 'My Second dataset',
            fillColor: 'rgba(44,78,166,0.8)',
            strokeColor: 'rgba(44,78,166,1)',
            pointColor: 'rgba(44,78,166,1)',
            pointStrokeColor: '#fff',
            pointHighlightFill: '#2c4ea6',
            pointHighlightStroke: 'rgba(44,78,166,0.9)',
            data: [165, 259, 180, 381, 586, 955, 440, 900, 200, 800, 100, 400]
          }
        ]
      },
      chartOptions: {
        scaleShowHorizontalLines: true,
        responsive: true
      },
      donutChartData: {
        data: [
          {
            value: 500,
            color: '#008aee',
            highlight: '#008aee',
            label: 'Legend 1'
          },
          {
            value: 50,
            color: '#1169a6',
            highlight: '#1169a6',
            label: 'Legend 2'
          },
          {
            value: 100,
            color: '#005f7a',
            highlight: '#005f7a',
            label: 'Legend 3'
          },
          {
            value: 250,
            color: '#98d5ee',
            highlight: '#98d5ee',
            label: 'Legend 4'
          }
        ],
        options: {
          //Boolean - Whether we should show a stroke on each segment
          segmentShowStroke: true,

          //String - The colour of each segment stroke
          segmentStrokeColor: '#fff',

          //Number - The width of each segment stroke
          segmentStrokeWidth: 2,

          //Number - The percentage of the chart that we cut out of the middle
          percentageInnerCutout: 50, // This is 0 for Pie charts

          //Number - Amount of animation steps
          animationSteps: 100,

          //String - Animation easing effect
          animationEasing: 'easeOutBounce',

          //Boolean - Whether we animate the rotation of the Doughnut
          animateRotate: true
        }
      },
      activeTab: 1
    }
  }


  render() {
    const { chartData, chartOptions, activeTab, donutChartData } = this.state
    const { history } = this.props
    
  const extra = (
    <a >
      <Icon name='caret down'/>
    </a>
  )
    return (
        <div className="w-100 ">
          <div style={{color: "red"}}>{TEST_VAR}</div>
          <div className = "fl w-two-thirds pa4 tc">

            <div className="mb4">
              <Card fluid >
                
                <Card.Content>
                  <Grid columns={2} relaxed>
                    <Grid.Column width={3}>
                      <img src={staffImage} alt="staff Image" height="100%" width="100%"/>
                    </Grid.Column>
        
                
                    <Grid.Column width={12} className = "tc">
                        <Card.Header className = "pa2"><h3>Staff Name</h3></Card.Header>
                        <Card.Meta><p>Head Of Engineering Unit</p></Card.Meta>
                        <Card.Description>
                          <a>
                            StaffName@venturegardengroup.com
                          </a>
                        </Card.Description>
                      
                    </Grid.Column>
                </Grid>
                 
                  
                </Card.Content>
              </Card>
            </div>

            <div>
              <Grid columns={3} relaxed>
                <Grid.Column>
                  <Segment basic><a>PAF History</a></Segment>
                </Grid.Column>
               
                <Grid.Column>
                  <Segment basic><a>SBU OKR</a></Segment>
                </Grid.Column>
             
                <Grid.Column>
                  <Segment basic><a>VGG OKR</a></Segment>
                </Grid.Column>
              </Grid>
            </div>

            <div className="w-100">
                <ProcessGraphComponent
                  chartData={chartData}
                  chartOptions={chartOptions}
                  fluid
                />
            </div>
            
          </div>




          <div className = "fl w-third pa4 tc">
             
            <Card className ="recentActivities mb4">
               <CardHeader className = "pa1 "> 
                <h3>Recent Activities</h3>
               </CardHeader> 
               <CardContent>
                 <p>This person is a staff of Venture Garden Group. She is reliable and trust worthy</p>

                 </CardContent>
            </Card>

            <Card className="pt4">
               <CardContent>
                 <h3>Previous PAF</h3>
                 <div className="pa4">
                    {extra}
                    <h1 >60%</h1>
                </div>

                 </CardContent>
            </Card>
          </div>
        </div>
    )
  }
}


export default StaffPageComponent
