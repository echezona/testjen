import React, {useState} from 'react';
import {Accordion, List, Icon, Header} from 'semantic-ui-react';

function WelcomeLabel(props) {
    const [activeIndex, setActiveIndex] = useState(-1);

    const handleClick = (e, titleProps) => {
        const { index } = titleProps;
        const newIndex = activeIndex === index ? -1 : index;
        setActiveIndex(newIndex);
      }

    return (
        <Accordion styled fluid style={{ backgroundColor: '#F2F2F2', marginBottom: '15px' }}>
          <Accordion.Title
            active={activeIndex === 0}
            index={0}
            onClick={handleClick}
            style={{ color: '#000000' }}
          >
            <Icon name='dropdown' />
            Welcome to the Set OKRs page. Here's what you need to know...
          </Accordion.Title>
          <Accordion.Content
            active={activeIndex === 0}
            style={{ color: '#000000' }}
          >
            <List bulleted>
                <List.Item>Use the form on the right of the page to set your OKRs</List.Item>
                <List.Item>Use the navigation menu at the left of the page to move between forms</List.Item>
                <List.Item>You need to have set an objective and selected an SBU objective to be able to SAVE!</List.Item>
                <List.Item>You must save entries for 3 Business Objectives as well as 1 Personal Objective</List.Item>
                <List.Item>All key results marked with "<span style={{ color: "red" }}>*</span>" (a red star) are compulsory and must be filled before you can CONTINUE TO SUMMARY!</List.Item>
            </List>
          </Accordion.Content>
        </Accordion>
    )
}

export default WelcomeLabel