import React, {Component} from 'react'
import {Card, Button, Segment} from 'semantic-ui-react'
import { CREATE_STAFF_APPRAISAL, FETCH_SBU_OBJECTIVES } from '../../constants';


class ExceptionHandler extends Component {
    componentDidMount(){
        const {dispatch} = this.props
        dispatch({ type: FETCH_SBU_OBJECTIVES })
    }

    onBegin = () => {
        // console.log("staffid in xcptnhndlr", this.props.staffID)
        const payload = { staffId: this.props.staffID, appraisalId: this.props.appraisalCycleID, sbuId: this.props.sbuId }
        console.log("see the payload you're posting", payload)
        // const payload = { staffID: "", appraisalCycleID: "" }
        this.props.dispatch({ type: CREATE_STAFF_APPRAISAL, payload })
    }


    render(){
        const messages = {
            NO_STAFF_APPRAISAL: "No Staff Appraisal exist yet",
            NO_STAFF_APPRAISAL_ALT: "There is no Staff Appraisal for the current Appraisal Cycle. Please create one first",
            STAFF_DOES_NOT_EXIST: "The Staff requesting this Staff Appraisal does not exist",
            SBU_OBJECTIVES_DO_NOT_EXIST: "No SBU Objective",
            //appraisalCycle
            NO_APPRAISAL_EXIST_YET: "No Appraisal exist yet",
        }
    
        const errorContentStyle = {
            color: "red"
        }

        let screenContent = "";

        console.log("apprisalCycle", this.props.appraisalCycle)

        const { appraisalCycle, sbuObjectives, appraisal, fetchStaffDetailsResponseStatus, saveStaffAppraisalLoadingStatus } = this.props

        //handle prepared messages 
        if(appraisalCycle.message === messages.NO_APPRAISAL_EXIST_YET) {
            screenContent = "An appraisal cycle period has not yet been set by People Ops"
        }else if(appraisalCycle.okrSettingLock === true) {
            screenContent = "The time window for setting OKRs has not been activated by PeopleOps hence you'll be unable to set your OKRs during this period"
        } else if(Object.prototype.toString.call(sbuObjectives) === '[object Object]' && sbuObjectives.message.includes(messages.SBU_OBJECTIVES_DO_NOT_EXIST)) {
            screenContent = "It appears that no SBU Objectives have been set for your SBU. Please refresh the page to see if it resolves and notify PeopleOps to rectify this if it doesn't."
        } else if(appraisal.message === messages.NO_STAFF_APPRAISAL || appraisal.message === messages.NO_STAFF_APPRAISAL_ALT) {
            screenContent = "Click the button below to begin."
        } else if(appraisal.message === messages.STAFF_DOES_NOT_EXIST) {
            screenContent = "It seems that you don't exist as a user on the PMS system. Please escalate this to PeopleOps to get it sorted."
        } else if(fetchStaffDetailsResponseStatus === 404) {
            screenContent = "It seems that you don't exist as a user on the PMS system. Please escalate this to PeopleOps to get it sorted."
        } else {
            screenContent = "Nothing to show"
        }

        //handle errors
        if(appraisal.error){
            screenContent = `An error occurred while creating your staff appraisal ${appraisal.error}. Please click the button to try again.`
        }


        return(
            <Segment basic padded>
                <Card centered>
                    <Card.Header as="h2" textAlign='center'>
                        WELCOME
                    </Card.Header>
                    <Card.Content>
                        <span style={appraisal.error ? errorContentStyle : null}>{screenContent}</span>
                    </Card.Content>
                    {   (appraisal.message === messages.NO_STAFF_APPRAISAL || appraisal.message === messages.NO_STAFF_APPRAISAL_ALT) && 
                        appraisalCycle.id && 
                        Object.prototype.toString.call(sbuObjectives) === '[object Array]' &&
                        !appraisalCycle.okrSettingLock &&
                        <Button color="blue" compact onClick={() => this.onBegin()} loading={saveStaffAppraisalLoadingStatus} disabled={saveStaffAppraisalLoadingStatus}>BEGIN</Button>
                    }
                </Card>
            </Segment>
        )
    }
}

// const ExceptionHandler = (props) => {

//     const messages = {
//         NO_STAFF_APPRAISAL: "No Staff Appraisal exist yet",
//         STAFF_DOES_NOT_EXIST: "The Staff requesting this Staff Appraisal does not exist",
//         SBU_OBJECTIVES_DO_NOT_EXIST: "No SBU Objective"
//     }

//     const errorContentStyle = {
//         color: "red"
//     }

//     const onBegin = () => {
//         // console.log("staffid in xcptnhndlr", props.staffID)
//         const payload = { staffId: props.staffID, appraisalId: props.appraisalCycleID, sbuId: props.sbuId }
//         console.log("see the payload you're posting", payload)
//         // const payload = { staffID: "", appraisalCycleID: "" }
//         props.dispatch({ type: CREATE_STAFF_APPRAISAL, payload })
//     }

//     let screenContent = "";

//     //handle prepared messages 
//     if(props.appraisal.message === messages.NO_STAFF_APPRAISAL) {
//         screenContent = "It seems you haven't started creating your staff appraisal yet. Click the button below to begin."
//     } else if(props.appraisal.message === messages.STAFF_DOES_NOT_EXIST) {
//         screenContent = "It seems that you don't exist as a user on the PMS system. Please escalate this to PeopleOps to get it sorted."
//     } else if(props.sbuObjectives.message.includes(messages.SBU_OBJECTIVES_DO_NOT_EXIST)) {
//         screenContent = "It appears that no SBU Objectives have been set for your SBU. Please refresh the page and notify PeopleOps to rectify this."
//     } else {
//         screenContent = "Nothing to show"
//     }

//     //handle errors
//     if(props.appraisal.error){
//         screenContent = `An error occurred while creating your staff appraisal ${props.appraisal.error}. Please click the button to try again.`
//     }

//     return (
//         <Segment basic padded>
//             <Card centered>
//                 <Card.Header as="h2" textAlign='center'>
//                     WELCOME
//                 </Card.Header>
//                 <Card.Content>
//                     <span style={props.appraisal.error ? errorContentStyle : null}>{screenContent}</span>
//                 </Card.Content>
//                 { props.appraisal.message === messages.NO_STAFF_APPRAISAL &&
//                     <Button color="blue" compact onClick={() => onBegin()}>BEGIN</Button>
//                 }
//             </Card>
//         </Segment>
//     )
// }

export default ExceptionHandler