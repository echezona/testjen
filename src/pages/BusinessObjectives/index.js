import React, { Component, Fragment } from 'react'
import { Button, Grid, Card, Header, Divider, Segment, Accordion, Message, Loader, Icon, Image, Modal } from 'semantic-ui-react'
import './style.css'
import BusinessObjectivesFormOne from '../../partials/businessObjectivesFormOne'
import BusinessObjectivesFormTwo from '../../partials/businessObjectivesFormTwo'
import BusinessObjectivesFormThree from '../../partials/businessObjectivesFormThree'
import BusinessObjectivesFormFour from '../../partials/businessObjectivesFormFour'
import BusinessObjectivesFormFive from '../../partials/businessObjectivesFormFive'
import PersonalObjectiveForm from '../../partials/personalObjectiveForm'
import { Formik } from 'formik'
import * as Yup from 'yup'
import { Link, withRouter, Switch, Route } from 'react-router-dom'
import { connect } from 'react-redux'
import { sbuObjectivesSelector, categorySelector, userObjectivesSelector, userIDsSelector, userKeyResultsSelector, userCategoriesSelector, staffIdSelector, staffAppraisalSelector, appraisalCycleIdSelector, sbuIdSelector, sbuNameSelector } from '../../reducer'

import { FETCH_SBU_OBJECTIVES, FETCH_CATEGORY, FETCH_STAFF_APPRAISAL, FETCH_APPRAISAL_CYCLE, FETCH_APPRAISAL_OBJECTIVES, FETCH_OBJECTIVES_AND_KEY_RESULTS, SAVE_DONE_MESSAGE_VERSION, FETCH_STAFF_DETAILS } from '../../constants';
import ExceptionHandler from './ExceptionHandler';
import {errorCodes} from '../../helper';
// import ReactModal from 'react-modal';
// import Dropzone from 'react-dropzone';
import WelcomeLabel from './WelcomeLabel';

const firstValidationSchema = Yup.object().shape({
  objective1: Yup.string().required('this field is required'),
  sbuObjectives1: Yup.string().required('You have to select an SBU objective'),
});

const secondValidationSchema = Yup.object().shape({
  objective2: Yup.string().required('this field is required'),
  sbuObjectives2: Yup.string().required('You have to select an SBU objective'),
});

const thirdValidationSchema = Yup.object().shape({
  objective3: Yup.string().required('this field is required'),
  sbuObjectives3: Yup.string().required('You have to select an SBU objective'),
});

const fourthValidationSchema = Yup.object().shape({
  objective4: Yup.string().required('this field is required'),
  sbuObjectives4: Yup.string().required('You have to select an SBU objective'),
});

const fifthValidationSchema = Yup.object().shape({
  objective5: Yup.string().required('this field is required'),
  sbuObjectives5: Yup.string().required('You have to select an SBU objective'),
});

const personalValidationSchema = Yup.object().shape({
  objective: Yup.string().required('this field is required'),
});

// const validationSchema = Yup.object().shape({
//   one: Yup.string().required('this field is required'),
//   two: Yup.string().required('this field is required'),
//   three: Yup.string().required('this field is required'),
//   objective: Yup.string().required('this field is required'),
//   sbuObjectives: Yup.string().required('You have to select an SBU objective'),
// });


class ObjectivesComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      // activeItem: props.location.pathname,
      currentOrder: "",
      uploadModalOpen: false,
    };
  }

  getOrder = (order) => {
    this.setState({
      currentOrder: order
    })
  }

  componentDidUpdate() {
    const { userStaffAppraisal, userStaffAppraisal: { staffSubmitted, firstApproved }, history, SAVING_OBJECTIVE_STATUS_MESSAGE_VERSION } = this.props
    console.log("staff", userStaffAppraisal)
    if (firstApproved !== undefined && (staffSubmitted || (firstApproved !== "pending"))) {
      history.replace("/summary")
    }

    // if(SAVING_OBJECTIVE_STATUS_MESSAGE_VERSION === true){
    //   document.getElementById('green').scrollIntoView({behavior: 'smooth'})
    // } else if (SAVING_OBJECTIVE_STATUS_MESSAGE_VERSION === false) {
    //   document.getElementById('red').scrollIntoView({behavior: 'smooth'})
    // }
    
    if(!this.state.currentOrder){
      this.handleDismiss();
    }

    console.log("sbutings", this.props.sbuObjectives)
  }

  componentDidMount() {
    const { dispatch } = this.props;
    // dispatch({ type: FETCH_SBU_OBJECTIVES })
    dispatch({ type: FETCH_CATEGORY })
    dispatch({ type: FETCH_STAFF_APPRAISAL })
    dispatch({ type: FETCH_APPRAISAL_CYCLE })
    dispatch({ type: FETCH_APPRAISAL_OBJECTIVES })
    dispatch({ type: FETCH_SBU_OBJECTIVES })
    // dispatch({ type: FETCH_OBJECTIVES_AND_KEY_RESULTS }) // currently being fetched in Auth and App component

    document.title = "PMS - Set OKRs" // set the document title in the browser
  }

  componentWillUnmount() {
    // console.log("please work on refresh")
    this.handleDismiss()
  }

  handleDismiss = () => {
    const { dispatch } = this.props;
    dispatch({ type: SAVE_DONE_MESSAGE_VERSION })
  }

  //   const { name } = e.target;
  //   this.setState({
  //     activeItem: name,
  //   })
  // }

  handleNavButtonClick = (e) => {
    const { name } = e.target;
    const { history } = this.props;
    // this.setState({
    //   activeItem: name,
    // })
    history.push(name)
  }

  handleUploadModalToggle = () => {
    this.setState((prevState) => ({ uploadModalOpen: !prevState.uploadModalOpen }))
  }

  render() {
    const { match, location, sbuObjectives, categories, userIDs, userObjectives, userKeyResults, userStaffAppraisal, SAVING_OBJECTIVE_STATUS, IS_REQUESTING_OKRs, IS_REQUESTING_SBUs, userCategories, SAVING_OBJECTIVE_STATUS_MESSAGE_VERSION, REQUESTING_SAVE_KEY_RESULTS, STAFF_APPRAISAL_IS_PRESENT, fetchStaffDetailsLoadingStatus, fetchStaffDetailsResponseStatus, fetchStaffAppraisalLoadingStatus } = this.props;
    const { currentOrder } = this.state;
    // const part1 = () => userIDs.filter(item => userObjectives[item].order === "1").map(item => userObjectives[item].keyResults).reduce((a,b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "1") ? userIDs.filter(item => userObjectives[item].order === "1").map(item => userObjectives[item].keyResults).reduce((a,b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "1").description : ''
    // console.log("a madness", part1())
    console.log("sbuObjectives", sbuObjectives,userObjectives)
    const preFirst = {
      one: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "1").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "1") ? userIDs.filter(item => userObjectives[item].order === "1").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "1").description : '')() : '',
      two: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "1").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "2") ? userIDs.filter(item => userObjectives[item].order === "1").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "2").description : '')() : "",
      three: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "1").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "3") ? userIDs.filter(item => userObjectives[item].order === "1").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "3").description : '')() : "",
      four: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "1").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "4") ? userIDs.filter(item => userObjectives[item].order === "1").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "4").description : '')() : "",
      five: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "1").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "5") ? userIDs.filter(item => userObjectives[item].order === "1").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "5").description : '')() : "",
      objective: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "1").map(item => userObjectives[item].description).find(item => item.constructor === String) ? userIDs.filter(item => userObjectives[item].order === "1").map(item => userObjectives[item].description).find(item => item.constructor === String) : '')() : "",
      sbuObjective: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "1").map(item => userObjectives[item].sbuObjectiveId).find(item => item.constructor === String) ? userIDs.filter(item => userObjectives[item].order === "1").map(item => userObjectives[item].sbuObjectiveId).find(item => item.constructor === String) : '')() : '',
    }

    const preSecond = {
      one: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "2").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "1") ? userIDs.filter(item => userObjectives[item].order === "2").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "1").description : '')() : '',
      two: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "2").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "2") ? userIDs.filter(item => userObjectives[item].order === "2").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "2").description : '')() : "",
      three: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "2").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "3") ? userIDs.filter(item => userObjectives[item].order === "2").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "3").description : '')() : "",
      four: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "2").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "4") ? userIDs.filter(item => userObjectives[item].order === "2").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "4").description : '')() : "",
      five: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "2").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "5") ? userIDs.filter(item => userObjectives[item].order === "2").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "5").description : '')() : "",
      objective: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "2").map(item => userObjectives[item].description).find(item => item.constructor === String) ? userIDs.filter(item => userObjectives[item].order === "2").map(item => userObjectives[item].description).find(item => item.constructor === String) : '')() : "",
      sbuObjective: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "2").map(item => userObjectives[item].sbuObjectiveId).find(item => item.constructor === String) ? userIDs.filter(item => userObjectives[item].order === "2").map(item => userObjectives[item].sbuObjectiveId).find(item => item.constructor === String) : '')() : '',
    }

    const preThird = {
      one: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "3").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "1") ? userIDs.filter(item => userObjectives[item].order === "3").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "1").description : '')() : '',
      two: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "3").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "2") ? userIDs.filter(item => userObjectives[item].order === "3").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "2").description : '')() : "",
      three: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "3").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "3") ? userIDs.filter(item => userObjectives[item].order === "3").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "3").description : '')() : "",
      four: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "3").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "4") ? userIDs.filter(item => userObjectives[item].order === "3").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "4").description : '')() : "",
      five: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "3").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "5") ? userIDs.filter(item => userObjectives[item].order === "3").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "5").description : '')() : "",
      objective: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "3").map(item => userObjectives[item].description).find(item => item.constructor === String) ? userIDs.filter(item => userObjectives[item].order === "3").map(item => userObjectives[item].description).find(item => item.constructor === String) : '')() : "",
      sbuObjective: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "3").map(item => userObjectives[item].sbuObjectiveId).find(item => item.constructor === String) ? userIDs.filter(item => userObjectives[item].order === "3").map(item => userObjectives[item].sbuObjectiveId).find(item => item.constructor === String) : '')() : '',
    }

    const preFourth = {
      one: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "4").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "1") ? userIDs.filter(item => userObjectives[item].order === "4").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "1").description : '')() : '',
      two: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "4").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "2") ? userIDs.filter(item => userObjectives[item].order === "4").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "2").description : '')() : "",
      three: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "4").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "3") ? userIDs.filter(item => userObjectives[item].order === "4").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "3").description : '')() : "",
      four: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "4").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "4") ? userIDs.filter(item => userObjectives[item].order === "4").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "4").description : '')() : "",
      five: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "4").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "5") ? userIDs.filter(item => userObjectives[item].order === "4").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "5").description : '')() : "",
      objective: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "4").map(item => userObjectives[item].description).find(item => item.constructor === String) ? userIDs.filter(item => userObjectives[item].order === "4").map(item => userObjectives[item].description).find(item => item.constructor === String) : '')() : "",
      sbuObjective: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "4").map(item => userObjectives[item].sbuObjectiveId).find(item => item.constructor === String) ? userIDs.filter(item => userObjectives[item].order === "4").map(item => userObjectives[item].sbuObjectiveId).find(item => item.constructor === String) : '')() : '',
    }

    const preFifth = {
      one: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "5").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "1") ? userIDs.filter(item => userObjectives[item].order === "5").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "1").description : '')() : '',
      two: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "5").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "2") ? userIDs.filter(item => userObjectives[item].order === "5").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "2").description : '')() : "",
      three: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "5").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "3") ? userIDs.filter(item => userObjectives[item].order === "5").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "3").description : '')() : "",
      four: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "5").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "4") ? userIDs.filter(item => userObjectives[item].order === "5").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "4").description : '')() : "",
      five: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "5").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "5") ? userIDs.filter(item => userObjectives[item].order === "5").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "5").description : '')() : "",
      objective: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "5").map(item => userObjectives[item].description).find(item => item.constructor === String) ? userIDs.filter(item => userObjectives[item].order === "5").map(item => userObjectives[item].description).find(item => item.constructor === String) : '')() : "",
      sbuObjective: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "5").map(item => userObjectives[item].sbuObjectiveId).find(item => item.constructor === String) ? userIDs.filter(item => userObjectives[item].order === "5").map(item => userObjectives[item].sbuObjectiveId).find(item => item.constructor === String) : '')() : '',
    }

    const prePersonal = {
      one: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "Personal").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "1") ? userIDs.filter(item => userObjectives[item].order === "Personal").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "1").description : '')() : '',
      two: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "Personal").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "2") ? userIDs.filter(item => userObjectives[item].order === "Personal").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "2").description : '')() : "",
      three: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "Personal").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "3") ? userIDs.filter(item => userObjectives[item].order === "Personal").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "3").description : '')() : "",
      four: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "Personal").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "4") ? userIDs.filter(item => userObjectives[item].order === "Personal").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "4").description : '')() : "",
      five: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "Personal").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "5") ? userIDs.filter(item => userObjectives[item].order === "Personal").map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).find(item => item.order === "5").description : '')() : "",
      objective: () => userObjectives !== undefined ? (() => userIDs.filter(item => userObjectives[item].order === "Personal").map(item => userObjectives[item].description).find(item => item.constructor === String) ? userIDs.filter(item => userObjectives[item].order === "Personal").map(item => userObjectives[item].description).find(item => item.constructor === String) : '')() : "",
    }

    const first = { one1: preFirst.one(), two1: preFirst.two(), three1: preFirst.three(), four1: preFirst.four(), five1: preFirst.five(), objective1: preFirst.objective(), sbuObjectives1: preFirst.sbuObjective() }
    const second = { one2: preSecond.one(), two2: preSecond.two(), three2: preSecond.three(), four2: preSecond.four(), five2: preSecond.five(), objective2: preSecond.objective(), sbuObjectives2: preSecond.sbuObjective() }
    const third = { one3: preThird.one(), two3: preThird.two(), three3: preThird.three(), four3: preThird.four(), five3: preThird.five(), objective3: preThird.objective(), sbuObjectives3: preThird.sbuObjective() }
    const fourth = { one4: preFourth.one(), two4: preFourth.two(), three4: preFourth.three(), four4: preFourth.four(), five4: preFourth.five(), objective4: preFourth.objective(), sbuObjectives4: preFourth.sbuObjective() }
    const fifth = { one5: preFifth.one(), two5: preFifth.two(), three5: preFifth.three(), four5: preFifth.four(), five5: preFifth.five(), objective5: preFifth.objective(), sbuObjectives5: preFifth.sbuObjective() }
    const personal = { one: prePersonal.one(), two: prePersonal.two(), three: prePersonal.three(), four: prePersonal.four(), five: prePersonal.five(), objective: prePersonal.objective() }

    //used by personal form until it is updated
    const values = { one: '', two: '', three: '', four: '', five: '', objective: '', sbuObjectives: '' }

    const fourEnabled = () => userObjectives !== undefined ? userIDs.map(item => userObjectives[item].categoryId).filter(item => userCategories[item].name === "Business").length >= 3 : false;
    const fiveEnabled = () => userObjectives !== undefined ? userIDs.map(item => userObjectives[item].categoryId).filter(item => userCategories[item].name === "Business").length >= 4 : false;
    const continueEnabled = () => userObjectives !== undefined ? userIDs.length >= 4 && userIDs.map(item => userObjectives[item].keyResults).every(item => item.length >= 3) && userIDs.map(item => userObjectives[item].order).includes("Personal") : false;
    const businessID = () => Array.isArray(categories) && categories.filter((item) => item.name === "Business")[0] ? categories.filter((item) => item.name === "Business")[0].id : null
    const personalID = () => Array.isArray(categories) && categories.filter((item) => item.name === "Personal")[0] ? categories.filter((item) => item.name === "Personal")[0].id : null
    // console.log(businessID(), personalID())    userObjectives[item].categoryId   userIDs.map(item => userObjectives[item].categoryId)

    if (fetchStaffAppraisalLoadingStatus || fetchStaffDetailsLoadingStatus || IS_REQUESTING_SBUs) {
      return <Loader active />
    }

    //!IMPORTANT
    // this.errorCodes = [404, 402, 401]

    if ((Object.prototype.toString.call(userStaffAppraisal) === '[object Object]' && userStaffAppraisal.message) ||
        (Object.prototype.toString.call(sbuObjectives) === '[object Object]' && sbuObjectives.message) ||
        this.props.appraisalCycle.okrSettingLock === true || errorCodes.some(code => code === fetchStaffDetailsResponseStatus) ) {
      console.log("staffid in businessobjectives", this.props.staffID)
      return (
        <ExceptionHandler appraisal={userStaffAppraisal} staffID={this.props.staffID} 
                          appraisalCycleID={this.props.appraisalCycleID}  sbuId={this.props.sbuId} 
                          fetchStaffDetailsResponseStatus={fetchStaffDetailsResponseStatus}
                          // sbuObjectives={sbuObjectives}
                          {...this.props}

        />
      )
    }

    return (
      <Fragment>
        {/* <ReactModal
          isOpen={this.state.uploadModalOpen}
          onRequestClose={this.handleUploadModalToggle}
          style={{
            overlay: {backgroundColor: 'rgba(0, 0, 0, 0.75)'},
            content: {
              top: '100px',
              left: '100px',
              right: '100px',
              bottom: '100px',
            },
          }}
        >
          <Header as='h2' content='Upload OKRs'/>
          <div style={{ display: 'flex', justifyContent: 'center', marginBottom: '30px' }}>
            <Button icon='download' content='Download Excel Template' color='green'/>
          </div>
          <Dropzone
            accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel, text/csv"
            onDrop={this.onDrop}
            // {...rest}
            className="w-100 h7 dropzone-container"
            activeClassName="bg-light-green"
            rejectClassName="bg-light-red"
            disableClick={this.state.currLevel === ''}
            name='formFiles'
          >
            {({ isDragActive, isDragReject, acceptedFiles, rejectedFiles }) => {
                return (
                    <Fragment>
                        <Icon name="upload" size="huge"></Icon>
                        <Header as='h3'
                            content={acceptedFiles.length || rejectedFiles.length
                                ? `Accepted ${acceptedFiles.length}, rejected ${rejectedFiles.length} files`
                                : 'Upload OKR file here.'
                            }
                            subheader={acceptedFiles.length || rejectedFiles.length
                                ? null
                                : 'Drag and drop, or click to select'
                            }
                        />
                    </Fragment>
                )
            }}
          </Dropzone>
        </ReactModal> */}
        
        <WelcomeLabel/>
        
        {/* <Message style={{marginTop: 0, cursor: 'pointer'}} color="green" onClick={this.handleUploadModalToggle}>
          <Icon name='file excel' size='large'/>
          <b>Click here to upload your OKRs as an excel sheet</b>
        </Message> */}
        
        <Grid>
          <Grid.Column computer={6} mobile={16}>
            <Grid>
              <Grid.Row>
                <Grid.Column computer={16} tablet={8}>
                  <Card centered >
                    <Header as="h3" content="SBU Objectives" style={{marginLeft: "15px", marginTop: "15px"}} subheader={`(Total: ${sbuObjectives.length})`}/>
                    <Card.Content style={{maxHeight: "200px", overflowY: "scroll"}}>
                      {IS_REQUESTING_SBUs &&
                        <div>
                          <br />
                          <br />
                          <Loader active />
                        </div>
                      }
                      {!IS_REQUESTING_SBUs &&
                        sbuObjectives.map((item, index) => <Header key={item.id} as="h5" content={`${index + 1}. ${item.description}`} />)
                      }
                      {/* filter(item => item.sbuName === this.props.sbuName). */}
                    </Card.Content>
                  </Card>
                  <br />
                </Grid.Column>
                <Grid.Column computer={16} tablet={8}>
                  <Card centered>
                    <Card.Content textAlign="center">
                      <Header as="h3" content="Navigation Menu" dividing />
                      <Button.Group vertical>
                        <Button name={`${match.path}`} disabled={REQUESTING_SAVE_KEY_RESULTS || IS_REQUESTING_OKRs} onClick={this.handleButtonClick} onClick={(e) => this.handleNavButtonClick(e)} color={this.props.location.pathname === `${match.path}` ? "blue" : null}>Business Objective 1</Button>
                        <Button name={`${match.path}/two`} disabled={REQUESTING_SAVE_KEY_RESULTS || IS_REQUESTING_OKRs} onClick={this.handleButtonClick} onClick={(e) => this.handleNavButtonClick(e)} color={this.props.location.pathname === `${match.path}/two` ? "blue" : null}>Business Objective 2</Button>
                        <Button name={`${match.path}/three`} disabled={REQUESTING_SAVE_KEY_RESULTS || IS_REQUESTING_OKRs} onClick={this.handleButtonClick} onClick={(e) => this.handleNavButtonClick(e)} color={this.props.location.pathname === `${match.path}/three` ? "blue" : null}>Business Objective 3</Button>
                        <Button name={`${match.path}/four`} disabled={!fourEnabled() || REQUESTING_SAVE_KEY_RESULTS || IS_REQUESTING_OKRs} onClick={(e) => this.handleNavButtonClick(e)} color={this.props.location.pathname === `${match.path}/four` ? "blue" : null}>Business Objective 4</Button>
                        <Button name={`${match.path}/five`} disabled={!fiveEnabled() || REQUESTING_SAVE_KEY_RESULTS || IS_REQUESTING_OKRs} onClick={(e) => this.handleNavButtonClick(e)} color={this.props.location.pathname === `${match.path}/five` ? "blue" : null}>Business Objective 5</Button>
                      </Button.Group>
                      <Divider />
                      <Button name={`${match.path}/personal`} disabled={REQUESTING_SAVE_KEY_RESULTS || IS_REQUESTING_OKRs} onClick={(e) => this.handleNavButtonClick(e)} color={this.props.location.pathname === `${match.path}/personal` ? "blue" : ""}>Personal Development Objective</Button>
                    </Card.Content>
                  </Card>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Grid.Column>

          <Grid.Column computer={10} mobile={16}>
            {/* <Header as="h3" content="Set Objectives and Key Results" textAlign="center" /> */}
            <Segment>
              {SAVING_OBJECTIVE_STATUS_MESSAGE_VERSION === true &&
                <Message id="green" color="green" visible={SAVING_OBJECTIVE_STATUS_MESSAGE_VERSION === true} onDismiss={this.handleDismiss}>
                  {currentOrder >= 1 && currentOrder <= 5 && `Successfully saved Business Objective ${currentOrder}`}
                  {currentOrder === "Personal" && `Successfully saved ${currentOrder} Objective`}
                </Message>
              }
              {SAVING_OBJECTIVE_STATUS_MESSAGE_VERSION === false &&
                <Message id="red" color="red" visible={SAVING_OBJECTIVE_STATUS_MESSAGE_VERSION === false} onDismiss={this.handleDismiss}>
                  {currentOrder >= 1 && currentOrder <= 5 && `Save failed for Business Objective ${currentOrder}. Please refresh the browser or logout and log back in`}
                  {currentOrder === "Personal" && `Save failed for ${currentOrder} Objective. Please refresh the browser or logout and log back in`}
                </Message>
              }
              {/* { IS_REQUESTING_OKRs &&
                <div>
                  <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                  <Loader active/>
                </div>
              } */}

              <Switch>
                <Route exact path={`${match.path}`}
                  render={props => <Formik {...props} initialValues={first} enableReinitialize={true} validationSchema={firstValidationSchema} onSubmit={this.submit} render={props => <BusinessObjectivesFormOne history={this.props.history} getOrder={(order) => this.getOrder(order)} order="1" categoryId={businessID()} {...props} />} />} />
                <Route path={`${match.path}/two`}
                  render={props => <Formik {...props} initialValues={second} enableReinitialize={true} validationSchema={secondValidationSchema} onSubmit={this.submit} render={props => <BusinessObjectivesFormTwo history={this.props.history} getOrder={(order) => this.getOrder(order)} order="2" categoryId={businessID()} {...props} />} />} />
                <Route path={`${match.path}/three`}
                  render={props => <Formik {...props} initialValues={third} enableReinitialize={true} validationSchema={thirdValidationSchema} onSubmit={this.submit} render={props => <BusinessObjectivesFormThree history={this.props.history} getOrder={(order) => this.getOrder(order)} order="3" categoryId={businessID()} {...props} />} />} />
                <Route path={`${match.path}/four`}
                  render={props => <Formik {...props} initialValues={fourth} enableReinitialize={true} validationSchema={fourthValidationSchema} onSubmit={this.submit} render={props => <BusinessObjectivesFormFour history={this.props.history} getOrder={(order) => this.getOrder(order)} order="4" categoryId={businessID()} {...props} />} />} />
                <Route path={`${match.path}/five`}
                  render={props => <Formik {...props} initialValues={fifth} enableReinitialize={true} validationSchema={fifthValidationSchema} onSubmit={this.submit} render={props => <BusinessObjectivesFormFive history={this.props.history} getOrder={(order) => this.getOrder(order)} order="5" categoryId={businessID()} {...props} />} />} />
                <Route path={`${match.path}/personal`}
                  render={props => <Formik {...props} initialValues={personal} enableReinitialize={true} validationSchema={personalValidationSchema} onSubmit={this.submit} render={props => <PersonalObjectiveForm history={this.props.history} getOrder={(order) => this.getOrder(order)} order="Personal" categoryId={personalID()} {...props} />} />} />
              </Switch>


              {/* continue to Summary button */}
              <br />
              {/* {! IS_REQUESTING_OKRs && */}
              <Segment basic textAlign="right">
                {/* <Button content="Continue to Summary" name="/summary" disabled={!continueEnabled() || IS_REQUESTING_OKRs} onClick={this.handleNavButtonClick} /> */}
                <Button icon labelPosition='right' color='blue' name="/summary" disabled={!continueEnabled() || IS_REQUESTING_OKRs} onClick={this.handleNavButtonClick}>
                  Continue to Summary
                  <Icon name='right arrow' />
                </Button>
                {/* expecting a minimum of 4 ids such that one is the personal id and also each obj has at least 3 keyresults */}
              </Segment>
            </Segment>
          </Grid.Column>
        </Grid>
      </Fragment>
    )
  }
}


function mapStateToProps(state) {
  return {
    appraisalCycle: state.requestingAppraisalCycle,
    sbuObjectives: sbuObjectivesSelector(state),
    categories: categorySelector(state),
    userIDs: userIDsSelector(state),
    userObjectives: userObjectivesSelector(state),
    userKeyResults: userKeyResultsSelector(state),
    userCategories: userCategoriesSelector(state),
    userStaffAppraisal: staffAppraisalSelector(state),
    SAVING_OBJECTIVE_STATUS: state.SAVING_OBJECTIVE_STATUS,
    SAVING_OBJECTIVE_STATUS_MESSAGE_VERSION: state.SAVING_OBJECTIVE_STATUS_MESSAGE_VERSION,
    IS_REQUESTING_OKRs: state.requestingOKRs,
    IS_REQUESTING_SBUs: state.requestingSBUs,
    REQUESTING_SAVE_KEY_RESULTS: state.REQUESTING_SAVE_KEY_RESULTS,
    STAFF_APPRAISAL_IS_PRESENT: state.requestingStaffAppraisalExistence,
    // sbuName: sbuNameSelector(state),
    fetchStaffDetailsLoadingStatus: state.fetchLoadingReducer.FETCH_STAFF_DETAILS_LOADING_STATUS,
    fetchStaffAppraisalLoadingStatus: state.fetchLoadingReducer.FETCH_STAFF_APPRAISAL_LOADING_STATUS,
    //the following are for the ExceptionHandler component
    staffID: staffIdSelector(state),
    appraisalCycleID: appraisalCycleIdSelector(state),
    sbuId: sbuIdSelector(state),
    saveStaffAppraisalLoadingStatus: state.saveLoadingReducer.SAVE_STAFF_APPRAISAL_LOADING_STATUS,
    fetchStaffDetailsResponseStatus: state.responseStatusReducer.FETCH_STAFF_DETAILS_RESPONSE_STATUS,
  }
}

export default withRouter(connect(mapStateToProps)(ObjectivesComponent))