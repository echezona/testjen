import React from 'react';
import { Button, Header, Segment, Form, Icon, Label } from 'semantic-ui-react';
import { connect } from "react-redux"
import './index.css'
import { CHANGE_PASSWORD } from '../../constants';

class ChangePassword extends React.Component {
    state = {
        oldPassword: '',
        newPassword1: '',
        newPassword2: '',
        validPasswordEnteredTwice: null,
        oldPasswordValid: null, 
        newPassword1Valid: null, 
        newPassword2Valid: null, 
    }

    componentDidUpdate(){
        
        console.log("change", this.props.changePasswordLoadingStatus)
    }

    handleChange = (e) => {
        const { name, value } = e.target
        const { newPassword1, newPassword2 } = this.state
        const validStateKey = name + "Valid"
        const validPasswordRegex = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/;
        this.setState({ 
            [name]: value,
            [validStateKey]: validPasswordRegex.test(value)
        })
        if(name === "newPassword2" && newPassword1){
            this.setState({ validPasswordEnteredTwice: value === this.state.newPassword1 })
        }
        if(name === "newPassword1" && newPassword2){
            this.setState({ validPasswordEnteredTwice: value === this.state.newPassword2 })
        }
    }

    onPasswordChange = () => {
        const { dispatch } = this.props
        const { oldPassword, newPassword2 } = this.state
        console.log("in here", dispatch)
        dispatch({ type: CHANGE_PASSWORD, currentPassword: oldPassword, newPassword: newPassword2 })
    }

    render() {
        const { oldPassword, newPassword1, newPassword2, oldPasswordValid, newPassword1Valid, newPassword2Valid, validPasswordEnteredTwice } = this.state 
        const { changePasswordLoadingStatus, CHANGE_PASSWORD_STATUS  } = this.props
        return (
            <section className='change-password-page-container'>
                <Header as='h1' content='CHANGE PASSWORD' className='header' />
                <Segment className='change-password-container'>
                    <Form>
                        <label htmlFor="oldPassword">
                            Please enter your <b>current</b> password:
                            <input name='oldPassword' value={oldPassword} onChange={(e) => this.handleChange(e)} type="password"/>
                        </label>
                        {oldPasswordValid === false &&
                            <span className="indicator"><Icon name='info circle' color='red' />Your password needs to be a minimum of 6 characters and include a combination of at least 1 Uppercase letter, 1 lowercase letter, 1 special character (@ $ ! % * ? & etc) and 1 number</span>
                        }
                        <br/><br/>
                        <label htmlFor="newPassword1">
                            Please enter your <b>new</b> password:
                            <input name='newPassword1' value={newPassword1} onChange={(e) => this.handleChange(e)} type="password" />
                        </label>
                        {newPassword1Valid === false &&
                            <span className="indicator"><Icon name='info circle' color='red' />Your password needs to be a minimum of 6 characters and include a combination of at least 1 Uppercase letter, 1 lowercase letter, 1 special character (@ $ ! % * ? & etc) and 1 number</span>
                        }
                        <br/><br/>
                        <label htmlFor="newPassword2">
                            Please enter your <b>new</b> password <b>again</b>:
                            <input name='newPassword2' value={newPassword2} onChange={(e) => this.handleChange(e)} type="password" />
                        </label>
                        {newPassword2Valid === false &&
                            <span className="indicator"><Icon name='info circle' color='red' />Your password needs to be a minimum of 6 characters and include a combination of at least 1 Uppercase letter, 1 lowercase letter, 1 special character (@ $ ! % * ? & etc) and 1 number</span>
                        }
                        <br/>
                        {validPasswordEnteredTwice === false &&
                            <span className="indicator"><Icon name='info circle' color='red' />This password does not match the first one you entered</span>
                        }
                    </Form>
                    <br /><br />
                    <div className="cp-button-container">
                        <Button
                            loading={changePasswordLoadingStatus}
                            disabled={!validPasswordEnteredTwice || changePasswordLoadingStatus}
                            // size='huge'
                            color="green"
                            labelPosition="right"
                            icon="arrow right"
                            content="CHANGE PASSWORD"
                            onClick={() => this.onPasswordChange()}
                        />
                        {CHANGE_PASSWORD_STATUS && CHANGE_PASSWORD_STATUS.status === true &&
                            <Label pointing='above' color="blue" >{CHANGE_PASSWORD_STATUS.message}</Label>
                        }
                        {CHANGE_PASSWORD_STATUS && CHANGE_PASSWORD_STATUS.status === false &&
                            <Label pointing='above' color="red" >Save Failed. Please try again or refresh page.</Label>
                        }
                    </div>
                </Segment>
            </section>
        )
    }
}

function mapStateToProps(state){
    return {
        changePasswordLoadingStatus: state.saveLoadingReducer.CHANGE_PASSWORD_LOADING_STATUS,
        CHANGE_PASSWORD_STATUS: state.CHANGE_PASSWORD_STATUS,
    }
}

export default connect(mapStateToProps)(ChangePassword)