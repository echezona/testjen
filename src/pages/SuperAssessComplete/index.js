import React, { Component } from 'react'
import { Grid, Segment, Header, Form, Label, Icon, Button, Popup, Message } from 'semantic-ui-react'
import { FETCH_LMPO_OKRS, FETCH_CATEGORY, FETCH_SBU_OBJECTIVES, FETCH_LINE_MANAGER_STAFF_APPRAISALS, FETCH_RECOMMENDATION_TYPES, FETCH_BEHAVIORAL_INDICATORS, FETCH_RECOMMENDATION, SUBMIT_LINE_MANAGER_ASSESSMENT, SUBMIT_SUPER_LINE_MANAGER_ASSESSMENT } from '../../constants';
import PageInfo from '../../components/PageInfo';


function SuperAssessComplete(props) {

    const { dispatch, history, routeState, staffId, staffAppraisalId, lmIDs, lmObjectives, lmKeyResults, behavioralIndicators, recommendationObject, SUBMIT_LINE_MANAGER_ASSESSMENT_STATUS, completeSubmitLoadingStatus } = props

    const handleSubmit = () => {

        const keyResultWasDisapproved = lmIDs.map(item => lmObjectives[item].keyResults)
                                .reduce((a, b) => a.concat(b), [])
                                .map(item => lmKeyResults[item])
                                .filter(item => item.superLineManagerStatus === "disapproved").length > 0 

        const behaviorWasDisapproved = behavioralIndicators.some(item => item.superLineManagerStatus === 'disapproved')


        let lineManagerTwoApproved;
        let superLineManagerStatus;

        if( keyResultWasDisapproved || behaviorWasDisapproved) {
            superLineManagerStatus = "awaiting resubmission"
            lineManagerTwoApproved = "disapproved"
        } else {
            superLineManagerStatus = "completed"
            lineManagerTwoApproved = "approved"
        }

        console.log("dets", superLineManagerStatus, lineManagerTwoApproved, keyResultWasDisapproved, behaviorWasDisapproved, behavioralIndicators)

        const data = {
            staffAppraisalId,
            staffId,
            lineManagerTwoApproved,
            superLineManagerStatus
        }

        console.log("datry", data)
        dispatch({ type: SUBMIT_SUPER_LINE_MANAGER_ASSESSMENT, data, history })
    }
    
    const doneWithPerformance = lmIDs.map(item => lmObjectives[item].keyResults)    
                                    .reduce((acc, item) => acc.concat(item), [])
                                    .map(item => lmKeyResults[item])
                                    .every(item => item.superLineManagerStatus === "disapproved" ? item.superLineManagerComment : item.superLineManagerStatus !== "pending")

    const doneWithBehaviour = behavioralIndicators.every(item => item.superLineManagerStatus)

    const doneWithRecommendations = Object.prototype.toString.call(recommendationObject) === '[object Object]' && recommendationObject.approvalStatus


    //Score Values start here
    const staffKeyResults = lmIDs.map(item => lmObjectives[item].keyResults)
                                .reduce((acc, item) => acc.concat(item), [])
                                .map(item => lmKeyResults[item])

    const staffPerformanceSum = staffKeyResults.reduce((acc, curr) => acc + curr.staffRating, 0)

    const staffPerformanceScore = ((staffPerformanceSum / (staffKeyResults.length * 5)) * 100)

    const staffBehaviorSum = behavioralIndicators.reduce((acc, curr) => acc + curr.staffRating,  0)

    const staffBehaviorScore = ((staffBehaviorSum / (behavioralIndicators.length * 5)) * 100)

    const staffTotalScore = ((staffPerformanceScore / 10) * 7) + ((staffBehaviorScore / 10) * 3)

    const lineManagerPerformanceSum = staffKeyResults.reduce((acc, curr) => acc + curr.lineManagerRating, 0)

    const lineManagerPerformanceScore = ((lineManagerPerformanceSum / (staffKeyResults.length * 5)) * 100)

    const lineManagerBehaviorSum = behavioralIndicators.reduce((acc, curr) => acc + curr.lineManagerRating,  0)

    const lineManagerBehaviorScore = ((lineManagerBehaviorSum / (behavioralIndicators.length * 5)) * 100)

    const lineManagerTotalScore = ((lineManagerPerformanceScore / 10) * 7) + ((lineManagerBehaviorScore / 10) * 3)


    const keyResultWasDisapproved = lmIDs.map(item => lmObjectives[item].keyResults)
                                        .reduce((a, b) => a.concat(b), [])
                                        .map(item => lmKeyResults[item])
                                        .filter(item => item.superLineManagerStatus === "disapproved").length > 0 

    const behaviorWasDisapproved = behavioralIndicators.some(item => item.superLineManagerStatus === 'disapproved')




    console.log('recommendObject', recommendationObject, doneWithPerformance)

    return (
        <Grid columns={16} padded>
            <Grid.Row>
                <PageInfo
                    title='Click To View/Hide Assessment Scores' 
                    content={
                        <Grid divided columns={2}>
                            <Grid.Column width={8}>
                                <Header as='h4' content="Staff"/>
                                <div>Performance: <span style={{fontWeight: "bold"}}>{staffPerformanceScore.toFixed(1) + '%'}</span></div>
                                <div>Behavior: <span style={{fontWeight: "bold"}}>{staffBehaviorScore.toFixed(1) + '%'}</span></div>
                                <div style={{marginTop: "10px", fontSize: "1.3em"}}>Total: <span style={{fontWeight: "bold"}}>{staffTotalScore.toFixed(1) + '%'}</span></div>
                            </Grid.Column>
                            <Grid.Column width={8}>
                                <Header as='h4' content="Line Manager"/>
                                <div >Performance: <span style={{fontWeight: "bold"}}>{lineManagerPerformanceScore.toFixed(1) + '%'}</span></div>
                                <div>Behavior: <span style={{fontWeight: "bold"}}>{lineManagerBehaviorScore.toFixed(1) + '%'}</span></div>
                                <div style={{marginTop: "10px", fontSize: "1.3em"}}>Total: <span style={{fontWeight: "bold"}}>{lineManagerTotalScore.toFixed(1) + '%'}</span></div>
                            </Grid.Column>
                        </Grid>
                    }
                />
            </Grid.Row>
            <Grid.Row>
                <Grid.Column width={3}></Grid.Column>
                <Grid.Column width={10}>
                    <Segment >
                        <Header as="h4" content="Assessment Progress" />
                        <Form>
                            <Form.Field>
                                Performance Assessment: &nbsp;
                                    <Label color={doneWithPerformance ? 'green' : 'red'}><Icon name={doneWithPerformance ? 'check circle' : 'close'} />  {doneWithPerformance ? 'COMPLETE' : 'INCOMPLETE'}</Label>
                            </Form.Field>
                            <Form.Field>
                                Behavioral Assessment: &nbsp;
                                    <Label color={doneWithBehaviour ? 'green' : 'red'}><Icon name={doneWithBehaviour ? 'check circle' : 'close'} />  {doneWithBehaviour ? 'COMPLETE' : 'INCOMPLETE'}</Label>
                            </Form.Field>
                            <Form.Field>
                                Recommendation: &nbsp;
                                    <Label color={doneWithRecommendations ? 'green' : 'red'}><Icon name={doneWithRecommendations ? 'check circle' : 'close'} />  {doneWithRecommendations ? 'COMPLETE' : 'INCOMPLETE'}</Label>
                            </Form.Field>
                        </Form>
                        <br />
                        <Segment basic textAlign='right'>
                            {/* { SUBMIT_LINE_MANAGER_ASSESSMENT_STATUS === true && 
                                <Label pointing='right' color="green" >Submission Successful</Label>
                            }
                            { SUBMIT_LINE_MANAGER_ASSESSMENT_STATUS === false && 
                                <Label pointing='right' color="red" >Submission Failed. Please try again or refresh page.</Label>
                            }
                            <Button 
                                content='Submit' 
                                color='blue'
                                onClick={handleSubmit}
                                disabled={!doneWithPerformance || !doneWithBehaviour || !doneWithRecommendations || completeSubmitLoadingStatus} 
                                loading={completeSubmitLoadingStatus}
                            /> */}

                            <Popup trigger={<Button color='blue'>View Scores and Submit</Button>} on='click' hoverable>
                                {/* warning message */}
                                {/* <Message warning style={{ marginBottom: '10px' }}>
                                    <Icon name='warning sign' style={{ color: '#ffa500' }}/> <span style={{ fontSize: '0.9em', fontWeight: 'bold' }}>Ensure that you save all changes made before submitting!</span>
                                </Message> */}
                                <Header as='h4'>Assessment Scores</Header>
                                <p>Performance: {lineManagerPerformanceScore.toFixed(1)}</p>
                                <p>Behavior: {lineManagerBehaviorScore.toFixed(1)}</p>
                                <p style={{fontWeight: 'bold', fontSize: '1.2em'}}>Total: {lineManagerTotalScore.toFixed(1)}</p>
                                <br/>
                                <Message>
                                    <Message.Header content='Approval Status Brief'/>
                                    <Message.List>
                                        <Message.Item>
                                            { keyResultWasDisapproved && 
                                                <span>You <span style={{ color: 'red' }}>disapproved</span> some <b>key result ratings</b></span>
                                            } 
                                            { !keyResultWasDisapproved &&
                                                <span>You <span style={{ color: 'green' }}> approved </span> all <b>key result ratings</b></span>
                                            }
                                        </Message.Item>
                                        <Message.Item>
                                            { behaviorWasDisapproved && 
                                                <span>You <span style={{ color: 'red' }}>disapproved</span>  some <b>behaviour ratings</b></span>
                                            } 
                                            { !behaviorWasDisapproved &&
                                                <span>You <span style={{ color: 'green' }}> approved </span>  all <b>behaviour ratings</b></span>
                                            }
                                        </Message.Item>
                                    </Message.List>
                                </Message>
                                {/* <div style={{fontWeight: 'bold', fontSize: '0.85em'}}>Are you sure you want to submit?</div> */}
                                <div>
                                    { SUBMIT_LINE_MANAGER_ASSESSMENT_STATUS === true && 
                                        <Label color="green" >Submission Successful</Label>
                                    }
                                    { SUBMIT_LINE_MANAGER_ASSESSMENT_STATUS === false && 
                                        <Label color="red" >Submission Failed. Please try again or refresh page.</Label>
                                    }
                                </div>
                                <div style={{marginTop: '5px', display: 'flex', justifyContent: 'center'}}>
                                    <Button content={ keyResultWasDisapproved || behaviorWasDisapproved ? 'Send back to Line Manager' : 'Send to People Ops' }
                                        onClick={handleSubmit} 
                                        disabled={!doneWithPerformance || !doneWithBehaviour || !doneWithRecommendations || completeSubmitLoadingStatus}
                                        color='blue' basic
                                        loading={completeSubmitLoadingStatus}
                                    />
                                </div>
                            </Popup>
                        </Segment>
                    </Segment>
                </Grid.Column>
            </Grid.Row>
        </Grid>
    )
}

// class SuperAssessComplete extends Component {

//     handleSubmit = () => {
//         const { dispatch, history, routeState, staffId } = this.props

//         console.log("props?", routeState.staffAppraisalId, this.props.staffId)
//         const data = {
//             staffAppraisalId: routeState.staffAppraisalId,
//             staffId,
//             firstApproved: 'approved',
//         }

//         dispatch({ type: SUBMIT_LINE_MANAGER_ASSESSMENT, data, history })
//     }

//     render() {
//         const { lmIDs, lmObjectives, lmKeyResults, behavioralIndicators, recommendationObject } = this.props
//         const doneWithPerformance = lmIDs.map(item => lmObjectives[item].keyResults)
//             .reduce((acc, item) => acc.concat(item), [])
//             .map(item => lmKeyResults[item])
//             .every(item => item.lineManagerRating > 0 && item.lineManagerComment)

//         const doneWithBehaviour = behavioralIndicators.every(item => item.lineManagerRating > 0 && item.lineManagerComment)

//         const doneWithRecommendations = Object.prototype.toString.call(recommendationObject) === '[object Object]' && recommendationObject.id

//         console.log('recommendObject', recommendationObject)

//         return (
//             <Grid columns={16}>
//                 <Grid.Column width={3}></Grid.Column>
//                 <Grid.Column width={10}>
//                     <Segment >
//                         <Header as="h4" content="Assessment Progress" />
//                         <Form>
//                             <Form.Field>
//                                 Performance Assessment: &nbsp;
//                                     <Label color={doneWithPerformance ? 'green' : 'red'}><Icon name={doneWithPerformance ? 'check circle' : 'close'} />  {doneWithPerformance ? 'COMPLETE' : 'INCOMPLETE'}</Label>
//                             </Form.Field>
//                             <Form.Field>
//                                 Behavioral Assessment: &nbsp;
//                                     <Label color={doneWithBehaviour ? 'green' : 'red'}><Icon name={doneWithBehaviour ? 'check circle' : 'close'} />  {doneWithBehaviour ? 'COMPLETE' : 'INCOMPLETE'}</Label>
//                             </Form.Field>
//                             <Form.Field>
//                                 Recommendations: &nbsp;
//                                     <Label color={doneWithRecommendations ? 'green' : 'red'}><Icon name={doneWithRecommendations ? 'check circle' : 'close'} />  {doneWithRecommendations ? 'COMPLETE' : 'INCOMPLETE'}</Label>
//                             </Form.Field>
//                         </Form>
//                         <br />
//                         <Container textAlign='right'>
//                             <Button content='Submit' onClick={this.handleSubmit} disabled={!doneWithPerformance || !doneWithBehaviour || !doneWithRecommendations} />
//                         </Container>
//                     </Segment>
//                 </Grid.Column>
//             </Grid>
//         )
//     }
// }


export default SuperAssessComplete