//Components
import React, {Fragment} from 'react';
import { Grid, Loader, Message, Button, Segment, Header } from 'semantic-ui-react';
import PageHeader from './Header'
import _ from 'lodash'
import { connect } from "react-redux"
import { withRouter, Switch, Route } from 'react-router-dom'
import { userObjectivesSelector, userIDsSelector, userKeyResultsSelector, userCategoriesSelector, lmIDsSelector, lmCategoriesSelector, lmStaffAppraisalsSelector, lmObjectivesSelector, lmKeyResultsSelector, categorySelector, appraisalCycleIdSelector, lmSbuObjectivesSelector } from '../../reducer'
import queryString from 'query-string'
// Css Files
import './index.css';
//Constatnts
import { FETCH_LINE_MANAGER_OKRS, FETCH_CATEGORY, FETCH_LINE_MANAGER_STAFF_APPRAISALS, SUBMIT_LINE_MANAGER_OKRS, FETCH_LMPO_OKRS, STORE_PENDING_KEY_RESULTS, FETCH_SBU_OBJECTIVES, FETCH_BEHAVIORAL_INDICATORS, FETCH_RECOMMENDATION_TYPES, FETCH_RECOMMENDATION } from '../../constants';
import AssessPerformance from '../AssessPerformance';
import AssessBehaviour from '../AssessBehaviour';
import AssessRecommendations from '../AssessRecommendations';
import AssessComplete from '../AssessComplete';
import ErrorBoundary from '../ErrorBoundary';
import PageInfo from '../../components/PageInfo';



class AssessOKRs extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            showSubmit: false,
            updatePage: false,
            routeState: this.props.location.state
        }
    }

    componentDidMount() {
        const { dispatch, match, location, history } = this.props;
        const queryValues = queryString.parse(location.search)
        // console.log("what is this", queryValues)

        dispatch({ type: FETCH_LMPO_OKRS, staffId: `?staffId=${queryValues.staffId}` })
        dispatch({ type: FETCH_CATEGORY })
        dispatch({ type: FETCH_SBU_OBJECTIVES})
        dispatch({ type: FETCH_LINE_MANAGER_STAFF_APPRAISALS })
        dispatch({ type: FETCH_RECOMMENDATION_TYPES })
        dispatch({ type: FETCH_BEHAVIORAL_INDICATORS, LM_DR_STAFF_ID: queryValues.staffId})  //LM_DR_STAFF_ID = Line Manager's Direct Report Staff ID
        dispatch({ type: FETCH_RECOMMENDATION, staffId: queryValues.staffId})  


        document.title = "PMS - Assess OKRs" // set the document title in the browser

        if(location.pathname.includes('performance') === false && location.pathname.includes('behaviour') === false && location.pathname.includes('recommendation') === false) {
            history.push({
                pathname: `${match.path}/assessperformance`,
                search: `?staffId=${queryValues.staffId}`,
                state: location.state,
            })
        }
    }


    showSubmit = (x) => {
        this.setState({
            showSubmit: x
        })
    }

    iUpdated = () => {
        this.forceUpdate();
    }

    onPerformanceClick = () => {
        const {history, match } = this.props
        const queryValues = queryString.parse(this.props.location.search)
        // history.push(`${match.path}/assessperformance?staffId=${queryValues.staffId}`)
        history.push({
            pathname: `${match.path}/assessperformance`,
            search: `?staffId=${queryValues.staffId}`,
            state: this.props.location.state,
        })
    }

    onBehaviourClick = () => {
        const {history, match } = this.props
        const queryValues = queryString.parse(this.props.location.search)
        // history.push(`${match.path}/assessbehaviour?staffId=${queryValues.staffId}`)
        history.push({
            pathname: `${match.path}/assessbehaviour`,
            search: `?staffId=${queryValues.staffId}`,
            state: this.props.location.state,
        })
    }

    onRecommendationsClick = () => {
        const {history, match } = this.props
        const queryValues = queryString.parse(this.props.location.search)
        // history.push(`${match.path}/assessrecommendations?staffId=${queryValues.staffId}`)
        history.push({
            pathname: `${match.path}/assessrecommendations`,
            search: `?staffId=${queryValues.staffId}`,
            state: this.props.location.state,
        })
    }

    onCompleteClick = () => {
        const {history, match, dispatch } = this.props
        const queryValues = queryString.parse(this.props.location.search)

        dispatch({ type: FETCH_LMPO_OKRS, staffId: `?staffId=${queryValues.staffId}` })

        history.push({
            pathname: `${match.path}/assesscomplete`,
            search: `?staffId=${queryValues.staffId}`,
            state: this.props.location.state,
        })
    }

    handleSubmit = () => {
        const { lmStaffAppraisals, match, location, history, dispatch, lmIDs, lmObjectives, lmKeyResults, lmPendingKeyResults } = this.props
        // console.log("lmstaff")
        const queryValues = queryString.parse(location.search)
        //turn this execution into a function later
        let firstApproved
        if (lmIDs.map(item => lmObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => lmKeyResults[item]).filter(item => item.lineManagerStatus === "disapproved").length > 0 || lmIDs.map(item => lmObjectives[item]).filter(item => item.lineManagerStatus === "disapproved").length > 0) {
            firstApproved = "disapproved"
        } else {
            firstApproved = "approved"
        }

        //move this into the dispatched function for better performance
        lmStaffAppraisals.find(item => item.staffId === queryValues.staffId).firstApproved = firstApproved
        
        this.iUpdated();

        dispatch({ type: SUBMIT_LINE_MANAGER_OKRS, data: { staffAppraisalId: lmIDs.map(item => lmObjectives[item].staffAppraisalId).find(item => item), staffId: queryValues.staffId, firstApproved }, history})
    }

    render() {
        const { IS_REQUESTING_OKRs, sbuObjectives, isFetchingStaffAppraisals, isFetchingLMPOOKRs, match, lmIDs, lmObjectives, lmStaffAppraisals, lmCategories, categories, lmKeyResults, lmSbuObjectives, behavioralIndicators } = this.props
        const { routeState } = this.state
        //create a variable to hold the parsed values kept in the query parameter
        const queryValues = queryString.parse(this.props.location.search)

        let pageContent;

        // const staffKeyResults = lmIDs.map(item => lmObjectives[item].keyResults)
        //                             .reduce((acc, item) => acc.concat(item), [])
        //                             .map(item => lmKeyResults[item])

        // const staffPerformanceSum = staffKeyResults.reduce((acc, curr) => acc + curr.staffRating, 0)

        // const staffPerformanceScore = ((staffPerformanceSum / (staffKeyResults.length * 5)) * 100)

        // const staffBehaviorSum = behavioralIndicators.reduce((acc, curr) => acc + curr.staffRating,  0)

        // const staffBehaviorScore = ((staffBehaviorSum / (behavioralIndicators.length * 5)) * 100)

        // const staffTotalScore = ((staffPerformanceScore / 10) * 7) + ((staffBehaviorScore / 10) * 3)

        // const lineManagerPerformanceSum = staffKeyResults.reduce((acc, curr) => acc + curr.lineManagerRating, 0)

        // const lineManagerPerformanceScore = ((lineManagerPerformanceSum / (staffKeyResults.length * 5)) * 100)

        // const lineManagerBehaviorSum = behavioralIndicators.reduce((acc, curr) => acc + curr.lineManagerRating,  0)

        // const lineManagerBehaviorScore = ((lineManagerBehaviorSum / (behavioralIndicators.length * 5)) * 100)

        // const lineManagerTotalScore = ((lineManagerPerformanceScore / 10) * 7) + ((lineManagerBehaviorScore / 10) * 3)


        // console.log("staffScores", staffPerformanceScore, staffPerformanceSum, staffKeyResults, staffTotalScore)

        // console.log("staffBeh", behavioralIndicators, staffBehaviorSum, staffBehaviorScore)

        // console.log("lineMnagerSckores", lineManagerPerformanceSum, lineManagerPerformanceScore, lineManagerBehaviorScore, lineManagerBehaviorSum, lineManagerTotalScore)



        if(isFetchingLMPOOKRs || isFetchingStaffAppraisals){
            pageContent = (
                <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                    Please Wait...
                    <Loader active inline='centered'/>
                </div>
            )
        } else {
            pageContent = (
                <>
                    <br/>
                    <br/>
                    <Switch>
                        <Route path={`${match.path}/assessperformance`} render={(props) => <ErrorBoundary><AssessPerformance {...props} {...this.props}/></ErrorBoundary>}/>
                        <Route path={`${match.path}/assessbehaviour`} render={(props) => <ErrorBoundary><AssessBehaviour {...props} {...this.props}/></ErrorBoundary>}/>
                        <Route path={`${match.path}/assessrecommendations`} render={(props) => <ErrorBoundary><AssessRecommendations routeState={routeState} staffId={queryValues.staffId} staffAppraisalId={lmIDs.map(item => lmObjectives[item].staffAppraisalId).find(item => item)} {...props} {...this.props}/></ErrorBoundary>}/>
                        <Route path={`${match.path}/assesscomplete`} render={(props) => <ErrorBoundary><AssessComplete routeState={routeState} staffId={queryValues.staffId} staffAppraisalId={lmIDs.map(item => lmObjectives[item].staffAppraisalId).find(item => item)} {...props} {...this.props}/></ErrorBoundary>}/>
                    </Switch>
                </>
            )
        }

        return (
            <Fragment id="okrObjectives" padded>
                {/* <PageInfo info={[
                    <p>Here is the information concerning this page</p>
                ]}/> */}
                <PageHeader id={queryValues.staffId}
                    lmStaffAppraisals={lmStaffAppraisals}
                />
                <Segment basic textAlign="center">
                    <Button.Group>
                        <Button content="Performance" onClick={this.onPerformanceClick} color={this.props.location.pathname.includes('performance') ? 'blue' : null}/>
                        <Button content="Behaviour" onClick={this.onBehaviourClick} color={this.props.location.pathname.includes('behaviour') ? 'blue' : null}/>
                        <Button content="Recommendation" onClick={this.onRecommendationsClick} color={this.props.location.pathname.includes('recommendation') ? 'blue' : null}/>
                        <Button content="Complete" icon='check circle outline' labelPosition='left' onClick={this.onCompleteClick} color='green' basic={this.props.location.pathname.includes('complete') ? false : true} />
                    </Button.Group>
                    <br/>
                    {/* <PageInfo 
                        title='Click To View Assessment Summary' 
                        info={
                            <Grid divided columns={2}>
                                <Grid.Column width={8}>
                                    <Header as='h4' content="Staff"/>
                                    <div>Performance: <span>{staffPerformanceScore}</span></div>
                                </Grid.Column>
                            </Grid>
                        }
                    /> */}
                </Segment>      
                {pageContent}
            </Fragment>
        )
    }
}

function mapStateToProps(state) {
    return {
        IS_REQUESTING_OKRs: state.requestingOKRs,
        // OKRs: state.setOKRs,
        lmIDs: lmIDsSelector(state),
        lmObjectives: lmObjectivesSelector(state),
        lmKeyResults: lmKeyResultsSelector(state),
        lmCategories: lmCategoriesSelector(state),
        lmSbuObjectives: lmSbuObjectivesSelector(state),
        lmStaffAppraisals: state.requestingLineManagerStaffAppraisals,
        categories: categorySelector(state),
        isFetchingStaffAppraisals: state.isFetchingStaffAppraisals,
        isFetchingLMPOOKRs: state.isFetchingLMPOOKRs,
        sbuObjectives: state.sbuObjectives,
        buttonLoadingStatus: state.saveLoadingReducer.SAVE_KEY_RESULTS_LOADING_STATUS,
        behavioralSaveLoadingStatus: state.saveLoadingReducer.SAVE_BEHAVIORAL_ASSESSMENT_LOADING_STATUS,
        recommendationSaveLoadingStatus: state.saveLoadingReducer.UPDATE_RECOMMENDATION_LOADING_STATUS,
        recommendationSubmitLoadingStatus: state.saveLoadingReducer.SUBMIT_RECOMMENDATION_LOADING_STATUS,
        completeSubmitLoadingStatus: state.saveLoadingReducer.SUBMIT_LINE_MANAGER_ASSESSMENT_LOADING_STATUS,
        behavioralIndicators: state.requestingBehavioralIndicators,
        recommendationTypes: state.requestingRecommendationTypes,
        appraisalCycleId: appraisalCycleIdSelector(state),
        recommendationObject: state.requestingRecommendation,
        SAVE_BEHAVIOUR_STATUS: state.SAVE_BEHAVIOUR_STATUS,
        UPDATE_RECOMMENDATION_STATUS: state.UPDATE_RECOMMENDATION_STATUS,
        SUBMIT_RECOMMENDATION_STATUS: state.SUBMIT_RECOMMENDATION_STATUS,
        SAVING_OBJECTIVE_STATUS: state.SAVING_OBJECTIVE_STATUS,
        SUBMIT_LINE_MANAGER_ASSESSMENT_STATUS: state.SUBMIT_LINE_MANAGER_ASSESSMENT_STATUS, 
    }
}

export default connect(mapStateToProps)(AssessOKRs)

// {lmIDs.map(item => lmObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => lmKeyResults[item]).every(item => item.status === "pending")}