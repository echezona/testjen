import React, { Fragment } from 'react'
import {List, Form, Button, Dropdown} from 'semantic-ui-react'
import './index.css'

export default class ListItem extends React.Component {

    state = {
        updating: false,
        value: this.props.item.description
    }

    handleChange = (e) => {
        this.setState({ value: e.target.value })
    }

    render(){
        const { handleUpdate, handleDelete, updateLoadingStatus, deleteLoadingStatus, item: { description, sbuName, id } } = this.props
        const { updating, value } = this.state
        return(
            <List.Item className='custom-list-item'>
                <List.Content>
                    {!updating && description}
                    {updating && 
                        <Form>
                            <input value={value} placeholder='Enter new SBU objective' onChange={this.handleChange} className='custom-input'/>
                        </Form>
                    }
                </List.Content>
                <List.Content floated='right' >
                    { !updating &&
                        <Fragment>
                            <Button icon='edit' circular onClick={() => this.setState({ updating: true })}></Button>
                            <Dropdown 
                                icon='delete'
                                inline
                                button
                                text='Delete'
                                header='Are you sure?'
                                options={[{key: "0", text: "No", value: "no"}, {key: "1", text: "Yes", value: "yes"}]}
                                onChange={(e, titleProps) => handleDelete(e, titleProps, id)}
                                // loading={deleteLoadingStatus}
                                disabled={deleteLoadingStatus}
                            />
                        </Fragment>
                    }
                    { updating &&
                        <Fragment>
                            <Button onClick={() => this.setState({ updating: false })} icon='arrow left' circular></Button>
                            <Button onClick={(e) => handleUpdate(e, id, sbuName, value)} 
                                loading={updateLoadingStatus}
                                disabled={updateLoadingStatus || !value}
                            >
                                Update
                            </Button>
                        </Fragment>
                    }     
                </List.Content>
            </List.Item>
        )
    }
}