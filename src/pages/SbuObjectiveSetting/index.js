import React, { Fragment } from 'react'
import { Header, List, Grid, Segment, Icon, Form, Button, Divider, Container, Label, Loader } from 'semantic-ui-react'
import { connect } from "react-redux"
import Dropzone from 'react-dropzone'
import _ from 'lodash'
import * as XLSX from 'xlsx';
import './index.css'
import ListItem from './listItem'
import { UPLOAD_BEHAVIOUR, FETCH_ALL_BUSINESS_UNITS, FETCH_ALL_SBU_OBJECTIVES, CREATE_SBU_OBJECTIVE, UPDATE_SBU_OBJECTIVE, DELETE_SBU_OBJECTIVE } from '../../constants';



class SbuObjectiveSetting extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currSbu: '',
            comment: "",
        }
    }

    componentDidMount() {
        const {dispatch} = this.props
        dispatch({ type: FETCH_ALL_BUSINESS_UNITS })
        dispatch({ type: FETCH_ALL_SBU_OBJECTIVES })
    }

    handleSelectChange = (e) => {
        console.log("it's e", e.target.value)
        const { value } = e.target
        this.setState({
            currSbu: value,
        })
    }

    handleCommentChange = (e) => {
        const { value } = e.target
        this.setState({
            comment: value
        })
    }

    handleSubmit = () => {
        const { dispatch } = this.props
        const { comment, currSbu } = this.state

        //create data object to be included in the action
        const data = { description: comment, sbuName: currSbu }
        //reset the sbu objective descripion field to be blank
        this.setState({ comment: "" })
        //dispatch the action
        dispatch({ type: CREATE_SBU_OBJECTIVE, data })
    }

    handleUpdate = (e, id, sbuName, description) => {
        const { dispatch } = this.props
        dispatch({ type: UPDATE_SBU_OBJECTIVE, data: { sbuName, description }, id })
        // console.log("them guys", id, sbuName, description)
    }

    handleDelete = (e, {value}, id) => {
        if(value == "no"){
            return
        }
        //do deleting stuff
        const { dispatch } = this.props
        dispatch({ type: DELETE_SBU_OBJECTIVE, id })
        console.log("val", value)
    }


    componentDidUpdate() {
        console.log("state level", this.state.currSbu)
        console.log("sbuObjs", this.props.sbuObjectives)
    }

    
    render() {
        const { levelOptions, currSbu, comment } = this.state
        const { businessUnits, sbuObjectives, createSbuObjectiveLoadingStatus, SAVING_SBU_OBJECTIVES_STATUS, updateSbuObjLoadingStatus, deleteSbuObjLoadingStatus, fetchAllBusinessUnitsLoadingStatus } = this.props
        console.log("biz concerns", businessUnits)
        console.log("sbu concerns", sbuObjectives)

        if(fetchAllBusinessUnitsLoadingStatus){
            return(
                <Loader active/>
            )
        }
        return (
            <Grid columns={16}>
                <Grid.Column width={3}></Grid.Column>
                <Grid.Column width={10}>
                    <Segment basic>
                        {/* <Header as="h2" content="Summary" /> */}

                        {/* {staffSubmitted &&
                            <Message id='green' color="green"></Message>
                        }

                        {!staffSubmitted &&
                            <Message color="yellow">Please review your OKRs carefully before you submit as you'll be unable to edit them once you do</Message>
                        } */}

                        <Segment>
                            <Header as="h3" content="Set SBU Objectives" />
                            <List as='ol'>
                                <List.Item as='li'>Select the desired SBU from the dropdown</List.Item>
                                <List.Item as='li'>Fill in a single SBU objective you wish to add for the selected SBU</List.Item>
                                <List.Item as='li'>Press the Save button to save the objective for the SBU</List.Item>
                            </List>
                            <br />
                            {/* <Divider/> */}
                            <br />
                            <Form>
                                <select name='' value={this.state.currSbu} onChange={(e) => this.handleSelectChange(e)}>
                                    <option value="" disabled>Select a business unit to upload business objectives for</option>
                                    {
                                        businessUnits.map((item, index) => <option value={item.name} key={item.id}>{`${item.name}`}</option>)
                                    }
                                </select>
                                <br/>
                                <Form.TextArea 
                                    name="textfield" 
                                    onChange={(e) => {this.handleCommentChange(e)}} 
                                    value={this.state.comment}
                                    placeholder="Please enter the objective you wish to add for this SBU"
                                />
                            </Form>
                            <br />
                            <Segment basic textAlign="right">
                                {SAVING_SBU_OBJECTIVES_STATUS === true &&
                                    <Label pointing='right' color="green" >Successfully added SBU objective</Label>
                                }
                                {SAVING_SBU_OBJECTIVES_STATUS === false &&
                                    <Label pointing='right' color="red" >Failed to add SBU objective</Label>
                                }
                                <Button content="Submit" color={'blue'} onClick={() => this.handleSubmit()} loading={createSbuObjectiveLoadingStatus} disabled={createSbuObjectiveLoadingStatus || !currSbu || !comment}/>
                            </Segment>
                            <br />
                            <aside>
                                <h4>Existing {this.state.currSbu} SBU Objectives</h4>
                                
                                { Object.prototype.toString.call(sbuObjectives) === '[object Array]' &&
                                    <div>
                                        { sbuObjectives.filter(item => item.sbuName === currSbu).length < 1 &&
                                            <ul>
                                                <li> No SBU Objectives exist yet </li>
                                            </ul>
                                        }
                                        { sbuObjectives.filter(item => item.sbuName === currSbu).length > 0 &&
                                            <List celled>
                                                { sbuObjectives.filter(item => item.sbuName === currSbu).map(item => (
                                                        <ListItem 
                                                            key={item.id} 
                                                            item={item} 
                                                            handleUpdate={this.handleUpdate} 
                                                            handleDelete={this.handleDelete} 
                                                            updateLoadingStatus={updateSbuObjLoadingStatus}
                                                            deleteLoadingStatus={deleteSbuObjLoadingStatus}
                                                        />
                                                    ))
                                                }
                                            </List>
                                        }
                                    </div>
                                }
                            </aside>
                        </Segment>
                    </Segment>
                </Grid.Column>
            </Grid>
        )
    }
}

function mapStateToProps(state) {
    return {
        businessUnits: state.requestingAllBusinessUnits,
        sbuObjectives: state.requestingAllSbuObjectives,
        createSbuObjectiveLoadingStatus: state.saveLoadingReducer.CREATE_SBU_OBJECTIVES_LOADING_STATUS,
        SAVING_SBU_OBJECTIVES_STATUS: state.SAVING_SBU_OBJECTIVES_STATUS,
        updateSbuObjLoadingStatus: state.saveLoadingReducer.UPDATE_SBU_OBJECTIVE_LOADING_STATUS,
        deleteSbuObjLoadingStatus: state.saveLoadingReducer.DELETE_SBU_OBJECTIVE_LOADING_STATUS,
        fetchAllBusinessUnitsLoadingStatus: state.fetchLoadingReducer.FETCH_ALL_BUSINESS_UNITS_LOADING_STATUS
    }
}


export default connect(mapStateToProps)(SbuObjectiveSetting)

// <List.Item key={item.sbuName}>
//     <List.Content>
//         {item.description}
//     </List.Content>
//     <List.Content floated='right'>
//         <Button size='mini'>Update</Button>
//         <Button size='mini'>Delete</Button>
//     </List.Content>
// </List.Item>