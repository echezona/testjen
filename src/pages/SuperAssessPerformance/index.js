//Components
import React from 'react';
import { Grid, Loader, Message, Button, Segment, Header } from 'semantic-ui-react';
import Staff from './Staff';
import Results from './results';
// import PageHeader from './Header'
import _ from 'lodash'
import { connect } from "react-redux"
import { withRouter, Switch, Route } from 'react-router-dom'
import { userObjectivesSelector, userIDsSelector, userKeyResultsSelector, userCategoriesSelector, lmIDsSelector, lmCategoriesSelector, lmStaffAppraisalsSelector, lmObjectivesSelector, lmKeyResultsSelector, categorySelector } from '../../reducer'
import queryString from 'query-string'
// Css Files
import './index.css';
//Constatnts
import { FETCH_LINE_MANAGER_OKRS, FETCH_CATEGORY, FETCH_LINE_MANAGER_STAFF_APPRAISALS, SUBMIT_LINE_MANAGER_OKRS, FETCH_LMPO_OKRS, STORE_PENDING_KEY_RESULTS, FETCH_SBU_OBJECTIVES } from '../../constants';
import PageInfo from '../../components/PageInfo';



class SuperAssessPerformance extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            showSubmit: false,
            updatePage: false,
        }
    }

    showSubmit = (x) => {
        this.setState({
            showSubmit: x
        })
    }

    iUpdated = () => {
        this.forceUpdate();
    }

    handleSubmit = () => {
        const { lmStaffAppraisals, match, location, history, dispatch, lmIDs, lmObjectives, lmKeyResults, lmPendingKeyResults } = this.props
        // console.log("lmstaff")
        const queryValues = queryString.parse(location.search)
        //turn this execution into a function later
        let firstApproved
        if (lmIDs.map(item => lmObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => lmKeyResults[item]).filter(item => item.lineManagerStatus === "disapproved").length > 0 || lmIDs.map(item => lmObjectives[item]).filter(item => item.lineManagerStatus === "disapproved").length > 0) {
            firstApproved = "disapproved"
        } else {
            firstApproved = "approved"
        }

        //move this into the dispatched function for better performance
        lmStaffAppraisals.find(item => item.staffId === queryValues.staffId).firstApproved = firstApproved
        
        this.iUpdated();

        dispatch({ type: SUBMIT_LINE_MANAGER_OKRS, data: { staffAppraisalId: lmIDs.map(item => lmObjectives[item].staffAppraisalId).find(item => item), staffId: queryValues.staffId, firstApproved }, history})
    }

    render() {        
        const { IS_REQUESTING_OKRs, sbuObjectives, isFetchingStaffAppraisals, isFetchingLMPOOKRs, match,location, lmIDs, lmObjectives, lmStaffAppraisals, lmCategories, categories, lmKeyResults, lmSbuObjectives, behavioralIndicators } = this.props
        //create a variable to hold the parsed values kept in the query parameter
        const queryValues = queryString.parse(this.props.location.search)
        //submitenabled checks whether all keyresults have their status as either "approved" or "disapporved" which woiuld 
        //indicate that they have been attended to
        const submitEnabled = lmIDs.map(item => lmObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => lmKeyResults[item]).every(item => item.lineManagerStatus !== "pending")
        //submitenabled2 checks whether all keyresults that have their status as "disapproved" also have content in their
        //lineManagerComment property
        const submitEnabled2 = lmIDs.map(item => lmObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => lmKeyResults[item]).filter(item => item.lineManagerStatus === "disapproved").every(item => item.lineManagerComment !== "")
        
        const submitEnabled3 = lmIDs.map(item => lmObjectives[item]).every(item => item.lineManagerStatus !== "pending")

        const submitEnabled4 = lmIDs.map(item => lmObjectives[item]).filter(item => item.lineManagerStatus === "disapproved").every(item => item.lineManagerComment !== "")



        //PageInfo Values start here
        const staffKeyResults = lmIDs.map(item => lmObjectives[item].keyResults)
                                    .reduce((acc, item) => acc.concat(item), [])
                                    .map(item => lmKeyResults[item])

        const staffPerformanceSum = staffKeyResults.reduce((acc, curr) => acc + curr.staffRating, 0)

        const staffPerformanceScore = ((staffPerformanceSum / (staffKeyResults.length * 5)) * 100)

        const staffBehaviorSum = behavioralIndicators.reduce((acc, curr) => acc + curr.staffRating,  0)

        const staffBehaviorScore = ((staffBehaviorSum / (behavioralIndicators.length * 5)) * 100)

        const staffTotalScore = ((staffPerformanceScore / 10) * 7) + ((staffBehaviorScore / 10) * 3)

        const lineManagerPerformanceSum = staffKeyResults.reduce((acc, curr) => acc + curr.lineManagerRating, 0)

        const lineManagerPerformanceScore = ((lineManagerPerformanceSum / (staffKeyResults.length * 5)) * 100)

        const lineManagerBehaviorSum = behavioralIndicators.reduce((acc, curr) => acc + curr.lineManagerRating,  0)

        const lineManagerBehaviorScore = ((lineManagerBehaviorSum / (behavioralIndicators.length * 5)) * 100)

        const lineManagerTotalScore = ((lineManagerPerformanceScore / 10) * 7) + ((lineManagerBehaviorScore / 10) * 3)



        let firstApproved
        if (lmIDs.map(item => lmObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => lmKeyResults[item]).filter(item => item.lineManagerStatus === "disapproved").length > 0 || lmIDs.map(item => lmObjectives[item]).filter(item => item.lineManagerStatus === "disapproved").length > 0) {
            firstApproved = "disapproved"
        } else if (lmIDs.map(item => lmObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => lmKeyResults[item]).filter(item => item.lineManagerStatus === "pending").length > 0 || lmIDs.map(item => lmObjectives[item]).filter(item => item.lineManagerStatus === "pending").length > 0) {
            firstApproved = "pending"
        } else {
            firstApproved = "approved"
        }

        console.log({location: location, match: match})
        console.log('lmStaffAppraisal' , lmStaffAppraisals )

        if(isFetchingLMPOOKRs && isFetchingStaffAppraisals){
            return <Loader active/>
        }

        return (
            <Grid id="okrObjectives" padded>
                <Grid.Row>
                    <PageInfo
                        title='Click To View/Hide Assessment Scores'  
                        content={
                            <Grid divided columns={2}>
                                <Grid.Column width={8}>
                                    <Header as='h4' content="Staff"/>
                                    <div>Performance: <span style={{fontWeight: "bold"}}>{staffPerformanceScore.toFixed(1) + '%'}</span></div>
                                    <div>Behavior: <span style={{fontWeight: "bold"}}>{staffBehaviorScore.toFixed(1) + '%'}</span></div>
                                    <div style={{marginTop: "10px", fontSize: "1.3em"}}>Total: <span style={{fontWeight: "bold"}}>{staffTotalScore.toFixed(1) + '%'}</span></div>
                                </Grid.Column>
                                <Grid.Column width={8}>
                                    <Header as='h4' content="Line Manager"/>
                                    <div >Performance: <span style={{fontWeight: "bold"}}>{lineManagerPerformanceScore.toFixed(1) + '%'}</span></div>
                                    <div>Behavior: <span style={{fontWeight: "bold"}}>{lineManagerBehaviorScore.toFixed(1) + '%'}</span></div>
                                    <div style={{marginTop: "10px", fontSize: "1.3em"}}>Total: <span style={{fontWeight: "bold"}}>{lineManagerTotalScore.toFixed(1) + '%'}</span></div>
                                </Grid.Column>
                            </Grid>
                        }
                    />
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column largeScreen={6} wideScreen={6} computer={6} tablet={6} mobile={16}>
                        <Staff userIDs={lmIDs}
                            userObjectives={lmObjectives}
                            userCategories={categories}
                            sbuObjectives={lmSbuObjectives}
                        />
                    </Grid.Column>
                    <Grid.Column largeScreen={10} wideScreen={10} computer={10} tablet={10} mobile={16}>
                        <Switch>
                            {lmIDs.map((item, index) => <Route key={item} path={`${match.path}/superassessperformance/${item}`} render={(props) => <Results showSubmit={(x) => this.showSubmit(x)} 
                                                                                                                                    iUpdated={this.iUpdated}
                                                                                                                                    objIndex={index} keyResults={lmKeyResults} 
                                                                                                                                    userCategories={lmCategories} categoryId={lmObjectives[item].categoryId} 
                                                                                                                                    id={item} objective={lmObjectives[item]} staffId={`?staffId=${queryValues.staffId}`}
                                                                                                                                    isFirstSave={lmStaffAppraisals.filter(item => item.staffId === queryValues.staffId).every(item => item.lineManagerStatus !== "in progress")} 
                                                                                                                                    // secondApproved={lmStaffAppraisals.find(item => item.staffId === queryValues.staffId).secondApproved}
                                                                                                                                    {...props} {...this.props}/>} />)}
                        </Switch>
                        {this.state.showSubmit &&
                            <Segment basic textAlign="right">
                                <Button content={firstApproved === "disapproved" ? "Send to direct report" : firstApproved === "approved" ? "Submit to Line Manager" : "Submit"}
                                    disabled={!(submitEnabled && submitEnabled2 && submitEnabled3 && submitEnabled4)}
                                    onClick={() => this.handleSubmit()}
                                    color='blue'
                                    icon='right arrow'
                                    labelPosition='right'
                                />
                            </Segment>
                        }

                    </Grid.Column>
                </Grid.Row>
            </Grid>
        )
    }
}


export default SuperAssessPerformance
// export default connect(mapStateToProps)(AssessPerformance)

// {lmIDs.map(item => lmObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => lmKeyResults[item]).every(item => item.status === "pending")}