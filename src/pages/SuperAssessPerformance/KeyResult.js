import React from 'react'
import { Segment,Grid,Header,Button,Form,Input,List,Icon,Popup, Message } from 'semantic-ui-react'
import "./index.css"

class KeyResult extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            isViewingKey: true,
            isCommenting: false,
            commentValue: this.props.data.superLineManagerComment,
            rating: this.props.data.superLineManagerRating,  
            file: {},
            // icon: this.props.data.superLineManagerRating !== 0 && this.props.data.superLineManagerComment ? "check" : "cancel",
            icon: (this.props.data.superLineManagerStatus == "disapproved" && this.props.data.superLineManagerComment) || this.props.data.superLineManagerStatus == "approved"? "check" : "cancel",
            link: this.props.data.supportingDocumentsLinks,
            superLineManagerStatus: this.props.data.superLineManagerStatus // approved, disapproved, pending
        }
    }

    // componentWillUpdate(nextProps, nextState){
    //     // this.localGetResponseObject(nextState);
    //     console.log("keyResult", this.props.data)
    // }

    static getDerivedStateFromProps(props, state) {
        if (props.data.supportingDocumentsLinks !== state.link) {
            return { link: props.data.supportingDocumentsLinks }
        }

        return null
    }

    handleKeyButtonClick = () => {
        this.setState({
            isViewingKey: true,
            isCommenting: false,
        })
    }

    handleCommentButtonClick = () => {
        this.setState({
            isCommenting: true,
            isViewingKey: false,
        })
    }

    handleCommentChange = (e) => {
        const {value} = e.target
        this.setState({
            commentValue: value,
            icon: value ? "check" : "cancel"
        })
    }

    // handleRatingClick = (e) => {
    //     const {name} = e.target
    //     const {commentValue} = this.state;
    //     this.setState({
    //         rating: parseInt(name),
    //         icon: commentValue ? "check" : "cancel",
    //     })
    // }

    handleApproveClick = (e) => {
        const {name} = e.target
        const {commentValue} = this.state;
        this.setState({
            superLineManagerStatus: name === "approved" ? "approved" : "disapproved",
            icon: (name === 'disapproved' && commentValue) || name === "approved" ? "check" : "cancel"
        })
        this.props.iUpdated()
    }

    handleLinkClick = (e) => {
        const link = e.target.innerHTML
        window.open(link, '_blank')
    }

    render(){
        const { rating, commentValue, link, superLineManagerStatus } = this.state;
        const {index, data, order, handleCommentEdit, handleApproval} = this.props;

        return(
            <Segment>
                <Grid>
                    <Grid.Column width={16}>
                        <div>
                            <h5 style={{marginRight:'10px', display:'inline-block'}}> {`Key Result ${index + 1}`} </h5>
                            <Icon className="keyResultIcon" size="small" name={this.state.icon} color={this.state.icon === "cancel" ? "red" : "green"} circular inverted padded/>
                            <div style={{ marginBottom: "10px" }} >{data.description}</div>
                        </div>

                        <Grid>
                            <Grid.Column width={8}>
                                <Message style={{overflowWrap: "break-word"}}>
                                    <Message.Header>Staff</Message.Header>
                                    
                                    <div>Rating: <span style={{fontWeight: "bold"}}>{data.staffRating}</span></div>
                                    <div>Comment: <span style={{fontWeight: "bold"}}>{data.staffComment}</span></div>
                                    { link &&
                                        <div>Link to supporting documents: <a onClick={this.handleLinkClick} 
                                                                            style={{fontWeight: "bold", overflowWrap: "break-word", cursor: "pointer"}}>
                                                                                {link}
                                                                        </a>
                                        </div>
                                    }
                                    
                                </Message>
                            </Grid.Column>
                            <Grid.Column width={8}>
                                <Message style={{overflowWrap: "break-word"}}>
                                    <Message.Header>Line Manager</Message.Header>
                                    <div>Rating: <span style={{fontWeight: "bold"}}>{data.lineManagerRating}</span></div>
                                    <div>Comment: <span style={{fontWeight: "bold"}}>{data.lineManagerComment}</span></div>
                                </Message>
                            </Grid.Column>
                        </Grid>
                    
                        <Segment basic textAlign="right">
                            {/* <Button content="1" compact color={rating===1 ? "red" : "green"} onClick={(e) => {this.handleRatingClick(e); handleRating(e, order)}} name={1}/>
                            <Button content="2" compact color={rating===2 ? "red" : "green"} onClick={(e) => {this.handleRatingClick(e); handleRating(e, order)}} name={2}/>                         */}

                            <Button.Group>
                                <Button 
                                    content="Approve"
                                    name='approved'
                                    color="green"
                                    basic={superLineManagerStatus === 'approved' ? false : true}
                                    onClick={(e) => { this.handleApproveClick(e); handleApproval(e, order) }}
                                />

                                <Button 
                                    content="Disapprove"
                                    name='disapproved'
                                    color="red"
                                    basic={superLineManagerStatus === 'disapproved' ? false : true}
                                    onClick={(e) => { this.handleApproveClick(e); handleApproval(e, order) }}
                                />
                            </Button.Group>
                        </Segment>
                    </Grid.Column>
                    { this.state.superLineManagerStatus === "disapproved" && 
                        <Grid.Column width={16}>
                            <Form>
                                <Form.TextArea 
                                    style={{ borderColor: "red" }}
                                    name="textfield" 
                                    onChange={(e) => {this.handleCommentChange(e); handleCommentEdit(e, order)}} 
                                    value={commentValue}
                                    placeholder="Please give reasons for disagreeing with the line manager's rating"/>
                                {/* <Form.Button color="blue" floated="right">Submit Comment</Form.Button> */}
                            </Form>
                        </Grid.Column>
                    }
                    {/* <Grid.Column width={2} textAlign="right">
                        <Button.Group vertical>
                            <Button active={this.state.isViewingKey===true ? true : false} icon="key" attached="right" onClick={this.handleKeyButtonClick}/>
                            <Button active={this.state.isCommenting===true ? true : false} icon="comments" attached="right" onClick={this.handleCommentButtonClick}/>
                        </Button.Group>
                    </Grid.Column> */}
                </Grid>
            </Segment>
        )
    }
}


export default KeyResult