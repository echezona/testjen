import React from 'react'
import { Segment,Grid,Header,Button,Form } from 'semantic-ui-react'
import "./index.css"


class KeyResult extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            isViewingKey: true,
            isCommenting: false,
            comment: this.props.data.peopleOpsComment,
            peopleOpsStatus: this.props.data.peopleOpsStatus,  // approved, disapproved, pending
        }
    }


    componentWillUpdate(nextProps, nextState){
        const {comment, peopleOpsStatus, isCommenting, isViewingKey} = nextState;
        if(peopleOpsStatus === "disapproved" && isCommenting === false && isViewingKey === true){
            this.handleCommentButtonClick();
        }
        if(peopleOpsStatus === "approved" && isViewingKey === false && isCommenting === true){
            this.handleKeyButtonClick();
        }
    }


    handleKeyButtonClick = () => {
        this.setState({
            isViewingKey: true,
            isCommenting: false
        })
    }

    handleCommentButtonClick = () => {
        this.setState({
            isViewingKey: false,
            isCommenting: true
        })
    }


    handleCommentChange = (e) => {
        const {value} = e.target
        this.setState({
            comment: value,
        })
    }

    handleApproveClick = (e) => {
        const {name} = e.target
        this.setState({
            peopleOpsStatus: name === "approved" ? "approved" : "disapproved"
        })
    }

    render(){
        const { peopleOpsStatus } = this.state;
        const { data, handleCommentEdit, handleApproval, order } = this.props;
        return(
            <Segment>
                <Grid>
                    {/* { this.state.isViewingKey &&  */}
                        <Grid.Column width={16}>
                            <Header as="h5" content={`Key Result ${order}`}/>
                            {data.description}
                            <br/>
                            <Button content={(peopleOpsStatus === "pending" || peopleOpsStatus === "approved") ? "Disapprove" : "Disapproved"}
                                    floated="right" 
                                    color="red" 
                                    onClick={(e) => {this.handleApproveClick(e); handleApproval(e, order)}} 
                                    name="disapproved"
                                    disabled={!(peopleOpsStatus === "pending" || peopleOpsStatus === "approved")}
                            />
                            <Button content={peopleOpsStatus === "pending" || peopleOpsStatus === "disapproved" ? "Approve" : "Approved"} 
                                    floated="right" 
                                    color="green" 
                                    onClick={(e) => {this.handleApproveClick(e); handleApproval(e, order)}}
                                    name="approved"
                                    disabled={!(peopleOpsStatus === "pending" || peopleOpsStatus === "disapproved")}
                            />  
                        </Grid.Column>
                       
                    { this.state.peopleOpsStatus === "disapproved" && 
                        <Grid.Column width={16}>
                            <Form>
                                <Form.TextArea 
                                    name="textfield" 
                                    onChange={(e) => {this.handleCommentChange(e); handleCommentEdit(e, order)}} 
                                    value={this.state.comment}
                                    placeholder="Please give reasons for disagreeing with this key result OR suggest a suitable key result"/>
                                {/* <Form.Button color="blue" floated="right">Submit Comment</Form.Button> */}
                            </Form>
                        </Grid.Column>
                    }
                    {/* <Grid.Column width={2} textAlign="right" verticalAlign="top">
                        <Button.Group vertical>
                            <Button active={this.state.isViewingKey===true ? true : false} icon="key" attached="right" onClick={this.handleKeyButtonClick}/>
                            <Button active={this.state.isCommenting===true ? true : false} icon="comments" attached="right" onClick={this.handleCommentButtonClick}/>
                        </Button.Group>
                    </Grid.Column> */}
                </Grid>
            </Segment>
        )
    }
}


export default KeyResult