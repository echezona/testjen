//Components
import React from 'react';
import { Grid, Loader, Message, Button, Container, Segment, Label } from 'semantic-ui-react';
import Staff from './Staff';
import Results from './results';
import Header from './Header'
import { connect } from "react-redux"
import { withRouter, Switch, Route } from 'react-router-dom'
import { userObjectivesSelector, userIDsSelector, userKeyResultsSelector, userCategoriesSelector, lmIDsSelector, lmCategoriesSelector, lmStaffAppraisalsSelector, lmObjectivesSelector, lmKeyResultsSelector, categorySelector, lmSbuObjectivesSelector } from '../../reducer'
import queryString from 'query-string'
// Css Files
import './index.css';
import { FETCH_LINE_MANAGER_OKRS, FETCH_CATEGORY, FETCH_LINE_MANAGER_STAFF_APPRAISALS, SUBMIT_LINE_MANAGER_OKRS, FETCH_PEOPLE_OPS_STAFF_APPRAISALS, FETCH_LMPO_OKRS, SUBMIT_PEOPLE_OPS_OKRS, FETCH_SBU_OBJECTIVES, LINE_MANAGER_UPDATE_KEY_RESULTS, PEOPLE_OPS_UPDATE_KEY_RESULTS } from '../../constants';



class AdminApproveOkrs extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            showSubmit: false
        }
    }

    componentDidMount() {
        const { dispatch, match } = this.props;
        const queryValues = queryString.parse(this.props.location.search)

        dispatch({ type: FETCH_LMPO_OKRS, staffId: `?staffId=${queryValues.staffId}` })
        dispatch({ type: FETCH_CATEGORY })
        dispatch({ type: FETCH_SBU_OBJECTIVES})
        dispatch({ type: FETCH_PEOPLE_OPS_STAFF_APPRAISALS })
    }

    showSubmit = (x) => {
        this.setState({
            showSubmit: x
        })
    }

    iUpdated = () => {
        this.forceUpdate();
    }

    // handleSubmit = () => {
    //     const { lmStaffAppraisals, match, history, dispatch, lmIDs, lmObjectives, lmKeyResults } = this.props
    //     console.log("lmstaff", lmStaffAppraisals)
    //     const queryValues = queryString.parse(this.props.location.search)
    //     //Turn this implementation into a function later
    //     let secondApproved
    //     let peopleOpsStatus

    //     if (lmIDs.map(item => lmObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => lmKeyResults[item]).filter(item => item.peopleOpsStatus === "disapproved").length > 0 || lmIDs.map(item => lmObjectives[item]).filter(item => item.lineManagerStatus === "disapproved").length > 0) {
    //         secondApproved = "disapproved"
    //         peopleOpsStatus = {peopleOpsStatus: "awaiting resubmission"}
    //     } else {
    //         secondApproved = "approved"
    //         peopleOpsStatus = {peopleOpsStatus: "completed"}
    //     }
    //     dispatch({ type: SUBMIT_PEOPLE_OPS_OKRS, data: { staffAppraisalId: lmIDs.map(item => lmObjectives[item].staffAppraisalId).find(item => item), staffId: queryValues.staffId, secondApproved }, history, peopleOpsStatus })
    // }

    handleSubmit = () => {
        const { dispatch, location, history, lmStaffAppraisals, lmIDs, lmObjectives, lmKeyResults } = this.props;
        const queryValues = queryString.parse(location.search);

        
        const OBJs = lmIDs.map(id => lmObjectives[id])
                                .map(item => ({
                                    description: item.description,
                                    id: item.id,
                                    categoryId: item.categoryId,
                                    order: item.order,
                                    lineManagerStatus: item.lineManagerStatus,
                                    lineManagerComment: item.lineManagerComment,
                                    peopleOpsStatus: item.peopleOpsStatus,
                                    peopleOpsComment: item.peopleOpsComment,
                                    sbuObjectiveId: item.sbuObjectiveId,
                                    staffAppraisalId: item.staffAppraisalId,
                                }))

        const KRs = lmIDs.map(id => lmObjectives[id].keyResults)
                                .reduce((acc, curr) => acc.concat(curr) ,[])
                                .map(item => lmKeyResults[item])

        const id = lmIDs.map(item => lmObjectives[item].staffAppraisalId).find(item => item)

        let secondApproved;
        let peopleOpsStatus;

        if (lmIDs.map(item => lmObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => lmKeyResults[item]).filter(item => item.peopleOpsStatus === "disapproved").length > 0 || lmIDs.map(item => lmObjectives[item]).filter(item => item.peopleOpsStatus === "disapproved").length > 0) {
            secondApproved = "disapproved"
            peopleOpsStatus = { peopleOpsStatus: "awaiting resubmission" }
        } else {
            secondApproved = "approved"
            peopleOpsStatus = { peopleOpsStatus: "completed" }
        }

        const forSubmitPeopleOpsOkrs = {
            data: {staffAppraisalId: id, staffId: queryValues.staffId, secondApproved}, history, peopleOpsStatus
        }

        dispatch({ type: PEOPLE_OPS_UPDATE_KEY_RESULTS, KRs, OBJs, id, forSubmitPeopleOpsOkrs })

    }

    render() {
        const { PEOPLE_OPS_UPDATE_KEY_RESULTS_STATUS, IS_REQUESTING_OKRs, isFetchingLMPOOKRs, isFetchingStaffAppraisals, sbuObjectives, match, lmIDs, lmObjectives, lmStaffAppraisals, lmCategories, categories, lmKeyResults, lmSbuObjectives, submitPeopleOpsOkrsLoadingStatus } = this.props
        //create a variable to hold the parsed values kept in the query parameter
        const queryValues = queryString.parse(this.props.location.search)
        //submitenabled checks whether all keyresults have their status as either "approved" or "disapproved" which would 
        //indicate that they have been attended to
        const submitEnabled = lmIDs.map(item => lmObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => lmKeyResults[item]).every(item => item.peopleOpsStatus !== "pending")
        //submitenabled2 checks whether all keyresults that have their status as "disapproved" also have content in their
        //lineManagerComment property
        const submitEnabled2 = lmIDs.map(item => lmObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => lmKeyResults[item]).filter(item => item.peopleOpsStatus === "disapproved").every(item => item.peopleOpsComment !== "")

        const submitEnabled3 = lmIDs.map(item => lmObjectives[item]).every(item => item.peopleOpsStatus !== "pending")

        const submitEnabled4 = lmIDs.map(item => lmObjectives[item]).filter(item => item.peopleOpsStatus === "disapproved").every(item => item.peopleOpsComment !== "")
        
        let secondApproved
        if (lmIDs.map(item => lmObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => lmKeyResults[item]).filter(item => item.peopleOpsStatus === "disapproved").length > 0) {
            secondApproved = "disapproved"
        } else {
            secondApproved = "approved"
        }
        
        if(isFetchingLMPOOKRs && isFetchingStaffAppraisals){
            return <Loader active/>
        }
        
        return (
            <Grid id="okrObjectives" padded>
                <Header id={queryValues.staffId}
                    disapprovedKRs={lmIDs.map(item => lmObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => lmKeyResults[item]).filter(item => item.peopleOpsStatus === "disapproved").length}
                    totalKRs={lmIDs.map(item => lmObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).length}
                    treatedKRs={lmIDs.map(item => lmObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => lmKeyResults[item]).filter(item => item.peopleOpsStatus !== "pending").length}
                    totalOBJs={lmIDs.length}
                    treatedOBJs={lmIDs.map(item => lmObjectives[item]).filter(item => item.peopleOpsStatus !== "pending").length}
                    disapprovedOBJs={lmIDs.map(item => lmObjectives[item]).filter(item => item.peopleOpsStatus === "disapproved").length}
                />
                <Grid.Column largeScreen={6} wideScreen={6} computer={6} tablet={6} mobile={16}>
                    <Staff userIDs={lmIDs}
                        userObjectives={lmObjectives}
                        userCategories={categories}
                        sbuObjectives={sbuObjectives}
                        lmSbuObjectives={lmSbuObjectives}
                    />
                </Grid.Column>
                <Grid.Column largeScreen={10} wideScreen={10} computer={10} tablet={10} mobile={16}>
                    <Switch>
                        {lmIDs.map((item, index) => <Route key={item} path={`${match.path}/${item}`} render={(props) => <Results staffId={queryValues.staffId} showSubmit={(x) => this.showSubmit(x)} 
                                                                                                                                iUpdated={this.iUpdated}
                                                                                                                                objIndex={index} keyResults={lmKeyResults} userCategories={lmCategories} 
                                                                                                                                categoryId={lmObjectives[item].categoryId} id={item} objective={lmObjectives[item]}
                                                                                                                                isFirstSave={lmStaffAppraisals.filter(item => item.staffId === queryValues.staffId).every(item => item.peopleOpsStatus !== "in progress")} {...props} />} />)}
                    </Switch>
                    {this.state.showSubmit &&
                        <Segment basic textAlign="right">
                            {PEOPLE_OPS_UPDATE_KEY_RESULTS_STATUS && PEOPLE_OPS_UPDATE_KEY_RESULTS_STATUS.status === true &&
                                <Label pointing='right' color="blue" >Save successful</Label>
                            }
                            {PEOPLE_OPS_UPDATE_KEY_RESULTS_STATUS && PEOPLE_OPS_UPDATE_KEY_RESULTS_STATUS.status === false &&
                                <Label pointing='right' color="red" >Save Failed. Please try again or refresh page.</Label>
                            }
                            <Button content={secondApproved === "disapproved" ? "Send to direct report" : secondApproved === "approved" ? "Submit" : "Submit"}
                                disabled={!(submitEnabled && submitEnabled2 && submitEnabled3 && submitEnabled4) || submitPeopleOpsOkrsLoadingStatus}
                                onClick={() => this.handleSubmit()}
                                color='blue'
                                icon='right arrow'
                                labelPosition='right'
                                loading={submitPeopleOpsOkrsLoadingStatus}
                            />
                        </Segment>
                    }
                </Grid.Column>
            </Grid>
        )
    }
}

function mapStateToProps(state) {
    return {
        IS_REQUESTING_OKRs: state.requestingOKRs,
        OKRs: state.setOKRs,
        lmIDs: lmIDsSelector(state),
        lmObjectives: lmObjectivesSelector(state),
        lmKeyResults: lmKeyResultsSelector(state),
        lmCategories: lmCategoriesSelector(state),
        lmStaffAppraisals: lmStaffAppraisalsSelector(state),
        lmSbuObjectives: lmSbuObjectivesSelector(state),
        categories: categorySelector(state),
        isFetchingStaffAppraisals: state.isFetchingStaffAppraisals,
        isFetchingLMPOOKRs: state.isFetchingLMPOOKRs,
        sbuObjectives: state.sbuObjectives,
        submitPeopleOpsOkrsLoadingStatus: state.saveLoadingReducer.SUBMIT_PEOPLE_OPS_OKRS_LOADING_STATUS,
        PEOPLE_OPS_UPDATE_KEY_RESULTS_STATUS: state.PEOPLE_OPS_UPDATE_KEY_RESULTS_STATUS,
    }
}

export default connect(mapStateToProps)(AdminApproveOkrs)

// {lmIDs.map(item => lmObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => lmKeyResults[item]).every(item => item.status === "pending")}