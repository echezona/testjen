import React, {Fragment} from 'react';
import { Segment,Header,Button,Container,Label,Divider,Form,Popup,Grid } from 'semantic-ui-react';
import KeyResult from './KeyResult'
import {SAVE_KEY_RESULTS, UPDATE_LINE_MANAGER_STATUS, UPDATE_PEOPLE_OPS_STATUS} from '../../constants'
import {connect} from 'react-redux'


class Results extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            description: props.objective.description,
            id: props.objective.id,
            categoryId: props.objective.categoryId,
            order: props.objective.order,
            peopleOpsStatus: props.objective.peopleOpsStatus,
            peopleOpsComment: props.objective.peopleOpsComment,
            sbuObjectiveId: props.objective.sbuObjectiveId,
            staffAppraisalId: props.objective.staffAppraisalId,
            keyResults: props.objective.keyResults.map(item => props.keyResults[item]),
        }
    }

    componentDidMount(){
        this.props.showSubmit(true)
    }

    componentWillUnmount(){
        const {order} = this.props.objective;
        console.log(`Now unmounting from Objective ${order}`)
    }

    handleObjCommentChange = (e) => {
        const {value} = e.target
        // console.log("something", value)
        this.setState({
            peopleOpsComment: value,
        })
        this.props.objective.peopleOpsComment = value
        this.props.iUpdated()
    }

    handleCommentEdit = (e, order) => {
        const {keyResults} = this.state;
        const {value} = e.target;
        let KRs = [...keyResults]
        KRs.find(item => item.order === order).peopleOpsComment = value;
        this.setState({
            keyResults: KRs
        })
        this.props.iUpdated()
    }

    //this function  is very faulty
    handleObjApproval = (e, titleProps) => {
        const {icon} = titleProps
        // console.log("bla bla bla", icon)
        this.setState({
            peopleOpsStatus: icon === "check" ? "approved" : "disapproved"
        })
        this.props.objective.peopleOpsStatus = icon === "check" ? "approved" : "disapproved" 
        this.props.iUpdated();
    }

    handleApproval = (e, order) => {
        const {keyResults} = this.state;
        const {name} = e.target;
        let KRs = [...keyResults]
        KRs.find(item => item.order === order).peopleOpsStatus = name;
        this.setState({
            keyResults: KRs
        })
        this.props.iUpdated();
    }


    handleSaveClick = () => {
        const {id, categoryId, sbuObjectiveId, staffAppraisalId, keyResults, description, order, peopleOpsStatus, peopleOpsComment} = this.state;
        const { match, dispatch, objective, isFirstSave } = this.props
        //Change the status on the manage OKRs page to "in progress" when the line manager makes his first save.
        if(isFirstSave){
            dispatch({ type: UPDATE_PEOPLE_OPS_STATUS, staffAppraisalId, peopleOpsStatus: "inprogress"})
        }
        dispatch({ type: SAVE_KEY_RESULTS, data: { id, order, peopleOpsStatus, peopleOpsComment, lineManagerStatus: this.props.objective.lineManagerStatus, lineManagerComment: this.props.objective.lineManagerComment, description, categoryId, sbuObjectiveId, staffAppraisalId, keyResults }, staffId: match.params.id })
        this.props.iUpdated()
    }



    render(){
        const { REQUESTING_SAVE_KEY_RESULTS, SAVING_OBJECTIVE_STATUS, objective, objIndex, userCategories, categoryId, isFirstSave, match } = this.props
        const { keyResults, description, peopleOpsStatus, peopleOpsComment  } = this.state
        // console.log("cat", isFirstSave)
        // console.log("here's the nigga", objective)
        return(
            <Fragment>
                <Header as="h2" textAlign="center" content="Approve OKRs"/>
                <Segment>
                    <div>
                        <Grid>
                            <Grid.Column width={12}>
                                <Header style={{display: "inline"}} as="h4" content={userCategories[categoryId].name === "Business" ? `Business Objective ${objIndex + 1}` : `Personal Objective`} subheader={objective.description}/>
                            </Grid.Column>
                            <Grid.Column  width={4} textAlign="right">
                                <Segment basic>
                                    <Popup trigger={<Button circular icon='check' color={peopleOpsStatus==="approved" ? "green" : null} onClick={(e, titleProps) => this.handleObjApproval(e, titleProps)}/>} content="approve"/>
                                    <Popup trigger={<Button circular icon='cancel' color={peopleOpsStatus==="disapproved" ? "red" : null} onClick={(e, titleProps) => this.handleObjApproval(e, titleProps)}/>} content="disapprove"/>
                                </Segment>
                            </Grid.Column>
                        </Grid>
                        { peopleOpsStatus === "disapproved" && 
                            <Form style={{marginTop: "20px"}}>
                                <Form.TextArea 
                                    name="textfield" 
                                    onChange={(e) => this.handleObjCommentChange(e)} 
                                    value={peopleOpsComment}
                                    placeholder="Please give reasons for disagreeing with this objective OR suggest a suitable objective"/>
                            </Form>
                        }
                        <Divider  style={{marginTop: "20px"}} horizontal> Key Results </Divider>
                        { keyResults.map((item,index) => <KeyResult key={item.id} order={item.order} data={item} handleApproval={(e, order) => this.handleApproval(e, order)} handleCommentEdit={(e, order) => this.handleCommentEdit(e, order)} iUpdated={this.iUpdated}/>) }
                    </div>
                    <br/>
                    <Segment basic textAlign="right">
                        { SAVING_OBJECTIVE_STATUS === true && 
                            <Label pointing='right'color="green" >Save Successful</Label>
                        }
                        { SAVING_OBJECTIVE_STATUS === false && 
                            <Label pointing='right' color="red" >Save Failed. Please try again or refresh page.</Label>
                        }
                        <Button content="Save" 
                                loading={REQUESTING_SAVE_KEY_RESULTS ? true : false} 
                                basic 
                                onClick={this.handleSaveClick} 
                                disabled={!(keyResults.filter(item => item.peopleOpsStatus === "disapproved").every(item => item.peopleOpsComment !== ""))}
                        />
                    </Segment>
                </Segment>
            </Fragment>
        )
    }
}


function mapStateToProps(state){
    return {
        REQUESTING_SAVE_KEY_RESULTS: state.REQUESTING_SAVE_KEY_RESULTS,
        SAVING_OBJECTIVE_STATUS: state.SAVING_OBJECTIVE_STATUS
    }
}

export default connect(mapStateToProps)(Results)