import React from 'react'
import { Table } from 'semantic-ui-react'
import { connect } from "react-redux"
import { poStaffAppraisalsSelector } from '../../reducer';

class Header extends React.Component {

    render() {
        const { poStaffAppraisals, id, totalKRs, treatedKRs, disapprovedKRs, totalOBJs, treatedOBJs, disapprovedOBJs } = this.props
        const info = () => poStaffAppraisals.length > 0 ? poStaffAppraisals.find(item => item.staffId === id) : {}

        return (
            <Table textAlign="center">
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell colSpan='4'>{`${info().fullName} - ${info().sbu}`}</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    <Table.Row>
                        {/* <Table.Cell>{info().emailAddress}</Table.Cell> */}
                        {/* <Table.Cell>{id === "VGG-021" ? "Avitech" : "VGN"}</Table.Cell> */}
                        <Table.Cell>{`${treatedOBJs}/${totalOBJs} objectives treated`}</Table.Cell>
                        <Table.Cell>{`${disapprovedOBJs} objective(s) disapproved`}</Table.Cell>
                        <Table.Cell>{`${treatedKRs}/${totalKRs} key results treated`}</Table.Cell>
                        <Table.Cell>{`${disapprovedKRs} key result(s) disapproved`}</Table.Cell>
                    </Table.Row>
                </Table.Body>
            </Table>
        )
    }
}

function mapStateToProps(state){
    return {
        poStaffAppraisals: poStaffAppraisalsSelector(state),
    }
}

export default connect(mapStateToProps)(Header)