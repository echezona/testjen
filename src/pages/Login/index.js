import React, { Component } from 'react'
// import { SSO_URL } from '../../appConstants'
import { Route, withRouter } from 'react-router-dom'
import { connect } from "react-redux"
import { Button, Icon } from 'semantic-ui-react'
import './login.css'


class Login extends Component {
    state = {
        loginClicked: false,
        onboardClicked: false,
    }

    handleLogin = (e) => {
        // e.persist();
        // e.preventDefault();
        this.setState({ loginClicked: true })
        this.props.client.signinRedirect().catch(() => this.setState({ loginClicked: false }));
    }

    handleOnboard = (e) => {
        // e.persist();
        // e.preventDefault();
        this.setState({ onboardClicked: true })
        // this.props.client.signinRedirect();
        this.props.history.push('/onboarding')
    }

    render() {
        const { loginClicked, onboardClicked } = this.state
        return (
            <section>
                <div class="container">
                    <div class="bar"></div>
                    <div class="content-box">
                        <span class="acronym">PMS</span>
                        <span class="fullname">Performance Management System</span>
                    </div>

                    <div>
                        <Button onClick={(e) => this.handleLogin(e)}
                            loading={loginClicked}
                            disabled={loginClicked || onboardClicked}
                            size='huge'
                            color="blue"
                            labelPosition="right"
                            icon="long arrow alternate right"
                            content="SIGN IN"
                        />

                        <Button onClick={(e) => this.handleOnboard(e)}
                            loading={onboardClicked}
                            disabled={onboardClicked || loginClicked}
                            size='huge'
                            color="green"
                            labelPosition="right"
                            icon="users"
                            content="ONBOARD"
                            basic
                        />
                    </div>

                    <div class="bar"></div>
                </div>
            </section>
        )
    }

}





// const Login = (props) => {
//     const handleLogin = function(e){
//         e.persist();
//         e.preventDefault();
//         props.client.signinRedirect();
//     }

//     return (
//         <section>
//             <div class="container">
//                 <div class="bar"></div>
//                 <div class="content-box">
//                     <span class="acronym">PMS</span>
//                     <span class="fullname">Performance Management System</span>
//                 </div>
//                 {/* <div class="button" onClick={handleLogin}>
//                     <a class="sign-in">
//                         <div id="btn-pt-1">SIGN IN</div>
//                         <div id="btn-pt-2">🡪</div>
//                     </a>
//                 </div> */}
//                 <Button onClick={(e) => handleLogin(e)} loading>SIGN IN</Button>
//                 <div class="bar"></div>
//             </div>
//         </section>
//     )
// }



export default withRouter(Login)