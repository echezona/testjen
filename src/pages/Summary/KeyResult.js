import React, { Fragment } from 'react'
import { Segment, Header, Grid, Container, Button, Message, Form } from 'semantic-ui-react'
import { connect } from 'react-redux'

import { userObjectivesSelector, userIDsSelector, userKeyResultsSelector, staffAppraisalSelector } from '../../reducer'


class KeyResult extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            comment: props.value
        }
        console.log("keyres", props.data)
    }

    handleCommentChange = (e) => {
        const { value } = e.target
        this.setState({
            comment: value
        })
    }

    render() {
        const { order, header, lineManagerComment, peopleOpsComment, secondApproved, firstApproved, handleCommentEdit, id } = this.props
        return (
            <Segment>
                <Header as="h4" content={`Key Result ${order}`} />
                <Form>
                    <Form.TextArea
                        name="textfield"
                        onChange={(e) => { this.handleCommentChange(e); handleCommentEdit(e, id) }}
                        value={this.state.comment}
                        placeholder="Please give reasons for disagreeing with this key result" />
                </Form>
                {secondApproved === "disapproved" && peopleOpsComment &&
                    <div>
                        <div style={{ color: "red", display: "inline-block", marginTop: "20px" }}>People Ops Comment:</div>
                        <div style={{ color: "gray", display: "inline-block", paddingLeft: "10px" }}>{peopleOpsComment}</div>
                    </div>
                }
                {firstApproved === "disapproved" && lineManagerComment &&
                    <div>
                        <div style={{ color: "red", display: "inline-block", marginTop: "20px" }}>Line Manager Comment:</div>
                        <div style={{ color: "gray", display: "inline-block", paddingLeft: "10px" }}>{lineManagerComment}</div>
                    </div>
                }
                {/* <Header as="h5" content={`${lineManagerComment}`}/> */}
            </Segment>
        )
    }
}



export default KeyResult