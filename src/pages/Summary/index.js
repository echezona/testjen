import React, { Fragment } from 'react'
import { Loader } from 'semantic-ui-react'
import { connect } from 'react-redux'

import { FETCH_OBJECTIVES_AND_KEY_RESULTS, FETCH_STAFF_APPRAISAL, FETCH_SBU_OBJECTIVES, FETCH_STAFF_DETAILS, FETCH_CATEGORY } from '../../constants';
import { userObjectivesSelector, userIDsSelector, userKeyResultsSelector, staffAppraisalSelector } from '../../reducer'
import Submit from './Submit'
import Review from './Review'
import EditOKRs from './EditOKRs';
import { ToastContainer } from 'react-toastify';
import { withRouter } from 'react-router';


//Include check for whether or not the user has met the criteria for okr submission (1 obj, 1 sbuobj, 3 key results)...

class Summary extends React.Component {
    componentDidMount() {
        const { dispatch, history } = this.props;
        dispatch({ type: FETCH_OBJECTIVES_AND_KEY_RESULTS })
        dispatch({ type: FETCH_STAFF_APPRAISAL })
        dispatch({ type: FETCH_STAFF_DETAILS, userEmail: this.props.user.profile.email })
        dispatch({ type: FETCH_CATEGORY })
        dispatch({type: FETCH_SBU_OBJECTIVES})
    }


    render() {        
        const { staffAppraisal: {  firstApproved,lineManagerStatus,superLineManagerStatus, staffSubmitted }, requestingOKRs, fetchingOkrsLoadingStatus } = this.props

        return (
            <Fragment>
                <ToastContainer/>
                {(staffSubmitted || (firstApproved !== "disapproved")) &&
                    <Submit {...this.props}/>
                }
                {(!staffSubmitted && (firstApproved === "disapproved")) &&
                    <EditOKRs {...this.props}/>
                }
            </Fragment>
        )
    }
}

// {
// (firstApproved !== "disapproved" && secondApproved !== "disapproved") &&
//     <Submit />
// }
// {
// (firstApproved === "disapproved" || secondApproved === "disapproved") &&
//     <Review />
// }


function mapStateToProps(state) {
    return {
        staffAppraisal: staffAppraisalSelector(state),
        requestingOKRs: state.fetchLoadingReducer.FETCH_OBJECTIVES_AND_KEY_RESULTS_LOADING_STATUS,
        fetchingOkrsLoadingStatus: state.fetchLoadingReducer.FETCH_OBJECTIVES_AND_KEY_RESULTS_LOADING_STATUS,
    }
}


export default withRouter(connect(mapStateToProps)(Summary));