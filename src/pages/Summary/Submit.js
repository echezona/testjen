import React from 'react'
import { Segment, Header, Grid, Loader, Button, Message, Divider, Label } from 'semantic-ui-react'
import { connect } from 'react-redux'
import { withRouter, Link } from 'react-router-dom'

import { userObjectivesSelector, userIDsSelector, userKeyResultsSelector, staffAppraisalSelector, sbuObjectivesSelector } from '../../reducer'
import { SUBMIT_OKRS, FETCH_OBJECTIVES_AND_KEY_RESULTS, FETCH_STAFF_APPRAISAL, FETCH_STAFF_DETAILS } from '../../constants';
import { sortFunctionOne } from '../../helper'

class Submit extends React.Component {

    componentDidMount() {
        const { dispatch, history } = this.props;
        dispatch({ type: FETCH_OBJECTIVES_AND_KEY_RESULTS })
        dispatch({ type: FETCH_STAFF_APPRAISAL })
        dispatch({ type: FETCH_STAFF_DETAILS, userEmail: this.props.user.profile.email })
        // dispatch({type: FETCH_SBU_OBJECTIVES})
    }

    handleEditButtonClick = (e) => {
        const { history } = this.props;
        const { name } = e.target;
        history.push(name)
    }

    handleSubmitButtonClick = () => {
        const { dispatch } = this.props;
        dispatch({ type: SUBMIT_OKRS })
    }

    componentDidUpdate(){
        const {staffAppraisal: {staffSubmitted}, history, userObjectives, userIDs } = this.props
        if(staffSubmitted){
            // document.getElementById('green').scrollIntoView({behavior: 'smooth'})
        }

        if(userObjectives && (!userIDs.length >= 4 || !userIDs.map(item => userObjectives[item].keyResults).every(item => item.length >= 3) || !userIDs.map(item => userObjectives[item].order).includes("Personal"))){
            history.push("/businessobjectives")
        }
    }

    render() {
        const { requestingOKRs, isSubmittingOKRs, userIDs, userObjectives, userKeyResults, sbuObjectives, staffAppraisal: { firstApproved, secondApproved,peopleOpsStatus,lineManagerStatus,superLineManagerStatus,staffSubmitted }, SUBMIT_OKRS_STATUS } = this.props
        console.log("ids:", userIDs)
        console.log("objs:", userObjectives)
        console.log("key results: ", userKeyResults)
        console.log("SBU-OBJs", sbuObjectives)
        console.log("STAAF APPRASAL", lineManagerStatus)

        if(requestingOKRs){
            return (
                <div>
                    <Loader active/>
                </div>
            )
        }
       
        return (
            <Grid columns={16}>
                <Grid.Column width={3}></Grid.Column>
                <Grid.Column width={10}>
                    <Segment basic>
                        <Header as="h2" content="Summary" />

                        {staffSubmitted &&
                           
                           <Message id='green' color="green">You have successfully submitted your OKRs.</Message>
                        
                        }
                         {staffSubmitted &&
                           
                           <Message color="green">  Line manager status is "{lineManagerStatus}"</Message>
                        
                        }
                         {staffSubmitted &&
                           
                           <Message color="green"> Super Line manager status is "{superLineManagerStatus}"</Message>
                        
                        }

                        {!staffSubmitted &&
                            <Message color="yellow">Please review your OKRs carefully before you submit as you'll be unable to edit them once you do</Message>
                        }

                        <Segment style={{ maxHeight: 500, overflow: "scroll" }} centered>
                            {
                                userIDs.sort((a,b) => sortFunctionOne(userObjectives[a], userObjectives[b])).map((item) => {
                                    return (
                                        <Segment>
                                            <Header as="h4" content={userObjectives[item].order > 0 && userObjectives[item].order < 6 ? `Objective ${userObjectives[item].order}` : `${userObjectives[item].order} Objective`} />
                                            <div>{`Staff Objective: ${userObjectives[item].description}`}</div>
                                            {userObjectives[item].order > 0 && userObjectives[item].order < 6 && Object.prototype.toString.call(sbuObjectives) === '[object Array]' &&
                                                <div>{`SBU Objective: ${sbuObjectives.find(sbuObj => sbuObj.id === userObjectives[item].sbuObjectiveId) !== undefined ? sbuObjectives.find(sbuObj => sbuObj.id === userObjectives[item].sbuObjectiveId).description : "None"}`}</div>                                            
                                            }
                                            <Divider />
                                            <Segment basic>
                                                {
                                                    userObjectives[item].keyResults.sort((a,b)=>userKeyResults[a].order > userKeyResults[b].order ? 1 : -1).map((item) => <Header as="h5" content={`Key Result ${userKeyResults[item].order}`} subheader={userKeyResults[item].description} />)
                                                }
                                            </Segment>
                                        </Segment>
                                    )
                                })
                            }
                        </Segment>
                        {!staffSubmitted &&
                            // <Container>
                            //     <Button content="Submit" onClick={this.handleSubmitButtonClick} floated="right" />
                            //     <Button name="/businessobjectives" content="Edit" onClick={this.handleEditButtonClick} floated="right" />
                            // </Container>
                            <Button.Group floated='right'>
                                { SUBMIT_OKRS_STATUS === true && 
                                    <Label pointing='right'color="green" >Save Successful</Label>
                                }
                                { SUBMIT_OKRS_STATUS === false && 
                                    <Label pointing='right' color="red" >Save Failed. Please try again or refresh page.</Label>
                                }
                                <Button name="/businessobjectives" content="Edit" onClick={this.handleEditButtonClick} floated="right" />                               
                                <Button.Or />
                                <Button color='blue' content="Submit" onClick={this.handleSubmitButtonClick} floated="right" loading={isSubmittingOKRs} 
                                    disabled={isSubmittingOKRs || userIDs.length < 1}
                                />
                            </Button.Group>
                        }
                    </Segment>
                </Grid.Column>
            </Grid>
        )
    }
}


function mapStateToProps(state) {
    return {
        userIDs: userIDsSelector(state),
        userObjectives: userObjectivesSelector(state),
        userKeyResults: userKeyResultsSelector(state),
        staffAppraisal: staffAppraisalSelector(state),
        sbuObjectives: sbuObjectivesSelector(state),
        isSubmittingOKRs: state.isSubmittingOKRs,
        SUBMIT_OKRS_STATUS: state.SUBMIT_OKRS_STATUS,
    }
}


export default withRouter(connect(mapStateToProps)(Submit))