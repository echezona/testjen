import React, {Fragment} from 'react'
import { Segment, Header, Grid, Container, Button, Message, Form, Label, Loader } from 'semantic-ui-react'
import { connect } from 'react-redux'
import _ from 'lodash'

import { userObjectivesSelector, userIDsSelector, userKeyResultsSelector, staffAppraisalSelector, sbuObjectivesSelector } from '../../reducer'
import KeyResult from './KeyResult'
import { UPDATE_KEY_RESULTS } from '../../constants';



class Review extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            keyResults: [],
            objectives: [],
            // unChangedKRs: []
        }
    }

    handleObjCommentEdit = (e) => {
        const {value, name } = e.target  // the name is the id
        const {objectives} = this.state
        let OBJs = _.cloneDeep(objectives)
        console.log("beep beep", OBJs)
        console.log("beep beep", name)
        OBJs.find(item => item.id === name).description = value
        this.setState({
            objectives: OBJs,
        })
    }

    handleCommentEdit = (e, id) => {
        const { keyResults } = this.state;
        const { value} = e.target; 
        let KRs = _.cloneDeep(keyResults)
        KRs.find(item => item.id === id).description = value;
        console.log("KRs: ", KRs)
        this.setState({
            keyResults: KRs
        })
    }

    handleSubmitButtonClick = () => {
        const { keyResults, objectives } = this.state;
        const { dispatch, staffAppraisal: {secondApproved, id}, staffAppraisal } = this.props;
        let KRs = [...keyResults]
        let OBJs = [...objectives]
        KRs.forEach(item => {
            item.peopleOpsStatus = "pending";
            item.lineManagerStatus = "pending";
        })
        OBJs.forEach(item => {
            item.peopleOpsStatus = "pending";
            item.lineManagerStatus = "pending";
            delete item.keyResults;
            delete item.sbuObjective;
            delete item.sbuObjectiveId;
        })
        console.log("OBJS", OBJs)
        console.log("KRs", KRs)
        console.log("staffAppraisal", staffAppraisal)
        // console.log("see what you're posting oo", OBJs)
        // secondApproved === "disapproved" ? OBJs.forEach(item => item.peopleOpsStatus = "pending") : OBJs.forEach(item => item.lineManagerStatus = "pending")
        dispatch({ type: UPDATE_KEY_RESULTS, KRs, OBJs, id })
    }

    componentDidUpdate() {
        const { userObjectives, userKeyResults, userIDs } = this.props

        if (this.state.keyResults.length < 1 && userIDs.sort(this.sortFunctionOne).map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).filter(item => item.lineManagerStatus === "disapproved" || item.peopleOpsStatus === "disapproved").length > 0) {
            this.setState({
                keyResults: [...userIDs.sort(this.sortFunctionOne).map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).filter(item => item.lineManagerStatus === "disapproved" || item.peopleOpsStatus === "disapproved")],
            })
        }

        if (this.state.objectives.length < 1 &&  userIDs.sort(this.sortFunctionOne).map(item => userObjectives[item]).filter(item => item.lineManagerStatus === "disapproved" || item.peopleOpsStatus === "disapproved").length > 0) {
            this.setState({
                objectives: userIDs.sort(this.sortFunctionOne).map(item => userObjectives[item]).filter(item => item.lineManagerStatus === "disapproved" || item.peopleOpsStatus === "disapproved"),
            })
        }
        console.log("state krs", this.state.keyResults)
    }

    //userIDs sort function
    sortFunctionOne = (a, b) => {
        const { userObjectives } = this.props
        if (userObjectives[a].order < userObjectives[b].order) {
            return -1
        } else if (userObjectives[a].order > userObjectives[b].order) {
            return 1
        } else {
            return 0
        }
    }


    render() {
        const { userIDs, userObjectives, userKeyResults, staffAppraisal, staffAppraisal: { staffSubmitted, firstApproved, secondApproved }, isSubmittingOKRs, updateKeyResultsLoadingStatus, UPDATE_KEY_RESULTS_STATUS } = this.props
        const { keyResults, objectives } = this.state

        console.log(this.state)
        //console.log("the staffff info is here", staffAppraisal)
        console.log("whats this", userIDs.map(item => userObjectives[item].order).includes("Personal"))

        //every

        const submitEnabled = () => {
            let valArr = [];
            const unchangedKRs = [...userIDs.sort(this.sortFunctionOne).map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).filter(item => item.lineManagerStatus === "disapproved" || item.peopleOpsStatus === "disapproved")];
            keyResults.forEach(flexItem => {
                let val = unchangedKRs.find(solidItem => solidItem.description === flexItem.description)
                // console.log("val", val)
                if (val !== undefined) {
                    valArr.push(val)
                }
            })
            // console.log("valArr", valArr)
            // console.log("state", keyResults)
            // console.log("const", unchangedKRs)
            if (valArr.length !== 0) {
                return false
            } else {
                return true
            }
        }

        const submitEnabled2 = keyResults.every(item => item.description !== "")

        const submitEnabled3 = () => {
            let valArr = [];
            const unchangedOBJs = [...userIDs.sort(this.sortFunctionOne).map(item => userObjectives[item]).filter(item => item.lineManagerStatus === "disapproved" || item.peopleOpsStatus === "disapproved")];
            objectives.forEach(flexItem => {
                let val = unchangedOBJs.find(solidItem => solidItem.description === flexItem.description)
                // console.log("val", val)
                if (val !== undefined) {
                    valArr.push(val)
                }
            })
            // console.log("valArrobj", valArr)
            // console.log("stateobj", objectives)
            // console.log("const", unchangedOBJs)
            if (valArr.length !== 0) {
                return false
            } else {
                return true
            }
        }

        const submitEnabled4 = objectives.every(item => item.description !== "")
        // console.log("beep", userIDs.sort(this.sortFunctionOne).map(item => userObjectives[item]).filter(item => item.lineManagerStatus === "disapproved" || item.peopleOpsStatus === "disapproved"))
        // console.log("submitProgess", submitEnabled2)
        // console.log("actual key results", userIDs.sort(this.sortFunctionOne).map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]))
        // console.log("filtered key results", userIDs.sort(this.sortFunctionOne).map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).filter(item => item.lineManagerStatus === "disapproved" || item.peopleOpsStatus === "disapproved"))
        if(userIDs.length < 1 ){
            return (
                <Segment basic textAlign='center'>
                    <Loader active/>
                </Segment>
            )
        }
        
        return (
            <Grid columns={16}>
                <Grid.Column width={3}></Grid.Column>
                <Grid.Column width={10}>
                    <Segment basic>
                        <Header as="h2" content="Update OKRs" />

                        {staffSubmitted &&
                            <Message color="green">You have successfully submitted your OKRs to your Line Manager</Message>
                        }

                        {!staffSubmitted &&
                            <Message color="yellow">Please use the respective comment of each Objective / Key Result to guide you in making the appropriate changes</Message>
                        }

                        {/* content box for Objectives */}
                        { this.state.objectives.length > 0 &&
                            <Fragment>
                                <Header as="h5" content="Objectives" /> {/*could be a label*/}
                                <Segment style={{ maxHeight: 500, overflow: "scroll" }} centered>
                                    {
                                        objectives.map(item => {
                                            // const {id} = item
                                            return (
                                                <Segment>
                                                    <Header as="h4" content={item.order > 0 && item.order < 6 ? `Objective ${item.order}` : `${item.order} Objective`}/>
                                                    <Form>
                                                        <Form.TextArea
                                                            name={item.id}
                                                            onChange={(e) => this.handleObjCommentEdit(e)}
                                                            value={item.description}
                                                            placeholder="Please edit this objective appropriately" />
                                                    </Form>
                                                    { secondApproved==="disapproved" && item.peopleOpsComment &&
                                                        <div>
                                                            <div style={{color: "red", display: "inline-block", marginTop: "20px"}}>People Ops Comment:</div>
                                                            <div style={{color: "gray", display: "inline-block", paddingLeft: "10px"}}>{item.peopleOpsComment}</div>
                                                        </div>
                                                    }
                                                    { firstApproved==="disapproved" && item.lineManagerComment &&
                                                        <div>
                                                            <div style={{color: "red", display: "inline-block", marginTop: "20px"}}>Line Manager Comment:</div>
                                                            <div style={{color: "gray", display: "inline-block", paddingLeft: "10px"}}>{item.lineManagerComment}</div>
                                                        </div>
                                                    }
                                                </Segment>
                                            )
                                        })
                                    }
                                </Segment>
                            </Fragment>
                        }
                        

                        {/* content box for Objectives */}
                        { this.state.keyResults.length > 0 &&
                            <Fragment>
                                <Header as="h5" content="Key Results" />
                                <Segment style={{ maxHeight: 500, overflow: "scroll" }} centered>
                                    {
                                        userIDs.sort(this.sortFunctionOne).map(item => userObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => userKeyResults[item]).filter(item => item.lineManagerStatus === "disapproved" || item.peopleOpsStatus === "disapproved").map(item => item.appraisalObjectiveId).filter((item, index, self) => self.indexOf(item) === index).map((item) => {
                                            return (
                                                <Segment>
                                                    <Header as="h4" content={userObjectives[item].order > 0 && userObjectives[item].order < 6 ? `Objective ${userObjectives[item].order}` : `${userObjectives[item].order} Objective`} subheader={userObjectives[item].description} dividing />
                                                    <Segment basic>
                                                        {
                                                            userObjectives[item].keyResults.map(item => userKeyResults[item]).filter(item => item.lineManagerStatus === "disapproved" || item.peopleOpsStatus === "disapproved").map(item => item.id).map((item) => <KeyResult id={userKeyResults[item].id} data={userKeyResults[item]} order={userKeyResults[item].order} value={userKeyResults[item].description} lineManagerComment={userKeyResults[item].lineManagerComment} peopleOpsComment={userKeyResults[item].peopleOpsComment} secondApproved={secondApproved} firstApproved={firstApproved} handleCommentEdit={(e, id) => this.handleCommentEdit(e, id)} />)
                                                        }
                                                    </Segment>
                                                </Segment>
                                            )
                                        })
                                    }
                                </Segment>
                            </Fragment>
                        }
                        

                        <Segment basic>
                            {UPDATE_KEY_RESULTS_STATUS === true &&
                                <Label pointing='right' color="green" >Save Successful</Label>
                            }
                            {UPDATE_KEY_RESULTS_STATUS === false &&
                                <Label pointing='right' color="red" >Save Failed. Please try again or refresh page.</Label>
                            }
                            <Button content="Submit"
                                onClick={() => this.handleSubmitButtonClick()}
                                floated="right"
                                disabled={!(submitEnabled() && submitEnabled2 && submitEnabled3() && submitEnabled4) || updateKeyResultsLoadingStatus}
                                color='blue'
                                icon='right arrow'
                                labelPosition='right'
                                loading={updateKeyResultsLoadingStatus}
                            />
                        </Segment>

                    </Segment>
                </Grid.Column>
            </Grid>
        )
    }
}

function mapStateToProps(state) {
    return {
        userIDs: userIDsSelector(state),
        userObjectives: userObjectivesSelector(state),
        userKeyResults: userKeyResultsSelector(state),
        staffAppraisal: staffAppraisalSelector(state),
        sbuObjectives: sbuObjectivesSelector(state),
        isSubmittingOKRs: state.isSubmittingOKRs,
        updateKeyResultsLoadingStatus: state.saveLoadingReducer.UPDATE_KEY_RESULTS_LOADING_STATUS,
        UPDATE_KEY_RESULTS_STATUS: state.UPDATE_KEY_RESULTS_STATUS,
    }
}

export default connect(mapStateToProps)(Review)