import React, {useState, Fragment, useEffect} from 'react';
import { Button, Header, Segment, Grid, Divider, Loader } from 'semantic-ui-react';
import { sortFunctionOne } from '../../../helper';
import { SUBMIT_OKRS, FETCH_OBJECTIVES_AND_KEY_RESULTS, REQUEST_ERROR } from '../../../constants';
import { toast } from 'react-toastify';

const Summary = ({ 
    userIDs, userObjectives, userKeyResults, sbuObjectives, 
    staffAppraisal: {staffSubmitted}, isSubmittingOKRs, history, 
    handlePageChange, dispatch, requestingOKRs
}) => {

    useEffect(() => {
        // dispatch({ type: REQUEST_ERROR })
        dispatch({ type: FETCH_OBJECTIVES_AND_KEY_RESULTS })
    }, [])

    const handleEditButtonClick = () => {
        handlePageChange()
    }

    const handleSubmitButtonClick = () => {
        
        if (!validations.HAS_REACHED_MINIMUM_ALLOWED_BUSINESS_OBJECTIVES()) {
            return toast.warn('You need a minimum of 3 business objectives')
        }

        if (!validations.DO_ALL_OBJECTIVES_HAVE_MINIMUM_ALLOWED_KEY_RESULTS()) {
            return toast.warn('All objectives need a minimum of three (3) key results')
        }

        if (!validations.IS_PERSONAL_OBJECTIVE_PRESENT()) {
            return toast.warn('You need to have a personal development objective')
        }

        dispatch({ type: SUBMIT_OKRS })
    }

    const validations = {
        DO_ALL_OBJECTIVES_HAVE_MINIMUM_ALLOWED_KEY_RESULTS: () => (
            userIDs
                .sort((a, b) => sortFunctionOne(userObjectives[a], userObjectives[b]))
                .map(item => userObjectives[item])
                .every(item => item.keyResults.length >= 3)
        ),
        IS_PERSONAL_OBJECTIVE_PRESENT: () => (
            userIDs
                .sort((a, b) => sortFunctionOne(userObjectives[a], userObjectives[b]))
                .map(item => userObjectives[item])
                .some(item => item.order == 'Personal')
        ),
        HAS_REACHED_MINIMUM_ALLOWED_BUSINESS_OBJECTIVES: () => (
            userIDs
                .sort((a, b) => sortFunctionOne(userObjectives[a], userObjectives[b]))
                .map(item => userObjectives[item])
                .filter(item => item.order != 'Personal')
                .length >= 3
        ),
    }

    if (requestingOKRs) {
        return (            
            <div>
                <Loader active/>
            </div>
        )
    }


    return (
        <Grid columns={16}>
            <Grid.Column width={3}></Grid.Column>
            <Grid.Column width={10}>
                <Segment basic>
                    <Header as="h2" content="Summary" />
                    <Segment centered>
                        {
                            userIDs.sort((a,b) => sortFunctionOne(userObjectives[a], userObjectives[b])).map((item) => {
                                return (
                                    <Segment>
                                        <Header as="h4" content={userObjectives[item].order > 0 && userObjectives[item].order < 6 ? `Objective ${userObjectives[item].order}` : `${userObjectives[item].order} Objective`} />
                                        <div>{`Staff Objective: ${userObjectives[item].description}`}</div>
                                        {userObjectives[item].order > 0 && userObjectives[item].order < 6 && Object.prototype.toString.call(sbuObjectives) === '[object Array]' &&
                                            <div>{`SBU Objective: ${sbuObjectives.find(sbuObj => sbuObj.id === userObjectives[item].sbuObjectiveId) !== undefined ? sbuObjectives.find(sbuObj => sbuObj.id === userObjectives[item].sbuObjectiveId).description : "None"}`}</div>                                            
                                        }
                                        <Divider />
                                        <Segment basic>
                                            {
                                                userObjectives[item].keyResults.sort((a,b)=>userKeyResults[a].order > userKeyResults[b].order ? 1 : -1).map((item) => <Header as="h5" content={`Key Result ${userKeyResults[item].order}`} subheader={userKeyResults[item].description} />)
                                            }
                                        </Segment>
                                    </Segment>
                                )
                            })
                        }
                    </Segment>
                    {!staffSubmitted &&
                        // <Container>
                        //     <Button content="Submit" onClick={this.handleSubmitButtonClick} floated="right" />
                        //     <Button name="/businessobjectives" content="Edit" onClick={this.handleEditButtonClick} floated="right" />
                        // </Container>
                        <Button.Group floated='right'>
                            {/* { SUBMIT_OKRS_STATUS === true && 
                                <Label pointing='right'color="green" >Save Successful</Label>
                            }
                            { SUBMIT_OKRS_STATUS === false && 
                                <Label pointing='right' color="red" >Save Failed. Please try again or refresh page.</Label>
                            } */}
                            <Button name="/businessobjectives" content="Edit" onClick={handleEditButtonClick} floated="right" />                               
                            <Button.Or />
                            <Button color='blue' content="Submit" onClick={handleSubmitButtonClick} floated="right" loading={isSubmittingOKRs} 
                                disabled={isSubmittingOKRs || userIDs.length < 1}
                            />
                        </Button.Group>
                    }
                </Segment>
            </Grid.Column>
        </Grid>
    )
}

export default Summary