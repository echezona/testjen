import React, { useState } from 'react';
import { Header, Dropdown, Label, Button, Divider, Icon, Form, Segment } from 'semantic-ui-react';
import './index.css';
import { sortFunctionOne } from '../../../helper';
import {cloneDeep} from 'lodash';
import { DELETE_KEY_RESULT } from '../../../constants';

const KeyResult = ({krItem, userIDs, userObjectives, userKeyResults, staffAppraisalId, dispatch}) => {

    const [ellipsisMenuOption, setEllipsisMenuOption] = useState('');
    const [krDescription, setKrDescription] = useState(krItem.description)

    const handleEllipsisMenuChange = (e, titleProps) => {
        setEllipsisMenuOption(titleProps.value);

        if(titleProps.value == 'delete') {
            let orderBase = 1
            const OKRs = userIDs.sort((a, b) => sortFunctionOne(userObjectives[a], userObjectives[b])).map(item => userObjectives[item])
                            // .filter(item => item.order !== objItem.order)
                            

            const OBJs = cloneDeep(OKRs)
                            .reduce((acc, curr) => {
                                // curr.peopleOpsStatus = "pending";
                                // curr.lineManagerStatus = "pending";
                                delete curr.keyResults;
                                delete curr.sbuObjective;
                                // delete curr.sbuObjectiveId;
                                acc.push(curr)
                                return acc
                            }, [])


            const relevantKRs = cloneDeep(OKRs)
                                    .flatMap(item => item.keyResults)
                                    .map(item => userKeyResults[item])
                                    .filter(item => item.appraisalObjectiveId == krItem.appraisalObjectiveId && item.order !== krItem.order)
                                    .reduce((acc, curr) => {
                                        curr.order = orderBase.toString();
                                        orderBase++;
                                        acc.push(curr)
                                        return acc
                                    }, [])

            const otherKRs = cloneDeep(OKRs)
                                .flatMap(item => item.keyResults)
                                .map(item => userKeyResults[item])
                                .filter(item => item.appraisalObjectiveId != krItem.appraisalObjectiveId)
                                
            const KRs = [...relevantKRs, ...otherKRs]
                            


            const forUpdateKeyResults = {KRs, OBJs, id: staffAppraisalId, krDeleteFlow: true }

            console.log('damn', forUpdateKeyResults)

            dispatch({ type: DELETE_KEY_RESULT, id: krItem.id, forUpdateKeyResults })
        }
    }

    const handleDescriptionChange = (e) => {
        setKrDescription(e.target.value);
        krItem.description = e.target.value;
        console.log('oh', krItem)
    }

    return (
        <Segment>
            <div>
                <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                    <div style={{ display: 'inline-block' }}>
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                            <Header 
                                as='h4' 
                                content={`Key Result ${krItem.order}`}
                                style={{ display: 'inline-block', margin: '0 10px 0 0' }}    
                            />
                            <Label
                                content={krItem.lineManagerStatus == 'disapproved' ? 'DENIED' : 'OK'}
                                color={krItem.lineManagerStatus == 'disapproved' ? 'red' : 'blue'}
                                size='mini'
                            />
                        </div>
                    </div>
                    <Dropdown 
                        id='ellipsis-dropdown'
                        icon={null}
                        direction='left'
                        trigger = {(
                            <Button circular icon='ellipsis vertical' />
                        )}
                        inline
                        header='OPTIONS'
                        options={[
                            {text: "Select an action", value:''},
                            {text: "Edit Key Result", value: "edit", icon:'edit'},
                            {text: "Delete Key Result", value: "delete", icon:'delete'},
                        ]}
                        onChange={(e, titleProps) => handleEllipsisMenuChange(e, titleProps)}
                        // loading={deleteLoadingStatus}
                        // disabled={deleteLoadingStatus}
                    />
                </div>
                <Divider style={{marginTop: '4px'}} />
                <p>
                    { ellipsisMenuOption !== 'edit' && krItem.description }
                    { ellipsisMenuOption == 'edit' &&
                        <Form>
                            <textarea rows={2} value={krDescription} placeholder='Enter an objective' onChange={handleDescriptionChange}/>
                        </Form>
                    }
                </p>
                { krItem.lineManagerStatus == 'disapproved' &&
                    <>
                        <div>
                            <Icon name='comment alternate outline' />
                            <Header as='h5' content='Line Manager Comment' style={{ display: 'inline-block', margin: '0 0 0 4px' }}  />
                        </div>
                        <p>{krItem.lineManagerComment}</p>
                    </>
                }
            </div>
        </Segment>
    )
} 

export default KeyResult;