import React, { useState } from 'react';
import { Header, Dropdown, Label, Button, Divider, Icon, Form, Segment } from 'semantic-ui-react';
import './index.css';

import KeyResult from './KeyResult';
import { CREATE_OKRs, DELETE_APPRAISAL_OBJECTIVE, ADD_KEY_RESULT } from '../../../constants';
import {cloneDeep} from 'lodash';
import { sortFunctionOne } from '../../../helper';
import { toast } from 'react-toastify';
import ReactModal from 'react-modal';



const Objective = ({
    objItem, sbuObjectives, keyResults, dispatch, userIDs, userObjectives, userKeyResults,
    isAddKrLoading, isOkrSaveLoading
}) => {

    const [ellipsisMenuOption, setEllipsisMenuOption] = useState('');
    const [objDescription, setObjDescription] = useState(objItem.description);
    const [sbuObjId, setSbuObjId] = useState(objItem.sbuObjectiveId);
    const [newKrDescription, setNewKrDescription] = useState('');
    const [addKrModalOpen, setAddKrModalOpen] = useState(false);

    const handleEllipsisMenuChange = (e, titleProps) => {
        setEllipsisMenuOption(titleProps.value);

        if(titleProps.value == 'delete') {
            let orderBase = 1
            const OKRs = userIDs.sort((a, b) => sortFunctionOne(userObjectives[a], userObjectives[b])).map(item => userObjectives[item])
                            .filter(item => item.order !== objItem.order)
                            .reduce((acc, curr) => {
                                if(curr.order != 'Personal') {
                                    curr.order = orderBase.toString();
                                    orderBase++;
                                }
                                acc.push(curr)
                                return acc
                            }, [])

            const OBJs = cloneDeep(OKRs)
                            .reduce((acc, curr) => {
                                // curr.peopleOpsStatus = "pending";
                                // curr.lineManagerStatus = "pending";
                                delete curr.keyResults;
                                delete curr.sbuObjective;
                                // delete curr.sbuObjectiveId;
                                acc.push(curr)
                                return acc
                            }, [])

            const KRs = cloneDeep(OKRs)
                            .flatMap(item => item.keyResults)
                            .map(item => userKeyResults[item])
                            // .reduce((acc, curr) => {
                            //     // curr.lineManagerStatus = "pending";
                            //     acc.push(curr)
                            //     return acc
                            // }, [])


            const forUpdateKeyResults = {KRs, OBJs, id: objItem.staffAppraisalId, okrDeleteFlow:true }

            console.log('damn', forUpdateKeyResults)

            dispatch({ type: DELETE_APPRAISAL_OBJECTIVE, id: objItem.id, forUpdateKeyResults })
        }

        // if ADD KEY RESULT is selected
        if (titleProps.value == 'add') {
            if (validations.HAS_KEY_RESULT_REACHED_MAX()) { // if key results is greater than or equal to 5
                return toast.warn("You've reached the max amount (5) of key results!")
            }
            setAddKrModalOpen(true);
        }
    }

    const handleDescriptionChange = (e) => {
        setObjDescription(e.target.value);
        objItem.description = e.target.value;
    }

    const handleSbuObjectiveChange = (e) => {
        setSbuObjId(e.target.value);
        objItem.sbuObjectiveId = e.target.value
    }

    const handleSaveClick = () => {
        // objItem.lineManagerStatus = 'pending'
        // objItem.keyResults = keyResults.filter(krItem => krItem.appraisalObjectiveId == objItem.id)

        const tempKeyResults = keyResults
                                    .filter(krItem => krItem.appraisalObjectiveId == objItem.id)
                                    .map(krItem => ({
                                        description: krItem.description,
                                        order: krItem.order,
                                        status: krItem.status,
                                        lineManagerStatus: krItem.lineManagerStatus,
                                        lineManagerComment: krItem.lineManagerComment,
                                        id: krItem.id,
                                        appraisalObjectiveId: krItem.appraisalObjectiveId,
                                        superLineManagerComment: krItem.superLineManagerComment,
                                    }))

        const data = {
            id: objItem.id,
            description: objItem.description,
            categoryId: objItem.categoryId,
            sbuObjectiveId: objItem.sbuObjectiveId,
            lineManagerStatus: "pending",
            lineManagerComment: objItem.lineManagerComment,
            // peopleOpsStatus: "pending",
            order: objItem.order,
            staffAppraisalId: objItem.staffAppraisalId,
            keyResults: tempKeyResults,
        }

        dispatch({ type: CREATE_OKRs, data })
    }

    const handleNewKrDescriptionChange = (e) => {
        setNewKrDescription(e.target.value)
    }

    const handleAddKrSaveClick = () => {
        const highestOrder = userIDs
            .sort((a, b) => sortFunctionOne(userObjectives[a], userObjectives[b]))
            .map(item => userObjectives[item])
            .flatMap(item => item.keyResults)
            .map(item => userKeyResults[item])
            .filter(item => item.appraisalObjectiveId == objItem.id)
            .reduce((acc, curr) => {
                if (parseInt(curr.order) > acc) {
                    acc = parseInt(curr.order);
                }
                return acc
            }, 0)

        const krData = {
            description: newKrDescription,
            order: (highestOrder + 1).toString(),
            appraisalObjectiveId: objItem.id,
            lineManagerStatus: 'pending',
            superLineManagerStatus: 'pending'
        }

        console.log('high', krData)
        dispatch({ type: ADD_KEY_RESULT, data: krData })
    }

    const handleModalToggle = () => {
        setAddKrModalOpen(!addKrModalOpen);
    }

    const validations = {
        HAS_KEY_RESULT_REACHED_MAX: () => (
            userIDs
                .sort((a, b) => sortFunctionOne(userObjectives[a], userObjectives[b]))
                .map(item => userObjectives[item])
                .flatMap(item => item.keyResults)
                .map(item => userKeyResults[item])
                .filter(item => item.appraisalObjectiveId == objItem.id)
                .length >= 5
        ),
    }

    return (
        <div>
            {/* Add Key Result Modal starts here */}
            <ReactModal
                id='addKrModal'
                isOpen={addKrModalOpen}
                onRequestClose={handleModalToggle}
                style={{
                    overlay: {backgroundColor: 'rgba(0, 0, 0, 0.75)'},
                    content: {
                        top: '30vh',
                        left: '20.5rem',
                        right: '20.5rem',
                        bottom: '37vh',
                    },
                }}
            >
                <Header
                    content='Add a New Key Result'
                />
                <div>
                    <Form>
                        <label>
                            Enter your key result
                            <textarea rows={2} value={newKrDescription} placeholder='Enter a key result' onChange={handleNewKrDescriptionChange}/>
                        </label>
                    </Form>
                    <br/>
                    <Button
                        loading={isAddKrLoading}
                        disabled={!newKrDescription.trim() || isAddKrLoading}
                        color='blue'
                        content='Save'
                        onClick={handleAddKrSaveClick}
                    />
                    <Button
                        disabled={isAddKrLoading}
                        color='red'
                        content='Cancel'
                        onClick={handleModalToggle}
                    />
                </div>
            </ReactModal>

            {/* Main Page starts here */}
            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                <div style={{ display: 'inline-block' }}>
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                        <Header 
                            as='h2' 
                            content={objItem.order=='Personal' ? 'Personal Development Objective' : `Business Objective ${objItem.order}`}
                            style={{ display: 'inline-block', margin: '0 10px 0 0' }}    
                        />
                        <Label
                            content={objItem.lineManagerStatus == 'disapproved' ? 'DENIED' : 'OK'}
                            color={objItem.lineManagerStatus == 'disapproved' ? 'red' : 'blue'}
                            size='mini'
                        />
                    </div>
                </div>
                <Dropdown 
                    id='ellipsis-dropdown'
                    icon={null}
                    direction='left'
                    trigger = {(
                        <Button circular icon='ellipsis vertical' />
                    )}
                    inline
                    header='OPTIONS'
                    options={[
                        {text: "Select an action", value:''},
                        {text: "Edit Objective", value: "edit", icon:'edit'},
                        {text: "Delete Objective", value: "delete", icon:'delete'},
                        {text: "Add Key Result", value: "add", icon:'add'},
                    ]}
                    onChange={(e, titleProps) => handleEllipsisMenuChange(e, titleProps)}
                    // loading={deleteLoadingStatus}
                    // disabled={deleteLoadingStatus}
                />
            </div>
            <Divider/>
            <p>
                { ellipsisMenuOption !== 'edit' && objItem.description }
                { ellipsisMenuOption == 'edit' &&
                    <Form>
                        <textarea rows={2} value={objDescription} placeholder='Enter an objective' onChange={handleDescriptionChange}/>
                    </Form>
                }
            </p>
            { objItem.order !== 'Personal' &&
                <>
                    <Header as='h5' content='SBU Objective'/>
                    { ellipsisMenuOption !== 'edit' &&
                        <p>{sbuObjectives && sbuObjectives.length > 0 ? sbuObjectives?.find(sbuObj => sbuObj.id === objItem.sbuObjectiveId)?.description : null}</p>
                    }
                    { ellipsisMenuOption == 'edit' &&
                        <Form>
                            <p>
                                <select
                                    style={{width:'100%'}}
                                    // name={`sbuObjectives${order}`}
                                    value={sbuObjId}
                                    onChange={handleSbuObjectiveChange}
                                >
                                    <option value="" disabled>
                                        Select the SBU objective that the objective above helps achieve
                                    </option>
                                    {sbuObjectives.map((item, index) => (
                                        <option style={{overflowWrap:'break-word'}} value={item.id} key={item.id}>{item.description}</option>
                                    ))}
                                </select>
                            </p>
                        </Form>
                    }
                </>
            }
            { objItem.lineManagerStatus == 'disapproved' &&
                <>
                    <div>
                        <Icon name='comment alternate outline' />
                        <Header as='h5' content='Line Manager Comment' style={{ display: 'inline-block', margin: '0 0 0 4px' }}  />
                    </div>
                    <p>{objItem.lineManagerComment}</p>
                </>
            }
            { keyResults
                .filter(krItem => krItem.appraisalObjectiveId == objItem.id)
                .map(krItem => (
                    <KeyResult krItem={krItem} userIDs={userIDs} 
                        userObjectives={userObjectives}
                        userKeyResults={userKeyResults} 
                        staffAppraisalId={objItem.staffAppraisalId}
                        dispatch={dispatch}
                    />
                ))
            }
            <Segment basic textAlign="right">
                {/* {SAVING_OBJECTIVE_STATUS === true && (
                    <Label pointing="right" color="green">
                        Save Successful
                    </Label>
                )}
                {SAVING_OBJECTIVE_STATUS === false && (
                    <Label pointing="right" color="red">
                        Save Failed. Please try again or refresh page.
                    </Label>
                )} */}
                <Button
                    basic
                    content="Save"
                    loading={isOkrSaveLoading}
                    disabled={isOkrSaveLoading}
                    onClick={handleSaveClick}
                />
            </Segment>
        </div>
    )
} 

export default Objective;