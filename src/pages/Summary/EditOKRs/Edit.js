import React, {useState, Fragment} from 'react';
import { Button, Tab, Header, Menu, Radio, Form, Segment, Loader } from 'semantic-ui-react';
import Objective from './Objective';
import ReactModal from 'react-modal';
import { toast, ToastType } from 'react-toastify';
import { sortFunctionOne as SFOne } from '../../../helper';
import { CREATE_OKRs } from '../../../constants';

const Edit = (props) => {
    const { userIDs, userObjectives, userKeyResults, staffAppraisal, sbuObjectives,
        staffAppraisal: { staffSubmitted, firstApproved, secondApproved }, 
        isSubmittingOKRs, updateKeyResultsLoadingStatus, UPDATE_KEY_RESULTS_STATUS,
        dispatch, isAddKrLoading, isDeleteKeyResultLoading, isDeleteObjectiveLoading,
        isOkrSaveLoading, categories, requestingOKRs
    } = props

    const [newObjDescription, setNewObjDescription] = useState('');
    const [addObjModalOpen, setAddObjModalOpen] = useState(false);
    const [objMode, setObjMode] = useState('');
    const [sbuObjId, setSbuObjId] = useState('');

    //userIDs sort function
    const sortFunctionOne = (a, b) => {
        if (userObjectives[a].order < userObjectives[b].order) {
            return -1
        } else if (userObjectives[a].order > userObjectives[b].order) {
            return 1
        } else {
            return 0
        }
    }

    const objectives = userIDs.sort(sortFunctionOne)
                                .map(item => userObjectives[item])

    const keyResults = userIDs.sort(sortFunctionOne)
                                .flatMap(item => userObjectives[item].keyResults)
                                .map(item => userKeyResults[item])
    
    const getPanes = () => {
        return objectives.map(item => {
            return (
                {
                    menuItem: (
                        <Menu.Item>
                            {item.order=='Personal' ? 'Personal Development Objective' : `Business Objective ${item.order}`}
                            { (objectives.filter(obj => item.order==obj.order).every(item => item.lineManagerStatus == 'disapproved') ||
                                keyResults.filter(kr => item.id == kr.appraisalObjectiveId).some(item => item.lineManagerStatus == 'disapproved')) && 
                                <span style={{ height: '8px', width: '8px', borderRadius: '8px', backgroundColor: 'red', marginLeft: '8px' }}></span>
                            }
                        </Menu.Item>
                    ),
                    render: () => (
                        <Tab.Pane key={item.order}>
                            <Objective 
                                objItem={item} sbuObjectives={sbuObjectives} keyResults={keyResults}
                                dispatch={dispatch} userIDs={userIDs} userObjectives={userObjectives}
                                userKeyResults={userKeyResults} isAddKrLoading={isAddKrLoading}
                                isOkrSaveLoading={isOkrSaveLoading}
                            />
                        </Tab.Pane>
                    )
                }
            )
        })
    }

    const handleNewObjDescriptionChange = (e) => {
        setNewObjDescription(e.target.value)
    }

    const handleModalToggle = () => {
        if (validations.HAS_REACHED_MAX_AMOUNT_OF_OBJECTIVES()) {
            return toast.warn('You already have the max amount of objectives');
        }
        setAddObjModalOpen(!addObjModalOpen);
    }

    const handleRadioToggle = (e, {value}) => {
        if(value==='Personal' && validations.PERSONAL_OBJECTIVE_ALREADY_EXISTS()) {
            return toast.warn('You cannot create a second personal development objective');
        } else if (value === 'Business' && validations.BUSINESS_OBJECTIVE_HAS_REACHED_MAX()) {
            return toast.warn('You cannot have more than 5 business objectives')
        }
        setObjMode(value);
    }

    const handleAddObjSaveClick = () => {
        const highestOrder = userIDs
            .sort((a, b) => SFOne(userObjectives[a], userObjectives[b]))
            .map(item => userObjectives[item])
            .reduce((acc, curr) => {
                if (parseInt(curr.order) > acc) {
                    acc = parseInt(curr.order);
                }
                return acc
            }, 0)

        let data = {
            description: newObjDescription,
            lineManagerStatus: "pending",
            lineManagerComment: "",
            order: objMode === 'Business' ? (highestOrder + 1).toString() : 'Personal',
            staffAppraisalId: staffAppraisal.id,
            keyResults: [],
            categoryId: Object.keys(categories)
                            .map(item => categories[item])
                            .reduce((acc, curr) => {
                                if(curr.name == objMode) {acc = curr.id} 
                                return acc
                            }, ''),
        }

        if (objMode == 'Business') {
            data.sbuObjectiveId = sbuObjId;
        }

        console.log('focus on me', data, categories)

        dispatch({ type: CREATE_OKRs, data })
    }

    const validations = {
        HAS_REACHED_MAX_AMOUNT_OF_OBJECTIVES: () => userIDs.length === 6,
        PERSONAL_OBJECTIVE_ALREADY_EXISTS: () => (
            userIDs
                .sort((a, b) => SFOne(userObjectives[a], userObjectives[b]))
                .map(item => userObjectives[item])
                .some(item => item.order === 'Personal')
        ),
        BUSINESS_OBJECTIVE_HAS_REACHED_MAX: () => (
            userIDs
                .sort((a, b) => SFOne(userObjectives[a], userObjectives[b]))
                .map(item => userObjectives[item])
                .filter(item => parseInt(item.order)/parseInt(item.order) === 1 )
                .length === 5
        )
    }

    if (requestingOKRs) {
        return (            
            <div>
                <Loader active/>
            </div>
        )
    }

    return (
        <Segment style={{ margin: '0 30px' }} loading={isDeleteKeyResultLoading || isDeleteObjectiveLoading}>
            {/* Add Key Result Modal starts here */}
            <ReactModal
                id='addObjModal'
                isOpen={addObjModalOpen}
                onRequestClose={handleModalToggle}
                style={{
                    overlay: {backgroundColor: 'rgba(0, 0, 0, 0.75)'},
                    content: {
                        top: '25vh',
                        left: '250px',
                        right: '250px',
                        bottom: '30vh',
                    },
                }}
            >
                <Header
                    content='Add a New Objective'
                />
                <div>
                    <Form>
                        <div style={styles.formSection}>
                            <label>Select the objective type</label>
                            <div>
                                <Radio
                                    label='Business Objective'
                                    name='radioGroup'
                                    value='Business'
                                    checked={objMode === 'Business'}
                                    onChange={handleRadioToggle}
                                />
                                <Radio
                                    style={styles.rightRadio}
                                    label='Personal Development Objective'
                                    name='radioGroup'
                                    value='Personal'
                                    checked={objMode === 'Personal'}
                                    onChange={handleRadioToggle}
                                />
                            </div>
                        </div>
                        <div style={styles.formSection}>
                            <label>
                                Enter your objective
                                <textarea rows={2} value={newObjDescription} placeholder='Enter your objective' onChange={handleNewObjDescriptionChange}/>
                            </label>
                        </div>
                        { objMode === 'Business' &&
                            <div style={styles.formSection}>
                                <label>Select an sbu objective</label>
                                <select
                                    value={sbuObjId}
                                    onChange={(e) => setSbuObjId(e.target.value)}
                                >
                                    <option value="" disabled>
                                        Select the SBU objective that the objective above helps achieve
                                    </option>
                                    {sbuObjectives.map((item, index) => (
                                        <option value={item.id} key={item.id}>{item.description}</option>
                                    ))}
                                </select>
                            </div>
                        }
                    </Form>
                    <br/>
                    <Button
                        loading={isOkrSaveLoading}
                        disabled={!newObjDescription.trim() || isOkrSaveLoading || ( objMode === 'Business' && !sbuObjId )}
                        color='blue'
                        content='Save'
                        onClick={handleAddObjSaveClick}
                    />
                    <Button
                        disabled={isOkrSaveLoading}
                        color='red'
                        content='Cancel'
                        onClick={handleModalToggle}
                    />
                </div>
            </ReactModal>

            {/* Main Page starts here */}
            <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                <Button
                    color='blue'
                    icon='add'
                    content='Add New Objective'
                    onClick={handleModalToggle}
                />
            </div>
            <Tab 
                panes={getPanes()}
            />
        </Segment>
    )
}

const styles = {
    formSection: {
        marginBottom: 20,
    },
    rightRadio: {
        marginLeft: 20
    }
}

export default Edit