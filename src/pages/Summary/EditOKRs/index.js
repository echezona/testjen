import React, {useState, Fragment} from 'react';
import { Button, Loader } from 'semantic-ui-react';
import './index.css';
import Edit from './Edit';
import Summary from './Summary';
import { userObjectivesSelector, userIDsSelector, userKeyResultsSelector, staffAppraisalSelector, sbuObjectivesSelector, userCategoriesSelector, categorySelector } from '../../../reducer'
import { connect } from 'react-redux';

const EditOKRs = (props) => {
    const [pageView, setPageView] = useState('edit') // edit || summary

    const handlePageViewClick = () => {
        const newPageView = pageView == 'edit' ? 'summary' : 'edit'
        setPageView(newPageView)
    }

    return (
        <Fragment>
            <div className="page-view-button-group">
                <Button.Group>
                    <Button content='Edit OKRs' color={pageView == 'edit' ? 'blue' : ''} onClick={handlePageViewClick} />
                    <Button content='View Summary & Submit' color={pageView == 'summary' ? 'blue' : ''} onClick={handlePageViewClick} />
                </Button.Group>
            </div>
        
            <div>
                { pageView == 'edit' &&
                    <Edit {...props}/>
                }
                { pageView == 'summary' &&
                    <Summary {...props} handlePageChange={handlePageViewClick}/>
                }
            </div>
            
        </Fragment>
    )
}

function mapStateToProps(state) {
    return {
        userIDs: userIDsSelector(state),
        userObjectives: userObjectivesSelector(state),
        userKeyResults: userKeyResultsSelector(state),
        staffAppraisal: staffAppraisalSelector(state),
        sbuObjectives: sbuObjectivesSelector(state),
        categories: categorySelector(state),
        isSubmittingOKRs: state.isSubmittingOKRs,
        updateKeyResultsLoadingStatus: state.saveLoadingReducer.UPDATE_KEY_RESULTS_LOADING_STATUS,
        UPDATE_KEY_RESULTS_STATUS: state.UPDATE_KEY_RESULTS_STATUS,
        isAddKrLoading: state.saveLoadingReducer.ADD_KEY_RESULT_LOADING_STATUS,
        isDeleteKeyResultLoading: state.saveLoadingReducer.DELETE_KEY_RESULT_LOADING_STATUS,
        isDeleteObjectiveLoading: state.saveLoadingReducer.DELETE_APPRAISAL_OBJECTIVE_LOADING_STATUS,
        isOkrSaveLoading: state.REQUESTING_SAVE_KEY_RESULTS,
    }
}

export default connect(mapStateToProps)(EditOKRs)