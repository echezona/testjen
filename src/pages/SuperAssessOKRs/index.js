//Components
import React, {Fragment} from 'react';
import { Grid, Loader, Message, Button, Segment } from 'semantic-ui-react';
import Header from './Header'
import _ from 'lodash'
import { connect } from "react-redux"
import { withRouter, Switch, Route } from 'react-router-dom'
import { userObjectivesSelector, userIDsSelector, userKeyResultsSelector, userCategoriesSelector, lmIDsSelector, lmCategoriesSelector, lmStaffAppraisalsSelector, lmObjectivesSelector, lmKeyResultsSelector, categorySelector, appraisalCycleIdSelector, lmSbuObjectivesSelector } from '../../reducer'
import queryString from 'query-string'
// Css Files
import './index.css';
//Constatnts
import { FETCH_LINE_MANAGER_OKRS, FETCH_CATEGORY, FETCH_LINE_MANAGER_STAFF_APPRAISALS, SUBMIT_LINE_MANAGER_OKRS, FETCH_LMPO_OKRS, STORE_PENDING_KEY_RESULTS, FETCH_SBU_OBJECTIVES, FETCH_BEHAVIORAL_INDICATORS, FETCH_RECOMMENDATION_TYPES, FETCH_RECOMMENDATION, FETCH_SUPER_LINE_MANAGER_STAFF_APPRAISALS, FETCH_SUPER_RECOMMENDATION } from '../../constants';
import SuperAssessPerformance from '../SuperAssessPerformance';
// import AssessBehaviour from '../AssessBehaviour';
// import AssessRecommendations from '../AssessRecommendations';
// import AssessComplete from '../AssessComplete';
import SuperAssessBehaviour from '../SuperAssessBehaviour';
import SuperAssessRecommendations from '../SuperAssessRecommendations';
import SuperAssessComplete from '../SuperAssessComplete';
import ErrorBoundary from '../ErrorBoundary';



class SuperAssessOKRs extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            showSubmit: false,
            updatePage: false,
            routeState: this.props.location.state
        }
    }

    componentWillMount() {
        const { dispatch, match, location, history } = this.props;
        const queryValues = queryString.parse(location.search)
        // console.log("what is this", queryValues)

        dispatch({ type: FETCH_LMPO_OKRS, staffId: `?staffId=${queryValues.staffId}` })
        dispatch({ type: FETCH_CATEGORY })
        dispatch({ type: FETCH_SBU_OBJECTIVES})
        dispatch({ type: FETCH_SUPER_LINE_MANAGER_STAFF_APPRAISALS })
        dispatch({ type: FETCH_RECOMMENDATION_TYPES })
        dispatch({ type: FETCH_BEHAVIORAL_INDICATORS, LM_DR_STAFF_ID: queryValues.staffId})  //LM_DR_STAFF_ID = Line Manager's Direct Report Staff ID
        dispatch({ type: FETCH_SUPER_RECOMMENDATION, staffId: queryValues.staffId})  


        document.title = "PMS - Assess OKRs (N-2s)" // set the document title in the browser

        if(location.pathname.includes('performance') === false && location.pathname.includes('behaviour') === false && location.pathname.includes('recommendation') === false) {
            history.push({
                pathname: `${match.path}/superassessperformance`,
                search: `?staffId=${queryValues.staffId}`,
                state: location.state,
            })
        }
    }


    showSubmit = (x) => {
        this.setState({
            showSubmit: x
        })
    }

    iUpdated = () => {
        this.forceUpdate();
    }

    onPerformanceClick = () => {
        const {history, match } = this.props
        const queryValues = queryString.parse(this.props.location.search)
        // history.push(`${match.path}/assessperformance?staffId=${queryValues.staffId}`)
        history.push({
            pathname: `${match.path}/superassessperformance`,
            search: `?staffId=${queryValues.staffId}`,
            state: this.props.location.state,
        })
    }

    onBehaviourClick = () => {
        const {history, match } = this.props
        const queryValues = queryString.parse(this.props.location.search)
        // history.push(`${match.path}/assessbehaviour?staffId=${queryValues.staffId}`)
        history.push({
            pathname: `${match.path}/superassessbehaviour`,
            search: `?staffId=${queryValues.staffId}`,
            state: this.props.location.state,
        })
    }

    onRecommendationsClick = () => {
        const {history, match } = this.props
        const queryValues = queryString.parse(this.props.location.search)
        // history.push(`${match.path}/assessrecommendations?staffId=${queryValues.staffId}`)
        history.push({
            pathname: `${match.path}/superassessrecommendations`,
            search: `?staffId=${queryValues.staffId}`,
            state: this.props.location.state,
        })
    }

    onCompleteClick = () => {
        const {history, match, dispatch } = this.props
        const queryValues = queryString.parse(this.props.location.search)

        dispatch({ type: FETCH_LMPO_OKRS, staffId: `?staffId=${queryValues.staffId}` })

        history.push({
            pathname: `${match.path}/superassesscomplete`,
            search: `?staffId=${queryValues.staffId}`,
            state: this.props.location.state,
        })
    }

    handleSubmit = () => {
        const { lmStaffAppraisals, match, location, history, dispatch, lmIDs, lmObjectives, lmKeyResults, lmPendingKeyResults } = this.props
        // console.log("lmstaff")
        const queryValues = queryString.parse(location.search)
        //turn this execution into a function later
        let firstApproved
        if (lmIDs.map(item => lmObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => lmKeyResults[item]).filter(item => item.lineManagerStatus === "disapproved").length > 0 || lmIDs.map(item => lmObjectives[item]).filter(item => item.lineManagerStatus === "disapproved").length > 0) {
            firstApproved = "disapproved"
        } else {
            firstApproved = "approved"
        }

        //move this into the dispatched function for better performance
        lmStaffAppraisals.find(item => item.staffId === queryValues.staffId).firstApproved = firstApproved
        
        this.iUpdated();

        dispatch({ type: SUBMIT_LINE_MANAGER_OKRS, data: { staffAppraisalId: lmIDs.map(item => lmObjectives[item].staffAppraisalId).find(item => item), staffId: queryValues.staffId, firstApproved }, history})
    }

    render() {
        console.log("route-state-prime", this.state.routeState)
        const { IS_REQUESTING_OKRs, sbuObjectives, isFetchingStaffAppraisals, isFetchingLMPOOKRs, match, lmIDs, lmObjectives, lmStaffAppraisals, lmCategories, categories, lmKeyResults } = this.props
        const { routeState } = this.state
        //create a variable to hold the parsed values kept in the query parameter
        const queryValues = queryString.parse(this.props.location.search)

        let pageContent;

        console.log("staffID @ assessokrs", this.props.staffId)

        // if(isFetchingLMPOOKRs && isFetchingStaffAppraisals){
        //     return <Loader active/>
        // }

        if(isFetchingLMPOOKRs || isFetchingStaffAppraisals){
            pageContent = (
                <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                    Please Wait...
                    <Loader active inline='centered'/>
                </div>
            )
        } else {
            pageContent = (
                <>
                    <br/>
                    <br/>
                    <Switch>
                        <Route path={`${match.path}/superassessperformance`} render={(props) => <ErrorBoundary><SuperAssessPerformance {...props} {...this.props}/></ErrorBoundary>}/>
                        <Route path={`${match.path}/superassessbehaviour`} render={(props) => <ErrorBoundary><SuperAssessBehaviour {...props} {...this.props}/></ErrorBoundary>}/>
                        <Route path={`${match.path}/superassessrecommendations`} render={(props) => <ErrorBoundary><SuperAssessRecommendations routeState={routeState} staffId={queryValues.staffId}  {...props} {...this.props}/></ErrorBoundary>}/>
                        <Route path={`${match.path}/superassesscomplete`} render={(props) => <ErrorBoundary><SuperAssessComplete routeState={routeState} staffId={queryValues.staffId} staffAppraisalId={lmIDs.map(item => lmObjectives[item].staffAppraisalId).find(item => item)} {...props} {...this.props}/></ErrorBoundary>}/>
                    </Switch>       
                </>
            )
        }

        return (
            <Fragment padded>
                <Header id={queryValues.staffId}
                    lmStaffAppraisals={lmStaffAppraisals}
                />
                <Segment basic textAlign="center">
                    <Button.Group>
                        <Button content="Performance" onClick={this.onPerformanceClick} color={this.props.location.pathname.includes('performance') ? 'blue' : null}/>
                        <Button content="Behaviour" onClick={this.onBehaviourClick} color={this.props.location.pathname.includes('behaviour') ? 'blue' : null}/>
                        <Button content="Recommendation" onClick={this.onRecommendationsClick} color={this.props.location.pathname.includes('recommendation') ? 'blue' : null}/>
                        <Button content="Complete" icon='check circle outline' labelPosition='left' onClick={this.onCompleteClick} color='green' basic={this.props.location.pathname.includes('complete') ? false : true} />
                    </Button.Group>
                </Segment>      
                {pageContent}
            </Fragment>
        )
    }
}

function mapStateToProps(state) {
    return {
        IS_REQUESTING_OKRs: state.requestingOKRs,
        // OKRs: state.setOKRs,
        lmIDs: lmIDsSelector(state),
        lmObjectives: lmObjectivesSelector(state),
        lmKeyResults: lmKeyResultsSelector(state),
        lmCategories: lmCategoriesSelector(state),
        lmSbuObjectives: lmSbuObjectivesSelector(state),
        lmStaffAppraisals: state.requestingSuperLineManagerStaffAppraisals,
        categories: categorySelector(state),
        isFetchingStaffAppraisals: state.isFetchingStaffAppraisals,
        isFetchingLMPOOKRs: state.isFetchingLMPOOKRs,
        sbuObjectives: state.sbuObjectives,
        buttonLoadingStatus: state.saveLoadingReducer.SAVE_KEY_RESULTS_LOADING_STATUS,
        behaviourSaveLoadingStatus: state.saveLoadingReducer.SAVE_BEHAVIORAL_ASSESSMENT_LOADING_STATUS,
        recommendationSaveLoadingStatus: state.saveLoadingReducer.UPDATE_RECOMMENDATION_LOADING_STATUS,
        completeSubmitLoadingStatus: state.saveLoadingReducer.SUBMIT_LINE_MANAGER_ASSESSMENT_LOADING_STATUS,
        behavioralIndicators: state.requestingBehavioralIndicators,
        recommendationTypes: state.requestingRecommendationTypes,
        appraisalCycleId: appraisalCycleIdSelector(state),
        recommendationObject: state.requestingSuperRecommendation,
        SAVING_OBJECTIVE_STATUS: state.SAVING_OBJECTIVE_STATUS,
        SAVE_BEHAVIOUR_STATUS: state.SAVE_BEHAVIOUR_STATUS,
        UPDATE_RECOMMENDATION_STATUS: state.UPDATE_RECOMMENDATION_STATUS,
        SUBMIT_LINE_MANAGER_ASSESSMENT_STATUS: state.SUBMIT_LINE_MANAGER_ASSESSMENT_STATUS, 
    }
}

export default connect(mapStateToProps)(SuperAssessOKRs)

// {lmIDs.map(item => lmObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => lmKeyResults[item]).every(item => item.status === "pending")}