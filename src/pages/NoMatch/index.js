import React, {Component, Fragment} from 'react'
import {Card, Header} from 'semantic-ui-react'
import {Link} from 'react-router-dom'
import './index.css'

const NoMatch = (props) => {
    return(
        <div class="page-content">
            {/* <Header as="h1" subheader="Sorry, we couldn't find that page" content="404"/>
            <Button color="blue"></Button> */}
            <span class="four-oh-four">404</span>
            <span class="subheader"> Sorry, we couldn't find that page</span>
            <span class="link"><Link to="/businessobjectives"> GO BACK HOME </Link></span>
        </div>
    )
}

export default NoMatch