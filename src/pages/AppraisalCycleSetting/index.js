import React, { Fragment } from 'react';
import { Form, Segment, Grid, Header, Tab, Button, Divider, List, Radio, Icon, Label, Loader } from 'semantic-ui-react';
import { connect } from 'react-redux';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import { FETCH_BEHAVIORAL_INDICATORS, SAVE_APPRAISAL_CYCLE, FETCH_APPRAISAL_CYCLE, LOCK_APPRAISAL_PERIOD, UNLOCK_APPRAISAL_PERIOD } from '../../constants';
// import './main.css';

class AppraisalCycleSetting extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            startDate: null,
            endDate: null,
            settingStartDate: null,
            settingEndDate: null,
            assessmentStartDate: null,
            assessmentEndDate: null,
            startDateRaw: null,
            endDateRaw: null,
            settingStartDateRaw: null,
            settingEndDateRaw: null,
            assessmentStartDateRaw: null,
            assessmentEndDateRaw: null,
            message: null,
            period: null,
            okrSettingLock: false,
            okrAssessmentLock: false,
            monthlyOneOnOnActiveLock: false,
        };
    }

    componentDidMount() {
        const { dispatch } = this.props
        dispatch({ type: FETCH_APPRAISAL_CYCLE })
    }

    // componentDidUpdate() {
    //     const { startDate, endDate, settingStartDate, settingEndDate, assessmentStartDate, assessmentEndDate } = this.state
    //     if (startDate) {
    //         console.log("See me ", moment.duration(startDate.diff(endDate)).as('milliseconds'))
    //     }
    //     console.log("all the states", startDate, endDate, settingStartDate, settingEndDate, assessmentStartDate, assessmentEndDate)
    //     console.log("locks", this.state.period, this.state.okrSettingLock, this.state.okrAssessmentLock)
    // }

    handleChangeStart = (date) => {
        const valueOfInput = date.format()
        this.setState({
            startDate: date,
            startDateRaw: valueOfInput
        });
    }

    handleChangeEnd = (date) => {
        const { startDate, endDate } = this.state
        const valueOfInput = date.format()

        if (!startDate) {
            this.setState({
                message: "1",
            })
        }
        if (startDate && moment.duration(startDate.diff(date)).as('milliseconds') < 0) {
            this.setState({
                endDate: date,
                endDateRaw: valueOfInput,
            });
        }
    }

    handleSettingChangeStart = (date) => {
        const valueOfInput = date.format()
        this.setState({
            settingStartDate: date,
            settingStartDateRaw: valueOfInput,
        });
    }

    handleSettingChangeEnd = (date) => {
        const { settingStartDate, settingEndDate } = this.state
        const valueOfInput = date.format()

        if (!settingStartDate) {
            this.setState({
                message: "1",
            })
        }
        if (settingStartDate && moment.duration(settingStartDate.diff(date)).as('milliseconds') < 0) {
            this.setState({
                settingEndDate: date,
                settingEndDateRaw: valueOfInput,
            });
        }
    }

    handleAssessmentChangeStart = (date) => {
        const valueOfInput = date.format()
        this.setState({
            assessmentStartDate: date,
            assessmentStartDateRaw: valueOfInput,
        });
    }

    handleAssessmentChangeEnd = (date) => {
        const { assessmentStartDate, assessmentEndDate } = this.state
        const valueOfInput = date.format()

        if (!assessmentStartDate) {
            this.setState({
                message: "1",
            })
        }
        if (assessmentStartDate && moment.duration(assessmentStartDate.diff(date)).as('milliseconds') < 0) {
            this.setState({
                assessmentEndDate: date,
                assessmentEndDateRaw: valueOfInput,
            });
        }
    }

    handleRadioChange = (e, {value}) => {
        this.setState({
            period: value,
            // [value]: true,
            // [opposite]: false
        })
    }

    handleLock = () => {
        const {dispatch, appraisalCycle} = this.props
        const { okrSettingLock, okrAssessmentLock, period } = this.state
        const { id, appraisalStartDate, appraisalEndDate, okrSettingStartDate, okrSettingEndDate, okrAssessmentStartDate, okrAssessmentEndDate } = appraisalCycle
        
        const data = {
            id, appraisalStartDate, appraisalEndDate, okrSettingStartDate, okrSettingEndDate,
            okrAssessmentStartDate, okrAssessmentEndDate, okrSettingLock: appraisalCycle.okrSettingLock,
            okrAssessmentLock: appraisalCycle.okrAssessmentLock, monthlyOneOnOnActiveLock: appraisalCycle.monthlyOneOnOnActiveLock
        }  

        if(data[period] === true){
            data[period] = false
        } else if(data[period] === false) {
            data[period] = true
        }

        dispatch({ type: LOCK_APPRAISAL_PERIOD, data })
    }



    handleUnlock = () => {
        const {dispatch, appraisalCycle} = this.props
        const { okrSettingLock, okrAssessmentLock, period } = this.state
        const { id, appraisalStartDate, appraisalEndDate, okrSettingStartDate, okrSettingEndDate, okrAssessmentStartDate, okrAssessmentEndDate } = appraisalCycle
        
        const data = {
            id, appraisalStartDate, appraisalEndDate, okrSettingStartDate, okrSettingEndDate,
            okrAssessmentStartDate, okrAssessmentEndDate, okrSettingLock: appraisalCycle.okrSettingLock,
            okrAssessmentLock: appraisalCycle.okrAssessmentLock, monthlyOneOnOnActiveLock: appraisalCycle.monthlyOneOnOnActiveLock
        }  

        if(data[period] === true){
            data[period] = false
        } else if(data[period] === false) {
            data[period] = true
        }

        dispatch({ type: UNLOCK_APPRAISAL_PERIOD, data })
    }


    handleSubmit = () => {
        const { dispatch } = this.props
        const { startDateRaw, endDateRaw, settingStartDateRaw, settingEndDateRaw, assessmentStartDateRaw, assessmentEndDateRaw } = this.state
        dispatch({
            type: SAVE_APPRAISAL_CYCLE,
            dates: {
                appraisalStartDate: startDateRaw,
                appraisalEndDate: endDateRaw,
                okrSettingStartDate: settingStartDateRaw,
                okrSettingEndDate: settingEndDateRaw,
                okrAssessmentStartDate: assessmentStartDateRaw,
                okrAssessmentEndDate: assessmentEndDateRaw,
                okrAssessmentLock: true,
                okrSettingLock: false,
            }
        })
    }

    render() {
        console.log("appraisalCycle", this.props.appraisalCycle)
        const { startDate, endDate, settingStartDate, settingEndDate, assessmentStartDate, assessmentEndDate, okrSettingLock, okrAssessmentLock, period } = this.state
        const { appraisalCycle, lockAppraisalPeriodLoadingStatus, unlockAppraisalPeriodLoadingStatus, LOCK_APPRAISAL_CYCLE_STATUS, UNLOCK_APPRAISAL_CYCLE_STATUS, fetchAppraisalCycleLoadingStatus, saveAppraisalCycleLoadingStatus, SAVE_APPRAISAL_CYCLE_STATUS } = this.props
        const panes = [
            {
                menuItem: 'Appraisal Cycle',
                render: () => {
                    return (
                        <Form className='date-form'>
                            <div className='date-form-part'>
                                <Header as='h4' content='From: '
                                    // style={{ display: "inline" }}
                                />
                                <DatePicker
                                    // style={{ width: '100%', display: 'block', borderColor: 'black' }}
                                    className='date-form-field'
                                    selectsStart
                                    minDate={moment()}
                                    startDate={this.state.startDate}
                                    endDate={this.state.endDate}
                                    placeholderText="Click to select a start date"
                                    todayButton={"Today"}
                                    dateFormat="DD-MM-YYYY"
                                    selected={this.state.startDate}
                                    onChange={(date) => this.handleChangeStart(date)}
                                />
                            </div>
                            <div className='date-form-part'>
                                <Header as='h4' content='To: '
                                    // style={{ display: "inline" }}
                                />
                                <DatePicker
                                    className='date-form-field'
                                    selectsEnd
                                    minDate={moment()}
                                    startDate={this.state.startDate}
                                    endDate={this.state.endDate}
                                    placeholderText="Click to select an end date"
                                    todayButton={"Today"}
                                    dateFormat="DD/MM/YYYY"
                                    selected={this.state.endDate}
                                    onChange={(date) => this.handleChangeEnd(date)}
                                />
                            </div>
                        </Form>
                    )
                }
            },
            {
                menuItem: 'OKR Setting Period',
                render: () => {
                    if (!startDate || !endDate) {
                        return (
                            <div>Please finish setting the dates for the Appraisal Cycle.</div>
                        )
                    }
                    return (
                        <Form className='date-form'>
                            <div className='date-form-part'>
                                <Header as='h4' content='From: '
                                    // style={{ display: "inline" }}
                                />
                                <DatePicker
                                    // style={{ display: "inline" }}
                                    className='date-form-field'
                                    selectsStart
                                    minDate={startDate}
                                    maxDate={endDate}
                                    startDate={settingStartDate}
                                    endDate={settingEndDate}
                                    placeholderText="Click to select a start date"
                                    // todayButton={"Today"}
                                    dateFormat="DD/MM/YYYY"
                                    selected={settingStartDate}
                                    onChange={this.handleSettingChangeStart}
                                />
                            </div>
                            <div className='date-form-part'>
                                <Header as='h4' content='To: '/>
                                <DatePicker
                                    className='date-form-field'
                                    selectsEnd
                                    minDate={startDate}
                                    maxDate={endDate}
                                    startDate={settingStartDate}
                                    endDate={settingEndDate}
                                    placeholderText="Click to select an end date"
                                    // todayButton={"Today"}
                                    dateFormat="DD/MM/YYYY"
                                    selected={settingEndDate}
                                    onChange={this.handleSettingChangeEnd}
                                />
                            </div>
                        </Form>
                    )
                }
            },
            {
                menuItem: 'OKR Assessment Period',
                render: () => {
                    if (!settingStartDate || !settingEndDate) {
                        return (
                            <div>Please finish setting the dates for the OKR Setting Period.</div>
                        )
                    }
                    return (
                        <Form className='date-form'>
                            <div className='date-form-part'>
                                <Header as='h4' content='From: '/>
                                <DatePicker
                                    className='date-form-field'
                                    selectsStart
                                    minDate={settingEndDate}
                                    maxDate={endDate}
                                    startDate={assessmentStartDate}
                                    endDate={assessmentEndDate}
                                    placeholderText="Click to select a start date"
                                    // todayButton={"Today"}
                                    dateFormat="DD/MM/YYYY"
                                    selected={assessmentStartDate}
                                    onChange={this.handleAssessmentChangeStart}
                                />
                            </div>
                            <div className='date-form-part'>
                                <Header as='h4' content='To: '/>
                                <DatePicker
                                    className='date-form-field'
                                    selectsEnd
                                    minDate={settingEndDate}
                                    maxDate={endDate}
                                    startDate={assessmentStartDate}
                                    endDate={assessmentEndDate}
                                    placeholderText="Click to select an end date"
                                    // todayButton={"Today"}
                                    dateFormat="DD/MM/YYYY"
                                    selected={assessmentEndDate}
                                    onChange={this.handleAssessmentChangeEnd}
                                />
                            </div>
                        </Form>
                    )
                }
            },
        ]

        if(fetchAppraisalCycleLoadingStatus){
            return(
                <Loader active/>
            )
        }

        if (appraisalCycle.id) {
            const { appraisalStartDate, appraisalEndDate, okrSettingStartDate, okrSettingEndDate, okrAssessmentStartDate, okrAssessmentEndDate } = appraisalCycle
            return (
                <Grid columns={16}>
                    <Grid.Column width={3}></Grid.Column>
                    <Grid.Column width={10}>
                        <Segment >
                            <Fragment>
                                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                    <Header as="h4" content="Appraisal Cycle" />
                                    <Button onClick={() => {appraisalCycle.id = null; this.forceUpdate();}} color='blue'>
                                        <Icon name='plus' />
                                        Create new appraisal cycle
                                    </Button>
                                </div>
                                <Divider />
                                <List as='ul'>
                                    <List.Item ><span style={{ fontWeight: "bold" }}>Appraisal Cycle: </span>{`${appraisalStartDate} - ${appraisalEndDate}`}</List.Item>
                                    <List.List>
                                        <List.Item as='li'><span style={{ fontWeight: "bold" }}>OKR Setting: </span>{`${okrSettingStartDate} - ${okrSettingEndDate}`}</List.Item>
                                        <List.Item as='li'><span style={{ fontWeight: "bold" }}>OKR Assessment: </span>{`${okrAssessmentStartDate} - ${okrAssessmentEndDate}`}</List.Item>
                                    </List.List>
                                </List>
                            </Fragment>
                            <Divider />
                            <Header as="h5" content="Select active period" />
                            <Form>
                                <Form.Field>
                                    <Radio
                                        label="OKR Setting Phase"
                                        name='radioGroup'
                                        value='okrSettingLock'
                                        checked={this.state.period === "okrSettingLock"}
                                        // opposite='okrAssessmentLock'
                                        onChange={this.handleRadioChange}
                                    />
                                    &nbsp;
                                    <Label color={this.props.appraisalCycle.okrSettingLock ? 'red' : 'green'}>
                                        <Icon name={this.props.appraisalCycle.okrSettingLock ? 'lock' : 'lock open'}/>  
                                        {this.props.appraisalCycle.okrSettingLock  ? 'LOCKED' : 'ACTIVE'}
                                    </Label>
                                </Form.Field>
                                <Form.Field>
                                    <Radio
                                        label="OKR Assessment Phase"
                                        name='radioGroup'
                                        value='okrAssessmentLock'
                                        checked={this.state.period === "okrAssessmentLock"}
                                        // opposite='okrSettingLock'
                                        onChange={this.handleRadioChange}
                                    />
                                    &nbsp;
                                    <Label color={this.props.appraisalCycle.okrAssessmentLock ? 'red' : 'green'}>
                                        <Icon name={this.props.appraisalCycle.okrAssessmentLock ? 'lock' : 'lock open'}/>  
                                        {this.props.appraisalCycle.okrAssessmentLock  ? 'LOCKED' : 'ACTIVE'}
                                    </Label>
                                </Form.Field>
                                <Divider/>
                                <Form.Field>
                                    <Radio
                                        label="One on One"
                                        name='radioGroup'
                                        value='monthlyOneOnOnActiveLock'
                                        checked={this.state.period === "monthlyOneOnOnActiveLock"}
                                        // opposite='okrSettingLock'
                                        onChange={this.handleRadioChange}
                                    />
                                    &nbsp;
                                    <Label color={this.props.appraisalCycle.monthlyOneOnOnActiveLock ? 'red' : 'green'}>
                                        <Icon name={this.props.appraisalCycle.monthlyOneOnOnActiveLock ? 'lock' : 'lock open'}/>  
                                        {this.props.appraisalCycle.monthlyOneOnOnActiveLock  ? 'LOCKED' : 'ACTIVE'}
                                    </Label>
                                </Form.Field>
                            </Form>
                            <Segment basic textAlign='right'>
                                {LOCK_APPRAISAL_CYCLE_STATUS === true &&
                                    <Label color="green" >Successfully updated appraisal cycle data</Label>
                                }
                                {LOCK_APPRAISAL_CYCLE_STATUS === false &&
                                    <Label color="red" >Faied to update appraisal cycle data</Label>
                                }
                                {UNLOCK_APPRAISAL_CYCLE_STATUS === true &&
                                    <Label color="green" >Successfully updated appraisal cycle data</Label>
                                }
                                {UNLOCK_APPRAISAL_CYCLE_STATUS === false &&
                                    <Label color="red" >Failed to update appraisal cycle data</Label>
                                }
                                <Button onClick={this.handleLock} color='red' basic
                                        loading={lockAppraisalPeriodLoadingStatus} 
                                        disabled={
                                            lockAppraisalPeriodLoadingStatus || (!period) || (period=="okrSettingLock" && appraisalCycle.okrSettingLock) || 
                                            (period=="okrAssessmentLock" && appraisalCycle.okrAssessmentLock) || 
                                            (period=="monthlyOneOnOnActiveLock" && appraisalCycle.monthlyOneOnOnActiveLock)
                                        }
                                >
                                    <Icon name="lock"/>
                                    Lock
                                </Button>

                                <Button onClick={this.handleUnlock} color='green' basic 
                                        loading={unlockAppraisalPeriodLoadingStatus} 
                                        disabled={
                                            unlockAppraisalPeriodLoadingStatus || (!period) || (period=="okrSettingLock" && !appraisalCycle.okrSettingLock) || 
                                            (period=="okrAssessmentLock" && !appraisalCycle.okrAssessmentLock) ||
                                            (period=="monthlyOneOnOnActiveLock" && !appraisalCycle.monthlyOneOnOnActiveLock)
                                        }
                                >
                                    <Icon name="lock open"/>
                                    Unlock
                                </Button>
                            </Segment>
                        </Segment>
                    </Grid.Column>
                </Grid>
            )
        }


        return (
            <Grid columns={16}>
                <Grid.Column width={3}></Grid.Column>
                <Grid.Column width={10}>
                    <Segment basic>
                        <Segment centered='true'>
                            <Header as="h3" content="Set Appraisal Dates" />
                            <div style={{ color: "red" }}>
                                Please set the following dates in the order that they are arranged on the tabs (from left to right)
                            </div>
                            <Tab menu={{ secondary: true, pointing: true }} panes={panes} />
                            <br />
                            {(startDate && endDate) &&
                                <Fragment>
                                    <Divider />
                                    <Header as="h5" content="Summary" />
                                    <List as='ul'>
                                        <List.Item as='li'><span style={{ fontWeight: "bold" }}>Appraisal Cycle: </span>{`${startDate} - ${endDate}`}</List.Item>
                                        {(settingStartDate && settingEndDate) &&
                                            <List.Item as='li'><span style={{ fontWeight: "bold" }}>OKR Setting: </span>{`${settingStartDate} - ${settingEndDate}`}</List.Item>
                                        }
                                        {(assessmentStartDate && assessmentEndDate) &&
                                            <List.Item as='li'><span style={{ fontWeight: "bold" }}>OKR Assessment: </span>{`${assessmentStartDate} - ${assessmentEndDate}`}</List.Item>
                                        }
                                    </List>
                                </Fragment>
                            }
                            <Segment basic textAlign='right'>
                                <Button fluid content="Submit"
                                    color={'blue'}
                                    onClick={() => this.handleSubmit()}
                                    loading={saveAppraisalCycleLoadingStatus}
                                    disabled={!startDate || !endDate || !settingStartDate || !settingEndDate || !assessmentStartDate || !assessmentEndDate || saveAppraisalCycleLoadingStatus}
                                />
                                {SAVE_APPRAISAL_CYCLE_STATUS === true &&
                                    <Label color="green" >Successfully created appraisal cycle. You'll be rerouted shortly.</Label>
                                }
                                {SAVE_APPRAISAL_CYCLE_STATUS === false &&
                                    <Label color="red" >Failed to create appraisal cycle.</Label>
                                }
                            </Segment>
                        </Segment>
                        
                    </Segment>
                </Grid.Column>
            </Grid>
        )
    }
}


function mapStateToProps(state) {
    return {
        appraisalCycle: state.requestingAppraisalCycle,
        lockAppraisalPeriodLoadingStatus: state.saveLoadingReducer.LOCK_APPRAISAL_PERIOD_LOADING_STATUS,
        unlockAppraisalPeriodLoadingStatus: state.saveLoadingReducer.UNLOCK_APPRAISAL_PERIOD_LOADING_STATUS,
        fetchAppraisalCycleLoadingStatus: state.fetchLoadingReducer.FETCH_APPRAISAL_CYCLE_LOADING_STATUS,
        LOCK_APPRAISAL_CYCLE_STATUS: state.LOCK_APPRAISAL_CYCLE_STATUS,
        UNLOCK_APPRAISAL_CYCLE_STATUS: state.UNLOCK_APPRAISAL_CYCLE_STATUS,
        SAVE_APPRAISAL_CYCLE_STATUS: state.SAVE_APPRAISAL_CYCLE_STATUS,
        saveAppraisalCycleLoadingStatus: state.saveLoadingReducer.SAVE_APPRAISAL_CYCLE_LOADING_STATUS,
    }
}

export default connect(mapStateToProps)(AppraisalCycleSetting)