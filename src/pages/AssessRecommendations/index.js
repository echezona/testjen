import React, { Component } from 'react'
import { Radio, Segment, Grid, Header, Form, Button, Message, Label } from 'semantic-ui-react'
import { SAVE_RECOMMENDATION, UPDATE_RECOMMENDATION, SUBMIT_RECOMMENDATION } from '../../constants';
import PageInfo from '../../components/PageInfo';

class AssessRecommendations extends Component {
    state = {
        recommendation: "",
        comment: "",
    }

    handleChange = (e, { value }) => {
        this.setState({ recommendation: value })
    }

    handleComment = (e) => {
        const { value } = e.target
        this.setState({ comment: value })
    }

    handleSave = () => {
        const { dispatch } = this.props
        const { recommendation, comment } = this.state
        const id = this.props.recommendationObject.id
        const data = {
            staffId: this.props.staffId,
            appraisalId: this.props.appraisalCycleId,
            staffAppraisalId: this.props.staffAppraisalId,
            recommendationType: recommendation,
            comment,
        }
        if(id){
            data.id = id
        }
        console.log("see data oo", data)
        if(data.id){
            dispatch({ type: UPDATE_RECOMMENDATION, data })
        } else {
            dispatch({ type: SUBMIT_RECOMMENDATION, data })
        }
        
    }

    render() {
        console.log("route-state", this.props.recommendationObject)

        const { recommendation, comment } = this.state
        const { recommendationTypes, recommendationObject, UPDATE_RECOMMENDATION_STATUS,
            recommendationSaveLoadingStatus, SUBMIT_RECOMMENDATION_STATUS, recommendationsSubmitLoadingStatus,
            lmIDs, lmObjectives, lmKeyResults, behavioralIndicators} = this.props

        const SUBMIT_ACTIVE = recommendation && comment


        //PageInfo Values start here
        const staffKeyResults = lmIDs.map(item => lmObjectives[item].keyResults)
                                    .reduce((acc, item) => acc.concat(item), [])
                                    .map(item => lmKeyResults[item])

        const staffPerformanceSum = staffKeyResults.reduce((acc, curr) => acc + curr.staffRating, 0)

        const staffPerformanceScore = ((staffPerformanceSum / (staffKeyResults.length * 5)) * 100)

        const staffBehaviorSum = behavioralIndicators.reduce((acc, curr) => acc + curr.staffRating,  0)

        const staffBehaviorScore = ((staffBehaviorSum / (behavioralIndicators.length * 5)) * 100)

        const staffTotalScore = ((staffPerformanceScore / 10) * 7) + ((staffBehaviorScore / 10) * 3)

        const lineManagerPerformanceSum = staffKeyResults.reduce((acc, curr) => acc + curr.lineManagerRating, 0)

        const lineManagerPerformanceScore = ((lineManagerPerformanceSum / (staffKeyResults.length * 5)) * 100)

        const lineManagerBehaviorSum = behavioralIndicators.reduce((acc, curr) => acc + curr.lineManagerRating,  0)

        const lineManagerBehaviorScore = ((lineManagerBehaviorSum / (behavioralIndicators.length * 5)) * 100)

        const lineManagerTotalScore = ((lineManagerPerformanceScore / 10) * 7) + ((lineManagerBehaviorScore / 10) * 3)

        //if the response has already been given
        // if(recommendationObject.recommendationType) {
        //     return (
        //         <Grid columns={16}>
        //             <Grid.Column width={3}></Grid.Column>
        //             <Grid.Column width={10}>
        //                 <Message color={recommendationObject.recommendationType ? 'green' : 'red'}>
        //                     <Header as="h4" content="Recommendation Summary" />
        //                     <Form>
        //                         <Form.Field>
        //                             Submitted recommendation: <b>{this.props.recommendationObject.recommendationType}</b>
        //                         </Form.Field>
        //                         <Form.Field>
        //                             Submitted Comment: <b>{this.props.recommendationObject.comment}</b>
        //                         </Form.Field>
        //                     </Form> 
        //                 </Message>
        //             </Grid.Column>
        //         </Grid>
        //     )
        // }

        return (
            <Grid padded>
                <Grid.Row>
                    <PageInfo
                        title='Click To View/Hide Assessment Scores'  
                        content={
                            <Grid divided columns={2}>
                                <Grid.Column width={8}>
                                    <Header as='h4' content="Staff"/>
                                    <div>Performance: <span style={{fontWeight: "bold"}}>{staffPerformanceScore.toFixed(1) + '%'}</span></div>
                                    <div>Behavior: <span style={{fontWeight: "bold"}}>{staffBehaviorScore.toFixed(1) + '%'}</span></div>
                                    <div style={{marginTop: "10px", fontSize: "1.3em"}}>Total: <span style={{fontWeight: "bold"}}>{staffTotalScore.toFixed(1) + '%'}</span></div>
                                </Grid.Column>
                                <Grid.Column width={8}>
                                    <Header as='h4' content="Line Manager"/>
                                    <div >Performance: <span style={{fontWeight: "bold"}}>{lineManagerPerformanceScore.toFixed(1) + '%'}</span></div>
                                    <div>Behavior: <span style={{fontWeight: "bold"}}>{lineManagerBehaviorScore.toFixed(1) + '%'}</span></div>
                                    <div style={{marginTop: "10px", fontSize: "1.3em"}}>Total: <span style={{fontWeight: "bold"}}>{lineManagerTotalScore.toFixed(1) + '%'}</span></div>
                                </Grid.Column>
                            </Grid>
                        }
                    />
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column width={3}></Grid.Column>
                    <Grid.Column width={10}>
                        <Segment >
                            <Header as="h4" content="Select Recommendation" />
                            <Message color={recommendationObject.recommendationType ? 'green' : 'red'}>
                                <Form>
                                    <Form.Field>
                                        Submitted Recommendation: <b>{this.props.recommendationObject.recommendationType || 'Nothing submitted yet'}</b>
                                    </Form.Field>
                                    <Form.Field>
                                        Submitted Comment: <b>{this.props.recommendationObject.comment || 'Nothing submitted yet'}</b>
                                    </Form.Field>
                                </Form> 
                            </Message>
                            <Form>
                                {recommendationTypes.map((type, index) => {
                                    return (
                                        <Form.Field>
                                            <Radio
                                                label={type.name}
                                                name='radioGroup'
                                                value={type.name}
                                                checked={recommendation === type.name}
                                                onChange={this.handleChange}
                                            />
                                        </Form.Field>
                                    )
                                })}
                                <Form.TextArea placeholder="Please give your reasons for selecting this recommendation." onChange={this.handleComment} value={comment} />
                            </Form>
                            <br/>
                            <Segment basic textAlign='right'>
                                { UPDATE_RECOMMENDATION_STATUS === true && 
                                    <Label color="green" >Save Successful</Label>
                                }
                                { UPDATE_RECOMMENDATION_STATUS === false && 
                                    <Label color="red" >Save Failed. Please try again or refresh page.</Label>
                                }
                                { SUBMIT_RECOMMENDATION_STATUS === true && 
                                    <Label color="green" >Submission Successful</Label>
                                }
                                { SUBMIT_RECOMMENDATION_STATUS === false && 
                                    <Label color="red" >Submission Failed. Please try again or refresh page.</Label>
                                }
                                <Button content="Save" onClick={this.handleSave} disabled={!SUBMIT_ACTIVE || recommendationSaveLoadingStatus || recommendationsSubmitLoadingStatus} color="blue" loading={recommendationSaveLoadingStatus || recommendationsSubmitLoadingStatus}/>
                            </Segment>
                        </Segment>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        )
    }
}

export default AssessRecommendations