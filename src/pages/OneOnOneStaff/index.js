//Components
import React, { Fragment, useEffect, useState } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import {  Button, Label, Loader, Dropdown, Header, Divider, Segment, TextArea, Form, Grid, Checkbox, Icon, Message } from 'semantic-ui-react';

// Css Files
import './index.css';
import { FETCH_LINE_MANAGER_OKRS, FETCH_LINE_MANAGER_STAFF_APPRAISALS, FETCH_PEOPLE_OPS_STAFF_APPRAISALS, FETCH_PEOPLE_OPS_COUNT, SUBMIT_PEOPLE_OPS_OKRS, FETCH_ALL_STAFF, FETCH_APPRAISAL_OBJECTIVES, FETCH_OBJECTIVES_AND_KEY_RESULTS, FETCH_STAFF_OKR_PERIOD, CREATE_STAFF_OKR_PERIOD, UPDATE_STAFF_OKR_PERIOD, FETCH_ALL_STAFF_OKR_PERIOD, FETCH_STAFF_APPRAISAL } from '../../constants';
import { poStaffAppraisalsSelector, userIDsSelector, userObjectivesSelector, userKeyResultsSelector, userCategoriesSelector, sbuObjectivesSelector, staffIdSelector } from '../../reducer';
import { ToastContainer } from "react-toastify";
import ReactModal from 'react-modal';
import ExceptionHandler from './ExceptionHandler';



const date = new Date();
const getCurrentMonth = () => {
    const monthArray = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    return monthArray[date.getMonth()];
}
const getCurrentYear = () => date.getFullYear();
const wizardSteps = [
    { step: 'STEP 1', name: 'OKR Progress' },  
    { step: 'STEP 2', name: 'General Comments' },  
    { step: 'STEP 3', name: 'Conclusion' }  
]

const OneOnOneStaff = ({ 
    dispatch, userIDs, userObjectives, userKeyResults, userCategories, isFetchingOKRs,
    staffOkrPeriod, isCreatingStaffOkrPeriod, sbuObjectives, staffId, isUpdatingStaffOkrPeriod,
    allStaffOkrPeriod, appraisalCycle, isFetchingAppraisalCycle, staffAppraisal, isFetchingStaffAppraisal
}) => {

    const [objOneOnOne, setObjOneOnOne] = useState([]);
    const [period, setPeriod] = useState(null);
    const [oneOnOneStatusOptions] = useState([
        { key: 'On track', text: 'On track', value: 'on track' },
        { key: 'Off track', text: 'Off track', value: 'off track' },
        { key: 'Paused', text: 'Paused', value: 'paused' },
        { key: 'Postponed', text: 'Postponed', value: 'postponed' },
        { key: 'Completed', text: 'Completed', value: 'completed' },
        { key: 'Late', text: 'Late', value: 'late' },
    ]);
    const [pageStep, setPageStep] = useState(wizardSteps[0]);
    const [modal, setModal] = useState({ open: false, mode: null, krId: null, objId: null });
    const [selectedPeriod, setSelectedPeriod] = useState(`${getCurrentMonth()}, ${getCurrentYear()}`);
    const [isComplete, setIsComplete] = useState(false);

    const onPeriodSelectChange = (e, {value}) => {
        //if dropdown value does not change, then do nothing.
        if (selectedPeriod === value) {
            return;
        }

        setSelectedPeriod(value);

        const month = value.substring(0, value.indexOf(','))
        const year = parseInt(value.substring(value.indexOf(',') + 2))

        dispatch({ type: FETCH_STAFF_OKR_PERIOD, payload: {month, year} });
    }

    const nextWizardClick = () => {
        switch (pageStep.step) {
            case 'STEP 1':
                setPageStep(wizardSteps[1]);
                return;
            case 'STEP 2':
                setPageStep(wizardSteps[2]);
                return;
            default:
                return;
        }
    }

    const prevWizardClick = () => {
        switch (pageStep.step) {
            case 'STEP 3':
                setPageStep(wizardSteps[1]);
                return;
            case 'STEP 2':
                setPageStep(wizardSteps[0]);
                return;
            default:
                return;
        }
    }

    useEffect(() => {
        if( staffOkrPeriod.data && userKeyResults ) {
            const objectives = userIDs.map(id => userObjectives[id])
            const okrs = objectives.reduce((acc, curr) => {
                curr = {
                    ...curr,
                    keyResults: curr.keyResults.map(id => ({
                        ...userKeyResults[id], 
                        oneOnOne: staffOkrPeriod.data.oneOnOne.find(oneOnOne => oneOnOne.keyResultId === id) 
                    }))
                }
                acc.push(curr)
                return acc;
            }, [])

            setObjOneOnOne(okrs);

            setPeriod({ ...staffOkrPeriod.data, oneOnOne: null, staffId })

            setIsComplete(staffOkrPeriod?.data?.periodStatusCompletion)

            console.log('okra', okrs)
        }

    }, [isFetchingOKRs, staffOkrPeriod.loading])

    useEffect(() => {
        dispatch({ type: FETCH_OBJECTIVES_AND_KEY_RESULTS });
        dispatch({ type: FETCH_ALL_STAFF_OKR_PERIOD });
        dispatch({ type: FETCH_STAFF_APPRAISAL });
        dispatch({ type: FETCH_STAFF_OKR_PERIOD, payload: {month: getCurrentMonth(), year: getCurrentYear()} });
    }, [])

    const createPeriod = () => {
        const keyResultIds = Object.keys(userKeyResults);
        const oneOnOneArray = keyResultIds.map((keyResultId) => ({
            lineManagerComment: "",
            staffComment: "",
            oneOnOneStatus: "",
            keyResultId
        }))
        const newPeriod = {
            generalLineManagerComment: "",
            generalStaffComment: "",
            period: `${getCurrentMonth()} ${getCurrentYear()}`,
            month: getCurrentMonth(),
            year: getCurrentYear(),
            periodStatusCompletion: false,
            oneOnOne: oneOnOneArray
        }
        console.log('do something now', newPeriod)

        dispatch({ type: CREATE_STAFF_OKR_PERIOD, payload: { data: newPeriod } });
    }

    const selectStatus = (e, {value}, krId) => {
        console.log('val', value, krId)

        const newObjOneOnOne = objOneOnOne.reduce((acc, curr) => {
            curr = {
                ...curr,
                keyResults: curr.keyResults.map(krItem => {
                    if (krItem.id == krId) {
                        return {
                            ...krItem,
                            oneOnOne: {
                                ...krItem.oneOnOne,
                                oneOnOneStatus: value
                            }
                        }
                    }
                    return krItem
                })
            }
            acc.push(curr);
            return acc
        }, [])

        setObjOneOnOne(newObjOneOnOne)
    }

    const onComplete = () => {
        const allOneOnOnes = objOneOnOne.reduce((acc, curr) => {
            curr = [...curr.keyResults].map(kr => kr.oneOnOne)
            acc = acc.concat(curr)
            return acc
        }, [])
        // const periodz = { ...period, oneOnOne: allOneOnOnes }
        // console.log('boo', periodz, allOneOnOnes)

        dispatch({ type: UPDATE_STAFF_OKR_PERIOD, payload: { data: { ...period, oneOnOne: allOneOnOnes, periodStatusCompletion: true } } });
    }

    const onSave = () => {
        const allOneOnOnes = objOneOnOne.reduce((acc, curr) => {
            curr = [...curr.keyResults].map(kr => kr.oneOnOne)
            acc = acc.concat(curr)
            return acc
        }, [])
        // const periodz = { ...period, oneOnOne: allOneOnOnes }
        // console.log('boo', periodz, allOneOnOnes)

        dispatch({ type: UPDATE_STAFF_OKR_PERIOD, payload: { data: { ...period, oneOnOne: allOneOnOnes } } });
    }

    const closeModal = () => setModal({ ...modal, open: false, mode: null });

    const onKrMenuChange = (e, {value}, krId, objId) => {
        //if dropdown value is not valid, then do nothing.
        if (!value) {
            return;
        }
        setModal({ open: true, mode: value, krId,  objId })
    }

    const onCommentChange = (e, krId) => {
        // console.log('val', value, krId)

        const newObjOneOnOne = objOneOnOne.reduce((acc, curr) => {
            curr = {
                ...curr,
                keyResults: curr.keyResults.map(krItem => {
                    if (krItem.id == krId) {
                        return {
                            ...krItem,
                            oneOnOne: {
                                ...krItem.oneOnOne,
                                staffComment: e.target.value
                            }
                        }
                    }
                    return krItem
                })
            }
            acc.push(curr);
            return acc
        }, [])

        setObjOneOnOne(newObjOneOnOne)
    }

    
    const conditions = () => ({
        IS_VIEWING_PAST_PERIOD: `${staffOkrPeriod.data?.month}, ${staffOkrPeriod.data?.year}` !== `${getCurrentMonth()}, ${getCurrentYear()}`,
        ONE_ON_ONE_PERIOD_DISABLED: appraisalCycle?.monthlyOneOnOnActiveLock
    })
    
    console.log('logged', appraisalCycle, staffAppraisal, staffOkrPeriod.data, conditions(), allStaffOkrPeriod.data, userKeyResults);

    if (staffOkrPeriod.loading || allStaffOkrPeriod.loading || isFetchingOKRs || isFetchingAppraisalCycle || isFetchingStaffAppraisal) {
        return <Loader active />
    }

    if (!userKeyResults || (appraisalCycle.monthlyOneOnOnActiveLock && !staffOkrPeriod.data && userKeyResults) || staffAppraisal.lineManagerTwoApproved !== 'approved') {
        return (
            <ExceptionHandler
                staffOkrPeriod={staffOkrPeriod}
                userKeyResults={userKeyResults}
                appraisalCycle={appraisalCycle}
                staffAppraisal={staffAppraisal}
            />
        )
    }

    if (!staffOkrPeriod.data && userKeyResults && !appraisalCycle.monthlyOneOnOnActiveLock) {
        return (
            <div className='general-comments' >
                <Segment placeholder textAlign='center'>
                    <Header icon>
                        <Icon name='warning sign' color='yellow' />
                        No One-on-One found for current period!
                    </Header>
                    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                        <Button color="blue" content="Start One on One" onClick={createPeriod} loading={isCreatingStaffOkrPeriod} disabled={isCreatingStaffOkrPeriod} />
                    </div>
                </Segment>
            </div>
        )
    }

    return (
        <div>
            {/* Toast Container */}
            <ToastContainer/>
            {/* Modal */}
            <ReactModal
                id='commentModal'
                isOpen={modal.open}
                onRequestClose={closeModal}
                style={{
                    overlay: {backgroundColor: 'rgba(0, 0, 0, 0.75)'},
                    content: {
                        top: '25vh',
                        left: '250px',
                        right: '250px',
                        bottom: '30vh',
                    },
                }}
            >
                <Header
                    content= {modal.mode === 'staffComment' ? 'Enter your comment' : 'Line Manager Comment'}
                />
                { modal.mode === 'staffComment' &&
                    <>
                        <Form>
                            <label>
                                {/* Enter your objective */}
                                <textarea rows={6} 
                                    value={objOneOnOne.find(objItem => objItem.id === modal.objId).keyResults.find(krItem => krItem.id === modal.krId).oneOnOne.staffComment} 
                                    placeholder='Enter your comment' 
                                    onChange={(e) => onCommentChange(e, modal.krId)}
                                />
                            </label>
                        </Form>
                        { !(conditions().IS_VIEWING_PAST_PERIOD || conditions().ONE_ON_ONE_PERIOD_DISABLED) &&
                            <div className='save-button'>
                                <Button
                                    content='Save'
                                    color='blue'
                                    onClick={() => {onSave(); closeModal()}}
                                    loading={isUpdatingStaffOkrPeriod}
                                    disabled={isUpdatingStaffOkrPeriod || !objOneOnOne.find(objItem => objItem.id === modal.objId).keyResults.find(krItem => krItem.id === modal.krId).oneOnOne.staffComment.trim()}
                                />
                            </div>
                        }
                    </>
                }
                { modal.mode === 'lineManagerComment' &&
                    <Form>
                        <textarea rows={6} 
                            value={objOneOnOne.find(objItem => objItem.id === modal.objId).keyResults.find(krItem => krItem.id === modal.krId).oneOnOne.lineManagerComment} 
                            readOnly
                        />
                    </Form>
                }

            </ReactModal>
            {/* Period Bar */}
            <div className='period-bar'>
                <Dropdown 
                    selection 
                    options={allStaffOkrPeriod.data?.map(period => ({
                        key: period.id,
                        text: `${period.month}, ${period.year}`,
                        value: `${period.month}, ${period.year}`,
                    }))} 
                    // options={[{ key: `${getCurrentMonth()}, ${getCurrentYear()}`, text: `${getCurrentMonth()}, ${getCurrentYear()}`, value: `${getCurrentMonth()}, ${getCurrentYear()}` }]} 
                    value={selectedPeriod}
                    onChange={onPeriodSelectChange}
                />
                <div className='period-bar__status'>
                    <Label
                        content={ staffOkrPeriod?.data?.periodStatusCompletion ? 'Complete' : 'Incomplete' }
                        color={ staffOkrPeriod?.data?.periodStatusCompletion ? 'green' : 'red' }
                        size='big'
                    />
                </div>
            </div>
            {/* Exception display message */}
            { conditions().IS_VIEWING_PAST_PERIOD
                ? (
                    <Message icon>
                        <Icon name='info circle'/>
                        <Message.Content>
                        <Message.Header>Attention</Message.Header>
                            You are viewing your past records and thus will be <b>UNABLE</b> to save your changes.
                        </Message.Content>
                    </Message>
                )
                : conditions().ONE_ON_ONE_PERIOD_DISABLED
                ? (
                    <Message icon>
                        <Icon name='info circle'/>
                        <Message.Content>
                        <Message.Header>Attention</Message.Header>
                            The One-on-one period has been disabled by People-Ops so you'll be <b>UNABLE</b> to save any changes.
                        </Message.Content>
                    </Message>
                )
                : null
            }
            {/* Wizard Switcher */}
            <div className='wizard-control'>
                <Header
                    as='h3'
                    content={pageStep.step}
                    subheader={pageStep.name}
                />
                <div className='wizard-control__buttons'>
                    <Button.Group>
                        <Button content='Prev' icon='arrow left' labelPosition='left' color='blue' onClick={prevWizardClick} disabled={pageStep.step === 'STEP 1'} />
                        <Button.Or text='' />
                        <Button content='Next' icon='arrow right' labelPosition='right' color='blue' onClick={nextWizardClick} disabled={pageStep.step === 'STEP 3'} />
                    </Button.Group>
                </div>
            </div>
            { pageStep.step === 'STEP 1' &&
                <>
                    {objOneOnOne.map((objItem, index) => (
                        <div className='objective'>
                            <Segment>
                                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                    <div style={{ display: 'inline-block' }}>
                                        {/* <div style={{ display: 'flex', alignItems: 'center' }}> */}
                                            <Header 
                                                as='h2' 
                                                content={objItem.order=='Personal' ? 'Personal Development Objective' : `Business Objective ${objItem.order}`}
                                                style={{ display: 'inline-block', margin: '0 10px 0 0' }}    
                                            />
                                        {/* </div> */}
                                    </div>
                                </div>
                                <Divider/>
                                <p>
                                    { objItem.description }
                                </p>
                                { objItem.order !== 'Personal' &&
                                    <>
                                        <Header as='h5' content='SBU Objective'/>
                                        <p>{sbuObjectives && sbuObjectives.length > 0 ? sbuObjectives?.find(sbuObj => sbuObj.id === objItem.sbuObjectiveId)?.description : null}</p>
                                    </>
                                }
                                <div>
                                    {objItem.keyResults.map(krItem => (
                                        <Segment>
                                            <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                                                <Header 
                                                    as='h4' 
                                                    content={`Key Result ${krItem.order}`}
                                                    style={{ display: 'inline-block', margin: '0 10px 0 0' }}    
                                                />
                                                <Dropdown 
                                                    id='ellipsis-dropdown'
                                                    icon={null}
                                                    direction='left'
                                                    trigger = {(
                                                        <Button circular icon='ellipsis vertical' />
                                                    )}
                                                    inline
                                                    header='OPTIONS'
                                                    options={[
                                                        {text: "Select an action", value:''},
                                                        {text: `Comment`, value: "staffComment", icon:'comment'},
                                                        {text: `Line Manager Comment`, value: 'lineManagerComment', icon:'comment', disabled: krItem.oneOnOne.lineManagerComment ? false : true },
                                                    ]}
                                                    onChange={(e, titleProps) => onKrMenuChange(e, titleProps, krItem.id, objItem.id)}
                                                    value={modal.mode}
                                                    // loading={deleteLoadingStatus}
                                                    // disabled={deleteLoadingStatus}
                                                />
                                            </div>
                                            <Divider style={{marginTop: '4px'}} />
                                            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                                <p>
                                                    {krItem.description}
                                                </p>
                                                <Dropdown 
                                                    placeholder='Status' 
                                                    selection 
                                                    options={oneOnOneStatusOptions} 
                                                    value={krItem.oneOnOne.oneOnOneStatus}
                                                    onChange={(e, titleProps) => selectStatus(e, titleProps, krItem.id)}
                                                />
                                            </div>
                                        </Segment>
                                    ))}
                                </div>
                            </Segment>
                        </div>
                    ))}
                    { !(conditions().IS_VIEWING_PAST_PERIOD || conditions().ONE_ON_ONE_PERIOD_DISABLED) &&
                        <div className='save-button'>
                            <Button
                                content='Save'
                                color='blue'
                                onClick={onSave}
                                loading={isUpdatingStaffOkrPeriod}
                                disabled={isUpdatingStaffOkrPeriod}
                            />
                        </div>
                    }
                </>
            }
            { pageStep.step === 'STEP 2' &&
               
                <div className='general-comments' >
                    <Segment placeholder>
                        <Grid columns={2} stackable textAlign='center'>
                            <Divider vertical></Divider>

                            <Grid.Row verticalAlign='middle'>
                                <Grid.Column>
                                    <Form>
                                        Your Comment
                                        <TextArea 
                                            rows={6}
                                            placeholder='Please provide a general comment for this session' 
                                            value={period.generalStaffComment}    
                                            onChange={(e, {value}) =>  setPeriod({ ...period, generalStaffComment: value })}
                                        />
                                    </Form>
                                </Grid.Column>

                                <Grid.Column>
                                    <Form>
                                        Your Line Manager's Comment
                                        <TextArea 
                                            rows={6}
                                            placeholder='No comment yet from your line manager' 
                                            value={ period.generalLineManagerComment }   
                                            readOnly 
                                        />
                                    </Form>
                                </Grid.Column>
                        </Grid.Row>
                        </Grid>
                    </Segment>
                    { !(conditions().IS_VIEWING_PAST_PERIOD || conditions().ONE_ON_ONE_PERIOD_DISABLED) &&
                        <div className='save-button'>
                            <Button
                                content='Save'
                                color='blue'
                                onClick={onSave}
                                loading={isUpdatingStaffOkrPeriod}
                                disabled={isUpdatingStaffOkrPeriod || !period.generalStaffComment.trim()}
                            />
                        </div>
                    }
                </div>
            }
            { pageStep.step === 'STEP 3' &&
                <>
                    { !staffOkrPeriod.data.periodStatusCompletion &&
                        <div className='general-comments' >
                            <Segment placeholder textAlign='center'>
                                <Header icon>
                                    <Icon name='info circle' color='blue' />
                                    Your line manager is yet to complete your one-on-one session for this period.
                                </Header>
                            </Segment>
                        </div>
                    }

                    { staffOkrPeriod.data.periodStatusCompletion &&
                        <div className='general-comments' >
                            <Segment placeholder textAlign='center'>
                                <Header icon>
                                    <Icon name='check circle outline' color='green' />
                                    You have completed your one-on-one session with your line manager for this period.
                                </Header>
                            </Segment>
                        </div>
                    }
                
                </>
            }
        </div>
    )
}

function mapStateToProps(state) {
    return {
        userIDs: userIDsSelector(state),
        userObjectives: userObjectivesSelector(state),
        userKeyResults: userKeyResultsSelector(state),
        userCategories: userCategoriesSelector(state),
        isFetchingOKRs: state.fetchLoadingReducer.FETCH_OBJECTIVES_AND_KEY_RESULTS_LOADING_STATUS,
        staffOkrPeriod: state.staffOkrPeriodReducer,
        isCreatingStaffOkrPeriod: state.saveLoadingReducer.CREATE_STAFF_OKR_PERIOD_LOADING_STATUS,
        isUpdatingStaffOkrPeriod: state.saveLoadingReducer.UPDATE_STAFF_OKR_PERIOD_LOADING_STATUS,
        sbuObjectives: sbuObjectivesSelector(state),
        staffId: staffIdSelector(state),
        allStaffOkrPeriod: state.allStaffOkrPeriodReducer,
        isFetchingAppraisalCycle: state.fetchLoadingReducer.FETCH_APPRAISAL_CYCLE_LOADING_STATUS,
        appraisalCycle: state.requestingAppraisalCycle,
        isFetchingStaffAppraisal: state.fetchLoadingReducer.FETCH_STAFF_APPRAISAL_LOADING_STATUS,
        staffAppraisal: state.requestingStaffAppraisal,
    }
}

export default connect(mapStateToProps)(OneOnOneStaff)