import React, { Component } from "react";
import axios from "axios";
import { Progress } from "reactstrap";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import DropdownSelection from "../../dropdown";
import ReactDropzone from "react-dropzone";
import Oidc from "oidc-client";
import Level from "../../helper";
import {
  SSO_REDIRECT_PATH,
  SSO_URL,
  REDIRECT_URI,
  POST_LOGOUT_REDIRECT_URI,
  CLIENT_ID
} from "../../appConstants";

import {
  Header,
  List,
  Grid,
  Segment,
  Icon,
  Form,
  Button,
  Divider,
  Container,
  Label
} from "semantic-ui-react";
import Dropzone from "react-dropzone";
import "./index.css";
import { staffBaseUrl } from "../../baseUrl";
const url = `${staffBaseUrl}/apimanager/UploadStaffs`;

const settings = {
  client_id: CLIENT_ID,
  authority: SSO_URL,
  redirect_uri: REDIRECT_URI,
  response_type: "id_token token",
  scope: "openid profile identity-server-api",
  post_logout_redirect_uri: POST_LOGOUT_REDIRECT_URI
};

const client = new Oidc.UserManager(settings);


class UploadStaff extends Component {
  state = {
    selectedFile: null,
    level: Level,
    currentLevel: "",
    fileUploading: ""
  };
  closeAfter20 = () => toast.warn("Uploading File....", { autoClose: 50000 });

  handleSelectChange = e => {
    const { value } = e.target;
    this.setState({
      currentLevel: value
    });
  };

  onDrop = async files => {
    //console.log(upload);
    files.forEach(async file => {
      let formData = new FormData();
      formData.append("formFiles", file);
      formData.append("currentLevel", this.state.currentLevel);
      try {
        this.setState({
          fileUploading: this.closeAfter20()
        });
        const user = await client.getUser();
        const upload = await axios.post(url, formData
        , {
          headers: {
            Authorization: `Bearer ${user.access_token}`
          }
        });

        const { data } = upload;
        if (data.message === "Worked fine") {
          this.setState({ fileUploading: toast.dismiss() });
          toast.success("Uploaded Successfully");
        }
        // console.log(data);
        // console.log(upload);
      } catch (error) {
        console.log(error.message);
        this.setState({ fileUploading: toast.dismiss() });
        toast.error("upload fail");
      }
    });
  };

  // axios.post("http://localhost:8000/upload", data, {
  //        onUploadProgress: ProgressEvent => {
  //          this.setState({
  //            loaded: (ProgressEvent.loaded / ProgressEvent.total*100),
  //        })
  //    },
  // })
  render() {
    const { level, currentLevel } = this.state;
        // console.log(currentLevel);

    return (
      <div>
        <ToastContainer />

        <Grid columns={16}>
          <Grid.Column width={3}></Grid.Column>
          <Grid.Column width={10}>
            <Segment basic>
              <Segment>
                <Header as="h3" content="Upload Staff " />
                <DropdownSelection />
                <List as="ol">
                  <List.Item as="li">
                    Drag the{" "}
                    <span style={{ fontWeight: "bold" }}>Excel or CSV</span>{" "}
                    file to the dropzone or click the dropzone and select the
                    file you want to upload
                  </List.Item>
                  <List.Item as="li">The file uploads automatically</List.Item>
                  {/* {depts.map(d => d.)} */}
                </List>
                <Form>
                  <select
                    name="currentLevel"
                    value={currentLevel}
                    onChange={e => this.handleSelectChange(e)}
                  >
                    <option value="" disabled>
                      Select the level for which you want to upload a file
                    </option>
                    {level.map((item, index) => (
                      <option value={item} key={item}>{`${item}`}</option>
                    ))}
                  </select>
                </Form>{" "}
                <br />
                <br />
                <ReactDropzone
                  accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel, text/csv"
                  onDrop={this.onDrop}
                  className="w-100 h7 dropzone-container"
                  activeClassName="bg-light-green"
                  rejectClassName="bg-light-red"
                  name="formFiles"
                >
                  Drag / Click to Upload Staff Here
                </ReactDropzone>
              </Segment>
            </Segment>
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}

export default UploadStaff;
