import React from 'react'
import { Grid, Loader, Message, Button, Container, Header, Segment, Divider, Form } from 'semantic-ui-react';
import { connect } from "react-redux"
import { lmIDsSelector, lmObjectivesSelector, lmKeyResultsSelector, lmSbuObjectivesSelector } from '../../reducer'
import { FETCH_CATEGORY, FETCH_SUPER_LINE_MANAGER_STAFF_APPRAISALS, SUBMIT_LINE_MANAGER_OKRS, FETCH_LMPO_OKRS, STORE_PENDING_KEY_RESULTS, FETCH_SBU_OBJECTIVES, SUBMIT_SUPER_LINE_MANAGER_OKRS } from '../../constants';
import queryString from 'query-string'
import { withRouter } from 'react-router-dom'


class Summary extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            status: "pending",
            comment: "",
            updated: false,
        }
        console.log("slm", props.slmStaffAppraisals)
    }

    //userIDs sort function
    sortFunctionOne = (a, b) => {
        const { lmObjectives } = this.props
        if (lmObjectives[a].order < lmObjectives[b].order) {
            return -1
        } else if (lmObjectives[a].order > lmObjectives[b].order) {
            return 1
        } else {
            return 0
        }
    }

    // componentDidUpdate(){
    //     const {slmStaffAppraisals} = this.props
    //     const {updated} = this.state
    //     const queryValues = queryString.parse(this.props.location.search)
    //     if(!updated && slmStaffAppraisals.length > 0){
    //         this.setState({
    //             status: this.props.slmStaffAppraisals.find(item => item.staffId === queryValues.staffId).lineManagerTwoApproved,
    //             comment: this.props.slmStaffAppraisals.find(item => item.staffId === queryValues.staffId).superLineManagerComment,
    //             updated: true,
    //         })
    //     }
    //     // console.log("undermam", this.state.status)
    // }

    // componentDidMount() {
    //     const { dispatch, match } = this.props;
    //     dispatch({ type: FETCH_LMPO_OKRS, staffId: match.params.id })
    //     dispatch({ type: FETCH_CATEGORY })
    //     dispatch({ type: FETCH_SBU_OBJECTIVES })
    //     dispatch({ type: FETCH_SUPER_LINE_MANAGER_STAFF_APPRAISALS })
    // }

    handleApproval = (e) => {
        const { name } = e.target
        this.setState({
            status: name
        })
    }

    handleCommentChange = (e) => {
        const {value} = e.target;
        this.setState({
            comment: value
        })
    }

    handleSubmit = () => {
        const { match, dispatch, history } = this.props;
        const { status, comment } = this.state;
        const queryValues = queryString.parse(this.props.location.search)

        let superLineManagerStatus;

        if(status === "approved"){
            superLineManagerStatus = {superLineManagerStatus: "completed"}
        } else if (status === "disapproved") {
            superLineManagerStatus = {superLineManagerStatus: "awaiting resubmission"}
        }

        //move this into the dispatched function for better performance
        this.props.slmStaffAppraisals.find(item => item.staffId === queryValues.staffId).lineManagerTwoApproved = status
        this.forceUpdate()

        dispatch({type: SUBMIT_SUPER_LINE_MANAGER_OKRS, data: {staffId: queryValues.staffId, lineManagerTwoComment: comment, lineManagerTwoApproved: status}, staffAppraisalId: this.props.slmStaffAppraisals.find(item => item.staffId === queryValues.staffId).staffAppraisalId, history, superLineManagerStatus})
    }

    render() {
        const { IS_REQUESTING_OKRs, sbuObjectives, isFetchingStaffAppraisals, isFetchingLMPOOKRs, match, lmIDs, lmObjectives, lmStaffAppraisals, lmCategories, categories, lmKeyResults, lmSbuObjectives , isSavingOkrs} = this.props
        const { status, comment } = this.state
        const submitEnabled = status === "approved"
        const submitEnabled2 = status === "disapproved" && comment !== ""

        if(isFetchingLMPOOKRs){
            return (
                <Loader active/>
            )
        }

        return (
            <Grid columns={16}>
                <Grid.Column width={3}></Grid.Column>
                <Grid.Column width={10}>
                    <Segment basic>
                        {/* <Header as="h2" content="Summary" /> */}

                        {/* {staffSubmitted &&
                            <Message id='green' color="green">You have successfully submitted your OKRs to your Line Manager</Message>
                        }

                        {!staffSubmitted &&
                            <Message color="yellow">Please review your OKRs carefully before you submit as you'll be unable to edit them once you do</Message>
                        } */}

                        <Segment style={{ maxHeight: 500, overflow: "scroll" }} centered>
                            {
                                lmIDs.sort(this.sortFunctionOne).map((item) => {
                                    return (
                                        <Segment>
                                            <Header as="h4" content={lmObjectives[item].order > 0 && lmObjectives[item].order < 6 ? `Objective ${lmObjectives[item].order}` : `${lmObjectives[item].order} Objective`} />
                                            <div>{`Staff Objective: ${lmObjectives[item].description}`}</div>
                                            {lmObjectives[item].order > 0 && lmObjectives[item].order < 6 && Object.prototype.toString.call(lmSbuObjectives) === '[object Array]' &&
                                                <div>{`SBU Objective: ${lmSbuObjectives.find(sbuObj => sbuObj.id === lmObjectives[item].sbuObjectiveId) !== undefined ? lmSbuObjectives.find(sbuObj => sbuObj.id === lmObjectives[item].sbuObjectiveId).description : "None"}`}</div>
                                            }
                                            <Divider />
                                            <Segment basic>
                                                {
                                                    lmObjectives[item].keyResults.map((item) => <Header as="h5" content={`Key Result ${lmKeyResults[item].order}`} subheader={lmKeyResults[item].description} />)
                                                }
                                            </Segment>
                                        </Segment>
                                    )
                                })
                            }
                        </Segment>
                        <Button.Group fluid>
                            <Button name="approved" color='green' content="Approve" disabled={status === "approved"} onClick={(e) => this.handleApproval(e)} />
                            <Button.Or />
                            <Button name="disapproved" color='red' content="Disapprove" disabled={status === "disapproved"} onClick={(e) => this.handleApproval(e)} />
                        </Button.Group>
                        {status === "disapproved" &&
                            <Grid.Column width={16}>
                                <br/>
                                <Form>
                                    <Form.TextArea
                                        style={{borderColor: "red"}}
                                        name="textfield"
                                        onChange={(e) => this.handleCommentChange(e)}
                                        value={comment}
                                        placeholder="Please suggest in detail what could be done to improve this staff's OKRs" 
                                    />
                                    {/* <Form.Button color="blue" floated="right">Submit Comment</Form.Button> */}
                                </Form>
                            </Grid.Column>
                        }
                        <Divider />
                        <Button fluid content="Submit" disabled={!(submitEnabled || submitEnabled2)} color="blue" onClick={() => this.handleSubmit()} loading={isSavingOkrs}/>
                    </Segment>
                </Grid.Column>
            </Grid>
        )
    }
}

function mapStateToProps(state) {
    return {
        lmIDs: lmIDsSelector(state),
        lmObjectives: lmObjectivesSelector(state),
        lmKeyResults: lmKeyResultsSelector(state),
        lmSbuObjectives: lmSbuObjectivesSelector(state),
        isFetchingLMPOOKRs: state.isFetchingLMPOOKRs,
        sbuObjectives: state.sbuObjectives,
        slmStaffAppraisals: state.requestingSuperLineManagerStaffAppraisals,
        isSavingOkrs: state.saveLoadingReducer.SUBMIT_LINE_MANAGER_OKRS_LOADING_STATUS,
    }
}

export default withRouter(connect(mapStateToProps)(Summary))