import React, {Fragment} from 'react'
import { Grid, Loader, Message, Button, Container, Header, Segment, Divider } from 'semantic-ui-react';
import { connect } from "react-redux"
import { lmIDsSelector, lmObjectivesSelector, lmKeyResultsSelector } from '../../reducer'
import { FETCH_LINE_MANAGER_OKRS, FETCH_CATEGORY, FETCH_LINE_MANAGER_STAFF_APPRAISALS, SUBMIT_LINE_MANAGER_OKRS, FETCH_LMPO_OKRS, STORE_PENDING_KEY_RESULTS, FETCH_SBU_OBJECTIVES, FETCH_SUPER_LINE_MANAGER_STAFF_APPRAISALS } from '../../constants';
import { withRouter } from 'react-router-dom'
import queryString from 'query-string'

import PageHeader from './Header';
import Summary from './Summary';


class SuperApproveOKRs extends React.Component {

    componentDidMount() {
        const { dispatch, match } = this.props;
        const queryValues = queryString.parse(this.props.location.search)

        dispatch({ type: FETCH_LMPO_OKRS, staffId: `?staffId=${queryValues.staffId}` })
        dispatch({ type: FETCH_CATEGORY })
        dispatch({ type: FETCH_SBU_OBJECTIVES })
        dispatch({ type: FETCH_SUPER_LINE_MANAGER_STAFF_APPRAISALS })
    }


    render(){
        const { IS_REQUESTING_OKRs, sbuObjectives, isFetchingStaffAppraisals, isFetchingLMPOOKRs, match, lmIDs, lmObjectives, lmStaffAppraisals, lmCategories, categories, lmKeyResults } = this.props
        //create a variable to hold the parsed values kept in the query parameter
        const queryValues = queryString.parse(this.props.location.search)
        return(
            <Fragment>
                <PageHeader id={queryValues.staffId}/>
                <Summary match={match}/>
            </Fragment>
        )
    }
}

function mapStateToProps(state) {
    return {
        lmIDs: lmIDsSelector(state),
        lmObjectives: lmObjectivesSelector(state),
        lmKeyResults: lmKeyResultsSelector(state),
        isFetchingLMPOOKRs: state.isFetchingLMPOOKRs,
        sbuObjectives: state.sbuObjectives,
    }
}

export default withRouter(connect(mapStateToProps)(SuperApproveOKRs))