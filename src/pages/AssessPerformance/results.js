import React, {Fragment} from 'react';
import { Segment,Header,Button,Container,Label } from 'semantic-ui-react';
import KeyResult from './KeyResult'
import {SAVE_KEY_RESULTS, SUBMIT_OKRS} from '../../constants'
import {connect} from 'react-redux'


class Results extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            description: props.objective.description,
            id: props.objective.id,
            categoryId: props.objective.categoryId,
            order: props.objective.order,
            lineManagerStatus: props.objective.lineManagerStatus,
            lineManagerComment: props.objective.lineManagerComment,
            sbuObjectiveId: props.objective.sbuObjectiveId,
            staffAppraisalId: props.objective.staffAppraisalId,
            peopleOpsStatus: props.objective.peopleOpsStatus,
            peopleOpsComment: props.objective.peopleOpsComment,
            keyResults: props.objective.keyResults.map(item => props.keyResults[item]),
        }
    }

    // componentWillUnmount(){
    //     const {order} = this.props.objective;
    //     console.log(`Now unmounting from Objective ${order}`)
    // }

    handleCommentEdit = (e, order) => {
        const {keyResults} = this.state;
        const {value} = e.target;
        let KRs = [...keyResults]
        KRs.find(item => item.order === order).lineManagerComment = value;
        this.setState({
            keyResults: KRs
        })
        this.props.iUpdated();
    }

    // handleUploading = (e, order) => {
    //     const {keyResults} = this.state;
    //     const {value} = e.target;
    //     let KRs = [...keyResults]
    //     KRs.find(item => item.order === order).supportingDocuments = value;
    //     this.setState({
    //         keyResults: KRs
    //     })
    //     this.props.iUpdated();
    // }

    handleRating = (e, order) => {
        const {keyResults} = this.state;
        const {name} = e.target;
        let KRs = [...keyResults]
        KRs.find(item => item.order === order).lineManagerRating = +name;
        // console.log("KRs: ", KRs)
        this.setState({
            keyResults: KRs
        })
        this.props.iUpdated();
    }

    goToBehavioral = () => {
        const {history} = this.props;
        history.push('/BehavioralAssessment')
    }

    handleSubmit = () => {
        const {dispatch} = this.props
        dispatch({ type: SUBMIT_OKRS })
    }


    handleSaveClick = () => {
        const {id, categoryId, sbuObjectiveId, staffAppraisalId, keyResults, description, order, lineManagerStatus, lineManagerComment, peopleOpsStatus, peopleOpsComment} = this.state;
        const { dispatch } = this.props
        
        dispatch({  type: SAVE_KEY_RESULTS, 
                    data: { id, order, lineManagerStatus, lineManagerComment, 
                        peopleOpsStatus, peopleOpsComment, description, categoryId,
                        sbuObjectiveId, staffAppraisalId, keyResults 
                    }, 
                    staffId: this.props.staffId 
        })
        console.log("something", { id, order, lineManagerStatus, lineManagerComment, 
            peopleOpsStatus, peopleOpsComment, description, categoryId,
            sbuObjectiveId, staffAppraisalId, keyResults 
        })
        this.props.iUpdated();
    }

    // handleParsing = (str) => {
    //     const canParse = (str) => {
    //         try {
    //             JSON.parse(str);
    //         } catch (e) {
    //             return false;
    //         }
    //         return true;
    //     }
        
    //     if(canParse(str)) {
    //         return JSON.parse(str)
    //     }

    //     return null
    // }


    // handleSaving = (respObj, index, objId) => {
    //     if (this.state.hasBeenUpdated !== true){
    //         this.setState(prevState => ({
    //             hasBeenUpdated: true,
    //             keyResults: prevState.keyResults.splice(index, 0, respObj)
    //         })) 
    //     }
            
    // }

    // componentWillUpdate(nextProps, nextState){
    //     this.responsePackage.objectiveId = this.props.id
    //     this.responsePackage.keyResults = nextState.keyResults
    // }


    render(){
        const { REQUESTING_SAVE_KEY_RESULTS, SAVING_OBJECTIVE_STATUS, objective, objIndex, lmCategories, categoryId, buttonLoadingStatus, behavioralIndicators, userIDs, userObjectives, userKeyResults } = this.props
        const { keyResults, description } = this.state
        // const CONTINUE_TO_BEHAVIORAL_ASSESSMENT_ACTIVE = userIDs.map(item => userObjectives[item].keyResults).reduce((acc, item) => acc.concat(...item), []).map((item) => userKeyResults[item]).every((item) => item.staffRating > 0 && item.staffComment !== null && item.staffComment !== "")
        // const SUBMIT_ACTIVE = behavioralIndicators.every((item) => item.staffRating > 0 && item.staffComment !== null && item.staffComment !== "")
        // console.log("KEY_RESULTS", )
        // console.log("SUBMIT_ACTIVE", SUBMIT_ACTIVE)
        return(
            <Fragment>
                <Header as="h2" textAlign="center" content="Performance Assessment"/>
                {/* <Header style={{display: "inline"}} as="h4" content={userCategories[categoryId].name === "Business" ? `Business Objective ${objIndex + 1}` : `Personal Objective`} subheader={objective.description}/> */}
                <Segment>
                    <div>
                        {/* <Header as="h5" content={`Business Objective ${objIndex + 1}`} dividing/> */}
                        <Header as="h4" content={lmCategories[categoryId].name === "Business" ? `Business Objective ${objIndex + 1}` : `Personal Objective`} subheader={objective.description}/>
                        { keyResults.map((item,index) => <KeyResult index={index} data={item} order={item.order} id={item.id} objId={this.props.id} key={item.id} handleCommentEdit={this.handleCommentEdit} handleRating={this.handleRating} iUpdated={this.props.iUpdated}/>) }
                    </div>
                    <br/>
                    <Segment basic textAlign="right">
                        { SAVING_OBJECTIVE_STATUS === true && 
                            <Label pointing='right' color="green" >Save Successful</Label>
                        }
                        { SAVING_OBJECTIVE_STATUS === false && 
                            <Label pointing='right' color="red" >Save Failed. Please try again or refresh page.</Label>
                        }
                        <Button content="Save" loading={buttonLoadingStatus} disabled={buttonLoadingStatus} basic onClick={this.handleSaveClick}/>
                        {/* { !SUBMIT_ACTIVE &&
                            <Button content="Continue to Behavioral Objectives" disabled={!CONTINUE_TO_BEHAVIORAL_ASSESSMENT_ACTIVE} onClick={this.goToBehavioral}/>
                        }
                        { SUBMIT_ACTIVE &&
                            <Button content="Submit" disabled={!CONTINUE_TO_BEHAVIORAL_ASSESSMENT_ACTIVE} loading={!SUBMIT_ACTIVE} onClick={this.handleSubmit}/>
                        } */}
                    </Segment>
                </Segment>
            </Fragment>
        )
    }
}



export default Results