import React from 'react'
import { Table } from 'semantic-ui-react'
// import { connect } from "react-redux"
// import { lmStaffAppraisalsSelector } from '../../reducer';

const Header = (props) => {
    const { lmStaffAppraisals, id, totalKRs, treatedKRs, disapprovedKRs, totalOBJs, treatedOBJs, disapprovedOBJs } = props
    const info = () => lmStaffAppraisals.length > 0 && typeof id !== 'undefined' ? lmStaffAppraisals.find(item => item.staffId === id) : {}

    return (
        <Table textAlign="center">
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell colSpan='4'>{`${info().fullName} - ${info().sbu}`}</Table.HeaderCell>
                </Table.Row>
            </Table.Header>
            {/* <Table.Body>
                <Table.Row>
                    <Table.Cell>{`${treatedOBJs}/${totalOBJs} objectives treated`}</Table.Cell>
                    <Table.Cell>{`${disapprovedOBJs} objective(s) disapproved`}</Table.Cell>
                    <Table.Cell>{`${treatedKRs}/${totalKRs} key results treated`}</Table.Cell>
                    <Table.Cell>{`${disapprovedKRs} key result(s) disapproved`}</Table.Cell>
                </Table.Row>
            </Table.Body> */}
        </Table>
    )
}

// function mapStateToProps(state){
//     return {
//         lmStaffAppraisals: lmStaffAppraisalsSelector(state),
//     }
// }

// export default connect(mapStateToProps)(Header)
export default Header