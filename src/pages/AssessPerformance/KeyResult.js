import React from 'react'
import { Segment,Grid,Header,Button,Form,Message,Icon,List } from 'semantic-ui-react'
import "./index.css"

class KeyResult extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            isViewingKey: true,
            isCommenting: false,
            commentValue: this.props.data.lineManagerComment,
            rating: this.props.data.lineManagerRating,  // 1, 2, 3, 4, 5
            file: {},
            icon: this.props.data.lineManagerRating !== 0 && this.props.data.lineManagerComment ? "check" : "cancel",
            link: this.props.data.supportingDocumentsLinks
        }
    }

    componentWillUpdate(nextProps, nextState){
        // this.localGetResponseObject(nextState);
        console.log("keyResult", this.props.data)
    }

    static getDerivedStateFromProps(props, state) {
        if (props.data.supportingDocumentsLinks !== state.link) {
            return { link: props.data.supportingDocumentsLinks }
        }

        return null
    }

    handleKeyButtonClick = () => {
        this.setState({
            isViewingKey: true,
            isCommenting: false,
        })
    }

    handleCommentButtonClick = () => {
        this.setState({
            isCommenting: true,
            isViewingKey: false,
        })
    }

    handleCommentChange = (e) => {
        const {value} = e.target
        const {rating} = this.state;
        this.setState({
            commentValue: value,
            icon: rating > 0 && value ? "check" : "cancel"
        })
    }

    handleRatingClick = (e) => {
        const {name} = e.target
        const {commentValue} = this.state;
        this.setState({
            rating: +name,
            icon: commentValue ? "check" : "cancel",
        })
        
    }

    handleFileUpload = (e) => {
        const {files} = e.target
        this.setState({
            file: files
        })
        console.log(this.state.file)
    }

    handleLinkClick = (e) => {
        const link = e.target.innerHTML
        window.open(link, '_blank')
    }

    render(){
        const { rating, commentValue, link } = this.state;
        const {index, data, order, handleCommentEdit, handleRating} = this.props;

        return(
            <Segment>
                <Grid>
                    { this.state.isViewingKey && 
                        <Grid.Column width={14}>
                            <div>
                                <h5 style={{marginRight:'10px', display:'inline-block'}}> {`Key Result ${index + 1}`} </h5>
                                <Icon className="keyResultIcon" size="small" name={this.state.icon} color={this.state.icon === "cancel" ? "red" : "green"} circular inverted padded/>
                            </div>

                            {data.description}

                            {/* <Segment basic textAlign="right"> */}
                            <Header as="h5" content={`Staff Rating: ${data.staffRating}`}/>
                            
                            {/* </Segment> */}
                            { link && 
                                <div>
                                    <Header as="h5" content={`Supporting Documents`}/>
                                    <List ordered>
                                        {/* {links.map((link, index) => ( */}
                                            <List.Item key={index}>
                                                <a onClick={this.handleLinkClick}>{link}</a>
                                                {/* <Button circular size='mini' icon="cancel"  onClick={this.fileAdder}/> */}
                                                {/* <Icon name="cancel" size='small' color='red' onClick={(e) => this.fileRemover(link, index)} style={{ display: "inline", marginLeft: '10px' }}/> */}
                                            </List.Item>
                                        {/* ))} */}
                                    </List>
                                </div>
                            }

                            {/* <Button icon="file" content="View Attachment" floated="left" compact/> */}

                            { data.superLineManagerStatus === 'disapproved' &&
                                <Message>
                                    <Message.Header content='Exec Line Manager Comment'/>
                                    {data.superLineManagerComment}
                                </Message>
                            }

                            <Segment basic textAlign="right">
                                <Button content="1" compact color={rating===1 ? "red" : "green"} onClick={(e) => {this.handleRatingClick(e); handleRating(e, order)}} name={1}/>
                                <Button content="2" compact color={rating===2 ? "red" : "green"} onClick={(e) => {this.handleRatingClick(e); handleRating(e, order)}} name={2}/>
                                <Button content="3" compact color={rating===3 ? "red" : "green"} onClick={(e) => {this.handleRatingClick(e); handleRating(e, order)}} name={3}/>
                                <Button content="4" compact color={rating===4 ? "red" : "green"} onClick={(e) => {this.handleRatingClick(e); handleRating(e, order)}} name={4}/>
                                <Button content="5" compact color={rating===5 ? "red" : "green"} onClick={(e) => {this.handleRatingClick(e); handleRating(e, order)}} name={5}/>                              
                            </Segment>
                        </Grid.Column>
                    }
                    { this.state.isCommenting && 
                        <Grid.Column width={14}>
                            <div>
                                <h5 style={{marginRight:'10px', display:'inline-block'}}> {`Key Result ${index + 1}`} </h5>
                                <Icon className="keyResultIcon" size="small" name={this.state.icon} color={this.state.icon === "cancel" ? "red" : "green"} circular inverted padded/>
                            </div>                            
                            <Header as="h5" content={`Staff Comment: ${data.staffComment}`}/>
                            <Form>
                                <Form.TextArea 
                                    name="textfield" 
                                    onChange={(e) => {this.handleCommentChange(e); handleCommentEdit(e, order)}} 
                                    value={commentValue}
                                    placeholder="Enter your comment here"/>
                                {/* <Form.Button color="blue" floated="right">Submit Comment</Form.Button> */}
                            </Form>
                        </Grid.Column>
                    }
                    <Grid.Column width={2} textAlign="right">
                        <Button.Group vertical>
                            <Button active={this.state.isViewingKey===true ? true : false} icon="key" attached="right" onClick={this.handleKeyButtonClick}/>
                            <Button active={this.state.isCommenting===true ? true : false} icon="comments" attached="right" onClick={this.handleCommentButtonClick}/>
                        </Button.Group>
                    </Grid.Column>
                </Grid>
            </Segment>
        )
    }
}


export default KeyResult