import React from 'react'
import { Table } from 'semantic-ui-react'
// import { connect } from "react-redux"
// import { poStaffAppraisalsSelector } from '../../reducer';

const Header = (props) => {
    const { poStaffAppraisals, id, totalKRs, treatedKRs, disapprovedKRs, totalOBJs, treatedOBJs, disapprovedOBJs } = props
    const info = () => poStaffAppraisals.length > 0 && typeof id !== 'undefined' ? poStaffAppraisals.find(item => item.staffId === id) : {}

    return (
        <Table textAlign="center">
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell colSpan='4'>{`${info().fullName} - ${info().sbu}`}</Table.HeaderCell>
                </Table.Row>
            </Table.Header>
        </Table>
    )
}

export default Header