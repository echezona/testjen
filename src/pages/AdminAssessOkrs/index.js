import React from 'react';
import { Loader, Grid, List, Segment, Header, Button, Divider, Label } from 'semantic-ui-react';
import {connect} from 'react-redux';
import PageHeader from './PageHeader';
import { FETCH_PEOPLE_OPS_STAFF_APPRAISALS, FETCH_LMPO_OKRS, FETCH_BEHAVIORAL_INDICATORS, FETCH_STAFF_APPRAISAL_SCORE, CALCULATE_STAFF_APPRAISAL_SCORE, FETCH_RECOMMENDATION } from '../../constants';
import queryString from 'query-string'
import { lmIDsSelector, lmObjectivesSelector, lmKeyResultsSelector, lmCategoriesSelector } from '../../reducer';
import SummaryTable from '../../components/SummaryTable';
import PageInfo from '../../components/PageInfo';


class AdminAssessOkrs extends React.Component {

    // queryValues = queryString.parse(this.props.location.search)

    componentDidMount() {
        const { dispatch, location } = this.props;
        const queryValues = queryString.parse(location.search)
        // console.log("what is this", queryValues)

        dispatch({ type: FETCH_LMPO_OKRS, staffId: `?staffId=${queryValues.staffId}` })
        // dispatch({ type: FETCH_CATEGORY })
        // dispatch({ type: FETCH_SBU_OBJECTIVES})
        dispatch({ type: FETCH_PEOPLE_OPS_STAFF_APPRAISALS })
        // dispatch({ type: FETCH_RECOMMENDATION_TYPES })
        dispatch({ type: FETCH_BEHAVIORAL_INDICATORS, LM_DR_STAFF_ID: queryValues.staffId})  //LM_DR_STAFF_ID = Line Manager's Direct Report Staff ID
        dispatch({ type: FETCH_RECOMMENDATION, staffId: queryValues.staffId}) 
        // dispatch({ type: FETCH_STAFF_APPRAISAL_SCORE, staffId: queryValues.staffId })
    }

    handleCalculate = () => {
        const {dispatch, location} = this.props;
        const queryValues = queryString.parse(location.search)

        dispatch({ type: CALCULATE_STAFF_APPRAISAL_SCORE, staffId: queryValues.staffId })
    }

    render() {
        const { poStaffAppraisals, location, isFetchingStaffAppraisals, 
            isFetchingBehavioralAssessment, isFetchingLMPOOKRs, behavioralIndicators,
            lmIDs, lmObjectives, lmKeyResults, appraisalScore, isFetchingAppraisalScore,
            calculateAppraisalScoreLoadingStatus, recommendationObject
         } = this.props

        const { approvalStatus, recommendationType, comment } = recommendationObject
        const queryValues = queryString.parse(location.search)
        const id = queryValues.staffId

        const info = () => poStaffAppraisals.length > 0 && typeof id !== 'undefined' ? poStaffAppraisals.find(item => item.staffId === id) : {}

        const staffKeyResults = lmIDs.map(item => lmObjectives[item].keyResults)
                                    .reduce((acc, item) => acc.concat(item), [])
                                    .map(item => lmKeyResults[item])

        const PERFORMANCE_CUMULATIVE = lmIDs.map(item => lmObjectives[item].keyResults)
                                .reduce((acc, curr) => acc.concat(curr), [])
                                .map(item => lmKeyResults[item])


        const PERFORMANCE_TOTAL = PERFORMANCE_CUMULATIVE.reduce((acc, curr) => acc + 1 , 0)
        const BEHAVIORAL_TOTAL = behavioralIndicators.reduce((acc, curr) => acc + 1 , 0)


        const STAFF_PERFORMANCE_CUMULATIVE = PERFORMANCE_CUMULATIVE.reduce((acc, curr) => acc + curr.staffRating , 0)

        const STAFF_PERFORMANCE_CUMULATIVE_PERCENTAGE = ((STAFF_PERFORMANCE_CUMULATIVE / (PERFORMANCE_TOTAL * 5)) * 100)

        const STAFF_BEHAVIORAL_CUMULATIVE = behavioralIndicators.reduce((acc, curr) => acc + curr.staffRating , 0)

        const STAFF_BEHAVIORAL_CUMULATIVE_PERCENTAGE = ((STAFF_BEHAVIORAL_CUMULATIVE / (BEHAVIORAL_TOTAL * 5)) * 100)

        const STAFF_TOTAL_CUMULATIVE_PERCENTAGE = (((STAFF_PERFORMANCE_CUMULATIVE_PERCENTAGE / 10) * 7) + ((STAFF_BEHAVIORAL_CUMULATIVE_PERCENTAGE / 10) * 3)).toFixed(1) + '%'

        const LINE_MANAGER_PERFORMANCE_CUMULATIVE = PERFORMANCE_CUMULATIVE.reduce((acc, curr) => acc + curr.lineManagerRating , 0)

        const LINE_MANAGER_PERFORMANCE_CUMULATIVE_PERCENTAGE = ((LINE_MANAGER_PERFORMANCE_CUMULATIVE / (PERFORMANCE_TOTAL * 5)) * 100)

        const LINE_MANAGER_BEHAVIORAL_CUMULATIVE = behavioralIndicators.reduce((acc, curr) => acc + curr.lineManagerRating , 0)

        const LINE_MANAGER_BEHAVIORAL_CUMULATIVE_PERCENTAGE = ((LINE_MANAGER_BEHAVIORAL_CUMULATIVE / (BEHAVIORAL_TOTAL * 5)) * 100)

        const LINE_MANAGER_TOTAL_CUMULATIVE_PERCENTAGE = (((LINE_MANAGER_PERFORMANCE_CUMULATIVE_PERCENTAGE / 10) * 7) + ((LINE_MANAGER_BEHAVIORAL_CUMULATIVE_PERCENTAGE / 10) * 3)).toFixed(1) + '%'

        // const SUPER_LINE_MANAGER_PERFORMANCE_CUMULATIVE = PERFORMANCE_CUMULATIVE.reduce((acc, curr) => acc + curr.superLineManagerRating , 0)

        // const SUPER_LINE_MANAGER_PERFORMANCE_CUMULATIVE_PERCENTAGE = ((SUPER_LINE_MANAGER_PERFORMANCE_CUMULATIVE / (PERFORMANCE_TOTAL * 5)) * 100).toFixed(1) + '%'

        // const SUPER_LINE_MANAGER_BEHAVIORAL_CUMULATIVE = behavioralIndicators.reduce((acc, curr) => acc + curr.superLineManagerRating , 0)

        // const SUPER_LINE_MANAGER_BEHAVIORAL_CUMULATIVE_PERCENTAGE = ((SUPER_LINE_MANAGER_BEHAVIORAL_CUMULATIVE / (BEHAVIORAL_TOTAL * 5)) * 100).toFixed(1) + '%'

        if(isFetchingStaffAppraisals || isFetchingBehavioralAssessment || isFetchingLMPOOKRs) {
            return <Loader active/>
        }
        
        return (
            <React.Fragment>
                <PageHeader id={queryValues.staffId}
                    poStaffAppraisals={poStaffAppraisals}
                />


                <Grid columns={16}>
                    <Grid.Column width={3}></Grid.Column>
                    <Grid.Column width={10}>
                        <Segment basic>
                            {/* <Header as="h2" content="Summary" /> */}

                            <Segment>
                                <Header as="h3" content="Staff Bio" />
                                <List >
                                    <List.Item as='li'>{`ID: `}<span style={{ fontWeight: 'bold' }}>{info().staffId}</span></List.Item>
                                    <List.Item as='li'>{`NAME: `}<span style={{ fontWeight: 'bold' }}>{info().fullName}</span></List.Item>
                                    <List.Item as='li'>{`EMAIL: `}<span style={{ fontWeight: 'bold' }}>{info().emailAddress}</span></List.Item>
                                    <List.Item as='li'>{`SBU: `}<span style={{ fontWeight: 'bold' }}>{info().sbu}</span></List.Item>
                                    <List.Item as='li'>{`Department: `}<span style={{ fontWeight: 'bold' }}>{info().department}</span></List.Item>
                                </List>

                                <Header as="h3" content="Staff Cumulative Scores" />
                                <List >
                                    <List.Item as='li'>{`Performance: `}<span style={{ fontWeight: 'bold' }}>{STAFF_PERFORMANCE_CUMULATIVE_PERCENTAGE.toFixed(1) + '%'}</span></List.Item>
                                    <List.Item as='li'>{`Behavioral: `}<span style={{ fontWeight: 'bold' }}>{STAFF_BEHAVIORAL_CUMULATIVE_PERCENTAGE.toFixed(1) + '%'}</span></List.Item>
                                    <List.Item as='li'>{`Total: `}<span style={{ fontWeight: 'bold' }}>{STAFF_TOTAL_CUMULATIVE_PERCENTAGE}</span></List.Item>
                                </List>

                                <Header as="h3" content="Line Manager Cumulative Scores" />
                                <List >
                                    <List.Item as='li'>{`Performance: `}<span style={{ fontWeight: 'bold' }}>{LINE_MANAGER_PERFORMANCE_CUMULATIVE_PERCENTAGE.toFixed(1) + '%'}</span></List.Item>
                                    <List.Item as='li'>{`Behavioral: `}<span style={{ fontWeight: 'bold' }}>{LINE_MANAGER_BEHAVIORAL_CUMULATIVE_PERCENTAGE.toFixed(1) + '%'}</span></List.Item>
                                    <List.Item as='li'>{`Total: `}<span style={{ fontWeight: 'bold' }}>{LINE_MANAGER_TOTAL_CUMULATIVE_PERCENTAGE}</span></List.Item>
                                </List>

                                <Header as="h3" content="Recommendation" />
                                <List >
                                    {/* <List.Item as='li'>{`Performance: ${SUPER_LINE_MANAGER_PERFORMANCE_CUMULATIVE_PERCENTAGE}`}</List.Item>
                                    <List.Item as='li'>{`Behavioral: ${SUPER_LINE_MANAGER_BEHAVIORAL_CUMULATIVE_PERCENTAGE}`}</List.Item> */}
                                    <List.Item as='li'>{`Line Manager Recommendation: `}<span style={{ fontWeight: 'bold' }}>{recommendationType}</span></List.Item>
                                    <List.Item as='li'>{`Line Manager Comment: `}<span style={{ fontWeight: 'bold' }}>{comment}</span></List.Item>
                                    <List.Item as='li'>{`Exec Line Manager Approval status: `}<Label color={approvalStatus == 'approved' ? 'green' : 'red'}>{approvalStatus}</Label></List.Item>
                                </List>
                                <br/>
                                <Divider />
                                <div style={{display: 'flex', justifyContent: 'space-between'}}>
                                    <Header as="h3" content="Final Scores" />
                                    {/* <Button onClick={this.handleCalculate} color='blue' content="Calculate" 
                                        loading={calculateAppraisalScoreLoadingStatus}
                                        disabled={calculateAppraisalScoreLoadingStatus}
                                    /> */}
                                </div>
                                
                                <List >
                                    <List.Item as='li'>{`Performance: `}<span style={{ fontWeight: 'bold' }}>{LINE_MANAGER_PERFORMANCE_CUMULATIVE_PERCENTAGE.toFixed(1) + '%'}</span></List.Item>
                                    <List.Item as='li'>{`Behavioral: `}<span style={{ fontWeight: 'bold' }}>{LINE_MANAGER_BEHAVIORAL_CUMULATIVE_PERCENTAGE.toFixed(1) + '%'}</span></List.Item>
                                    <List.Item as='li' style={{ fontSize: '1.2em', marginTop: '10px' }}>{`Total: `}<span style={{ fontWeight: 'bold' }}>{LINE_MANAGER_TOTAL_CUMULATIVE_PERCENTAGE}</span></List.Item>
                                </List>
                                {/* <List >
                                    <List.Item as='li'>{`Performance: `}<span style={{ fontWeight: 'bold' }}>{`${appraisalScore.okrPercentScore ? appraisalScore.okrPercentScore : 0}%`}</span></List.Item>
                                    <List.Item as='li'>{`Behavioral: `}<span style={{ fontWeight: 'bold' }}>{`${appraisalScore.behavioralPercentScore ? appraisalScore.behavioralPercentScore : 0}%`}</span></List.Item>
                                    <List.Item as='li' style={{ fontSize: '1.2em', marginTop: '10px' }}>{`Total: `}<span style={{ fontWeight: 'bold' }}>{`${appraisalScore.totalPercentScore ? appraisalScore.totalPercentScore : 0}%`}</span></List.Item>
                                </List> */}
                                <br />
                                {/* <Segment basic textAlign="right">
                                    {SAVING_SBU_OBJECTIVES_STATUS === true &&
                                        <Label pointing='right' color="green" >Successfully added SBU objective</Label>
                                    }
                                    {SAVING_SBU_OBJECTIVES_STATUS === false &&
                                        <Label pointing='right' color="red" >Failed to add SBU objective</Label>
                                    }
                                    <Button content="Submit" color={'blue'} onClick={() => this.handleSubmit()} loading={createSbuObjectiveLoadingStatus} disabled={createSbuObjectiveLoadingStatus || !currSbu || !comment}/>
                                </Segment> */}
                            </Segment>
                        </Segment>
                    </Grid.Column>
                </Grid>     
                <Segment basic>
                    <PageInfo 
                        title='Click to View/Hide Performance Score Summary' 
                        content={
                            <SummaryTable objectives={lmIDs.map(item => lmObjectives[item])} keyResults={staffKeyResults} variant={'performance'}/>
                        } 
                    />
                </Segment>
                <Segment basic>
                    <PageInfo 
                        title='Click to View/Hide Behaviour Score Summary' 
                        content={
                            <SummaryTable indicators={behavioralIndicators} variant={'behavior'}/>
                        }
                    />
                </Segment>
            </React.Fragment>
        )
    }
}


function mapStateToProps(state) {
    return {
        poStaffAppraisals: state.requestingPeopleOpsStaffAppraisals,
        isFetchingStaffAppraisals: state.isFetchingStaffAppraisals,
        isFetchingLMPOOKRs: state.isFetchingLMPOOKRs,
        isFetchingBehavioralAssessment: state.fetchLoadingReducer.FETCH_BEHAVIORAL_ASSESSMENT_LOADING_STATUS,
        isFetchingAppraisalScore: state.fetchLoadingReducer.FETCH_STAFF_APPRAISAL_SCORE_LOADING_STATUS,
        calculateAppraisalScoreLoadingStatus: state.saveLoadingReducer.CALCULATE_STAFF_APPRAISAL_SCORE_LOADING_STATUS,
        lmIDs: lmIDsSelector(state),
        lmObjectives: lmObjectivesSelector(state),
        lmKeyResults: lmKeyResultsSelector(state),
        lmCategories: lmCategoriesSelector(state),
        behavioralIndicators: state.requestingBehavioralIndicators,
        // appraisalScore: state.requestingStaffAppraisalScore,
        recommendationObject: state.requestingRecommendation,
    }
}


export default connect(mapStateToProps)(AdminAssessOkrs)