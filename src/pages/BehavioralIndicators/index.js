import React from 'react'
import { Table, Loader, Grid, Segment, Form, Tab, List, Header } from 'semantic-ui-react'
import Exceptions from './exceptions'
import { connect } from 'react-redux'
import { FETCH_BEHAVIORAL_INDICATORS } from '../../constants';
import './index.css'


class BehavioralIndicators extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            descriptionIndex: 0
        }
    }

    panes = [
        {
            menuItem: 'Learner(2)',
            render: () => {
                return (
                    <p><pre>{this.props.behavioralIndicators[this.state.descriptionIndex].behaviour.learnerDescription}</pre></p>
                )
            }
        },
        {
            menuItem: 'Emerging Performer(3)',
            render: () => {
                return (
                    <p><pre>{this.props.behavioralIndicators[this.state.descriptionIndex].behaviour.emergingPerformerDescription}</pre></p>
                )
            }
        },
        {
            menuItem: 'Performer(4)',
            render: () => {
                return (
                    <p><pre>{this.props.behavioralIndicators[this.state.descriptionIndex].behaviour.performerDescription}</pre></p>
                )
            }
        },
        {
            menuItem: 'Influencer(5)',
            render: () => {
                return (
                    <p><pre>{this.props.behavioralIndicators[this.state.descriptionIndex].behaviour.influencerDescription}</pre></p>
                )
            }
        },
    ]

    handleItemSelection = (value) => {
        this.setState({
            descriptionIndex: value,
        })
    }

    componentDidMount() {
        const { dispatch } = this.props
        dispatch({ type: FETCH_BEHAVIORAL_INDICATORS })

        document.title = "PMS - Behavioral Indicators" // set the document title in the browser
    }

    render() {
        console.log("@show me something", this.props.behavioralIndicators)
        console.log("@show me else", this.props.behavioralIndicators)
        const { behavioralIndicators } = this.props
        const { descriptionIndex } = this.state

        if(behavioralIndicators.length === 0){
            return(
                <Loader active/>
            )
        }

        if(Object.prototype.toString.call(behavioralIndicators) !== '[object Array]'){
            return(
                <Exceptions BI={behavioralIndicators}/>
            )
        }

        return (
            <div>
                <Grid>
                    <Grid.Column width={3}></Grid.Column>
                    <Grid.Column width={10}>
                        <Segment centered >
                            <Header as="h3" content="View Behavioral Indicators" />
                            <List as='ul'>
                                <List.Item as='li'>Select a behavioral indicator from the dropdown</List.Item>
                                <List.Item as='li'>Browse through the tabs to pick a different level for the selected indicator</List.Item>
                            </List>
                            <Form>
                                <select value={descriptionIndex} onChange={(e) => this.handleItemSelection(e.target.value)}> 
                                    {
                                        behavioralIndicators.map((item, index) => <option className='option' value={index} key={item.behaviour.description}>{item.behaviour.description.substr(0, item.behaviour.description.indexOf(':'))}</option>)
                                    }
                                </select>
                            </Form>
                            <br/>
                            <p>{behavioralIndicators[descriptionIndex].behaviour.description}</p>
                            <Tab menu={{ secondary: true, pointing: true }} panes={this.panes} />
                        </Segment>
                    </Grid.Column>
                </Grid>

                {/* <Table definition compact={'very'} celled >
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell />
                            <Table.HeaderCell>LEARNER(2)</Table.HeaderCell>
                            <Table.HeaderCell>EMERGING PERFORMER(3)</Table.HeaderCell>
                            <Table.HeaderCell>PERFORMER(4)</Table.HeaderCell>
                            <Table.HeaderCell>INFLUENCER(5)</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        {
                            behavioralIndicators.map(row => (
                                <Table.Row>
                                    <Table.Cell verticalAlign='top'><pre>{row.behaviour.description}</pre></Table.Cell>
                                    <Table.Cell verticalAlign='top'><pre>{row.behaviour.learnerDescription}</pre></Table.Cell>
                                    <Table.Cell verticalAlign='top'><pre>{row.behaviour.emergingPerformerDescription}</pre></Table.Cell>
                                    <Table.Cell verticalAlign='top'><pre>{row.behaviour.performerDescription}</pre></Table.Cell>
                                    <Table.Cell verticalAlign='top'><pre>{row.behaviour.influencerDescription}</pre></Table.Cell>
                                </Table.Row>
                            ))
                        }
                    </Table.Body>
                </Table> */}
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        behavioralIndicators: state.requestingBehavioralIndicators
    }
}

export default connect(mapStateToProps)(BehavioralIndicators)
