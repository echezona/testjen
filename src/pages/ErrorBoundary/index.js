import React from 'react'
import { Button, Icon } from 'semantic-ui-react'

// export default class ErrorBoundary extends Component {
//     constructor(props) {
//       super(props);
//       this.state = { hasError: false };
//     }
  
//     static getDerivedStateFromError(error) {
//       // Update state so the next render will show the fallback UI.
//       return { hasError: true };
//     }
  
//     componentDidCatch(error, info) {
//       // You can also log the error to an error reporting service
//       logErrorToMyService(error, info);
//     }
  
//     render() {
//       if (this.state.hasError) {
//         // You can render any custom fallback UI
//         return <h1>Something went wrong.</h1>;
//       }
  
//       return this.props.children; 
//     }
//   }


  class ErrorBoundary extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        hasError: false,
        error: null,
        info: null
      };
    }

    componentDidCatch(error, info) {
      this.setState({
        hasError: true,
        error: error,
        info: info
      });
    }

    handleButtonClick = () => {
      this.props.history.replace("/businessobjectives");
      window.location.reload();
    }

    render() {
      // const {history} = this.props
      if (this.state.hasError) {
        return (
          <div style={{ padding: '20px' }}>
            <h1>Oops, something went wrong :(</h1>
            <p>The error: {this.state.error.toString()}</p>
            {/* <p>Where it occurred: {this.state.info.componentStack}</p> */}
            <Button icon labelPosition='left' onClick={this.handleButtonClick}>
              <Icon name='home' />
              GO BACK HOME
            </Button>
          </div>
        );
      }
      return this.props.children;
    }
  }


  export default ErrorBoundary