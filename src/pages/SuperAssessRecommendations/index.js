import React, { Component } from 'react'
import { Segment, Grid, Header, Form, Label, Button } from 'semantic-ui-react'
import { SAVE_RECOMMENDATION, UPDATE_RECOMMENDATION, SUBMIT_RECOMMENDATION, UPDATE_SUPER_RECOMMENDATION } from '../../constants';
import PageInfo from '../../components/PageInfo';

class SuperAssessRecommendations extends Component {
    // state = {
    //     // recommendation: "",
    //     // comment: "",
    //     approvalStatus: this.props.recommendationObject.approvalStatus
    // }

    constructor(props) {
        super(props);
        this.state = {
            approvalStatus: this.props.recommendationObject.approvalStatus
        }
    }
    // handleChange = (e, { value }) => {
    //     this.setState({ recommendation: value })
    // }

    handleClick = (e, {value}) => {
        this.setState({
            approvalStatus: value
        })
    }

    handleComment = (e) => {
        const { value } = e.target
        this.setState({ comment: value })
    }

    handleSave = () => {
        const { dispatch, recommendationObject} = this.props
        const { recommendation, comment, approvalStatus } = this.state
        // const id = this.props.recommendationObject.id
        const data = {
            id: recommendationObject.id, 
            staffId: recommendationObject.staffId,
            appraisalId: recommendationObject.appraisalId,
            staffAppraisalId: recommendationObject.staffAppraisalId,
            recommendationType: recommendationObject.recommendationType,
            comment: recommendationObject.comment,
            approvalStatus,
        }
        // if(id){
        //     data.id = id
        // }
        console.log("see data oo", data)
        // if(data.id){
        //     dispatch({ type: UPDATE_RECOMMENDATION, data })
        // } else {
        //     dispatch({ type: SUBMIT_RECOMMENDATION, data })
        // }
        dispatch({ type: UPDATE_SUPER_RECOMMENDATION, data })
        
    }

    render() {
        console.log("route-state", this.props.routeState)
        console.log("state", this.state)

        const { approvalStatus } = this.state
        const { recommendationTypes, recommendationObject, UPDATE_RECOMMENDATION_STATUS, recommendationSaveLoadingStatus,
            lmIDs, lmObjectives, lmKeyResults, behavioralIndicators } = this.props


        //PageInfo Values start here
        const staffKeyResults = lmIDs.map(item => lmObjectives[item].keyResults)
                                    .reduce((acc, item) => acc.concat(item), [])
                                    .map(item => lmKeyResults[item])

        const staffPerformanceSum = staffKeyResults.reduce((acc, curr) => acc + curr.staffRating, 0)

        const staffPerformanceScore = ((staffPerformanceSum / (staffKeyResults.length * 5)) * 100)

        const staffBehaviorSum = behavioralIndicators.reduce((acc, curr) => acc + curr.staffRating,  0)

        const staffBehaviorScore = ((staffBehaviorSum / (behavioralIndicators.length * 5)) * 100)

        const staffTotalScore = ((staffPerformanceScore / 10) * 7) + ((staffBehaviorScore / 10) * 3)

        const lineManagerPerformanceSum = staffKeyResults.reduce((acc, curr) => acc + curr.lineManagerRating, 0)

        const lineManagerPerformanceScore = ((lineManagerPerformanceSum / (staffKeyResults.length * 5)) * 100)

        const lineManagerBehaviorSum = behavioralIndicators.reduce((acc, curr) => acc + curr.lineManagerRating,  0)

        const lineManagerBehaviorScore = ((lineManagerBehaviorSum / (behavioralIndicators.length * 5)) * 100)

        const lineManagerTotalScore = ((lineManagerPerformanceScore / 10) * 7) + ((lineManagerBehaviorScore / 10) * 3)

        return (
            <Grid columns={16} padded>
                <Grid.Row>
                    <PageInfo
                        title='Click To View/Hide Assessment Scores'  
                        content={
                            <Grid divided columns={2}>
                                <Grid.Column width={8}>
                                    <Header as='h4' content="Staff"/>
                                    <div>Performance: <span style={{fontWeight: "bold"}}>{staffPerformanceScore.toFixed(1) + '%'}</span></div>
                                    <div>Behavior: <span style={{fontWeight: "bold"}}>{staffBehaviorScore.toFixed(1) + '%'}</span></div>
                                    <div style={{marginTop: "10px", fontSize: "1.3em"}}>Total: <span style={{fontWeight: "bold"}}>{staffTotalScore.toFixed(1) + '%'}</span></div>
                                </Grid.Column>
                                <Grid.Column width={8}>
                                    <Header as='h4' content="Line Manager"/>
                                    <div >Performance: <span style={{fontWeight: "bold"}}>{lineManagerPerformanceScore.toFixed(1) + '%'}</span></div>
                                    <div>Behavior: <span style={{fontWeight: "bold"}}>{lineManagerBehaviorScore.toFixed(1) + '%'}</span></div>
                                    <div style={{marginTop: "10px", fontSize: "1.3em"}}>Total: <span style={{fontWeight: "bold"}}>{lineManagerTotalScore.toFixed(1) + '%'}</span></div>
                                </Grid.Column>
                            </Grid>
                        }
                    />
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column width={3}></Grid.Column>
                    <Grid.Column width={10}>
                        <Segment >
                            <Header as="h4" content="Recommendation" subheader="The following was recommended by the line manager"/>
                            {/* <Message color={recommendationObject.recommendationType ? 'green' : 'red'}> */}
                                <Form>
                                    <Form.Field>
                                        Recommendation: <b>{recommendationObject.recommendationType || 'Nothing submitted yet'}</b>
                                    </Form.Field>
                                    <Form.Field>
                                        Comment: <b>{recommendationObject.comment || 'No Comment'}</b>
                                    </Form.Field>
                                </Form> 
                            {/* </Message> */}
                            <br/>
                            <Button.Group fluid>
                                <Button onClick={this.handleClick} value={'approved'} color='green' basic={approvalStatus!=='approved'}>Approve</Button>
                                <Button onClick={this.handleClick} value={'disapproved'} color='red' basic={approvalStatus!=='disapproved'}>Decline</Button>
                            </Button.Group>
                            
                            <br/><br/>
                            <Segment basic textAlign='right'>
                                { UPDATE_RECOMMENDATION_STATUS === true && 
                                    <Label color="green" >Save Successful</Label>
                                }
                                { UPDATE_RECOMMENDATION_STATUS === false && 
                                    <Label color="red" >Save Failed. Please try again or refresh page.</Label>
                                }
                                <Button content="Save" onClick={this.handleSave} loading={recommendationSaveLoadingStatus} disabled={recommendationSaveLoadingStatus} color='blue' />
                            </Segment>
                        </Segment>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        )
    }
}

export default SuperAssessRecommendations