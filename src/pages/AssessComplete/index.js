import React, {Component} from 'react'
import {Grid, Segment, Header, Form, Label, Icon, Button, Popup, Message} from 'semantic-ui-react'
import { FETCH_LMPO_OKRS, FETCH_CATEGORY, FETCH_SBU_OBJECTIVES, FETCH_LINE_MANAGER_STAFF_APPRAISALS, FETCH_RECOMMENDATION_TYPES, FETCH_BEHAVIORAL_INDICATORS, FETCH_RECOMMENDATION, SUBMIT_LINE_MANAGER_ASSESSMENT } from '../../constants';
import PageInfo from '../../components/PageInfo';


class AssessComplete extends Component {
    // componentDidMount() {

    //     const { dispatch, match, location, history, staffId } = this.props;

    //     dispatch({ type: FETCH_LMPO_OKRS, staffId: `?staffId=${staffId}` })
    //     dispatch({ type: FETCH_CATEGORY })
    //     dispatch({ type: FETCH_SBU_OBJECTIVES})
    //     dispatch({ type: FETCH_LINE_MANAGER_STAFF_APPRAISALS })
    //     dispatch({ type: FETCH_RECOMMENDATION_TYPES })
    //     dispatch({ type: FETCH_BEHAVIORAL_INDICATORS, LM_DR_STAFF_ID: staffId})  //LM_DR_STAFF_ID = Line Manager's Direct Report Staff ID
    //     dispatch({ type: FETCH_RECOMMENDATION, staffId: staffId})

    // }

    state = {
        popupOpen: false,
    }

    handleSubmit = () => {
        const {dispatch, history, routeState, staffId, staffAppraisalId} = this.props

        // console.log("props?", routeState.staffAppraisalId, this.props.staffId)
        const data = {
            staffAppraisalId,
            staffId,
            firstApproved: 'approved',
        }

        dispatch({ type: SUBMIT_LINE_MANAGER_ASSESSMENT, data, history })
    }

    handlePopup = () => {
        this.setState(prevState => ({ popupOpen: !prevState.popupOpen }))
    }

    render(){
        const { lmIDs, lmObjectives, lmKeyResults, behavioralIndicators, recommendationObject, completeSubmitLoadingStatus, SUBMIT_LINE_MANAGER_ASSESSMENT_STATUS } = this.props
        
        const {popupOpen} = this.state
        
        const doneWithPerformance = lmIDs.map(item => lmObjectives[item].keyResults)
                                        .reduce((acc, item) => acc.concat(item), [])
                                        .map(item => lmKeyResults[item])
                                        .every(item => item.lineManagerRating > 0 && item.lineManagerComment)

        const doneWithBehaviour = behavioralIndicators.every(item => item.lineManagerRating > 0 && item.lineManagerComment)

        const doneWithRecommendations = Object.prototype.toString.call(recommendationObject) === '[object Object]' && recommendationObject.id


        //PageInfo Values start here
        const staffKeyResults = lmIDs.map(item => lmObjectives[item].keyResults)
                                    .reduce((acc, item) => acc.concat(item), [])
                                    .map(item => lmKeyResults[item])

        const staffPerformanceSum = staffKeyResults.reduce((acc, curr) => acc + curr.staffRating, 0)

        const staffPerformanceScore = ((staffPerformanceSum / (staffKeyResults.length * 5)) * 100)

        const staffBehaviorSum = behavioralIndicators.reduce((acc, curr) => acc + curr.staffRating,  0)

        const staffBehaviorScore = ((staffBehaviorSum / (behavioralIndicators.length * 5)) * 100)

        const staffTotalScore = ((staffPerformanceScore / 10) * 7) + ((staffBehaviorScore / 10) * 3)

        const lineManagerPerformanceSum = staffKeyResults.reduce((acc, curr) => acc + curr.lineManagerRating, 0)

        const lineManagerPerformanceScore = ((lineManagerPerformanceSum / (staffKeyResults.length * 5)) * 100)

        const lineManagerBehaviorSum = behavioralIndicators.reduce((acc, curr) => acc + curr.lineManagerRating,  0)

        const lineManagerBehaviorScore = ((lineManagerBehaviorSum / (behavioralIndicators.length * 5)) * 100)

        const lineManagerTotalScore = ((lineManagerPerformanceScore / 10) * 7) + ((lineManagerBehaviorScore / 10) * 3)

        console.log('recommendObject', recommendationObject)

        return(
            <Grid columns={16} padded>
                <Grid.Row>
                    <PageInfo
                        title='Click To View/Hide Assessment Scores' 
                        content={
                            <Grid divided columns={2}>
                                <Grid.Column width={8}>
                                    <Header as='h4' content="Staff"/>
                                    <div>Performance: <span style={{fontWeight: "bold"}}>{staffPerformanceScore.toFixed(1) + '%'}</span></div>
                                    <div>Behavior: <span style={{fontWeight: "bold"}}>{staffBehaviorScore.toFixed(1) + '%'}</span></div>
                                    <div style={{marginTop: "10px", fontSize: "1.3em"}}>Total: <span style={{fontWeight: "bold"}}>{staffTotalScore.toFixed(1) + '%'}</span></div>
                                </Grid.Column>
                                <Grid.Column width={8}>
                                    <Header as='h4' content="Line Manager"/>
                                    <div >Performance: <span style={{fontWeight: "bold"}}>{lineManagerPerformanceScore.toFixed(1) + '%'}</span></div>
                                    <div>Behavior: <span style={{fontWeight: "bold"}}>{lineManagerBehaviorScore.toFixed(1) + '%'}</span></div>
                                    <div style={{marginTop: "10px", fontSize: "1.3em"}}>Total: <span style={{fontWeight: "bold"}}>{lineManagerTotalScore.toFixed(1) + '%'}</span></div>
                                </Grid.Column>
                            </Grid>
                        }
                    />
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column width={3}></Grid.Column>
                    <Grid.Column width={10}>
                        <Segment >
                            <Header as="h4" content="Assessment Progress" />
                            <Form>
                                <Form.Field>
                                    Performance Assessment: &nbsp;
                                    <Label color={doneWithPerformance ? 'green' : 'red'}><Icon name={doneWithPerformance ? 'check circle' : 'close'}/>  {doneWithPerformance ? 'COMPLETE' : 'INCOMPLETE'}</Label>
                                </Form.Field>
                                <Form.Field>
                                    Behavioral Assessment: &nbsp;
                                    <Label color={doneWithBehaviour ? 'green' : 'red'}><Icon name={doneWithBehaviour ? 'check circle' : 'close'}/>  {doneWithBehaviour ? 'COMPLETE' : 'INCOMPLETE'}</Label>
                                </Form.Field>
                                <Form.Field>
                                    Recommendation: &nbsp;
                                    <Label color={doneWithRecommendations ? 'green' : 'red'}><Icon name={doneWithRecommendations ? 'check circle' : 'close'}/>  {doneWithRecommendations ? 'COMPLETE' : 'INCOMPLETE'}</Label>
                                </Form.Field>
                            </Form>
                            <br/>
                            <Segment basic textAlign='right'>
                                {/* { SUBMIT_LINE_MANAGER_ASSESSMENT_STATUS === true && 
                                    <Label pointing='right' color="green" >Submission Successful</Label>
                                }
                                { SUBMIT_LINE_MANAGER_ASSESSMENT_STATUS === false && 
                                    <Label pointing='right' color="red" >Submission Failed. Please try again or refresh page.</Label>
                                } */}
                                {/* <Button content='Submit' 
                                    onClick={this.handleSubmit} 
                                    disabled={!doneWithPerformance || !doneWithBehaviour || !doneWithRecommendations || completeSubmitLoadingStatus}
                                    color='blue'
                                    loading={completeSubmitLoadingStatus}
                                /> */}

                                <Popup open={popupOpen} trigger={<Button color='blue' onClick={this.handlePopup}>View Scores and Submit</Button>} on='click' hoverable>
                                    {/* warning message */}
                                    <Message warning style={{ marginBottom: '10px' }}>
                                        <Icon name='warning sign' style={{ color: '#ffa500' }}/> <span style={{ fontSize: '0.9em', fontWeight: 'bold' }}>Ensure that you save all changes made before submitting!</span>
                                    </Message>
                                    <Header as='h4'>Assessment Scores</Header>
                                    <p>Performance: {lineManagerPerformanceScore.toFixed(1)}</p>
                                    <p>Behavior: {lineManagerBehaviorScore.toFixed(1)}</p>
                                    <p style={{fontWeight: 'bold', fontSize: '1.2em'}}>Total: {lineManagerTotalScore.toFixed(1)}</p>
                                    <br/>
                                    
                                    <div style={{fontWeight: 'bold', fontSize: '0.85em'}}>Are you sure you want to submit?</div>
                                    <div>
                                        { SUBMIT_LINE_MANAGER_ASSESSMENT_STATUS === true && 
                                            <Label color="green" >Submission Successful</Label>
                                        }
                                        { SUBMIT_LINE_MANAGER_ASSESSMENT_STATUS === false && 
                                            <Label color="red" >Submission Failed. Please try again or refresh page.</Label>
                                        }
                                    </div>
                                    <div style={{marginTop: '5px', display: 'flex'}}>
                                        <Button content='Yes, submit' 
                                            onClick={this.handleSubmit} 
                                            disabled={!doneWithPerformance || !doneWithBehaviour || !doneWithRecommendations || completeSubmitLoadingStatus}
                                            color='blue' basic
                                            loading={completeSubmitLoadingStatus}
                                        />
                                        <Button color='red' basic onClick={this.handlePopup}>No, close this</Button>
                                    </div>
                                </Popup>
                            </Segment>
                        </Segment>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        )
    }
}


export default AssessComplete