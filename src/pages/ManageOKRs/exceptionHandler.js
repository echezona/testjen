import React from 'react';
import {Card, Segment} from 'semantic-ui-react';

const ExceptionHandler = (props) => {

    const messages = {
        NO_STAFF_APPRAISAL: "No Staff Appraisal exist yet",
        STAFF_DOES_NOT_EXIST: "The Staff requesting this Staff Appraisal does not exist",
        SBU_OBJECTIVES_DO_NOT_EXIST: "No SBU Objective",
        //appraisalCycle
        NO_APPRAISAL_EXIST_YET: "No Appraisal exist yet",
    }

    let screenContent;

    if(props.appraisalCycle.message === messages.NO_APPRAISAL_EXIST_YET){
        screenContent = "An appraisal cycle period has not yet been set by People Ops";
    }else if(props.appraisalCycle.okrSettingLock === true){
        screenContent = "The time window for setting OKRs has not been activated by PeopleOps hence you'll be unable to approve your direct reports' OKRs during this period";
    }

    return(
        <Segment basic padded>
            <Card centered>
                <Card.Header as="h2" textAlign='center'>
                    HELLO
                </Card.Header>
                <Card.Content>
                    {/* <span style={props.appraisal.error ? errorContentStyle : null}>{screenContent}</span> */}
                    {screenContent}
                </Card.Content>
            </Card>
        </Segment>
    )
}

export default ExceptionHandler