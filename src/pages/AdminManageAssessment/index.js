//Components
import React, { Fragment } from 'react';
import _ from 'lodash'
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux'
import { Form, Grid, Header, Table, Segment, Button, Card, Input, Loader, Pagination, Dropdown, Icon } from 'semantic-ui-react';

// Css Files
import './index.css';
import { FETCH_LINE_MANAGER_OKRS, FETCH_LINE_MANAGER_STAFF_APPRAISALS, FETCH_PEOPLE_OPS_STAFF_APPRAISALS, FETCH_PEOPLE_OPS_COUNT } from '../../constants';
import { poStaffAppraisalsSelector } from '../../reducer';

class AdminManageOKRs extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            column: null,
            data: [],
            direction: null,
            analyticsCardData: [
                {
                    heading: 'Total Number of Staff Appraisals',
                    leftData: '0',
                    cardNumber: 1,
                },
                {
                    heading: 'Number of submitted OKRs',
                    leftData: '0',
                    cardNumber: 2,
                },
                {
                    heading: 'Number of Outstanding Appraisals',
                    leftData: '0',
                    cardNumber: 3,
                },
                {
                    heading: 'Total Number of Employees',
                    leftData: '0',
                    cardNumber: 4,
                }
            ],
            filterOptions: [
                { key: "0", text: "All", value: "all"},
                { key: "1", text: "Completed", value: "completed"},
                { key: "2", text: "Uncompleted", value: "uncompleted"},
            ],
            filterBy: "all",
            // results: [],
            value: " ",
            isLoading: false,
            searching: false,
            activePage: 1,
            totalPages: 0,
            itemsPerPage: 10,
            indexStart: 0, 
            indexEnd: 9,
        }
    }

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch({ type: FETCH_PEOPLE_OPS_STAFF_APPRAISALS })
        dispatch({ type: FETCH_PEOPLE_OPS_COUNT })
    }

    resetComponent = () => this.setState({ isLoading: false, data: this.props.poStaffAppraisals, searching: false })

    handleSearchChange = (e) => {
        console.log("whew")
        const { value } = e.target
        const { poStaffAppraisals } = this.props

        this.setState({ isLoading: true, value, searching: true })

        console.log("blew", this.state.value)


        console.log("kew", poStaffAppraisals)

        const re = new RegExp(_.escapeRegExp(value), 'i')
        const isMatch = result => {
            return (
                re.test(result.fullName) ||
                re.test(result.sbu) ||
                re.test(result.department) ||
                // re.test(result.emailAddress) ||
                re.test(result.peopleOpsStatus) ||
                re.test(result.submissionDate) ||
                re.test(result.submittedAppraisal)
                // re.test(`Avitech`) || re.test(`UI/UX`) ||
                // re.test(`Analyst`) || re.test(`VGN`)
            )
        }

        this.setState({
            isLoading: false,
            data: _.filter(poStaffAppraisals, isMatch),
        })

    }

    componentWillUpdate(nextProps, nextState) {
        // console.log("dataaaaa", nextState.data)
        // console.log("isLoading", nextState.isLoading)
        console.log("activePage", nextState.activePage)
    }

    componentDidUpdate() {
        const { column, data, analyticsCardData, value, searching, itemsPerPage, activePage } = this.state
        const { poStaffAppraisals, peopleOpsCount } = this.props
        if (column === null && poStaffAppraisals.length > 0 && data.length === 0 && !searching) {
            this.setState({
                data: poStaffAppraisals,
            })
        }

        if(poStaffAppraisals.length > 1 && data.length === poStaffAppraisals.length && this.state.totalPages < 1){
            const totalPages = Math.ceil(poStaffAppraisals.length / itemsPerPage)
            const control = itemsPerPage * activePage

            this.setState({
                totalPages,
                indexStart: control - itemsPerPage,
                indexEnd: control - 1,
            })
        }

        if (peopleOpsCount.data && analyticsCardData.find(item => item.cardNumber === 2).leftData !== peopleOpsCount.data.noStaffSubmitted) {
            analyticsCardData.find(item => item.cardNumber === 1).leftData = peopleOpsCount.data.totalNoStaffAppraisal
            analyticsCardData.find(item => item.cardNumber === 2).leftData = peopleOpsCount.data.noStaffSubmitted
            analyticsCardData.find(item => item.cardNumber === 3).leftData = peopleOpsCount.data.noStaffNotSubmitted
            analyticsCardData.find(item => item.cardNumber === 4).leftData = peopleOpsCount.data.totalNoStaffs
        }
        // console.log("ayo", column, poStaffAppraisals, data)
        if (this.state.value.length < 1 && searching) return this.resetComponent()
    }

    gotoOkr = (id) => {
        const { history } = this.props
        history.push(`/adminassessokrs${id}`)
    }

    handleSort = clickedColumn => () => {
        const { column, data, direction } = this.state

        if (column !== clickedColumn) {
            this.setState({
                column: clickedColumn,
                data: _.sortBy(data, [clickedColumn]),
                direction: 'ascending',
            })
            console.log("clickedColumn,", clickedColumn)
            return
        }

        console.log(column, data, direction)

        this.setState({
            data: data.reverse(),
            direction: direction === 'ascending' ? 'descending' : 'ascending',
        })
    }

    handlePaginationChange = (e, { activePage }) => {
        const { itemsPerPage } = this.state
        const control = itemsPerPage * activePage

        this.setState({ 
            activePage,
            indexStart: control - itemsPerPage,
            indexEnd: control - 1,
        })
    }

    handleFilter = (e, {value}) => {
        console.log("titleProps", value)
        const { data, itemsPerPage } = this.state
        const newTotalPages =  Math.ceil(data.filter(item => {
            switch(value){
                case "all":
                    return item
                case 'completed':
                    return item.lineManagerTwoApproved == "approved"
                case 'uncompleted':
                    return item.lineManagerTwoApproved !== "approved"
                default:
                    return item
            }
        }).length / itemsPerPage)

        this.setState({ 
            filterBy: value,
            totalPages: newTotalPages < 1 ? 1 : newTotalPages,
        })
    }

    buttonFilterFunction = (item, index, filterBy) => {
        switch(filterBy){
            case "all":
                return item
            case 'completed':
                return item.lineManagerTwoApproved == "approved"
            case 'uncompleted':
                return item.lineManagerTwoApproved !== "approved"
            default:
                return item
        }
    }



    render() {
        console.log("count", this.props.peopleOpsCount)
        const { history, lmIDs, lmObjectives, poStaffAppraisals, isFetchingStaffAppraisals, firstApproved } = this.props
        // console.log("lmtemp:",poStaffAppraisals)
        const { column, data, direction, analyticsCardData, results, indexStart, indexEnd, totalPages, activePage, filterOptions, itemsPerPage, filterBy } = this.state
        // const staffAprraisalArr = lmIDs.map(item => lmObjectives[item].staffAppraisal).filter((item, index, self) => self.indexOf(item) === index).map(item => poStaffAppraisals[item].staffId)
        // console.log("datum", data)
        if (isFetchingStaffAppraisals) {
            return <Loader active />
        }

        return (
            <Segment basic>
                <Grid id="okrDataTable" padded>
                    <Grid.Row>
                        <Header as="h3" content="Overview" dividing />
                        <div className="w-100">
                            {analyticsCardData.map((card, index, array) => (
                                <div className={`fl w-25 ${index === 0 ? 'pl4' : 'pl3'} ${index === array.length - 1 ? 'pr4' : 'pr3'}`} key={index}>
                                    <Card className="card-layout fluid tc" style={{ paddingTop: '.5rem', paddingBottom: '.5rem' }}>
                                        <Card.Content>
                                            <Grid columns={1}>
                                                <Grid.Row>
                                                    <Grid.Column>
                                                        <Header as="h5">{card.heading}</Header>
                                                    </Grid.Column>
                                                </Grid.Row>
                                                <Grid.Row>
                                                    <Grid.Column>
                                                        <Header as="h2" className="no-m">
                                                            {card.leftData}
                                                        </Header>
                                                    </Grid.Column>
                                                </Grid.Row>
                                            </Grid>
                                        </Card.Content>
                                    </Card>
                                </div>
                            ))}
                        </div>
                    </Grid.Row>
                    <Grid.Row>

                    </Grid.Row>
                    <Grid.Row style={{ marginTop: "10px" }}>
                        <Grid.Column width={10}>
                            <Header as="h3" content="Manage Assessment" dividing />
                        </Grid.Column>
                        <Grid.Column width={3} textAlign="right">
                            {/* <Header as='h4'>
                                <Icon name='filter' />
                                <Header.Content>
                                    Filter by{' '} */}
                                    <Dropdown
                                        inline
                                        header='FILTER STAFF'
                                        options={filterOptions}
                                        defaultValue={filterOptions[0].value}
                                        onChange={this.handleFilter}
                                        button
                                    />
                                {/* </Header.Content>
                            {/* </Header> */}
                        </Grid.Column>
                        <Grid.Column width={3}>
                            <Input icon='search' placeholder='Search...' onChange={(e) => this.handleSearchChange(e)} />
                        </Grid.Column>
                    </Grid.Row>

                    {/* <Grid.Row>
                        
                    </Grid.Row> */}
                    <Grid.Row>
                        {/* <Table padded collapsing sortable celled striped fixed singleLine headerRow={headerRow} renderBodyRow={renderBodyRow} tableData={tableData} /> */}
                        <Table sortable celled padded striped singleLine size="small">

                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell sorted={column === 'fullName' ? direction : null} onClick={this.handleSort('fullName')}>
                                        Name
                                    </Table.HeaderCell>
                                    <Table.HeaderCell sorted={column === 'sbu' ? direction : null} onClick={this.handleSort('sbu')}>
                                        SBU
                                    </Table.HeaderCell>
                                    <Table.HeaderCell sorted={column === 'department' ? direction : null} onClick={this.handleSort('department')}>
                                        Department
                                    </Table.HeaderCell>
                                    {/* <Table.HeaderCell sorted={column === 'emailAddress' ? direction : null} onClick={this.handleSort('emailAddress')}>
                                        Email
                                    </Table.HeaderCell> */}
                                    <Table.HeaderCell sorted={column === 'status' ? direction : null} onClick={this.handleSort('status')}>
                                        Status
                                    </Table.HeaderCell>
                                    <Table.HeaderCell sorted={column === 'submissionDate' ? direction : null} onClick={this.handleSort('submissionDate')}>
                                        Staff Submission Date
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                        Action
                                    </Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>
                            
                            <Table.Body>
                                {data.filter((item, index) => index >= indexStart && index <= indexEnd)
                                    .filter((item, index) => this.buttonFilterFunction(item, index, filterBy))
                                    .map(({ fullName, sbu, department, emailAddress, peopleOpsStatus, submissionDate, staffId, submittedAppraisal, lineManagerTwoApproved, firstApproved, assessedAppraisal }) => (
                                        <Table.Row key={emailAddress}>
                                            <Table.Cell>{fullName}</Table.Cell>
                                            <Table.Cell>{sbu}</Table.Cell>
                                            <Table.Cell>{department}</Table.Cell>
                                            {/* <Table.Cell>{emailAddress}</Table.Cell> */}
                                            <Table.Cell>{peopleOpsStatus}</Table.Cell>
                                            <Table.Cell>{submissionDate === "01/01/0001 12:00 AM" ? "Yet to Submit" : submissionDate}</Table.Cell>
                                            <Table.Cell>
                                                <Button 
                                                    content="View" 
                                                    disabled={!assessedAppraisal || (peopleOpsStatus !== "pending" && peopleOpsStatus !== "in progress") || lineManagerTwoApproved !== "approved" || firstApproved !== "approved" } 
                                                    compact onClick={() => this.gotoOkr(`?staffId=${staffId}`)} 
                                                />
                                            </Table.Cell>
                                        </Table.Row>
                                    ))}
                                {data.filter((item, index) => index >= indexStart && index <= indexEnd).filter((item, index) => this.buttonFilterFunction(item, index, filterBy)).length < 1 &&
                                    <Table.Row textAlign="center">
                                        <Table.Cell colSpan={6}>
                                        <br/>
                                            <Header as='h2' content="Nothing To Display"/>
                                        <br/>
                                        </Table.Cell>
                                    </Table.Row>
                                }
                            </Table.Body>
                        </Table>
                        { totalPages > 1 &&
                            <Pagination
                                activePage={activePage}
                                onPageChange={this.handlePaginationChange}
                                totalPages={totalPages}
                            />
                        }
                    </Grid.Row>
                </Grid>
            </Segment>
        )
    }
}

function mapStateToProps(state) {
    return {
        poStaffAppraisals: poStaffAppraisalsSelector(state),
        isFetchingStaffAppraisals: state.isFetchingStaffAppraisals,
        peopleOpsCount: state.requestingPeopleOpsCount,
    }
}

export default connect(mapStateToProps)(AdminManageOKRs)