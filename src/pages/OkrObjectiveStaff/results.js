import React, { Fragment } from 'react';
import { Segment, Header, Button, Container, Label, Divider, Form, Popup, Grid } from 'semantic-ui-react';
import KeyResult from './KeyResult'
import { SAVE_KEY_RESULTS, UPDATE_LINE_MANAGER_STATUS, GET_CONTINUE_ENABLED_OBJECTIVES, GET_CONTINUE_ENABLED_KEY_RESULTS, MODIFY_CONTINUE_ENABLED_OBJECTIVES_ON_SAVE, MODIFY_CONTINUE_ENABLED_KEY_RESULTS_ON_SAVE } from '../../constants'
import { connect } from 'react-redux'
import _ from 'lodash'


class Results extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            description: props.objective.description,
            id: props.objective.id,
            categoryId: props.objective.categoryId,
            order: props.objective.order,
            lineManagerStatus: props.objective.lineManagerStatus,
            lineManagerComment: props.objective.lineManagerComment,
            sbuObjectiveId: props.objective.sbuObjectiveId,
            staffAppraisalId: props.objective.staffAppraisalId,
            keyResults: props.objective.keyResults.map(item => props.keyResults[item]),
        }
        // console.log("this is it: ", this.state.id, this.state.keyResults, "Objective",this.props.objective)
    }

    // componentDidUpdate() {
    //     const { dispatch, continueEnabledObjs, continueEnabledKrs } = this.props
        
    //     if (!continueEnabledObjs.find(item => item.id === this.props.objective.id)) {
    //         const objective = {
    //             description: this.props.objective.description,
    //             id: this.props.objective.id,
    //             categoryId: this.props.objective.categoryId,
    //             order: this.props.objective.order,
    //             lineManagerStatus: this.props.objective.lineManagerStatus,
    //             lineManagerComment: this.props.objective.lineManagerComment,
    //             sbuObjectiveId: this.props.objective.sbuObjectiveId,
    //             staffAppraisalId: this.props.objective.staffAppraisalId,
    //         }

    //         const keyResults = this.props.objective.keyResults.map(item => this.props.keyResults[item])

    //         dispatch({
    //             type: GET_CONTINUE_ENABLED_OBJECTIVES,
    //             payload: {...objective}
    //         })

    //         dispatch({
    //             type: GET_CONTINUE_ENABLED_KEY_RESULTS,
    //             payload: _.cloneDeep(keyResults)
    //         })
    //     }
    // }

    componentDidMount() {
        this.props.showSubmit(true)
    }

    componentWillUnmount() {
        const { order } = this.props.objective;
        console.log(`Now unmounting from Objective ${order}`)
    }

    handleObjCommentChange = (e) => {
        const { value } = e.target
        // console.log("something", value)
        this.setState({
            lineManagerComment: value,
        })
        this.props.objective.lineManagerComment = value
        this.props.iUpdated()
    }

    handleCommentEdit = (e, order) => {
        const { keyResults } = this.state;
        const { value } = e.target;
        let KRs = [...keyResults]
        KRs.find(item => item.order === order).lineManagerComment = value;
        // console.log("KRs: ", KRs)
        this.setState({
            keyResults: KRs
        })
        this.props.iUpdated()
    }

    handleObjApproval = (e, titleProps) => {
        const { icon } = titleProps
        console.log("bla bla bla", icon)
        this.setState({
            lineManagerStatus: icon === "check" ? "approved" : "disapproved"
        })
        this.props.objective.lineManagerStatus = icon === "check" ? "approved" : "disapproved" // line to make it more like a SPA
        this.props.iUpdated();
    }

    handleApproval = (e, order) => {
        const { keyResults } = this.state;
        const { name } = e.target;
        let KRs = [...keyResults]
        KRs.find(item => item.order === order).lineManagerStatus = name;
        // console.log("KRs: ", KRs)
        this.setState({
            keyResults: KRs
        })
        this.props.iUpdated();
    }


    handleSaveClick = () => {
        const { id, categoryId, sbuObjectiveId, staffAppraisalId, keyResults, description, order, lineManagerStatus, lineManagerComment } = this.state;
        const { match, dispatch, isFirstSave } = this.props
        //Change the status on the manage OKRs page to "in progress" when the line manager makes his first save.
        if (isFirstSave) {
            dispatch({ type: UPDATE_LINE_MANAGER_STATUS, staffAppraisalId, lineManagerStatus: "inprogress" })
        }
        //save the objective and it's associated key results to the database
        const { peopleOpsStatus, peopleOpsComment } = this.props.objective
        dispatch({ type: SAVE_KEY_RESULTS, data: { id, order, lineManagerStatus, lineManagerComment, peopleOpsStatus: peopleOpsStatus, peopleOpsComment: peopleOpsComment, description, categoryId, sbuObjectiveId, staffAppraisalId, keyResults }, staffId: this.props.staffId })
        this.props.iUpdated();

        //continue enabled logic starts here
        const objective = {id, order, lineManagerStatus, lineManagerComment, peopleOpsStatus: this.props.objective.peopleOpsStatus, peopleOpsComment: this.props.objective.peopleOpsComment, description, categoryId, sbuObjectiveId, staffAppraisalId}

        dispatch({ type: MODIFY_CONTINUE_ENABLED_OBJECTIVES_ON_SAVE, data: {...objective} })
        dispatch({ type: MODIFY_CONTINUE_ENABLED_KEY_RESULTS_ON_SAVE, data: _.cloneDeep(keyResults) })
    }


    render() {
        const { REQUESTING_SAVE_KEY_RESULTS, SAVING_OBJECTIVE_STATUS, objective, objIndex, userCategories, categoryId, isFirstSave, match, secondApproved } = this.props
        const { keyResults, description, lineManagerStatus, lineManagerComment } = this.state
        // console.log("cat", isFirstSave)
        // console.log("somwthing", status)
        // console.log("ye", lineManagerComment)
        return (
            <Fragment>
                <Header as="h2" textAlign="center" content="Staff OKR" />
                <Segment >
                    <div>
                        <Grid>
                            <Grid.Column width={12}>
                                <Header style={{ display: "inline" }} as="h4" content={userCategories[categoryId].name === "Business" ? `Business Objective ${objIndex + 1}` : `Personal Objective`} subheader={objective.description} />
                                {secondApproved === "disapproved" && this.props.objective.peopleOpsComment && this.props.objective.peopleOpsComment !== "" &&
                                    <div>
                                        <span style={{ color: "red" }}>People Ops Comment:</span>
                                        <span>{this.props.objective.peopleOpsComment}</span>
                                    </div>
                                }
                            </Grid.Column>
                            <Grid.Column width={4} textAlign="right">
                                <Segment basic>
                                    {/* <Popup trigger={<Button disabled circular icon='check' color={lineManagerStatus === "approved" ? "green" : null} onClick={(e, titleProps) => this.handleObjApproval(e, titleProps)} />} content="approve" />
                                    <Popup trigger={<Button disabled circular icon='cancel' color={lineManagerStatus === "disapproved" ? "red" : null} onClick={(e, titleProps) => this.handleObjApproval(e, titleProps)} />} content="disapprove" /> */}
                                </Segment>
                            </Grid.Column>
                        </Grid>
                        {lineManagerStatus === "disapproved" &&
                            <Form style={{ marginTop: "20px" }}>
                                <Form.TextArea
                                    name="textfield"
                                    onChange={(e) => this.handleObjCommentChange(e)}
                                    value={lineManagerComment}
                                    placeholder="Please give reasons for disagreeing with this objective OR suggest a suitable objective" />
                            </Form>
                        }
                        <Divider style={{ marginTop: "20px" }} horizontal> Key Results </Divider>
                        {keyResults.map((item, index) => <KeyResult key={item.id} secondApproved={secondApproved} order={item.order} data={item} handleApproval={(e, order) => this.handleApproval(e, order)} handleCommentEdit={(e, order) => this.handleCommentEdit(e, order)} iUpdated={this.props.iUpdated} />)}
                    </div>
                    <br />
                    <Segment basic textAlign="right">
                        {SAVING_OBJECTIVE_STATUS === true &&
                            <Label pointing='right' color="green" >Save Successful</Label>
                        }
                        {SAVING_OBJECTIVE_STATUS === false &&
                            <Label pointing='right' color="red" >Save Failed. Please try again or refresh page.</Label>
                        }
                        {/* <Button content="Save"
                            loading={REQUESTING_SAVE_KEY_RESULTS ? true : false}
                            basic
                            onClick={this.handleSaveClick}
                            disabled={!(keyResults.filter(item => item.lineManagerStatus === "disapproved").every(item => item.lineManagerComment !== ""))}
                        /> */}
                    </Segment>
                </Segment>
            </Fragment>
        )
    }
}


function mapStateToProps(state) {
    return {
        REQUESTING_SAVE_KEY_RESULTS: state.REQUESTING_SAVE_KEY_RESULTS,
        SAVING_OBJECTIVE_STATUS: state.SAVING_OBJECTIVE_STATUS
    }
}

export default connect(mapStateToProps)(Results)