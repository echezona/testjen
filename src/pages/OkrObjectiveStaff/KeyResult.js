import React from 'react'
import { Segment,Grid,Header,Button,Form } from 'semantic-ui-react'
import "./index.css"


class KeyResult extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            isViewingKey: true,
            isCommenting: false,
            comment: this.props.data.lineManagerComment,
            lineManagerStatus: this.props.data.lineManagerStatus // approved, disapproved, pending
        }
    }


    componentWillUpdate(nextProps, nextState){
        const {comment, lineManagerStatus, isCommenting, isViewingKey} = nextState;
        if(lineManagerStatus === "disapproved" && isCommenting === false && isViewingKey === true){
            this.handleCommentButtonClick();
        }
        if(lineManagerStatus === "approved" && isViewingKey === false && isCommenting === true){
            this.handleKeyButtonClick();
        }
    }


    handleKeyButtonClick = () => {
        this.setState({
            isViewingKey: true,
            isCommenting: false
        })
    }

    handleCommentButtonClick = () => {
        this.setState({
            isViewingKey: false,
            isCommenting: true
        })
    }

    handleCommentChange = (e) => {
        const {value} = e.target
        this.setState({
            comment: value,
        })
    }

    handleApproveClick = (e) => {
        const {name} = e.target
        this.setState({
            lineManagerStatus: name === "approved" ? "approved" : "disapproved"
        })
        this.props.iUpdated()
    }

    render(){
        const { lineManagerStatus } = this.state;
        const { data, handleCommentEdit, handleApproval, order, secondApproved } = this.props;
        return(
            <Segment>
                <Grid>
                    {/* { this.state.isViewingKey &&  */}
                        <Grid.Column width={16}>
                            <Header as="h5" content={`Key Result ${order}`}/>
                            {data.description}
                            { secondApproved === "disapproved" && data.peopleOpsComment && data.peopleOpsComment !== "" &&
                                <div>
                                    <span style={{color:"red"}}>People Ops Comment:</span>
                                    <span>{data.peopleOpsComment}</span>
                                </div>
                            }
                            <br/>
                            {/* <Button content={(lineManagerStatus === "pending" || lineManagerStatus === "approved") ? "Disapprove" : "Disapproved"}
                                    floated="right" 
                                    color="red" 
                                    onClick={(e) => {this.handleApproveClick(e); handleApproval(e, order)}} 
                                    name="disapproved"
                                    disabled={!(lineManagerStatus === "pending" || lineManagerStatus === "approved")}
                            /> */}
                            {/* <Button content={lineManagerStatus === "pending" || lineManagerStatus === "disapproved" ? "Approve" : "Approved"} 
                                    floated="right" 
                                    color="green" 
                                    onClick={(e) => {this.handleApproveClick(e); handleApproval(e, order)}}
                                    name="approved"
                                    disabled={!(lineManagerStatus === "pending" || lineManagerStatus === "disapproved")}
                            />   */}
                        </Grid.Column>
                       
                    { this.state.lineManagerStatus === "disapproved" && 
                        <Grid.Column width={16}>
                            <Form>
                                <Form.TextArea 
                                    name="textfield" 
                                    onChange={(e) => {this.handleCommentChange(e); handleCommentEdit(e, order)}} 
                                    value={this.state.comment}
                                    placeholder="Please give reasons for disagreeing with this key result OR suggest a suitable key result"/>
                                <Form.Button color="blue" floated="right">Submit Comment</Form.Button>
                            </Form>
                        </Grid.Column>
                    }
                    {/* <Grid.Column width={2} textAlign="right" verticalAlign="top">
                        <Button.Group vertical>
                            <Button active={this.state.isViewingKey===true ? true : false} icon="key" attached="right" onClick={this.handleKeyButtonClick}/>
                            <Button active={this.state.isCommenting===true ? true : false} icon="comments" attached="right" onClick={this.handleCommentButtonClick}/>
                        </Button.Group>
                    </Grid.Column> */}
                </Grid>
            </Segment>
        )
    }
}


export default KeyResult