import React, {Component, Fragment} from 'react';
import { Segment,Header,Button, Label } from 'semantic-ui-react';
import Indicator from './Indicator'
import { SAVE_BEHAVIORAL_INDICATORS, SUBMIT_OKRS } from '../../constants';


class Results extends Component {
    constructor(props){
        super(props)
        this.state = {
            clicked: "",
            comment: props.lineManagerComment,
            rating: props.lineManagerRating,
            objIndex: props.index,
        }
    }

    handleRatingClick = (e, titleProps) => {
        const {name} = e.target
        const {content} = titleProps
        this.setState({
            clicked: name,
            rating: parseInt(content),
        })
    }

    onComment = (e) => {
        const {value} = e.target
        this.setState({
            comment: value,
        })
    }


    handleSaveClick = () => {
        const {comment, rating, objIndex} = this.state;
        const { dispatch, behaviourId, staffAppraisalId, peer1Rating, 
            peer2Rating, staffRating, peer1Comment, 
            peer2Comment, staffComment, id, superLineManagerStatus, superLineManagerComment } = this.props
        
        dispatch({  type: SAVE_BEHAVIORAL_INDICATORS, 
                    data: { id, behaviourId, staffAppraisalId, peer1Rating, 
                        peer2Rating, staffRating, peer1Comment, 
                        peer2Comment, staffComment, superLineManagerComment, superLineManagerStatus,
                        lineManagerRating: rating, lineManagerComment: comment
                    },
                    objIndex
        })

        // console.log("saving this guy", { id, behaviourId, staffAppraisalId, peer1Rating, 
        //     peer2Rating, staffRating, peer1Comment, 
        //     peer2Comment, staffComment,
        //     lineManagerRating: rating, lineManagerComment: comment
        // })
    }

    // componentDidUpdate(){
    //     console.log("see the state", this.state)
    // }

    render(){
        const {buttonLoadingStatus, userIDs, userObjectives, userKeyResults, behavioralIndicators, behavioralSaveLoadingStatus, SAVE_BEHAVIOUR_STATUS} = this.props
        // const SUBMIT_ACTIVE = userIDs.map(item => userObjectives[item].keyResults).reduce((acc, item) => acc.concat(...item), []).map((item) => userKeyResults[item]).every((item) => item.staffRating > 0 && item.staffComment !== null && item.staffComment !== "")
        // const CONTINUE_TO_PERFORMANCE_ASSESSMENT_ACTIVE = behavioralIndicators.every((item) => item.staffRating > 0 && item.staffComment !== null && item.staffComment !== "")
        return(
            <Fragment>
                <Header as="h2" textAlign="center" content="Behavioral Assessment"/>
                <Segment>
                    <Header as="h4" content="Behavioral Score Card" dividing/>
                    <Indicator {...this.props} {...this.state} handleRatingClick={this.handleRatingClick} onComment={this.onComment}/>
                    <br/>
                    <Segment basic textAlign="right">
                        { SAVE_BEHAVIOUR_STATUS === true && 
                            <Label pointing='right' color="green" >Save Successful</Label>
                        }
                        { SAVE_BEHAVIOUR_STATUS === false && 
                            <Label pointing='right' color="red" >Save Failed. Please try again or refresh page.</Label>
                        }
                        <Button content="Save" color="blue" onClick={this.handleSaveClick} disabled={behavioralSaveLoadingStatus} loading={behavioralSaveLoadingStatus}/> 
                        {/* { !SUBMIT_ACTIVE &&
                            <Button content="Continue to Behavioral Objectives" disabled={!CONTINUE_TO_PERFORMANCE_ASSESSMENT_ACTIVE} onClick={this.goToBehavioral}/>
                        }
                        { SUBMIT_ACTIVE &&
                            <Button content="Submit" disabled={!CONTINUE_TO_PERFORMANCE_ASSESSMENT_ACTIVE} loading={!SUBMIT_ACTIVE} onClick={this.handleSubmit}/>
                        }  */}
                    </Segment>
                </Segment>
            </Fragment>
        )
    }
}


export default Results