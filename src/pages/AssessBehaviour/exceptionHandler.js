import React from 'react'
import {Card} from 'semantic-ui-react'

const ExceptionHandler = (props) => {
    const {BI} = props
    return(
        <Card>
            { Object.prototype.toString.call(BI) === '[object String]' &&
                <Card.Content>{BI}</Card.Content>
            }
            { Object.prototype.toString.call(BI) === '[object Object]' &&
                <Card.Content>{BI.message}</Card.Content>
            }
        </Card>
    )
}

export default ExceptionHandler