import React, {Component, Fragment} from 'react';
import { Segment,Header,Button,Label,Popup,Icon,Message } from 'semantic-ui-react';
import Indicator from './Indicator'
import { SAVE_BEHAVIORAL_INDICATORS, SUBMIT_OKRS } from '../../constants';


class Results extends Component {
    constructor(props){
        super(props)
        this.state = {
            clicked: "",
            comment: props.comment,
            rating: props.rating,
            objIndex: props.index,
            popupOpen: false,
        }
    }

    handleRatingClick = (e, titleProps) => {
        const {name} = e.target
        const {content} = titleProps
        this.setState({
            clicked: name,
            rating: parseInt(content),
        })
    }

    onComment = (e) => {
        const {value} = e.target
        this.setState({
            comment: value,
        })
    }

    goToPerformance = () => {
        const {history} = this.props;
        history.push('/PerformanceAssessment')
    }

    handleSubmit = () => {
        const {dispatch} = this.props
        dispatch({ type: SUBMIT_OKRS, fromAssessmentFlow: true })
    }

    handleSaveClick = () => {
        const {comment, rating, objIndex} = this.state;
        const { dispatch, behaviourId, staffAppraisalId, peer1Rating, 
            peer2Rating, lineManagerRating, peer1Comment, 
            peer2Comment, lineManagerComment, id } = this.props
        
        dispatch({  type: SAVE_BEHAVIORAL_INDICATORS, 
                    data: { id, behaviourId, staffAppraisalId, peer1Rating, 
                        peer2Rating, lineManagerRating, peer1Comment, 
                        peer2Comment, lineManagerComment,
                        staffRating: rating, staffComment: comment
                    },
                    objIndex
        })

        console.log("saving this guy", { id, behaviourId, staffAppraisalId, peer1Rating, 
                        peer2Rating, lineManagerRating, peer1Comment, 
                        peer2Comment, lineManagerComment,
                        staffRating: rating, staffComment: comment
                    })
    }

    handlePopup = () => {
        this.setState(prevState => ({ popupOpen: !prevState.popupOpen }))
    }

    // componentDidUpdate(){
    //     console.log("see the state", this.state)
    // }

    render(){
        const {buttonLoadingStatus, userIDs, userObjectives, userKeyResults, behavioralIndicators, SAVE_BEHAVIOUR_STATUS, isSubmittingOKRs, SUBMIT_OKRS_STATUS} = this.props
        const { popupOpen } = this.state
        const SUBMIT_ACTIVE = userIDs.map(item => userObjectives[item].keyResults).reduce((acc, item) => acc.concat(...item), []).map((item) => userKeyResults[item]).every((item) => item.staffRating > 0 && item.staffComment)
        const CONTINUE_TO_PERFORMANCE_ASSESSMENT_ACTIVE = behavioralIndicators.every((item) => item.staffRating > 0 && item.staffComment !== null && item.staffComment !== "")


        //Summary values start here
        const staffKeyResults = userIDs.map(item => userObjectives[item].keyResults)
                                    .reduce((acc, item) => acc.concat(item), [])
                                    .map(item => userKeyResults[item])

        const staffPerformanceSum = staffKeyResults.reduce((acc, curr) => acc + curr.staffRating, 0)

        const staffPerformanceScore = ((staffPerformanceSum / (staffKeyResults.length * 5)) * 100)

        const staffBehaviorSum = behavioralIndicators.reduce((acc, curr) => acc + curr.staffRating,  0)

        const staffBehaviorScore = ((staffBehaviorSum / (behavioralIndicators.length * 5)) * 100)

        const staffTotalScore = ((staffPerformanceScore / 10) * 7) + ((staffBehaviorScore / 10) * 3)

        
        console.log("KEY_RESULTS", )
        console.log("SUBMIT_ACTIVE", SUBMIT_ACTIVE)
        return(
            <Fragment>
                <Header as="h2" textAlign="center" content="Behavioral Assessment"/>
                <Segment>
                    <Header as="h4" content="Behavioral Score Card" dividing/>
                    <Indicator {...this.props} {...this.state} handleRatingClick={this.handleRatingClick} onComment={this.onComment}/>
                    <br/>
                    <Segment basic textAlign="right">
                        {/* Messages for Save button click */}
                        { SAVE_BEHAVIOUR_STATUS === true && 
                            <Label pointing='right' color="green" >Save Successful</Label>
                        }
                        { SAVE_BEHAVIOUR_STATUS === false && 
                            <Label pointing='right' color="red" >Save Failed. Please try again or refresh page.</Label>
                        }
                        {/* Messages for Submit button click */}
                        {/* { SUBMIT_OKRS_STATUS === true && 
                            <Label pointing='right' color="green" >Submission Successful</Label>
                        }
                        { SUBMIT_OKRS_STATUS === false && 
                            <Label pointing='right' color="red" >Submission Failed. Please try again or refresh page.</Label>
                        } */}
                        <Button content="Save" basic onClick={this.handleSaveClick} disabled={buttonLoadingStatus} loading={buttonLoadingStatus}/> 
                        { !SUBMIT_ACTIVE &&
                            <Button color='blue' content="Continue to Performance Assessment" disabled={!CONTINUE_TO_PERFORMANCE_ASSESSMENT_ACTIVE} onClick={this.goToPerformance}/>
                        }
                        { SUBMIT_ACTIVE &&
                            <Popup open={popupOpen} trigger={<Button color='blue' onClick={this.handlePopup}>View Scores and Submit</Button>} on='click' hoverable>
                                {/* warning message */}
                                <Message warning style={{ marginBottom: '10px' }}>
                                    <Icon name='warning sign' style={{ color: '#ffa500' }}/> <span style={{ fontSize: '0.9em', fontWeight: 'bold' }}>Ensure that you save all changes made before submitting!</span>
                                </Message>
                                <Header as='h4'>Assessment Summary</Header>
                                <p>Performance: {staffPerformanceScore.toFixed(1)}</p>
                                <p>Behavior: {staffBehaviorScore.toFixed(1)}</p>
                                <p style={{fontWeight: 'bold', fontSize: '1.2em'}}>Total: {staffTotalScore.toFixed(1)}</p>
                                <br/>
                                <div style={{fontWeight: 'bold', fontSize: '0.9em'}}>Are you sure you want to submit?</div>
                                <div>
                                    { SUBMIT_OKRS_STATUS === true && 
                                        <Label color="green" >Submission Successful</Label>
                                    }
                                    { SUBMIT_OKRS_STATUS === false && 
                                        <Label color="red" >Submission Failed. Please try again or refresh page.</Label>
                                    }
                                </div>
                                <div style={{marginTop: '5px', display: 'flex'}}>
                                    <Button content="Yes, submit" disabled={!CONTINUE_TO_PERFORMANCE_ASSESSMENT_ACTIVE || buttonLoadingStatus} loading={isSubmittingOKRs} onClick={this.handleSubmit} color='blue' basic/>
                                    <Button color='red' basic onClick={this.handlePopup}>No, close this</Button>
                                </div>
                            </Popup>
                        } 
                    </Segment>
                </Segment>
            </Fragment>
        )
    }
}


export default Results