import React from 'react'
import {Card, Segment} from 'semantic-ui-react'

const ExceptionHandler = (props) => {
    const {BI, appraisalCycle, userIDs} = props

    const messages = {
        NO_STAFF_APPRAISAL: "No Staff Appraisal exist yet",
        STAFF_DOES_NOT_EXIST: "The Staff requesting this Staff Appraisal does not exist",
        SBU_OBJECTIVES_DO_NOT_EXIST: "No SBU Objective"
    }
    
    let screenContent = "The time window for assessing OKRs has not been activated by PeopleOps hence you'll be unable to assess your OKRs during this period";

    if(appraisalCycle.okrAssessmentLock === true){
        screenContent = screenContent
    } else if(Object.prototype.toString.call(BI) === '[object String]'){
        screenContent = BI
    } else if (Object.prototype.toString.call(BI) === '[object Object]'){
        screenContent = BI.message
    }

    if (userIDs.length < 1) {
        screenContent = "We can't find your OKRs. Please confirm with PeopleOps that you successfully set your objectives during the appraisal setting period";
    }
    return(
        // <Card>
        //     { Object.prototype.toString.call(BI) === '[object String]' &&
        //         <Card.Content>{BI}</Card.Content>
        //     }
        //     { Object.prototype.toString.call(BI) === '[object Object]' &&
        //         <Card.Content>{BI.message}</Card.Content>
        //     }
        // </Card>
        
        <Segment basic padded>
            <Card centered>
                <Card.Header as="h2" textAlign='center'>
                    HELLO
                </Card.Header>
                <Card.Content>
                    {/* <span style={props.appraisal.error ? errorContentStyle : null}>{screenContent}</span> */}
                    {screenContent}
                </Card.Content>
            </Card>
        </Segment>
    )
}

export default ExceptionHandler