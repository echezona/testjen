//Components
import React, {Component, Fragment} from 'react';
import { Grid, Loader, Segment, Card, Header, Icon } from 'semantic-ui-react';
import Staff from './Staff';
import Results from './results';
import { connect } from "react-redux"
import {withRouter,Switch,Route} from 'react-router-dom'
import { FETCH_BEHAVIORAL_INDICATORS, FETCH_OBJECTIVES_AND_KEY_RESULTS, FETCH_STAFF_APPRAISAL, FETCH_APPRAISAL_CYCLE, FETCH_STAFF_DETAILS } from '../../constants';
import PageInfo from '../../components/PageInfo';

// Css Files
import './index.css';
import ExceptionHandler from './exceptionHandler';
import { userIDsSelector, userObjectivesSelector, userKeyResultsSelector } from '../../reducer';
import SummaryTable from '../../components/SummaryTable';


const submittedStyle = {
    fontSize: "30px",
    textAlign: "center",
}

class BehavioralAssessment extends Component {

    componentDidMount() {
        const { dispatch } = this.props
        dispatch({ type: FETCH_BEHAVIORAL_INDICATORS })
        dispatch({ type: FETCH_OBJECTIVES_AND_KEY_RESULTS })
        dispatch({ type: FETCH_STAFF_APPRAISAL })
        dispatch({ type: FETCH_APPRAISAL_CYCLE })

        document.title = "PMS - Behavioral Assessment" // set the document title in the browser
    }

    render(){
        const { match, behavioralIndicators, behavioralLoadingStatus, staffAppraisal, appraisalCycle, userIDs, userObjectives, userKeyResults } = this.props

        let staffKeyResults, staffPerformanceSum, staffPerformanceScore, staffBehaviorSum, staffBehaviorScore, staffTotalScore,
        lineManagerPerformanceSum, lineManagerPerformanceScore, lineManagerBehaviorSum, lineManagerBehaviorScore, lineManagerTotalScore

        if (
            // (Object.prototype.toString.call(appraisalCycle) === '[object Object]' && appraisalCycle.message) || 
            (Object.prototype.toString.call(behavioralIndicators) === '[object Array]' && behavioralIndicators.length > 0) || 
            (userIDs.length > 0 && typeof userIDs[0] != "string")
        ) {
            staffKeyResults = userIDs.map(item => userObjectives[item].keyResults)
                                    .reduce((acc, item) => acc.concat(item), [])
                                    .map(item => userKeyResults[item])

            staffPerformanceSum = staffKeyResults.reduce((acc, curr) => acc + curr.staffRating, 0)

            staffPerformanceScore = ((staffPerformanceSum / (staffKeyResults.length * 5)) * 100)

            staffBehaviorSum = behavioralIndicators.reduce((acc, curr) => acc + curr.staffRating,  0)

            staffBehaviorScore = ((staffBehaviorSum / (behavioralIndicators.length * 5)) * 100)

            staffTotalScore = ((staffPerformanceScore / 10) * 7) + ((staffBehaviorScore / 10) * 3)

            lineManagerPerformanceSum = staffKeyResults.reduce((acc, curr) => acc + curr.lineManagerRating, 0)

            lineManagerPerformanceScore = ((lineManagerPerformanceSum / (staffKeyResults.length * 5)) * 100)

            lineManagerBehaviorSum = behavioralIndicators.reduce((acc, curr) => acc + curr.lineManagerRating,  0)

            lineManagerBehaviorScore = ((lineManagerBehaviorSum / (behavioralIndicators.length * 5)) * 100)

            lineManagerTotalScore = ((lineManagerPerformanceScore / 10) * 7) + ((lineManagerBehaviorScore / 10) * 3)
        }

        
        if(behavioralLoadingStatus || this.props.fetchStaffDetailsLoadingStatus || this.props.fetchStaffAppraisalsStatus){
            return (
                <Loader active/>
            )
        }

        console.log("BI", behavioralIndicators, appraisalCycle, staffAppraisal)
        if(
            Object.prototype.toString.call(behavioralIndicators) !== '[object Array]' || 
            appraisalCycle.okrAssessmentLock === true ||
            userIDs.length < 1
        ){
            return(
                <ExceptionHandler BI={behavioralIndicators} appraisalCycle={this.props.appraisalCycle} userIDs={userIDs}/>
            )
        }

        if( staffAppraisal.staffAssessed === true && appraisalCycle.okrAssessmentLock === false) {

            return (
                <Fragment>
                    <p style={submittedStyle}>Congratulations! You've completed your staff assessment exercise</p>

                    <Segment basic>
                        <PageInfo 
                            title='Click to View/Hide Behaviour Score Summary' 
                            content={
                                <SummaryTable indicators={behavioralIndicators} variant={'behavior'}/>
                            }
                        />
                    </Segment>

                    {/* <Segment basic style={{ display: 'flex', justifyContent: 'center'}}>
                        <Card style={{ padding: '10px', margin: '20px' }}>
                            <Header content='Self Rating' subheader="Here's how you rated yourself" dividing/>
                            <div>Performance: {staffPerformanceScore.toFixed(1) + '%'}</div>
                            <div>Behavior: {staffBehaviorScore.toFixed(1) + '%'}</div>
                            <div style={{ fontSize: '1.2em', marginTop: '10px', fontWeight: 'bold' }}>Total: {staffTotalScore.toFixed(1) + '%'}</div>
                        </Card>

                        <Card style={{ padding: '10px', margin: '20px' }}>
                            <Header content='Final Rating' subheader={"Here's how your line manager rated you"}  dividing/>
                            { staffAppraisal.lineManagerTwoApproved === 'approved' &&
                                <>
                                    <div>Performance: {lineManagerPerformanceScore.toFixed(1) + '%'}</div>
                                    <div>Behavior: {lineManagerBehaviorScore.toFixed(1) + '%'}</div>
                                    <div style={{ fontSize: '1.2em', marginTop: '10px', fontWeight: 'bold' }}>Total: {lineManagerTotalScore.toFixed(1) + '%'}</div>
                                </>
                            }
                            { staffAppraisal.lineManagerTwoApproved !== 'approved' &&
                                <div> <Icon name='info circle'/> Your final rating is not yet available </div>
                            }
                        </Card>
                    </Segment> */}
                </Fragment>
            )
        }

        return(
            <React.Fragment>
                {/* <PageInfo info={[
                     <p>Here is the information concerning this page</p>
                ]}/> */}
                <Grid padded>
                    <Grid.Column computer={6} tablet={6} mobile={16}>
                        <Staff {...this.props}/>
                    </Grid.Column>
                    <Grid.Column computer={10} tablet={10} mobile={16}>
                        <Switch>
                            {behavioralIndicators.map((item, index) => <Route key={item.behaviour.id} path={`${match.path}/${item.behaviour.id}`} render={(props) => <Results 
                                                                                                                                                                name={item.behaviour.description.substr(0, item.behaviour.description.indexOf(':'))} 
                                                                                                                                                                description={item.behaviour.description.substr(item.behaviour.description.indexOf(':')+2)} 
                                                                                                                                                                behaviour={item.behaviour} comment={item.staffComment} rating={item.staffRating} 
                                                                                                                                                                behaviourId={item.behaviourId} staffAppraisalId={item.staffAppraisalId} id={item.id}
                                                                                                                                                                peer1Rating={item.peer1Rating} peer2Rating={item.peer2Rating} lineManagerRating={item.lineManagerRating}
                                                                                                                                                                peer1Comment={item.peer1Comment} peer2Comment={item.peer2Comment} lineManagerComment={item.lineManagerComment}
                                                                                                                                                                index={index}
                                                                                                                                                                {...this.props} {...props}/>}/>)}
                        </Switch>
                    </Grid.Column>
                </Grid>
            </React.Fragment>
        )
    }
}

function mapStateToProps(state) {
    return {
        appraisalCycle: state.requestingAppraisalCycle,
        behavioralIndicators: state.requestingBehavioralIndicators,
        buttonLoadingStatus: state.saveLoadingReducer.SAVE_BEHAVIORAL_ASSESSMENT_LOADING_STATUS,
        behavioralLoadingStatus: state.fetchLoadingReducer.FETCH_BEHAVIORAL_ASSESSMENT_LOADING_STATUS,
        userIDs: userIDsSelector(state),
        userObjectives: userObjectivesSelector(state),
        userKeyResults: userKeyResultsSelector(state),
        staffAppraisal: state.requestingStaffAppraisal,
        fetchStaffDetailsLoadingStatus: state.fetchLoadingReducer.FETCH_STAFF_DETAILS_LOADING_STATUS,
        SAVE_BEHAVIOUR_STATUS: state.SAVE_BEHAVIOUR_STATUS,
        isSubmittingOKRs: state.isSubmittingOKRs,
        SUBMIT_OKRS_STATUS: state.SUBMIT_OKRS_STATUS,
        fetchStaffAppraisalsStatus: state.fetchLoadingReducer.FETCH_STAFF_APPRAISAL_LOADING_STATUS,
    }
}

export default withRouter(connect(mapStateToProps)(BehavioralAssessment))
