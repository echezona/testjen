import React,{Component, Fragment} from 'react'
import { Segment,Grid,Header,Button,Form,Input,Container, Tab } from 'semantic-ui-react'
import "./index.css"


class Indicator extends Component {
    // constructor(props){
    //     super(props);
    //     this.state = {
    //         clicked: "",
    //     }
    // }

    // handleRatingClick = (e) => {
    //     const {name} = e.target
    //     this.setState({
    //         clicked: name,
    //     })
    // }

    panes = [
        {
            menuItem: 'Learner(2)',
            render: () => {
                return (
                    <p style={{overflow: 'auto', maxHeight: 200}}><pre>{this.props.behaviour.learnerDescription}</pre></p>
                )
            }
        },
        {
            menuItem: 'Emerging Performer(3)',
            render: () => {
                return (
                    <p style={{overflow: 'auto', maxHeight: 200}}><pre>{this.props.behaviour.emergingPerformerDescription}</pre></p>
                )
            }
        },
        {
            menuItem: 'Performer(4)',
            render: () => {
                return (
                    <p style={{overflow: 'auto', maxHeight: 200}}><pre>{this.props.behaviour.performerDescription}</pre></p>
                )
            }
        },
        {
            menuItem: 'Influencer(5)',
            render: () => {
                return (
                    <p style={{overflow: 'auto', maxHeight: 200}}><pre>{this.props.behaviour.influencerDescription}</pre></p>
                )
            }
        },
    ]

    render(){
        // const {clicked} = this.state
        const {behavioralIndicator, name, description, clicked, handleRatingClick, onComment, rating, comment} = this.props
         return(
            // ${this.props.item.keyResultNumber}
            // {this.props.item.keyResultContent}
            <Fragment>
                <Segment>
                    <Header as="h5" content={name}/>
                    {/* {`Identifiable by the following traits: Focused, Excellence Driven, Effective Communicator, Diligent Planner`} */}
                    {description}
                    <br/>   
                    <Tab menu={{ secondary: true, pointing: true }} panes={this.panes} />
                    <br/>
                    <Segment basic textAlign="right">
                        <Button content="2" compact color={rating===2 ? "red" : "green"} onClick={handleRatingClick} name="two"/> 
                        <Button content="3" compact color={rating===3 ? "red" : "green"} onClick={handleRatingClick} name="three"/> 
                        <Button content="4" compact color={rating===4 ? "red" : "green"} onClick={handleRatingClick} name="four"/> 
                        <Button content="5" compact color={rating===5 ? "red" : "green"} onClick={handleRatingClick} name="five"/> 
                    </Segment>
                </Segment>
                <Form>
                    <Form.TextArea placeholder="Please provide a reason for selecting this rating..." onChange={(e)=>onComment(e)} value={comment}/>
                </Form>
            </Fragment>
        )
    }
}


export default Indicator