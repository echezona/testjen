import React, { Fragment, Component } from 'react'
import { Tab, Header, Accordion, Segment, Icon, Menu } from 'semantic-ui-react'
// import queryString from 'query-string'


class Staff extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeIndex: -1,
        }
    }

    handleClick = (e, titleProps) => {
        const { index, name } = titleProps
        const { activeIndex } = this.state
        const { history } = this.props
        const newIndex = activeIndex === index ? -1 : index
        history.push(name)
        this.setState({
            activeIndex: newIndex
        })
    }

    render() {
        const { behavioralIndicators, match } = this.props
        const { activeIndex } = this.state
        return (
            <Fragment>
                <Header as="h3" textAlign="center" content="Behavioral Indicators" />
                <Segment basic>
                    <Accordion styled>
                        {behavioralIndicators.map((item, index) => {
                            return (
                                <Fragment key={item.behaviour.id}>
                                    <Accordion.Title active={activeIndex === index} index={index} onClick={this.handleClick} name={`${match.url}/${item.behaviour.id}`}>
                                        <Icon name='dropdown' />
                                        {item.behaviour.description.substr(0, item.behaviour.description.indexOf(':'))}
                                    </Accordion.Title>
                                    <Accordion.Content active={activeIndex === index}>
                                        {item.behaviour.description.substr(item.behaviour.description.indexOf(':') + 2)}
                                    </Accordion.Content>
                                </Fragment>
                            )
                        })}
                    </Accordion>
                </Segment>
            </Fragment>
        )
    }
}

export default Staff

