import React, { Component, Fragment } from 'react';
import { Button, Segment, Container, Form, Header, Label, Icon } from 'semantic-ui-react';
import './index.css'
import { ONBOARD_USER } from '../../constants';
import { connect } from "react-redux"
import { Link } from 'react-router-dom'


class Onboarding extends Component {
    state = {
        value: '',
        validEmail: null,
    }

    handleOnboarding = () => {
        const { dispatch } = this.props
        const { value } = this.state
        dispatch({ type: ONBOARD_USER, email: value })
    }

    handleChange = (e) => {
        const { value } = e.target;
        const emailReg1 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@venturegardengroup\.com$/;
        const emailReg2 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@splasherstech\.com$/;
        const emailReg3 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@avipayng\.com$/;
        const emailReg4 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@fidesicng\.com$/;
        const emailReg5 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@cloudtechng\.com$/;
        const emailReg6 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@avitechng\.com$/;
        const emailReg7 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@edutechng\.com$/;
        const emailReg8 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@powertechng\.com$/;
        const emailReg9 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@fueltechng\.com$/;
        const emailReg10 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@finatechng\.com$/;
        const emailReg11 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@powertechnigeria\.com$/;
        const emailReg12 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@avicollectng\.com$/;
        this.setState({
            value,
            validEmail: emailReg1.test(value) || emailReg2.test(value) || emailReg3.test(value) || emailReg4.test(value) || emailReg5.test(value) || emailReg6.test(value) || emailReg7.test(value) || emailReg8.test(value)
        })
    }

    render() {
        const {onboardingUserLoadingStatus, ONBOARDING_USER_STATUS} = this.props
        return (
            <section className='page-container'>
                <Header as='h1' content='VGG PMS ONBOARDING' className='header' />
                <Segment className='onboarding-container'>
                    <Form className='form'>
                        <label htmlFor="mail">
                            Please provide your official email:
                                <input id='mail' name='mail' value={this.state.value} onChange={(e) => this.handleChange(e)} />
                        </label>
                        {this.state.validEmail === false &&
                            <span className="indicator"><Icon name='info circle' color='red'/>Invalid Email </span>
                        }
                    </Form>
                    <br /><br />
                    <div className="button-container">
                        <Button
                            loading={onboardingUserLoadingStatus}
                            disabled={!this.state.validEmail || onboardingUserLoadingStatus}
                            // size='huge'
                            color="green"
                            labelPosition="right"
                            icon="arrow right"
                            content="ONBOARD"
                            onClick={() => this.handleOnboarding()}
                            className='button'
                        />
                        { ONBOARDING_USER_STATUS && ONBOARDING_USER_STATUS.status === true && 
                            <Label pointing='above' color="blue" >{ ONBOARDING_USER_STATUS.message }</Label>
                        }
                        { ONBOARDING_USER_STATUS && ONBOARDING_USER_STATUS.status === false && 
                            <Label pointing='above' color="red" >Onboarding Failed</Label>
                        }
                        <Link to="/login">Back to Sign In</Link>
                    </div>
                </Segment>
            </section>
        )
    }
}

function mapStateToProps(state) {
    return {
        onboardingUserLoadingStatus: state.saveLoadingReducer.ONBOARDING_USER_LOADING_STATUS,
        ONBOARDING_USER_STATUS: state.ONBOARDING_USER_STATUS,
    }
}

export default connect(mapStateToProps)(Onboarding)