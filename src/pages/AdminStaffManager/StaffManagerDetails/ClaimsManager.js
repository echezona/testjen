import React, { useState, useEffect } from 'react';
import { Segment, Header, Label, Button, Loader } from 'semantic-ui-react';
import { toast } from 'react-toastify';
import {cloneDeep} from 'lodash';
import { UPDATE_USER_CLAIMS } from '../../../constants';

const ClaimsManager = ({ dispatch, staffClaims, email, buttonLoading }) => {
    const [claims, setClaims] = useState({
        adding: false,
        removing: false,
        currentClaims: [],
        claimsToAdd: [],
        claimsToRemove: [],
    })

    useEffect(() => {
        setClaims({ ...claims, currentClaims: staffClaims.data })
    }, [staffClaims.data])

    // 0: {type: "application", value: "pmsapi"}
    // 1: {type: "pmsapi.role", value: "vgg_user"}
    // 2: {type: "pmsapi.role", value: "Staff"}
    // 3: {type: "application", value: "vggpmsclient"}
    // 4: {type: "vggpmsclient.role", value: "Staff"}
    // 5: {type: "vggpmsclient.role", value: "vgg_user"}
    // 6: {type: "pmsapi.role", value: "LineManager"}
    // 7: {type: "pmsapi.role", value: "PeopleOps"}

    const claimsValidations = {
        hasUiApplicationClaims: claims.currentClaims.some(item => (
            item.type === 'application' && item.value === 'vggpmsclient'
        )),
        hasApiApplicationClaims: claims.currentClaims.some(item => (
            item.type === 'application' && item.value === 'pmsapi'
        )),
        hasUiStaffClaims: claims.currentClaims.some(item => (
            item.type === 'vggpmsclient.role' && item.value === 'Staff'
        )),
        hasApiStaffClaims: claims.currentClaims.some(item => (
            item.type === 'pmsapi.role' && item.value === 'Staff'
        )),
        hasUiUserClaims: claims.currentClaims.some(item => (
            item.type === 'vggpmsclient.role' && item.value === 'vgg_user'
        )),
        hasApiUserClaims: claims.currentClaims.some(item => (
            item.type === 'pmsapi.role' && item.value === 'vgg_user'
        )),
        hasLineManagerClaims: claims.currentClaims.some(item => (
            item.type === 'pmsapi.role' && item.value === 'LineManager'
        )),
        hasPeopleOpsClaims: claims.currentClaims.some(item => (
            item.type === 'pmsapi.role' && item.value === 'PeopleOps'
        ))
    };

    const onApplicationTagClick = () => {
        //They should remain unable to disable the application tag
        if (!hasUiApplicationClaims || !hasApiApplicationClaims) {
            if(!hasUiApplicationClaims){
                setClaims({
                    ...claims,
                    claimsToAdd: [
                        ...claims.claimsToAdd,
                        { Type: 'application', Value: 'vggpmsclient' }
                    ],
                    //make changes to the ui
                    currentClaims: [
                        ...claims.currentClaims,
                        { type: 'application', value: 'vggpmsclient' }
                    ]
                })
            }
            if(!hasApiApplicationClaims){
                setClaims({
                    ...claims,
                    claimsToAdd: [
                        ...claims.claimsToAdd,
                        { Type: 'application', Value: 'pmsapi' }
                    ],
                    //make changes to the ui
                    currentClaims: [
                        ...claims.currentClaims,
                        { type: 'application', value: 'pmsapi' }
                    ]
                })
            }
        } else {
            toast.warn('You Cannot Remove Application Claims')
        }
    };

    const onStaffTagClick = () => {
        //They should remain unable to disable the staff tag
        if (!hasUiStaffClaims || !hasApiStaffClaims) {
            if(!hasUiStaffClaims){
                setClaims({
                    ...claims,
                    claimsToAdd: [
                        ...claims.claimsToAdd,
                        { Type: 'vggpmsclient.role', Value: 'Staff' }
                    ],
                    //make changes to the ui
                    currentClaims: [
                        ...claims.currentClaims,
                        { type: 'vggpmsclient.role', value: 'Staff' }
                    ]
                })
            }
            if(!hasApiStaffClaims){
                setClaims({
                    ...claims,
                    claimsToAdd: [
                        ...claims.claimsToAdd,
                        { Type: 'pmsapi.role', Value: 'Staff' }
                    ],
                    //make changes to the ui
                    currentClaims: [
                        ...claims.currentClaims,
                        { type: 'pmsapi.role', value: 'Staff' }
                    ]
                })
            }
        } else {
            toast.warn('You Cannot Remove Staff Claims')
        }
    };

    const onUserTagClick = () => {
        //They should remain unable to disable the staff tag
        if (!hasUiUserClaims || !hasApiUserClaims) {
            if(!hasUiUserClaims){
                setClaims({
                    ...claims,
                    claimsToAdd: [
                        ...claims.claimsToAdd,
                        { Type: 'vggpmsclient.role', Value: 'vgg_user' }
                    ],
                    //make changes to the ui
                    currentClaims: [
                        ...claims.currentClaims,
                        { type: 'vggpmsclient.role', value: 'vgg_user' }
                    ]
                })
            }
            if(!hasApiUserClaims){
                setClaims({
                    ...claims,
                    claimsToAdd: [
                        ...claims.claimsToAdd,
                        { Type: 'pmsapi.role', Value: 'vgg_user' }
                    ],
                    //make changes to the ui
                    currentClaims: [
                        ...claims.currentClaims,
                        { type: 'pmsapi.role', value: 'vgg_user' }
                    ]
                })
            }
        } else {
            toast.warn('You Cannot Remove User Claims')
        }
    };

    const onLineManagerTagClick = () => {
        if (!hasLineManagerClaims) {
            setClaims({
                ...claims,
                claimsToAdd: [
                    ...claims.claimsToAdd,
                    { Type: 'pmsapi.role', Value: 'LineManager' }
                ],
                //make changes to the ui
                currentClaims: [
                    ...claims.currentClaims,
                    { type: 'pmsapi.role', value: 'LineManager' }
                ]
            })
        } else {
            setClaims({
                ...claims,
                claimsToRemove: [
                    ...claims.claimsToRemove,
                    { Type: 'pmsapi.role', Value: 'LineManager' }
                ],
                //make changes to the ui
                currentClaims: claims.currentClaims.filter(item => item.value !== 'LineManager')
            })
        }
    };

    const onPeopleOpsTagClick = () => {
        if (!hasPeopleOpsClaims) {
            setClaims({
                ...claims,
                claimsToAdd: [
                    ...claims.claimsToAdd,
                    { Type: 'pmsapi.role', Value: 'PeopleOps' }
                ],
                //make changes to the ui
                currentClaims: [
                    ...claims.currentClaims,
                    { type: 'pmsapi.role', value: 'PeopleOps' }
                ]
            })
        } else {
            setClaims({
                ...claims,
                claimsToRemove: [
                    ...claims.claimsToRemove,
                    { Type: 'pmsapi.role', Value: 'PeopleOps' }
                ],
                //make changes to the ui
                currentClaims: claims.currentClaims.filter(item => item.value !== 'PeopleOps')
            })
        }
    };

    const handleSubmit = () => {
        const { claimsToAdd, claimsToRemove, currentClaims } = claims;

        const uniqueClaimsToAdd = claimsToAdd.reduce((acc, curr) => {
                const isInState = acc.find(item => item?.Type === curr.Type && item?.Value === curr.Value);
                if(!isInState) {
                    acc.push(curr);
                }
                return acc
            }, [])
            .filter(claim => {
                const itemShouldBeIgnored = staffClaims.data.find(item => item.type === claim.Type && item.value === claim.Value)
                if (itemShouldBeIgnored){
                    return false
                }
                return true
            });
        
        const uniqueClaimsToRemove = claimsToRemove.reduce((acc, curr) => {
                const isInState = acc.find(item => item?.Type === curr.Type && item?.Value === curr.Value);
                if(!isInState) {
                    acc.push(curr);
                }
                return acc
            }, [])
            .filter(claim => {
                const itemShouldBeIgnored = currentClaims.find(item => item.type === claim.Type && item.value === claim.Value)
                if (itemShouldBeIgnored){
                    return false
                }
                return true
            });

        console.log('uniqueClaimsToAdd', uniqueClaimsToAdd)

        const uiClaimsToAdd = cloneDeep(uniqueClaimsToAdd)
                                .filter(item => item.Type.includes('vggpmsclient'))
                                .reduce((acc, curr) => {
                                    curr.Type = 'role';
                                    acc.push(curr);
                                    return acc;
                                }, []);

        const apiClaimsToAdd = cloneDeep(uniqueClaimsToAdd)
                                .filter(item => item.Type.includes('pmsapi'))
                                .reduce((acc, curr) => {
                                    curr.Type = 'role';
                                    acc.push(curr);
                                    return acc;
                                }, []);

        const otherClaimsToAdd = cloneDeep(uniqueClaimsToAdd)
                                    .filter(item => item.Type.includes('application'))

        // if both arrays are empty then let the user know no changes were made.
        if (!uniqueClaimsToAdd.length && !uniqueClaimsToRemove.length) {
            toast.warn('No Changes Were Made')
        } else {
            dispatch({ 
                type: UPDATE_USER_CLAIMS, 
                payload: { 
                    email, 
                    uiClaimsToAdd, 
                    apiClaimsToAdd: [...apiClaimsToAdd, ...otherClaimsToAdd],
                    claimsToRemove: uniqueClaimsToRemove 
                }
            })
        }
        
        console.log('a ti ri', [...uiClaimsToAdd,...otherClaimsToAdd], apiClaimsToAdd, uniqueClaimsToRemove)
    }
 
    const { 
        hasUiApplicationClaims, hasApiApplicationClaims, hasUiStaffClaims, hasApiStaffClaims,
        hasUiUserClaims, hasApiUserClaims, hasLineManagerClaims, hasPeopleOpsClaims
    } = claimsValidations;


    return (
        <Segment>
            <Header as='h2' content='SSO Claims' style={{ marginBottom: '30px' }} />
            { staffClaims.loading &&
                <div style={styles.loaderContainer}>
                    <Loader active/>
                </div>
            }
            { !staffClaims.loading &&
                <div>
                    <Label
                        style={{ cursor: 'pointer' }}
                        content='Application' // Application || Staff || LineManager || User || People Ops
                        color={ hasUiApplicationClaims && hasApiApplicationClaims ? 'green' : 'red' }
                        icon={ hasUiApplicationClaims && hasApiApplicationClaims ? 'check' : 'cancel' }
                        onClick={onApplicationTagClick}
                    />
                    <Label
                        style={{ cursor: 'pointer' }}
                        content='Staff' // Application || Staff || LineManager || User || People Ops
                        color={ hasUiStaffClaims && hasApiStaffClaims ? 'green' : 'red' }
                        icon={ hasUiStaffClaims && hasApiStaffClaims ? 'check' : 'cancel' }
                        onClick={onStaffTagClick}
                    />
                    <Label
                        style={{ cursor: 'pointer' }}
                        content='User' // Application || Staff || LineManager || User || People Ops
                        color={ hasUiUserClaims && hasApiUserClaims ? 'green' : 'red' }
                        icon={ hasUiUserClaims && hasApiUserClaims ? 'check' : 'cancel' }
                        onClick={onUserTagClick}
                    />
                    <Label
                        style={{ cursor: 'pointer' }}
                        content='Line Manager'
                        color={ hasLineManagerClaims ? 'green' : 'red' }
                        icon={ hasLineManagerClaims ? 'check' : 'cancel' }
                        onClick={onLineManagerTagClick}
                    />
                    <Label
                        style={{ cursor: 'pointer' }}
                        content='People Ops'
                        color={ hasPeopleOpsClaims ? 'green' : 'red' }
                        icon={ hasPeopleOpsClaims ? 'check' : 'cancel' }
                        onClick={onPeopleOpsTagClick}
                    />
                </div>
            }
            <div style={styles.submitButton}>
                <Button
                    content='Update User Claims'
                    onClick={handleSubmit}
                    loading={buttonLoading}
                    disabled={buttonLoading || staffClaims.loading}
                />
            </div>
        </Segment>
    )
}

const styles = {
    submitButton: {
        display: 'flex',
        justifyContent: 'center',
        marginTop: '25px',
    },
    loaderContainer: {
        margin: '20px',
        height: '30px'
    }
}

export default ClaimsManager;