//Components
import React, { Fragment, useEffect, useState, useRef } from 'react';
import { connect } from 'react-redux'
import { Form, Grid, Header, Button, Segment, Search, Loader, Radio } from 'semantic-ui-react';
import _ from 'lodash';
import queryString from 'query-string';

// Css Files
// import './index.css';
import { FETCH_ALL_BUSINESS_UNITS, FETCH_ALL_LINE_MANAGERS, FETCH_ALL_DEPARTMENTS, FETCH_STAFF_DETAILS_BY_ADMIN, UPDATE_STAFF_DETAILS, FETCH_STAFF_CLAIMS } from '../../../constants';
import { ToastContainer } from "react-toastify";
import ClaimsManager from './ClaimsManager';

const levelOptions = ['level 1', 'level 2', 'level 3', 'level 3.5', 'level 4', 'level 4.5', 'level 5', 'level 6', 'level 7']

const StaffManagerDetails = (props) => {
    const {
        isFetchingAllBusinessUnits, allBusinessUnits, allLineManagers, allDepartments,
        staffDetails, dispatch, location, isUpdatingStaffDetails, staffClaims,
        updateStaffClaimsIsLoading
    } = props;

    const [isFinalApprover, setIsFinalApprover] = useState(false);
    const [staffNames, setStaffNames] = useState({ firstName: '', middleName: '', lastName: '' });
    const [email, setEmail] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');
    const [currLevel, setCurrLevel] = useState('');
    const [businessUnit, setBusinessUnit] = useState({id: '', name: '', open: false});
    const [lineManager, setLineManager] = useState({id: '', name: '', open: false});
    const [department, setDepartment] = useState({id: '', name: '', open: false});

    useEffect(() => {
        // const { dispatch, location } = props;
        dispatch({ type: FETCH_ALL_DEPARTMENTS })
        dispatch({ type: FETCH_ALL_LINE_MANAGERS })
        dispatch({ type: FETCH_ALL_BUSINESS_UNITS })
        dispatch({ type: FETCH_STAFF_DETAILS_BY_ADMIN, payload: { staffId: queryString.parse(location.search).staffId } })
        dispatch({ type: FETCH_STAFF_CLAIMS, payload: { email: location.state.email } })

    }, []);

    useEffect(() => {
        setIsFinalApprover(staffDetails?.data?.isFinalApproval);
        setStaffNames({ firstName: staffDetails?.data?.firstName, middleName: staffDetails?.data?.middleName, lastName: staffDetails?.data?.surName });
        setEmail(staffDetails?.data?.emailAddress);
        setPhoneNumber(staffDetails?.data?.mobileNumber);
        setCurrLevel(staffDetails?.data?.currentLevel);
        setBusinessUnit({id: staffDetails?.data?.businessUnit?.id, name: staffDetails?.data?.businessUnit?.name, open: false});
        setLineManager({id: staffDetails?.data?.lineManager?.id, name: staffDetails?.data?.lineManager?.name, open: false});
        setDepartment({id: staffDetails?.data?.department?.id, name: staffDetails?.data?.department?.name, open: false});
    }, [staffDetails.data])

    const handleSearchChange = (e, {value, name}) => {
        switch(name) {
            case 'businessUnit':
                if (!value.trim()) {
                    return setBusinessUnit({ id: '', name: '', open: false });
                }
                return setBusinessUnit({ ...businessUnit, name: value, open: true });
            case 'lineManager':
                if (!value.trim()) {
                    return setLineManager({ id: '', name: '', open: false });
                }
                return setLineManager({ ...lineManager, name: value, open: true });
            case 'department':
                if (!value.trim()) {
                    return setDepartment({ id: '', name: '', open: false });
                }
                return setDepartment({ ...department, name: value, open: true });
            default: 
                return null
        }
    }

    const handleSbuResultSelect = (e, {result}) => {
        setBusinessUnit({ ...businessUnit, id: result.id, name: result.name, open: false })
    }

    const handleLineManagerResultSelect = (e, {result}) => {
        setLineManager({ ...lineManager, id: result.id, name: result.name, open: false })
    }

    const handleDepartmentResultSelect = (e, {result}) => {
        setDepartment({ ...department, id: result.id, name: result.name, open: false })
    }

    const getFilteredResults = (id) => {
        let re;
        switch(id) {
            case 'businessUnit':
                re = new RegExp(_.escapeRegExp(businessUnit.name), 'i')
                return allBusinessUnits.filter(item => re.test(item.name));
            case 'lineManager':
                re = new RegExp(_.escapeRegExp(lineManager.name), 'i')
                return allLineManagers.data.filter(item => re.test(item.name));
            case 'department':
                re = new RegExp(_.escapeRegExp(department.name), 'i')
                return allDepartments.data.filter(item => re.test(item.name));
            default:
                return []
        }
    }

    const handleSubmit = () => {
        const data = {
            staffId: staffDetails.data.staffId,
            firstName: staffNames.firstName,
            middleName: staffNames.middleName,
            surName: staffNames.lastName,
            emailAddress: email,
            mobileNumber: phoneNumber,
            currentLevel: currLevel,
            employmentDate: staffDetails.data.employmentDate,
            lineManagerId: lineManager.id,
            businessUnitId: businessUnit.id,
            departmentId: department.id,
            isFinalApproval: isFinalApprover
        }

        console.log('dat', data)

        dispatch({ 
            type: UPDATE_STAFF_DETAILS, 
            payload: {
                staffId: queryString.parse(location.search).staffId,
                data
            }
        })
    }


    if (isFetchingAllBusinessUnits || allLineManagers.loading || allDepartments.loading || staffDetails.loading) {
        return <Loader active/>
    }

    console.log('clIaims', staffClaims, location.state)

    return (
        <Segment basic>
            <ToastContainer/>
            <Grid columns={16}>
                <Grid.Column width={10}>
                    <Segment>
                        <Header as='h2' content='Staff Details' style={{ marginBottom: '30px' }} />
                        <Form>  
                            { staffClaims.data.some(claim => claim.value === "LineManager") &&
                                <Radio
                                    style={{...styles.mb, ...styles.labelText}}
                                    toggle
                                    label='Final Approver'
                                    checked={isFinalApprover}
                                    onChange={() => setIsFinalApprover(!isFinalApprover)}
                                />
                            }
                            <Form.Group widths='equal'>
                                <Form.Input
                                    fluid 
                                    label='First name' 
                                    placeholder='First name'
                                    value={staffNames.firstName}
                                    onChange={(e, {value}) => setStaffNames({ ...staffNames, firstName: value })}
                                />
                                <Form.Input
                                    fluid
                                    label='Middle name'
                                    placeholder='Middle name'
                                    value={staffNames.middleName}
                                    onChange={(e, {value}) => setStaffNames({ ...staffNames, middleName: value })}
                                />
                                <Form.Input
                                    fluid
                                    label='Last name'
                                    placeholder='Last name'
                                    value={staffNames.lastName}
                                    onChange={(e, {value}) => setStaffNames({ ...staffNames, lastName: value })}
                                />
                            </Form.Group>
                            <div style={styles.formRow}>
                                <span>
                                    <Form.Input 
                                        fluid 
                                        label='Email' 
                                        placeholder='staff@venturegardengroup.com' 
                                        style={styles.midFields}    
                                        value={email}
                                        readOnly
                                    />
                                </span>
                                <span>
                                    <Form.Input 
                                        fluid
                                        label='Phone number'
                                        placeholder='eg: 08077665544'
                                        style={styles.midFields}
                                        value={phoneNumber}
                                        readOnly
                                    />
                                </span>
                            </div>
                            <div style={{...styles.levelContainer}}>
                                <label style={styles.labelText}>Level</label>
                                {
                                    levelOptions.map(item => (
                                        <div style={styles.levelItem} key={item}>
                                            <label style={styles.labelText}>{item}</label>
                                            <Radio
                                                name={item}
                                                checked={item === currLevel}
                                                onChange={() => setCurrLevel(item)}
                                            />
                                        </div>
                                    ))
                                }
                            </div>
                            <div style={styles.formRow}>
                                <label style={styles.labelText}>
                                    Line manager
                                    <Search
                                        name='lineManager'
                                        open={lineManager.open}
                                        style={styles.search}
                                        input={{ fluid: true }}
                                        icon={null}
                                        value={lineManager.name}
                                        onSearchChange={handleSearchChange}
                                        results={getFilteredResults('lineManager')}
                                        resultRenderer={({name}) => <div>{name}</div>} 
                                        onResultSelect={handleLineManagerResultSelect}
                                    />
                                </label>
                                <label style={{...styles.labelText, ...styles.middleSearch }}>
                                    Department
                                    <Search
                                        name='department'
                                        open={department.open}
                                        style={styles.search}
                                        input={{ fluid: true }}
                                        icon={null}
                                        value={department.name}
                                        onSearchChange={handleSearchChange}
                                        results={getFilteredResults('department')}
                                        resultRenderer={({name}) => <div>{name}</div>} 
                                        onResultSelect={handleDepartmentResultSelect}
                                    />
                                </label>
                                <label style={styles.labelText}>
                                    Business Unit
                                    <Search
                                        name='businessUnit'
                                        open={businessUnit.open}
                                        style={styles.search}
                                        input={{ fluid: true }}
                                        icon={null}
                                        value={businessUnit.name}
                                        onSearchChange={handleSearchChange}
                                        results={getFilteredResults('businessUnit')}
                                        resultRenderer={({name}) => <div>{name}</div>} 
                                        onResultSelect={handleSbuResultSelect}
                                    />
                                </label>
                            </div>
                            <div style={styles.submitButton}>
                                <Button
                                    content='Update Staff Details'
                                    onClick={handleSubmit}
                                    loading={isUpdatingStaffDetails}
                                    disabled={isUpdatingStaffDetails}
                                />
                            </div>
                        </Form>
                    </Segment>
                </Grid.Column>
                <Grid.Column width={6}>
                    <ClaimsManager
                        staffClaims={staffClaims}
                        dispatch={dispatch}
                        email={location.state.email}
                        buttonLoading={updateStaffClaimsIsLoading}
                    />
                </Grid.Column>
            </Grid>
        </Segment>
    )
}

const styles = {
    mb: {
        marginBottom: '25px'
    },
    levelContainer: {
        display: 'flex',
        alignItems: 'center',
        margin: '25px 0'
    },
    levelItem: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        margin: '0 15px'
    },
    labelText: {
        fontWeight: 'bold',
        color: 'black'
    },
    formRow: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    search: {
        width: '18vw'
    },
    midFields: {
        width: '28vw'
    },
    middleSearch: {
        margin: '0 10px',
    },
    submitButton: {
        display: 'flex',
        justifyContent: 'center',
        marginTop: '25px',
    }
}


function mapStateToProps(state) {
    return {
        isFetchingAllBusinessUnits: state.fetchLoadingReducer.FETCH_ALL_BUSINESS_UNITS_LOADING_STATUS,
        allBusinessUnits: state.requestingAllBusinessUnits,
        allLineManagers: state.allLineManagersReducer,
        allDepartments: state.allDepartmentsReducer,
        staffDetails: state.staffDetailsByAdminReducer,
        isUpdatingStaffDetails: state.saveLoadingReducer.UPDATE_STAFF_DETAILS_LOADING_STATUS,
        staffClaims: state.staffClaimsReducer,
        updateStaffClaimsIsLoading: state.saveLoadingReducer.UPDATE_USER_CLAIMS_LOADING_STATUS,
        // poStaffAppraisals: poStaffAppraisalsSelector(state),
        // isFetchingStaffAppraisals: state.isFetchingStaffAppraisals,
        // peopleOpsCount: state.requestingPeopleOpsCount,
        // // resetStatusIsLoading: state.saveLoadingReducer.SUBMIT_PEOPLE_OPS_OKRS_LOADING_STATUS,
        // allStaff: state.staffManagerReducer,
    }
}

export default connect(mapStateToProps)(StaffManagerDetails)