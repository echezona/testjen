//Components
import React, { Fragment } from 'react';
import _ from 'lodash'
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux'
import { Form, Grid, Header, Table, Segment, Button, Card, Input, Loader, Pagination, Dropdown, Icon } from 'semantic-ui-react';

// Css Files
import './index.css';
import { FETCH_LINE_MANAGER_OKRS, FETCH_LINE_MANAGER_STAFF_APPRAISALS, FETCH_PEOPLE_OPS_STAFF_APPRAISALS, FETCH_PEOPLE_OPS_COUNT, SUBMIT_PEOPLE_OPS_OKRS, FETCH_ALL_STAFF } from '../../constants';
import { poStaffAppraisalsSelector } from '../../reducer';
import { ToastContainer } from "react-toastify";

class AdminStaffManager extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            column: null,
            data: [],
            direction: null,
            filterOptions: [
                { key: "0", text: "All", value: "all"},
                { key: "1", text: "Completed", value: "completed"},
                { key: "2", text: "Uncompleted", value: "uncompleted"},
            ],
            filterBy: "all",
            // results: [],
            value: " ",
            isLoading: false,
            searching: false,
            activePage: 1,
            totalPages: 0,
            itemsPerPage: 10,
            indexStart: 0, 
            indexEnd: 9,
            resetStatusModalOpen: false,
        }
    }

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch({ type: FETCH_PEOPLE_OPS_STAFF_APPRAISALS })
        dispatch({ type: FETCH_PEOPLE_OPS_COUNT })
        dispatch({ type: FETCH_ALL_STAFF })
    }

    resetComponent = () => this.setState({ isLoading: false, data: this.props.poStaffAppraisals, searching: false })

    handleSearchChange = (e) => {
        const { value } = e.target;
        this.setState({ isLoading: true, value, searching: true, activePage: 1 });
    }

    searchFilterFunction = (item, index) => {
        const { value } = this.state
        const re = new RegExp(_.escapeRegExp(value), 'i')
        if (!value.trim()) {
            return true;
        }
        return (
            re.test(item.staffId) ||
            re.test(item.firstName) ||
            re.test(item.surName) ||
            re.test(item.currentLevel) ||
            re.test(item.businessUnit.name) ||
            re.test(item.department.name) ||
            re.test(item.lineManager.name)
        )
    } 

    pageChunker = (acc, curr, idx, arr) => { 
        const { itemsPerPage } = this.state;
        acc[1].push(curr);
        if (acc[1].length === itemsPerPage) {
            acc[0].push(acc[1])
            acc[1] = []
        }
        if (arr.length - 1 === idx) {
            acc[0].push(acc[1])
        }

        return acc
        // const acc = [[], []]
    }

    gotoDetail = (id, email) => {
        const { history, match } = this.props
        // history.push(`/staff-management-details?staffId=${id}`)
        history.push({
            pathname: `/staff-management-details`,
            search: `?staffId=${id}`,
            state: {email},
        })
    }

    handleSort = clickedColumn => () => {
        const { column, data, direction } = this.state

        if (column !== clickedColumn) {
            this.setState({
                column: clickedColumn,
                data: _.sortBy(data, [clickedColumn]),
                direction: 'ascending',
            })
            console.log("clickedColumn,", clickedColumn)
            return
        }

        console.log(column, data, direction)

        this.setState({
            data: data.reverse(),
            direction: direction === 'ascending' ? 'descending' : 'ascending',
        })
    }

    handlePaginationChange = (e, { activePage }) => {
        const { itemsPerPage } = this.state
        const control = itemsPerPage * activePage

        this.setState({ 
            activePage,
            indexStart: control - itemsPerPage,
            indexEnd: control - 1,
        })
    }

    render() {
        const { history, lmIDs, lmObjectives, poStaffAppraisals, isFetchingStaffAppraisals, firstApproved, allStaff } = this.props
        // console.log("lmtemp:",poStaffAppraisals)
        const { column, data, direction, analyticsCardData, results, indexStart, indexEnd, totalPages, activePage, filterOptions, itemsPerPage, filterBy } = this.state
        // const staffAprraisalArr = lmIDs.map(item => lmObjectives[item].staffAppraisal).filter((item, index, self) => self.indexOf(item) === index).map(item => poStaffAppraisals[item].staffId)
        // console.log("datum", data)
        if (isFetchingStaffAppraisals || allStaff.loading) {
            return <Loader active />
        }

        console.log('staffList', allStaff.data)

        return (
            <Segment basic>
                <ToastContainer/>              

                {/* Main Page starts here */}
                <Grid id="okrDataTable" padded>
                    <Grid.Row style={{ marginTop: "10px" }}>
                        <Grid.Column width={10}>
                            <Header as="h3" content="Staff Manager" dividing />
                        </Grid.Column>
                        
                        <Grid.Column width={6} textAlign='right'>
                            <Input icon='search' placeholder='Search...' onChange={(e) => this.handleSearchChange(e)} />
                        </Grid.Column>
                    </Grid.Row>

                    
                    <Grid.Row>
                        <Table sortable celled padded striped singleLine size="small">

                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell sorted={column === 'staffId' ? direction : null} onClick={this.handleSort('fullName')}>
                                        ID
                                    </Table.HeaderCell>
                                    <Table.HeaderCell sorted={column === 'sbu' ? direction : null} onClick={this.handleSort('sbu')}>
                                        Name
                                    </Table.HeaderCell>
                                    <Table.HeaderCell sorted={column === 'department' ? direction : null} onClick={this.handleSort('department')}>
                                        Level
                                    </Table.HeaderCell>
                                    <Table.HeaderCell sorted={column === 'lineManager' ? direction : null} onClick={this.handleSort('lineManager')}>
                                        Business Unit
                                    </Table.HeaderCell>
                                    <Table.HeaderCell sorted={column === 'superlinemanager' ? direction : null} onClick={this.handleSort('superlinemanager')}>
                                        Department
                                    </Table.HeaderCell>
                                    <Table.HeaderCell sorted={column === 'submissionDate' ? direction : null} onClick={this.handleSort('submissionDate')}>
                                        Line Manager
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                        Action
                                    </Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>
                            
                            <Table.Body>
                                {allStaff
                                    ?.data
                                    ?.filter(this.searchFilterFunction)
                                    ?.reduce(this.pageChunker, [[], []])
                                    ?.find(item => item)
                                    ?.find((item, idx) => idx === this.state.activePage - 1)
                                    ?.map((item) => (
                                        <Table.Row>
                                            <Table.Cell>{item.staffId}</Table.Cell>
                                            <Table.Cell>{`${item.firstName} ${item.surName}`}</Table.Cell>
                                            <Table.Cell>{item.currentLevel}</Table.Cell>
                                            <Table.Cell>{item.businessUnit.name}</Table.Cell>
                                            <Table.Cell>{item.department.name}</Table.Cell>
                                            <Table.Cell>{item.lineManager.name}</Table.Cell>
                                            <Table.Cell collapsing>
                                                <Button compact content="Manage" onClick={() => this.gotoDetail(item.staffId, item.emailAddress)} />
                                            </Table.Cell>
                                        </Table.Row>
                                    )
                                )}
                                {allStaff 
                                    .data
                                    .filter(this.searchFilterFunction)
                                    .length < 1
                                    &&
                                    <Table.Row textAlign="center">
                                        <Table.Cell colSpan={6}>
                                        <br/>
                                            <Header as='h2' content="Nothing To Display"/>
                                        <br/>
                                        </Table.Cell>
                                    </Table.Row>
                                }
                            </Table.Body>
                        </Table>
                        {/* { Math.ceil(allStaff.data.length / itemsPerPage) > 1 && */}
                            <Pagination
                                activePage={activePage}
                                onPageChange={this.handlePaginationChange}
                                totalPages={Math.ceil(allStaff.data.filter(this.searchFilterFunction).length / itemsPerPage)}
                            />
                        {/* } */}
                    </Grid.Row>
                </Grid>
            </Segment>
        )
    }
}

function mapStateToProps(state) {
    return {
        poStaffAppraisals: poStaffAppraisalsSelector(state),
        isFetchingStaffAppraisals: state.isFetchingStaffAppraisals,
        peopleOpsCount: state.requestingPeopleOpsCount,
        // resetStatusIsLoading: state.saveLoadingReducer.SUBMIT_PEOPLE_OPS_OKRS_LOADING_STATUS,
        allStaff: state.staffManagerReducer,
    }
}

export default connect(mapStateToProps)(AdminStaffManager)