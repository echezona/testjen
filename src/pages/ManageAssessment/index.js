//Components
import React, { Fragment } from 'react';
import _ from 'lodash'
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux'
import { Grid, Header, Table, Segment, Button, Container, Loader } from 'semantic-ui-react';

// Css Files
import './index.css';
import { FETCH_LINE_MANAGER_OKRS, FETCH_LINE_MANAGER_STAFF_APPRAISALS, FETCH_APPRAISAL_CYCLE } from '../../constants';
import { lmStaffAppraisalsSelector } from '../../reducer';
import ExceptionHandler from './exceptionHandler';


class ManageAssessment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            column: null,
            data: [],
            direction: null,
        }
    }

    componentDidUpdate(){ 
        const { column, data } = this.state
        const { lmStaffAppraisals } = this.props
        if(column === null && lmStaffAppraisals.length > 0 && data.length === 0){
            this.setState({
                data: lmStaffAppraisals,
            })
        }
        console.log("ayo", column, lmStaffAppraisals, data)
    }

    gotoOkr = (id, staffAppraisalId) => {
        const {history} = this.props
        // history.push(`/assessokrs${id}`)
        history.push({
            pathname: `/assessokrs`,
            search: id,
            state: {
                staffAppraisalId,
            }
        })
    }

    handleSort = clickedColumn => () => {
        const { column, data, direction } = this.state

        if (column !== clickedColumn) {
            this.setState({
                column: clickedColumn,
                data: _.sortBy(data, [clickedColumn]),
                direction: 'ascending',
            })
            console.log("clickedColumn,", clickedColumn)
            return
        }

        console.log(column, data, direction)

        this.setState({
            data: data.reverse(),
            direction: direction === 'ascending' ? 'descending' : 'ascending',
        })
    }

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch({ type: FETCH_LINE_MANAGER_STAFF_APPRAISALS })
        dispatch({ type: FETCH_APPRAISAL_CYCLE })

        document.title = "PMS - Manage Assessment" // set the document title in the browser
    }
    
    // componentDidMount(){
    //     this.forceUpdate()
    //     // window.location.reload()
    // }

    render() {
        const { history, lmIDs, lmObjectives, lmStaffAppraisals, isFetchingStaffAppraisals, appraisalCycle} = this.props
        // console.log("lmtemp:",lmStaffAppraisals)
        const { column, data, direction } = this.state
        // const staffAprraisalArr = lmIDs.map(item => lmObjectives[item].staffAppraisal).filter((item, index, self) => self.indexOf(item) === index).map(item => lmStaffAppraisals[item].staffId)
        console.log("datum", lmStaffAppraisals)
        if(isFetchingStaffAppraisals){
            return <Loader active/>
        }

        if(appraisalCycle.okrAssessmentLock === true || (Object.prototype.toString.call(appraisalCycle) === '[object Object]' && appraisalCycle.message)){
            return (
                <ExceptionHandler {...this.props} />
            )
        }

        return (
            <Segment basic>
                <Grid id="okrDataTable" padded>
                    <Grid.Row>
                        <Header as="h3" content="Manage Assessment" />
                    </Grid.Row>
                    <Grid.Row>
                        {/* <Table padded collapsing sortable celled striped fixed singleLine headerRow={headerRow} renderBodyRow={renderBodyRow} tableData={tableData} /> */}

                        <Table sortable celled padded striped singleLine size="small">
                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell sorted={column === 'fullName' ? direction : null} onClick={this.handleSort('fullName')}>
                                        Name
                                    </Table.HeaderCell>
                                    <Table.HeaderCell sorted={column === 'sbu' ? direction : null} onClick={this.handleSort('sbu')}>
                                        Department
                                    </Table.HeaderCell>
                                    <Table.HeaderCell sorted={column === 'department' ? direction : null} onClick={this.handleSort('department')}>
                                        SBU
                                    </Table.HeaderCell>
                                    {/* <Table.HeaderCell sorted={column === 'emailAddress' ? direction : null} onClick={this.handleSort('emailAddress')}>
                                        Email
                                    </Table.HeaderCell> */}
                                    <Table.HeaderCell sorted={column === 'status' ? direction : null} onClick={this.handleSort('status')}>
                                        Status
                                    </Table.HeaderCell>
                                    <Table.HeaderCell sorted={column === 'submissionDate' ? direction : null} onClick={this.handleSort('submissionDate')}>
                                        Submission Date
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                        Action
                                    </Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>
                            <Table.Body>
                                {data.map(({fullName, sbu, department, emailAddress, lineManagerStatus, submissionDate, staffId, assessedAppraisal, submittedAppraisal, lineManagerTwoApproved, staffAppraisalId }) => (
                                    <Table.Row key={emailAddress}>
                                        <Table.Cell>{fullName}</Table.Cell>
                                        <Table.Cell>{department}</Table.Cell>
                                        <Table.Cell>{sbu}</Table.Cell>
                                        {/* <Table.Cell>{emailAddress}</Table.Cell> */}
                                        <Table.Cell>{lineManagerStatus}</Table.Cell>
                                        <Table.Cell>{submissionDate === "01/01/0001 12:00 AM" ? "Yet to Submit" : submissionDate}</Table.Cell>
                                        <Table.Cell><Button content="Review" disabled={!assessedAppraisal || (lineManagerStatus !== "pending" && lineManagerStatus !== "in progress")} compact onClick={() =>this.gotoOkr(`?staffId=${staffId}`, staffAppraisalId)}/></Table.Cell>
                                    </Table.Row>
                                ))}
                                {data.length < 1 &&
                                    <Table.Row textAlign="center">
                                        <Table.Cell colSpan={6}>
                                        <br/>
                                            <Header as='h2' content="Nothing To Display"/>
                                        <br/>
                                        </Table.Cell>
                                    </Table.Row>
                                }
                            </Table.Body>
                        </Table>
                    </Grid.Row>
                </Grid>
            </Segment>
        )
    }
}

function mapStateToProps(state) {
    return {
        appraisalCycle: state.requestingAppraisalCycle,
        lmStaffAppraisals: lmStaffAppraisalsSelector(state),
        isFetchingStaffAppraisals: state.isFetchingStaffAppraisals,
    }
}

export default connect(mapStateToProps)(ManageAssessment)