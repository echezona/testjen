import React, { Fragment } from 'react'
import { Header, List, Grid, Segment, Icon, Form, Button, Divider, Container, Label } from 'semantic-ui-react'
import { connect } from "react-redux"
import Dropzone from 'react-dropzone';

import _ from 'lodash'
import * as XLSX from 'xlsx';
import './index.css'
import { UPLOAD_BEHAVIOUR, FETCH_BEHAVIOURS } from '../../constants';



class BehavioralSetting extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            files: [],
            filesToSend: [],
            levelOptions: ["Level 0", "Level 1", "Level 2", "Level 3", "Level 3.5", "Level 4", "Level 5", "Level 6", "Level 7", "Level 8"],
            currLevel: '',
        }
    }

    componentDidMount(){
        const {dispatch} = this.props
        dispatch({ type: FETCH_BEHAVIOURS })
    }

    handleSelectChange = (e) => {
        console.log("it's e", e.target.value)
        const { value } = e.target
        this.setState({
            currLevel: value,
            filesToSend: [],
            files: []
        })
    }

    handleNextClick = () => {
        this.setState({
            currLevel: '',
        })
    }

    handleSubmit = () => {
        const { dispatch } = this.props
        const { files, filesToSend } = this.state



        // let formData = new FormData()
        // for (var x = 0; x < files.length; x++){
        //     console.log("why me!")
        //     formData.append("formFiles" , files[x]);
        // }
        // files.forEach((file, index) => formData.append("formFiles", file))
        // console.log("one", formData)
        // console.log("two", formData.get("formFiles"))
        // console.log("wetin", formData.get("formFiles1"))
        // console.log("kilode", files[0])
        console.log("for fun", files)
        // console.log("stuff", files[1].behaviourFile.get("formFiles"))

        dispatch({ type: UPLOAD_BEHAVIOUR, files: filesToSend })
    }

    // [
    //     { level: "L1", file: FILE},
    //     { level: "L2", file: FILE},
    // ]

    componentDidUpdate() {
        console.log("state level", this.state.currLevel)
    }

    onDrop = (acceptedFiles) => {
        // const req = request.post('/upload')
        const { files, currLevel } = this.state

        acceptedFiles.forEach((file, index) => {


            console.log("file", file)
            console.log("what is this?", typeof files.find(item => item.level === currLevel))
            // if(typeof files.find(item => item.level === currLevel) !== 'undefined'){
            //     const newFiles = _.cloneDeep(files)
            //     formData.append("formFiles", file)
            //     newFiles.find(item => item.level === currLevel).behaviourFile = formData
            //     this.setState({
            //         files: newFiles
            //     })
            // } else {
            //     formData.append("formFiles", file)
            //     const newFileObject = {level: currLevel, behaviourFile: formData}
            //     this.setState(prevState => ({
            //         files: prevState.files.concat(newFileObject)
            //     }))
            // }

            // if (typeof files.find(item => item.level === currLevel) !== 'undefined') {
            //     const newFiles = _.cloneDeep(files)
            //     // const newFilesToSend = _.cloneDeep(filesToSend)
            //     // formData.append("formFiles", file)
            //     newFiles.find(item => item.level === currLevel).behaviourFile = file
            //     this.setState({
            //         files: newFiles
            //     })
            // } else {
            //     let formData = new FormData()
            //     formData.append("formFiles", file)
            //     formData.append("level", currLevel)
            //     const newFileObject = { level: currLevel, behaviourFile: file }
            //     this.setState(prevState => ({
            //         files: prevState.files.concat(newFileObject),
            //         filesToSend: prevState.filesToSend.concat(formData)
            //     }))
            // }

            //We should keep this sir
            let formData = new FormData()
            formData.append("formFiles", file)
            formData.append("level", currLevel)
            const newFileObject = { level: currLevel, behaviourFile: file }
            this.setState(prevState => ({
                files: [newFileObject],
                filesToSend: formData,
            }))

            // this.setState(prevState => ({
            //     files: prevState.files.concat(file)
            // }))
            console.log("filezilla", this.state.files)
            console.log("sending", this.state.filesToSend)

            // const reader = new FileReader()
            // reader.onload = () => {
            //     const dataUrl = reader.result
            //     console.log(dataUrl)

            // req
            //   .set('Content-Type', 'image/png)
            //   .set('Content-Disposition', 'attachment; filename='+file.fileName)
            //   .send(filePath)
            //   .end()
            // }

            // reader.readAsDataURL(file)
        })
    }

    render() {
        const { levelOptions } = this.state
        const { uploadBehavioursLoadingStatus, UPLOAD_BEHAVIOUR_STATUS, behaviours } = this.props
        console.log("indi",this.props.behavioralIndicators)

        return (
            <Grid columns={16}>
                <Grid.Column width={3}></Grid.Column>
                <Grid.Column width={10}>
                    <Segment basic>
                        {/* <Header as="h2" content="Summary" /> */}

                        {/* {staffSubmitted &&
                            <Message id='green' color="green">You have successfully submitted your OKRs to your Line Manager</Message>
                        }

                        {!staffSubmitted &&
                            <Message color="yellow">Please review your OKRs carefully before you submit as you'll be unable to edit them once you do</Message>
                        } */}

                        <Segment>
                            <Header as="h3" content="How to upload" />
                            <List as='ol'>
                                <List.Item as='li'>Select the level for which you want to upload a file.</List.Item>
                                <List.Item as='li'>Drag the <span style={{ fontWeight: "bold" }}>Excel or CSV</span> file to the dropzone or click the dropzone and select the file you want to upload</List.Item>
                                <List.Item as='li'>Press the Save button to save the file for the level</List.Item>
                            </List>
                            <br />
                            {/* <Divider/> */}
                            <br />
                            <Form>
                                <select name='' value={this.state.currLevel} onChange={(e) => this.handleSelectChange(e)}>
                                    <option value="" disabled>Select the level for which you want to upload a file</option>
                                    {  
                                        levelOptions
                                            .map((item, index) => <option value={item} key={item}>{`${item}`}</option>)
                                    }
                                </select>
                            </Form>
                            <br />
                            <Dropzone
                                accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel, text/csv"
                                onDrop={this.onDrop}
                                // {...rest}
                                className="w-100 h7 dropzone-container"
                                activeClassName="bg-light-green"
                                rejectClassName="bg-light-red"
                                disableClick={this.state.currLevel === ''}
                                name='formFiles'
                            >
                                {({ isDragActive, isDragReject, acceptedFiles, rejectedFiles }) => {
                                    return (
                                        <Fragment>
                                            <Icon name="upload" size="huge"></Icon>
                                            <Header as='h3'
                                                content={acceptedFiles.length || rejectedFiles.length
                                                    ? `Accepted ${acceptedFiles.length}, rejected ${rejectedFiles.length} files`
                                                    : 'Upload behavioral indicator file here.'
                                                }
                                                subheader={acceptedFiles.length || rejectedFiles.length
                                                    ? null
                                                    : 'Drag and drop, or click to select'
                                                }
                                            />
                                        </Fragment>
                                    )
                                }}
                            </Dropzone>
                            <br />
                            { this.state.files.length > 0 &&
                                <aside>
                                    <h4>Dropped files ({this.state.files.length})</h4>
                                    <ul>
                                        {
                                            this.state.files.sort((a, b) => a.level < b.level ? -1 : a.level > b.level ? 1 : 0).map(f => <li key={f.level}>{`${f.level}`} - {f.behaviourFile.name} - {f.behaviourFile.size} bytes</li>)
                                        }
                                    </ul>
                                </aside>
                            }
                            <aside>
                                <h4>Treated Behaviours</h4>
                                <ul>
                                    { Object.prototype.toString.call(behaviours) === '[object Array]' &&
                                        this.props.behaviours.sort((a, b) => a.behaviourLevel > b.behaviourLevel ? 1 : -1).map(item => item.behaviourLevel)
                                                            .reduce((acc, curr, index) => {
                                                                if(index === this.props.behaviours.map(item => item.behaviourLevel).indexOf(curr)){
                                                                    return acc.concat(curr)
                                                                }
                                                                return acc
                                                            }, [])
                                                            .map(item => {
                                                                return (
                                                                    <li key={item}>{item}</li>
                                                                )
                                                            })
                                                            
                                        
                                    }
                                </ul>
                            </aside>
                            <Segment basic textAlign="right">
                                {UPLOAD_BEHAVIOUR_STATUS === true &&
                                    <Label pointing='right' color="green" >Save Successful</Label>
                                }
                                {UPLOAD_BEHAVIOUR_STATUS === false &&
                                    <Label pointing='right' color="red" >Save Failed. Please try again or refresh page.</Label>
                                }
                                <Button content="Submit" color={'blue'} onClick={() => this.handleSubmit()} loading={uploadBehavioursLoadingStatus} disabled={uploadBehavioursLoadingStatus}/>
                            </Segment>
                        </Segment>
                        {/* {!staffSubmitted &&
                            // <Container>
                            //     <Button content="Submit" onClick={this.handleSubmitButtonClick} floated="right" />
                            //     <Button name="/businessobjectives" content="Edit" onClick={this.handleEditButtonClick} floated="right" />
                            // </Container>
                            <Button.Group floated='right'>
                                <Button name="/businessobjectives" content="Edit" onClick={this.handleEditButtonClick} floated="right" />                               
                                <Button.Or />
                                <Button color='blue' content="Submit" onClick={this.handleSubmitButtonClick} floated="right" loading={isSubmittingOKRs}/>
                            </Button.Group>
                        } */}
                        {/* <Button fluid content="Next" onClick={()=>this.handleNextClick()}/> */}
                    </Segment>
                </Grid.Column>
            </Grid>
        )
    }
}

function mapStateToProps(state) {
    return {
        behaviours: state.requestingBehaviours,
        uploadBehavioursLoadingStatus: state.saveLoadingReducer.UPLOAD_BEHAVIOURS_LOADING_STATUS,
        UPLOAD_BEHAVIOUR_STATUS: state.UPLOAD_BEHAVIOUR_STATUS,
    }
}


export default connect(mapStateToProps)(BehavioralSetting)


// .filter(loItem => {
//     const existingItem = this.props.behaviours.sort((a, b) => a.behaviourLevel > b.behaviourLevel ? 1 : -1).map(item => item.behaviourLevel)
//                     .reduce((acc, curr, index) => {
//                         if(index === this.props.behaviours.map(item => item.behaviourLevel).indexOf(curr)){
//                             return acc.concat(curr)
//                         }
//                         return acc
//                     }, [])
//                     .find(item => loItem === item)
//     return loItem !== existingItem
// })