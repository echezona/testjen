import React, { Component } from "react";
import axios from "axios";
import { Progress } from "reactstrap";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import DropdownSelection from "../../dropdown";
import {
  Header,
  List,
  Grid,
  Segment,
  Icon,
  Form,
  Button,
  Divider,
  Container,
  Label
} from "semantic-ui-react";
import Dropzone from "react-dropzone";
import "./index.css";
import { staffBaseUrl } from "../../baseUrl";

//const url = `${staffBaseUrl}/api/Category`;
//const url = "https://pms-api-mainone.test.vggdev.com/api/AppraisalCycle";
//onst url = "https://pms-api-mainone.test.vggdev.com/api/Category"
//console.log(url, "THIS IS THE URL");"
//const url = "https://jsonplaceholder.typicode.com/todos";

//const url = "http://staff-api.test.vggdev.com/api/BusinessUnit";
class StaffList extends Component {
  state = {
    selectedFile: null,
    fileUploading: 0,
    depts: []
  };

  // componentDidMount() {
  //   this.getDept();
  // }

  // getDept = async () => {
  //   const getDepts = await axios.get(url);
  //   console.log("THIS IS THE DEPTS", getDepts);
  //   let { data } = getDepts.data;
  //   this.setState({ depts: data });
  // };

  render() {
    const { depts } = this.state;
    return (
      <div>
        <Grid columns={16}>
          <Grid.Column width={3}></Grid.Column>
          <Grid.Column width={10}>
            <Segment basic>
              <Segment>
                <Header as="h3" content="Click Dropdown to Upload Line Managers, Staffs, SBUs & Departments " />
                <DropdownSelection />
                <List as="ol">
                  <List.Item as="li">
                    Drag the{" "}
                    <span style={{ fontWeight: "bold" }}>Excel or CSV</span>{" "}
                    file to the dropzone or click the dropzone and select the
                    file you want to upload
                  </List.Item>
                  <List.Item as="li">The file uploads automatically </List.Item>
                </List>
              </Segment>
            </Segment>
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}

export default StaffList;
