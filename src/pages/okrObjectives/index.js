//Components
import React from 'react';
import { Grid, Loader, Message, Button, Segment, Label } from 'semantic-ui-react';
import Staff from './Staff';
import Results from './results';
import Header from './Header'
import _ from 'lodash'
import { connect } from "react-redux"
import { withRouter, Switch, Route } from 'react-router-dom'
import { userObjectivesSelector, userIDsSelector, userKeyResultsSelector, userCategoriesSelector, lmIDsSelector, lmCategoriesSelector, lmStaffAppraisalsSelector, lmObjectivesSelector, lmKeyResultsSelector, categorySelector, lmSbuObjectivesSelector } from '../../reducer'
import queryString from 'query-string'
// Css Files
import './index.css';
//Constatnts
import { FETCH_LINE_MANAGER_OKRS, FETCH_CATEGORY, FETCH_LINE_MANAGER_STAFF_APPRAISALS, SUBMIT_LINE_MANAGER_OKRS, FETCH_LMPO_OKRS, STORE_PENDING_KEY_RESULTS, FETCH_SBU_OBJECTIVES, GET_CONTINUE_ENABLED_OBJECTIVES, GET_CONTINUE_ENABLED_KEY_RESULTS, LINE_MANAGER_UPDATE_KEY_RESULTS, FETCH_STAFF_DETAILS } from '../../constants';



class OkrObjectives extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showSubmit: false,
            updatePage: false,
        }
    }

    componentDidMount() {
        const { dispatch, match, location } = this.props;
        const queryValues = queryString.parse(location.search)
        console.log("what is this", queryValues)

        dispatch({ type: FETCH_LMPO_OKRS, staffId: `?staffId=${queryValues.staffId}` })
        dispatch({ type: FETCH_CATEGORY })
        dispatch({ type: FETCH_SBU_OBJECTIVES })
        dispatch({ type: FETCH_LINE_MANAGER_STAFF_APPRAISALS })
        dispatch({ type: FETCH_STAFF_DETAILS })

        document.title = "PMS - Approve OKRs" // set the document title in the browser
    }

    // componentDidUpdate(){
    //     const {lmIDs, lmObjectives, lmKeyResults, lmPendingKeyResults, dispatch, lmStaffAppraisals, match} = this.props
    //     if(lmPendingKeyResults.length < 1 && lmObjectives !== null){
    //         if(lmStaffAppraisals.find(item => item.staffId === match.params.id).secondApproved === "pending"){
    //             dispatch({ type: STORE_PENDING_KEY_RESULTS, keyResults: _.cloneDeep(lmIDs.map(item => lmObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => lmKeyResults[item])) })
    //         } else if (lmStaffAppraisals.find(item => item.staffId === match.params.id).secondApproved === "disapproved"){
    //             dispatch({ type: STORE_PENDING_KEY_RESULTS, keyResults: _.cloneDeep(lmIDs.map(item => lmObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => lmKeyResults[item]).filter(item => item.status === "pending")) })
    //         }
    //     }
    // }

    componentDidUpdate() {
        const { lmIDs, lmKeyResults, lmObjectives, continueEnabledKrs, continueEnabledObjs, dispatch } = this.props

        if(lmObjectives && continueEnabledObjs.length < 1) {
            console.log("butterfly")
            const objectives = lmIDs.map(id => lmObjectives[id])
                                    .map(item => ({
                                        description: item.description,
                                        id: item.id,
                                        categoryId: item.categoryId,
                                        order: item.order,
                                        lineManagerStatus: item.lineManagerStatus,
                                        lineManagerComment: item.lineManagerComment,
                                        sbuObjectiveId: item.sbuObjectiveId,
                                        staffAppraisalId: item.staffAppraisalId,
                                    }))

            const keyResults = lmIDs.map(id => lmObjectives[id].keyResults)
                                    .reduce((acc, curr) => acc.concat(curr) ,[])
                                    .map(item => lmKeyResults[item])

            // console.log("peem-peem-objectives", objectives)
            // console.log("peem-peem-key-results", keyResults)
            
            dispatch({
                type: GET_CONTINUE_ENABLED_OBJECTIVES,
                payload: _.cloneDeep(objectives)
            })

            dispatch({
                type: GET_CONTINUE_ENABLED_KEY_RESULTS,
                payload: _.cloneDeep(keyResults)
            })
        }

        // console.log("passage rights", lmObjectives)

        // console.log("continueEnabledObjs", continueEnabledObjs)
        // console.log("continueEnabledKrs", continueEnabledKrs)
        
    }

    showSubmit = (x) => {
        this.setState({
            showSubmit: x
        })
    }

    iUpdated = () => {
        this.forceUpdate();
    }

    // handleSubmit = () => {
    //     const { lmStaffAppraisals, match, location, history, dispatch, lmIDs, lmObjectives, lmKeyResults, lmPendingKeyResults } = this.props
    //     // console.log("lmstaff")
    //     const queryValues = queryString.parse(location.search)
    //     //turn this execution into a function later
    //     let firstApproved
    //     //if the line manager disapproved any of the objectives or key results, the lineManagerStatus should be "awaiting resubmission"
    //     //else (i.e; if everything was approved, the lineManagerStatus should be "completed")
    //     let lineManagerStatus

    //     if (lmIDs.map(item => lmObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => lmKeyResults[item]).filter(item => item.lineManagerStatus === "disapproved").length > 0 || lmIDs.map(item => lmObjectives[item]).filter(item => item.lineManagerStatus === "disapproved").length > 0) {
    //         firstApproved = "disapproved"
    //         lineManagerStatus = { lineManagerStatus: "awaiting resubmission" }
    //     } else {
    //         firstApproved = "approved"
    //         lineManagerStatus = { lineManagerStatus: "completed" }
    //     }

    //     //move this into the dispatched function for better performance
    //     // lmStaffAppraisals.find(item => item.staffId === queryValues.staffId).firstApproved = firstApproved
    //     // this.iUpdated();

    //     dispatch({ type: SUBMIT_LINE_MANAGER_OKRS, data: { staffAppraisalId: lmIDs.map(item => lmObjectives[item].staffAppraisalId).find(item => item), staffId: queryValues.staffId, firstApproved }, history, lineManagerStatus })
    // }


    handleSubmit = () => {
        const { dispatch, location, history, lmStaffAppraisals, lmIDs, lmObjectives, lmKeyResults, staffDetails } = this.props;
        const queryValues = queryString.parse(location.search);

        
        const OBJs = lmIDs.map(id => lmObjectives[id])
                                .map(item => ({
                                    description: item.description,
                                    id: item.id,
                                    categoryId: item.categoryId,
                                    order: item.order,
                                    lineManagerStatus: item.lineManagerStatus,
                                    lineManagerComment: item.lineManagerComment,
                                    peopleOpsStatus: item.peopleOpsStatus,
                                    peopleOpsComment: item.peopleOpsComment,
                                    sbuObjectiveId: item.sbuObjectiveId,
                                    staffAppraisalId: item.staffAppraisalId,
                                }))

        const KRs = lmIDs.map(id => lmObjectives[id].keyResults)
                                .reduce((acc, curr) => acc.concat(curr) ,[])
                                .map(item => lmKeyResults[item])

        const id = lmIDs.map(item => lmObjectives[item].staffAppraisalId).find(item => item)

        let firstApproved;
        let lineManagerStatus;
        let lineManagerTwoApproved;
        let superLineManagerStatus;
        let staffSubmitted;

        if (lmIDs.map(item => lmObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => lmKeyResults[item]).filter(item => item.lineManagerStatus === "disapproved").length > 0 || lmIDs.map(item => lmObjectives[item]).filter(item => item.lineManagerStatus === "disapproved").length > 0) {
            firstApproved = "disapproved"
            lineManagerStatus = "awaiting resubmission"
            staffSubmitted = false
        } else {
            firstApproved = "approved"
            lineManagerStatus = "completed"
            staffSubmitted = true
        }

        if (firstApproved === "approved" && staffDetails.isFinalApproval) {
            lineManagerTwoApproved = "approved";
            superLineManagerStatus = "completed";
        } else {
            lineManagerTwoApproved = "pending";
            superLineManagerStatus = "pending";
        }

        const forSubmitLineManagerOkrs = {
            data: {staffAppraisalId: id, staffId: queryValues.staffId, firstApproved}, 
            history, lineManagerStatus, lineManagerTwoApproved,
            superLineManagerStatus, staffSubmitted
        }

        console.log("objsandkrs", OBJs, KRs)
        
        console.log("staffAprId", id)

        console.log('see me', forSubmitLineManagerOkrs)

        dispatch({ type: LINE_MANAGER_UPDATE_KEY_RESULTS, KRs, OBJs, id, forSubmitLineManagerOkrs })

    }

    render() {
        const { LINE_MANAGER_UPDATE_KEY_RESULTS_STATUS, IS_REQUESTING_OKRs, sbuObjectives, isFetchingStaffAppraisals, isFetchingLMPOOKRs, match, lmIDs, lmObjectives, lmStaffAppraisals, lmCategories, categories, lmKeyResults, lmSbuObjectives, continueEnabledObjs, continueEnabledKrs, submitLineManagerOkrsLoadingStatus } = this.props
        //create a variable to hold the parsed values kept in the query parameter
        const queryValues = queryString.parse(this.props.location.search)
        //submitenabled checks whether all keyresults have their status as either "approved" or "disapporved" which woiuld 
        //indicate that they have been attended to
        const submitEnabled = lmIDs.map(item => lmObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => lmKeyResults[item]).every(item => item.lineManagerStatus !== "pending")
        //submitenabled2 checks whether all keyresults that have their status as "disapproved" also have content in their
        //lineManagerComment property
        const submitEnabled2 = lmIDs.map(item => lmObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => lmKeyResults[item]).filter(item => item.lineManagerStatus === "disapproved").every(item => item.lineManagerComment)

        const submitEnabled3 = lmIDs.map(item => lmObjectives[item]).every(item => item.lineManagerStatus !== "pending")

        const submitEnabled4 = lmIDs.map(item => lmObjectives[item]).filter(item => item.lineManagerStatus === "disapproved").every(item => item.lineManagerComment)

        //continue enabled version of submitted enabled satrts here

        const ceSubmitEnabled = continueEnabledKrs.filter(item => item).every(item => item.lineManagerStatus !== "pending")
        const ceSubmitEnabled2 = continueEnabledKrs.filter(item => item).filter(item => item.lineManagerStatus === "disapproved").every(item => item.lineManagerComment)
        const ceSubmitEnabled3 = continueEnabledObjs.filter(item => item).every(item => item.lineManagerStatus !== "pending")
        const ceSubmitEnabled4 = continueEnabledObjs.filter(item => item).filter(item => item.lineManagerStatus === "disapproved").every(item => item.lineManagerComment)

        console.log('staffDets', this.props.staffDetails)

        let firstApproved
        if (lmIDs.map(item => lmObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => lmKeyResults[item]).filter(item => item.lineManagerStatus === "disapproved").length > 0 || lmIDs.map(item => lmObjectives[item]).filter(item => item.lineManagerStatus === "disapproved").length > 0) {
            firstApproved = "disapproved"
        } else if (lmIDs.map(item => lmObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => lmKeyResults[item]).filter(item => item.lineManagerStatus === "pending").length > 0 || lmIDs.map(item => lmObjectives[item]).filter(item => item.lineManagerStatus === "pending").length > 0) {
            firstApproved = "pending"
        } else {
            firstApproved = "approved"
        }

        console.log("lmObjs", lmObjectives)

        if (isFetchingLMPOOKRs && isFetchingStaffAppraisals) {
            return <Loader active />
        }

        return (
            <Grid id="okrObjectives" padded>
                <Header id={queryValues.staffId}
                    disapprovedKRs={lmIDs.map(item => lmObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => lmKeyResults[item]).filter(item => item.lineManagerStatus === "disapproved").length}
                    totalKRs={lmIDs.map(item => lmObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).length}
                    treatedKRs={lmIDs.map(item => lmObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => lmKeyResults[item]).filter(item => item.lineManagerStatus !== "pending").length}
                    totalOBJs={lmIDs.length}
                    treatedOBJs={lmIDs.map(item => lmObjectives[item]).filter(item => item.lineManagerStatus !== "pending").length}
                    disapprovedOBJs={lmIDs.map(item => lmObjectives[item]).filter(item => item.lineManagerStatus === "disapproved").length}
                />
                {/* <br/> */}
                {(lmStaffAppraisals.length > 0 && lmStaffAppraisals.find(item => item.staffId === queryValues.staffId).lineManagerTwoApproved === "disapproved" && lmStaffAppraisals.find(item => item.staffId === queryValues.staffId).firstApproved !== "disapproved") &&
                    <Grid.Row>
                        <Grid.Column largeScreen={16}>
                            <Message color="blue" fluid>
                                <Message.Header> Here's the comment from your Line Manager </Message.Header>
                                <p>{lmStaffAppraisals.find(item => item.staffId === queryValues.staffId).superLineManagerComment}</p>
                            </Message>
                        </Grid.Column>
                    </Grid.Row>
                }
                <Grid.Column largeScreen={6} wideScreen={6} computer={6} tablet={6} mobile={16}>
                    <Staff userIDs={lmIDs}
                        userObjectives={lmObjectives}
                        userCategories={categories}
                        sbuObjectives={sbuObjectives}
                        lmSbuObjectives={lmSbuObjectives}
                    />
                </Grid.Column>
                <Grid.Column largeScreen={10} wideScreen={10} computer={10} tablet={10} mobile={16}>
                    <Switch>
                        {lmIDs.map((item, index) => <Route key={item} path={`${match.path}/${item}`} render={(props) => <Results showSubmit={(x) => this.showSubmit(x)}
                            iUpdated={this.iUpdated}
                            continueEnabledKrs={continueEnabledKrs} continueEnabledObjs={continueEnabledObjs}
                            objIndex={index} keyResults={lmKeyResults}
                            userCategories={lmCategories} categoryId={lmObjectives[item].categoryId}
                            id={item} objective={lmObjectives[item]} staffId={`?staffId=${queryValues.staffId}`}
                            isFirstSave={lmStaffAppraisals.filter(item => item.staffId === queryValues.staffId).every(item => item.lineManagerStatus !== "in progress")}
                            secondApproved={lmStaffAppraisals.find(item => item.staffId === queryValues.staffId).secondApproved}
                            {...props} />} />)}
                    </Switch>
                    {this.state.showSubmit &&
                        <Segment basic textAlign="right">
                            {LINE_MANAGER_UPDATE_KEY_RESULTS_STATUS && LINE_MANAGER_UPDATE_KEY_RESULTS_STATUS.status === true &&
                                <Label pointing='right' color="blue" >Save successful</Label>
                            }
                            {LINE_MANAGER_UPDATE_KEY_RESULTS_STATUS && LINE_MANAGER_UPDATE_KEY_RESULTS_STATUS.status === false &&
                                <Label pointing='right' color="red" >Save Failed. Please try again or refresh page.</Label>
                            }
                            <Button content={firstApproved === "disapproved" ? "Send to direct report" : firstApproved === "approved" ? "Submit to Line Manager" : "Submit"}
                                disabled={!(submitEnabled && submitEnabled2 && submitEnabled3 && submitEnabled4)}
                                // disabled={!(ceSubmitEnabled && ceSubmitEnabled2 && ceSubmitEnabled3 && ceSubmitEnabled4 && submitEnabled && submitEnabled2 && submitEnabled3 && submitEnabled4)}
                                onClick={() => this.handleSubmit()}
                                color='blue'
                                icon='right arrow'
                                labelPosition='right'
                                loading={submitLineManagerOkrsLoadingStatus}
                            />
                        </Segment>
                    }

                </Grid.Column>
            </Grid>
        )
    }
}

function mapStateToProps(state) {
    return {
        IS_REQUESTING_OKRs: state.requestingOKRs,
        OKRs: state.setOKRs,
        lmIDs: lmIDsSelector(state),
        lmObjectives: lmObjectivesSelector(state),
        lmKeyResults: lmKeyResultsSelector(state),
        lmCategories: lmCategoriesSelector(state),
        lmStaffAppraisals: lmStaffAppraisalsSelector(state),
        lmSbuObjectives: lmSbuObjectivesSelector(state),
        categories: categorySelector(state),
        isFetchingStaffAppraisals: state.isFetchingStaffAppraisals,
        isFetchingLMPOOKRs: state.isFetchingLMPOOKRs,
        sbuObjectives: state.sbuObjectives,
        continueEnabledObjs: state.continueEnabledLineManagerObjectives,
        continueEnabledKrs: state.continueEnabledLineManagerKeyResults,
        submitLineManagerOkrsLoadingStatus: state.saveLoadingReducer.SUBMIT_LINE_MANAGER_OKRS_LOADING_STATUS,
        LINE_MANAGER_UPDATE_KEY_RESULTS_STATUS: state.LINE_MANAGER_UPDATE_KEY_RESULTS_STATUS,
        staffDetails: state.requestingStaffDetails,
    }
}

export default connect(mapStateToProps)(OkrObjectives)

// {lmIDs.map(item => lmObjectives[item].keyResults).reduce((a, b) => a.concat(b), []).map(item => lmKeyResults[item]).every(item => item.status === "pending")}