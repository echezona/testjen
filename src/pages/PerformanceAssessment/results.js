import React, {Fragment} from 'react';
import { Segment,Header,Button,Popup,Label,Icon,Message } from 'semantic-ui-react';
import KeyResult from './KeyResult'
import {SAVE_KEY_RESULTS, SUBMIT_OKRS} from '../../constants'
import {connect} from 'react-redux'


class Results extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            description: props.objective.description,
            id: props.objective.id,
            categoryId: props.objective.categoryId,
            order: props.objective.order,
            lineManagerStatus: props.objective.lineManagerStatus,
            lineManagerComment: props.objective.lineManagerComment,
            sbuObjectiveId: props.objective.sbuObjectiveId,
            staffAppraisalId: props.objective.staffAppraisalId,
            peopleOpsStatus: props.objective.peopleOpsStatus,
            peopleOpsComment: props.objective.peopleOpsComment,
            keyResults: props.objective.keyResults.map(item => props.keyResults[item]),
            popupOpen: false,
        }
    }

    componentWillUnmount(){
        const {order} = this.props.objective;
        console.log(`Now unmounting from Objective ${order}`)
    }

    handleCommentEdit = (e, order) => {
        const {keyResults} = this.state;
        const {value} = e.target;
        let KRs = [...keyResults]
        KRs.find(item => item.order === order).staffComment = value;
        this.setState({
            keyResults: KRs
        })
        this.props.iUpdated();
    }

    // handleUploading = (e, order) => {
    //     const {keyResults} = this.state;
    //     const {value} = e.target;
    //     let KRs = [...keyResults]
    //     KRs.find(item => item.order === order).supportingDocuments = value;
    //     this.setState({
    //         keyResults: KRs
    //     })
    //     this.props.iUpdated();
    // }

    handleRating = (e, order) => {
        const {keyResults} = this.state;
        const {name} = e.target;
        let KRs = [...keyResults]
        KRs.find(item => item.order === order).staffRating = +name;
        // console.log("KRs: ", KRs)
        this.setState({
            keyResults: KRs
        })
        this.props.iUpdated();
    }

    handleSupportingDocsAdd = (order, link) => {
        const {keyResults} = this.state;
        let KRs = [...keyResults]
        // let supportingDocuments = KRs.find(item => item.order === order).supportingDocuments
        // if(!supportingDocuments) {
        //     KRs.find(item => item.order === order).supportingDocuments = this.handleParsing(KRs.find(item => item.order === order).supportingDocumentsLinks) || []
        // }
        KRs.find(item => item.order === order).supportingDocumentsLinks = link;
        console.log("pls work", KRs.find(item => item.order === order).supportingDocumentsLinks, KRs)
        // console.log("KRs: ", KRs)
        this.setState({
            keyResults: KRs
        })
        this.props.iUpdated();
    }

    handleSupportingDocsRemove = (order, linkToRemove, index) => {
        const {keyResults} = this.state;
        let KRs = [...keyResults]
        // let supportingDocuments = KRs.find(item => item.order === order).supportingDocuments
        // if(!supportingDocuments) {
        //     KRs.find(item => item.order === order).supportingDocuments = this.handleParsing(KRs.find(item => item.order === order).supportingDocumentsLinks) || []
        // }
        KRs.find(item => item.order === order).supportingDocumentsLinks = ""
        console.log("conce", KRs.find(item => item.order === order).supportingDocumentsLinks, linkToRemove)
        this.setState({
            keyResults: KRs
        })
        this.props.iUpdated();
    }
 
    goToBehavioral = () => {
        const {history} = this.props;
        history.push('/BehavioralAssessment')
    }

    handleSubmit = () => {
        const {dispatch} = this.props
        dispatch({ type: SUBMIT_OKRS, fromAssessmentFlow: true })
    }

    // handleParsing = (str) => {
    //     const canParse = (str) => {
    //         try {
    //             JSON.parse(str);
    //         } catch (e) {
    //             return false;
    //         }
    //         return true;
    //     }
        
    //     if(canParse(str)) {
    //         return JSON.parse(str)
    //     }

    //     return null
    // }


    handleSaveClick = () => {
        const {id, categoryId, sbuObjectiveId, staffAppraisalId, keyResults, description, order, lineManagerStatus, lineManagerComment, peopleOpsStatus, peopleOpsComment} = this.state;
        const { dispatch } = this.props
        
        dispatch({  type: SAVE_KEY_RESULTS, 
                    data: { id, order, lineManagerStatus, lineManagerComment, 
                        peopleOpsStatus, peopleOpsComment, description, categoryId,
                        sbuObjectiveId, staffAppraisalId, keyResults 
                    }, 
                    staffId: this.props.staffId 
        })
        console.log("something", { id, order, lineManagerStatus, lineManagerComment, 
            peopleOpsStatus, peopleOpsComment, description, categoryId,
            sbuObjectiveId, staffAppraisalId, keyResults 
        })
        this.props.iUpdated();
    }


    // handleSaving = (respObj, index, objId) => {
    //     if (this.state.hasBeenUpdated !== true){
    //         this.setState(prevState => ({
    //             hasBeenUpdated: true,
    //             keyResults: prevState.keyResults.splice(index, 0, respObj)
    //         })) 
    //     }
            
    // }

    // componentWillUpdate(nextProps, nextState){
    //     this.responsePackage.objectiveId = this.props.id
    //     this.responsePackage.keyResults = nextState.keyResults
    // }

    handlePopup = () => {
        this.setState(prevState => ({ popupOpen: !prevState.popupOpen }))
    }


    render(){
        
        const { SUBMIT_OKRS_STATUS, SAVING_OBJECTIVE_STATUS, objective, objIndex, userCategories, categoryId, buttonLoadingStatus, behavioralIndicators, userIDs, userObjectives, userKeyResults, isSubmittingOKRs } = this.props
        console.log("KEY_RESULTS", behavioralIndicators)
        const { keyResults, description, popupOpen } = this.state
        const CONTINUE_TO_BEHAVIORAL_ASSESSMENT_ACTIVE = userIDs.map(item => userObjectives[item].keyResults).reduce((acc, item) => acc.concat(...item), []).map((item) => userKeyResults[item]).every((item) => item.staffRating > 0 && item.staffComment !== null && item.staffComment !== "")
        const SUBMIT_ACTIVE = behavioralIndicators.every((item) => item.staffRating > 0 && item.staffComment)

        //Summary values start here
        const staffKeyResults = userIDs.map(item => userObjectives[item].keyResults)
                                    .reduce((acc, item) => acc.concat(item), [])
                                    .map(item => userKeyResults[item])

        const staffPerformanceSum = staffKeyResults.reduce((acc, curr) => acc + curr.staffRating, 0)

        const staffPerformanceScore = ((staffPerformanceSum / (staffKeyResults.length * 5)) * 100)

        const staffBehaviorSum = behavioralIndicators.reduce((acc, curr) => acc + curr.staffRating,  0)

        const staffBehaviorScore = ((staffBehaviorSum / (behavioralIndicators.length * 5)) * 100)

        const staffTotalScore = ((staffPerformanceScore / 10) * 7) + ((staffBehaviorScore / 10) * 3)
        
        console.log("SUBMIT_ACTIVE", SUBMIT_ACTIVE, keyResults)
        return(
            <Fragment>
                <Header as="h2" textAlign="center" content="Performance Assessment"/>
                {/* <Header style={{display: "inline"}} as="h4" content={userCategories[categoryId].name === "Business" ? `Business Objective ${objIndex + 1}` : `Personal Objective`} subheader={objective.description}/> */}
                <Segment>
                    <div>
                        <Header as="h5" content={`Business Objective ${objIndex + 1}`} dividing/>
                        { keyResults.map((item,index) => <KeyResult index={index} data={item} order={item.order} id={item.id} objId={this.props.id} key={item.id} handleCommentEdit={this.handleCommentEdit} handleRating={this.handleRating} handleSupportingDocsAdd={this.handleSupportingDocsAdd} handleSupportingDocsRemove={this.handleSupportingDocsRemove} handleParsing={this.handleParsing} iUpdated={this.props.iUpdated}/>) }
                    </div>
                    <br/>
                    <Segment basic textAlign="right">
                        
                        { SAVING_OBJECTIVE_STATUS === true && 
                            <Label pointing='right' color="green" >Save Successful</Label>
                        }
                        { SAVING_OBJECTIVE_STATUS === false && 
                            <Label pointing='right' color="red" >Save Failed. Please try again or refresh page.</Label>
                        }
                        <Button content="Save" loading={buttonLoadingStatus} disabled={buttonLoadingStatus} basic onClick={this.handleSaveClick}/>
                        { !SUBMIT_ACTIVE &&
                            <Button content="Continue to Behavioral Assessment" color='blue' disabled={!CONTINUE_TO_BEHAVIORAL_ASSESSMENT_ACTIVE} onClick={this.goToBehavioral}/>
                        }
                        { SUBMIT_ACTIVE &&
                            <Popup open={popupOpen} trigger={<Button color='blue' onClick={this.handlePopup}>View Scores and Submit</Button>} on='click' hoverable>
                                {/* warning message */}
                                <Message warning style={{ marginBottom: '10px' }}>
                                    <Icon name='warning sign' style={{ color: '#ffa500' }}/> <span style={{ fontSize: '0.9em', fontWeight: 'bold' }}>Ensure that you save all changes made before submitting!</span>
                                </Message>
                                <Header as='h4'>Assessment Scores</Header>
                                <p>Performance: {staffPerformanceScore.toFixed(1)}</p>
                                <p>Behavior: {staffBehaviorScore.toFixed(1)}</p>
                                <p style={{fontWeight: 'bold', fontSize: '1.2em'}}>Total: {staffTotalScore.toFixed(1)}</p>
                                <br/>
                                
                                <div style={{fontWeight: 'bold', fontSize: '0.85em'}}>Are you sure you want to submit?</div>
                                <div>
                                    { SUBMIT_OKRS_STATUS === true && 
                                        <Label color="green" >Submission Successful</Label>
                                    }
                                    { SUBMIT_OKRS_STATUS === false && 
                                        <Label color="red" >Submission Failed. Please try again or refresh page.</Label>
                                    }
                                </div>
                                <div style={{marginTop: '5px', display: 'flex'}}>
                                    <Button content="Yes, submit" disabled={!CONTINUE_TO_BEHAVIORAL_ASSESSMENT_ACTIVE || buttonLoadingStatus} loading={isSubmittingOKRs} onClick={this.handleSubmit} color='blue' basic/>
                                    <Button color='red' basic onClick={this.handlePopup}>No, close this</Button>
                                </div>
                            </Popup>
                        }
                    </Segment>
                </Segment>
            </Fragment>
        )
    }
}



export default Results