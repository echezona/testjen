import React, { Fragment } from 'react'
import { Tab, Header, Accordion, Segment, Icon, Menu } from 'semantic-ui-react'
// import {withRouter,Link} from 'react-router-dom'
import queryString from 'query-string'


class Staff extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            activeIndex: -1,
        }
    }

    sortFunctionOne = (a, b) => {
        const { userObjectives } = this.props
        if (userObjectives[a].order < userObjectives[b].order) {
            return -1
        } else if (userObjectives[a].order > userObjectives[b].order) {
            return 1
        } else {
            return 0
        }
    }

    handleClick = (e, titleProps) => {
        const { index, name } = titleProps
        const { activeIndex } = this.state
        const { history } = this.props
        const newIndex = activeIndex === index ? -1 : index
        history.push(name)
        this.setState({
            activeIndex: newIndex
        })
    }

    render(){
        const { userIDs, userObjectives, userCategories, sbuObjectives, match } = this.props
        const {activeIndex} = this.state
        const queryValues = queryString.parse(this.props.location.search)
        return(
            <Fragment>
                <Header as="h3" textAlign="center" content="Staff Objectives" />
                <Segment basic>
                    <Accordion styled>
                        {userIDs.sort(this.sortFunctionOne).map((item, index) => {
                            const categoryId = userObjectives[item].categoryId;
                            const category = () => userCategories.length > 0 ? userCategories.find(item => item.id === categoryId).name : "";
                            console.log("find me", userCategories)
                            return (
                                <Fragment key={item}>
                                    <Accordion.Title active={activeIndex === index } index={index} onClick={(e, titleProps) => this.handleClick(e, titleProps)} name={`${match.url}/${item}?staffId=${queryValues.staffId}`}>
                                        <Icon name='dropdown' />
                                        {category() === "Business" && `Business Objective ${index + 1}`} {category() === "Personal" && `Personal Objective`}
                                    </Accordion.Title>
                                    <Accordion.Content active={activeIndex === index}>
                                        <Header as="h3" content="Staff" />
                                        <p>
                                            {userObjectives[item].description}
                                        </p>
                                        {category() === "Business" &&
                                            <Fragment>
                                                <Header as="h3" content="SBU"/>
                                                <p>
                                                    {sbuObjectives.find(sbuItem => sbuItem.id === userObjectives[item].sbuObjectiveId) 
                                                        ? sbuObjectives.find(sbuItem => sbuItem.id === userObjectives[item].sbuObjectiveId).description
                                                        : ""
                                                    }
                                                </p>
                                            </Fragment>
                                        }
                                    </Accordion.Content>
                                </Fragment>
                            )
                        })
                        }
                    </Accordion>
                </Segment>
            </Fragment>
        )
    }
}

export default Staff