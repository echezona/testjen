import React from 'react'
import { Card, Segment } from 'semantic-ui-react'

const ExceptionHandler = (props) => {

    const messages = {
        NO_STAFF_APPRAISAL: "No Staff Appraisal exist yet",
        STAFF_DOES_NOT_EXIST: "The Staff requesting this Staff Appraisal does not exist",
        SBU_OBJECTIVES_DO_NOT_EXIST: "No SBU Objective",
        //appraisalCycle
        NO_APPRAISAL_EXIST_YET: "No Appraisal exist yet",
    }
    
    let screenContent = "The time window for assessing OKRs has not been activated by PeopleOps hence you'll be unable to assess your OKRs during this period";

    if (props.appraisalCycle.message === messages.NO_APPRAISAL_EXIST_YET) {
        screenContent = "An appraisal cycle period has not yet been set by People Ops";
    } else if (props.appraisalCycle.okrAssessmentLock ===true) {
        screenContent = "The time window for assessing OKRs has not been activated by PeopleOps hence you'll be unable to assess your OKRs during this period";
    } else if (typeof props.behavioralIndicators === 'string' || props.behavioralIndicators instanceof String) {
        screenContent = props.behavioralIndicators
    } else if (!props.behavioralIndicators.data) {
        screenContent = props.behavioralIndicators.message
    } 
    
    if (props.userIDs.length < 1) {
        screenContent = "We can't find your OKRs. Please confirm with PeopleOps that you successfully set your objectives during the appraisal setting period";
    }
    
    // if(props.appraisal.message === messages.NO_STAFF_APPRAISAL) {
    //     screenContent = "It seems you haven't started creating your staff appraisal yet. Click the button below to begin."
    // } else if(props.appraisal.message === messages.STAFF_DOES_NOT_EXIST) {
    //     screenContent = "It seems that you don't exist as a user on the PMS system. Please escalate this to PeopleOps to get it sorted."
    // } else if(props.sbuObjectives.message.includes(messages.SBU_OBJECTIVES_DO_NOT_EXIST)) {
    //     screenContent = "It appears that no SBU Objectives have been set for your SBU. Please refresh the page and notify PeopleOps to rectify this."
    // } else {
    //     screenContent = "Nothing to show"
    // }

    return(
        <Segment basic padded>
            <Card centered>
                <Card.Header as="h2" textAlign='center'>
                    HELLO
                </Card.Header>
                <Card.Content>
                    {/* <span style={props.appraisal.error ? errorContentStyle : null}>{screenContent}</span> */}
                    {screenContent}
                </Card.Content>
            </Card>
        </Segment>
    )
}

export default ExceptionHandler