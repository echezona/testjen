import React, {Fragment} from 'react'
import { Segment,Grid,Header,Button,Form,Input,List,Icon,Popup } from 'semantic-ui-react'
import "./index.css"

// const buttonPopupContent = {
//     one: "I have not at all met the conditions outlined for me",
//     two: "I have met some of the conditions outlined for me",
//     three: "I have met most of the conditions outlined for me",
//     four: "I have exceeded all of the conditions outlined for me",
//     five: "I have superbly exceeded conditions outlined for me",
// }

class KeyResult extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            isViewingKey: true,
            isCommenting: false,
            isAttaching: false,
            commentValue: this.props.data.staffComment,
            rating: this.props.data.staffRating,  // approved, disapproved, pending,
            link: this.props.data.supportingDocumentsLinks, // || this.props.handleParsing(this.props.data.supportingDocumentsLinks) || [],
            fileLink: "", 
            icon: this.props.data.staffRating !== 0 && this.props.data.staffComment ? "check" : "cancel",
        }
    }

    componentDidUpdate() {
        // const { fileLink, links } = this.state
        console.log("state", this.props.data, this.props.data.supportingDocumentsLinks )
    }

    static getDerivedStateFromProps(props, state) {
        if (props.data.supportingDocumentsLinks !== state.link) {
            return { link: props.data.supportingDocumentsLinks }
        }

        return null
    }

    // localGetResponseObject = (nxt) => {
    //     const {index, data, id, objId} = this.props
    //     const {rating, commentValue} = nxt

    //     let respObj = {
    //         id: id,
    //         description: data.description,
    //         staffRating: rating,
    //         lineManagerRating: data.lineManagerRating,
    //         staffComment: commentValue,
    //         lineManagerComment: data.lineManagerComment,
    //         appraisalObjectiveId: data.appraisalObjectiveId,
    //     }
    //     this.props.handleSaving(respObj, index, objId)
    // }

    handleKeyButtonClick = () => {
        this.setState({
            isViewingKey: true,
            isCommenting: false,
            isAttaching: false,
        })
    }

    handleCommentButtonClick = () => {
        this.setState({
            isCommenting: true,
            isViewingKey: false,
            isAttaching: false,
        })
    }

    handleAttachmentButtonClick = () => {
        this.setState({
            isAttaching: true,
            isViewingKey: false,
            isCommenting: false,
        })
    }

    handleCommentChange = (e) => {
        const {value} = e.target
        const {rating} = this.state;
        this.setState({
            commentValue: value,
            icon: rating > 0 && value ? "check" : "cancel"
        })
    }

    handleRatingClick = (e) => {
        const {name} = e.target
        const {commentValue} = this.state;
        this.setState({
            rating: parseInt(name),
            icon: commentValue ? "check" : "cancel",
        })
        
    }

    // handleFileUpload = (e) => {
    //     const {files} = e.target
    //     this.setState({
    //         links: files
    //     })
    //     console.log(this.state.links)
    // }

    handleFileLinkAdd = () => {
        // const {fileLink, links} = this.state;

        // if (!fileLink) return

        this.setState(prevState => {
            // let newFile = [...prevState.links]
            // newFile.push(fileLink)
            return {
                // links: newFile,
                fileLink: ""
            }
        })
    }

    handleFileLinkChange = (e) => {
        const {value} = e.target;
        this.setState({ fileLink: value })
    }

    handleLinkClick = (e) => {
        const link = e.target.innerHTML
        window.open(link, '_blank')
    }

    fileAdder = () => {
        if (!this.state.fileLink) return
        this.handleFileLinkAdd();
        this.props.handleSupportingDocsAdd(this.props.order, this.state.fileLink)
    }

    fileRemover = (link, index) => {
        this.props.handleSupportingDocsRemove(this.props.order, link, index)
    }

    render(){
        const { rating, commentValue, fileLink, link } = this.state;
        // const {one,two,three,four,five} = buttonPopupContent;
        const {index, data, order, handleCommentEdit, handleRating} = this.props;

        return(
            <Segment>
                <Grid>
                    { this.state.isViewingKey && 
                        <Grid.Column width={14}>
                            <div>
                                <h5 style={{marginRight:'10px', display:'inline-block'}}> {`Key Result ${index + 1}`} </h5>
                                <Icon className="keyResultIcon" size="small" name={this.state.icon} color={this.state.icon === "cancel" ? "red" : "green"} circular inverted padded/>
                            </div>
                            {data.description}
                            <br/>   
                            <Segment basic textAlign="right">
                                <Button content="1" compact color={rating===1 ? "red" : "green"} onClick={(e) => {this.handleRatingClick(e); handleRating(e, order)}} name={1}/>
                                <Button content="2" compact color={rating===2 ? "red" : "green"} onClick={(e) => {this.handleRatingClick(e); handleRating(e, order)}} name={2}/>
                                <Button content="3" compact color={rating===3 ? "red" : "green"} onClick={(e) => {this.handleRatingClick(e); handleRating(e, order)}} name={3}/>
                                <Button content="4" compact color={rating===4 ? "red" : "green"} onClick={(e) => {this.handleRatingClick(e); handleRating(e, order)}} name={4}/>
                                <Button content="5" compact color={rating===5 ? "red" : "green"} onClick={(e) => {this.handleRatingClick(e); handleRating(e, order)}} name={5}/>                         
                            </Segment>
                        </Grid.Column>
                    }
                    { this.state.isCommenting && 
                        <Grid.Column width={14}>
                            <div>
                                <h5 style={{marginRight:'10px', display:'inline-block'}}> {`Key Result ${index + 1}`} </h5>
                                <Icon className="keyResultIcon" size="small" name={this.state.icon} color={this.state.icon === "cancel" ? "red" : "green"} circular inverted padded/>
                            </div>                            
                            <Form>
                                <Form.TextArea 
                                    name="textfield" 
                                    onChange={(e) => {this.handleCommentChange(e); handleCommentEdit(e, order)}} 
                                    value={commentValue}
                                    placeholder="Enter your comment here"/>
                                {/* <Form.Button color="blue" floated="right">Submit Comment</Form.Button> */}
                            </Form>
                        </Grid.Column>
                    }
                    { this.state.isAttaching && 
                        <Grid.Column width={14}>
                            <div>
                                <h5 style={{marginRight:'10px', display:'inline-block'}}> {`Key Result ${index + 1}`} </h5>
                                <Icon className="keyResultIcon" size="small" name={this.state.icon} color={this.state.icon === "cancel" ? "red" : "green"} circular inverted padded/>
                            </div>
                            {/* <Header as="h6" content="Please upload a file to support the rating you've provided. Note that you're only allowed a single upload per Key Result so if multiple files exist, merge them into a single PDF doc and upload that."/>
                            <Input type="file" onChange={this.handleFileUpload}/> */}
                            <div style={{display: "flex", justifyContent: "space-between"}} onSubmit={this.fileAdder}>
                                <Form style={{ flex: 1, marginRight: '10px' }}>
                                    <Form.Input 
                                        name="fileLink" 
                                        onChange={this.handleFileLinkChange} 
                                        value={fileLink}
                                        placeholder="Please provide a link to your supporting document"
                                    />
                                </Form>
                                <div>
                                    <Button circular icon="add" color="orange" onClick={this.fileAdder} />
                                </div>
                            </div>
                            <List >
                                {/* {links.map((link, index) => ( */}
                                    <List.Item key={index}>
                                        <a onClick={this.handleLinkClick}>{link}</a>
                                        {link && <Icon name="cancel" size='small' color='red' onClick={(e) => this.fileRemover(link, index)} style={{ display: "inline", marginLeft: '10px', cursor: 'pointer' }}/>}
                                    </List.Item>
                                {/* ))} */}
                            </List>
                        </Grid.Column>
                    }
                    <Grid.Column width={2} textAlign="right">
                        <Button.Group vertical>
                            <Button active={this.state.isViewingKey===true ? true : false} icon="key" attached="right" onClick={this.handleKeyButtonClick}/>
                            <Button active={this.state.isCommenting===true ? true : false} icon="comments" attached="right" onClick={this.handleCommentButtonClick}/>
                            <Button active={this.state.isAttaching===true ? true : false} icon="paperclip" attached="right" onClick={this.handleAttachmentButtonClick}/>
                        </Button.Group>
                    </Grid.Column>
                </Grid>
            </Segment>
        )
    }
}


export default KeyResult