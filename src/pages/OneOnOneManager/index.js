//Components
import React, { Fragment } from 'react';
import _ from 'lodash'
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux'
import { Form, Grid, Header, Table, Segment, Button, Card, Input, Loader, Pagination, Dropdown, Icon } from 'semantic-ui-react';

// Css Files
import './index.css';
import { FETCH_LINE_MANAGER_OKRS, FETCH_LINE_MANAGER_STAFF_APPRAISALS, FETCH_PEOPLE_OPS_STAFF_APPRAISALS, FETCH_PEOPLE_OPS_COUNT, SUBMIT_PEOPLE_OPS_OKRS, FETCH_ALL_STAFF } from '../../constants';
import { poStaffAppraisalsSelector } from '../../reducer';
import { ToastContainer } from "react-toastify";

class OneOnOneManager extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            column: null,
            data: [],
            direction: null,
            filterOptions: [
                { key: "0", text: "All", value: "all"},
                { key: "1", text: "Completed", value: "completed"},
                { key: "2", text: "Uncompleted", value: "uncompleted"},
            ],
            filterBy: "all",
            // results: [],
            value: " ",
            isLoading: false,
            searching: false,
            activePage: 1,
            totalPages: 0,
            itemsPerPage: 10,
            indexStart: 0, 
            indexEnd: 9,
            resetStatusModalOpen: false,
        }
    }

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch({ type: FETCH_LINE_MANAGER_STAFF_APPRAISALS })
    }

    // resetComponent = () => this.setState({ isLoading: false, data: this.props.poStaffAppraisals, searching: false })

    handleSearchChange = (e) => {
        const { value } = e.target;
        this.setState({ isLoading: true, value, searching: true, activePage: 1 });
    }

    searchFilterFunction = (item, index) => {
        const { value } = this.state
        const re = new RegExp(_.escapeRegExp(value), 'i')
        if (!value.trim()) {
            return true;
        }
        return (
            re.test(item.fullName) ||
            re.test(item.department) ||
            re.test(item.sbu) 
            // re.test(item.currentLevel) ||
            // re.test(item.businessUnit.name) ||
            // re.test(item.department.name) ||
            // re.test(item.lineManager.name)
        )
    } 

    pageChunker = (acc, curr, idx, arr) => { 
        const { itemsPerPage } = this.state;
        acc[1].push(curr);
        if (acc[1].length === itemsPerPage) {
            acc[0].push(acc[1])
            acc[1] = []
        }
        if (arr.length - 1 === idx) {
            acc[0].push(acc[1])
        }

        return acc
        // const acc = [[], []]
    }

    gotoDetail = (id, email, reportName) => {
        const { history, match } = this.props
        // history.push(`/staff-management-details?staffId=${id}`)
        history.push({
            pathname: `/oneonone-direct-report-details`,
            search: `?staffId=${id}`,
            state: {email, reportName},
        })
    }

    handleSort = clickedColumn => () => {
        const { column, data, direction } = this.state

        if (column !== clickedColumn) {
            this.setState({
                column: clickedColumn,
                data: _.sortBy(data, [clickedColumn]),
                direction: 'ascending',
            })
            console.log("clickedColumn,", clickedColumn)
            return
        }

        console.log(column, data, direction)

        this.setState({
            data: data.reverse(),
            direction: direction === 'ascending' ? 'descending' : 'ascending',
        })
    }

    handlePaginationChange = (e, { activePage }) => {
        const { itemsPerPage } = this.state
        const control = itemsPerPage * activePage

        this.setState({ 
            activePage,
            indexStart: control - itemsPerPage,
            indexEnd: control - 1,
        })
    }

    render() {
        const { history, lmIDs, lmObjectives, poStaffAppraisals, isFetchingStaffAppraisals, firstApproved, lmStaffAppraisals } = this.props
        // console.log("lmtemp:",poStaffAppraisals)
        const { column, data, direction, analyticsCardData, results, indexStart, indexEnd, totalPages, activePage, filterOptions, itemsPerPage, filterBy } = this.state
        // const staffAprraisalArr = lmIDs.map(item => lmObjectives[item].staffAppraisal).filter((item, index, self) => self.indexOf(item) === index).map(item => poStaffAppraisals[item].staffId)
        // console.log("datum", data)
        if (isFetchingStaffAppraisals) {
            return <Loader active />
        }

        console.log('staffList', lmStaffAppraisals)

        return (
            <Segment basic>
                <ToastContainer/>              

                {/* Main Page starts here */}
                <Grid id="okrDataTable" padded>
                    <Grid.Row style={{ marginTop: "10px" }}>
                        <Grid.Column width={10}>
                            <Header as="h3" content="Staff Manager" dividing />
                        </Grid.Column>
                        
                        <Grid.Column width={6} textAlign='right'>
                            <Input icon='search' placeholder='Search...' onChange={(e) => this.handleSearchChange(e)} />
                        </Grid.Column>
                    </Grid.Row>

                    
                    <Grid.Row>
                        <Table sortable celled padded striped singleLine size="small">

                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell sorted={column === 'fullName' ? direction : null} onClick={this.handleSort('fullName')}>
                                        Name
                                    </Table.HeaderCell>
                                    <Table.HeaderCell sorted={column === 'sbu' ? direction : null} onClick={this.handleSort('sbu')}>
                                        Department
                                    </Table.HeaderCell>
                                    <Table.HeaderCell sorted={column === 'department' ? direction : null} onClick={this.handleSort('department')}>
                                        SBU
                                    </Table.HeaderCell>
                                    <Table.HeaderCell sorted={column === 'lineManager' ? direction : null} onClick={this.handleSort('lineManager')}>
                                        Line Manager Status
                                    </Table.HeaderCell>
                                    <Table.HeaderCell sorted={column === 'superlinemanager' ? direction : null} onClick={this.handleSort('superlinemanager')}>
                                       Super  LineManager Status
                                    </Table.HeaderCell>
                                    <Table.HeaderCell sorted={column === 'submissionDate' ? direction : null} onClick={this.handleSort('submissionDate')}>
                                        Submission Date
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                        Action
                                    </Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>
                            
                            <Table.Body>
                                {lmStaffAppraisals
                                    ?.filter(this.searchFilterFunction)
                                    ?.reduce(this.pageChunker, [[], []])
                                    ?.find(item => item)
                                    ?.find((item, idx) => idx === this.state.activePage - 1)
                                    ?.map(({fullName, sbu, department, emailAddress, lineManagerStatus,superLineManagerStatus, submissionDate, staffId, submittedAppraisal, lineManagerTwoApproved, firstApproved }) => (
                                        <Table.Row key={emailAddress}>
                                            <Table.Cell>{fullName}</Table.Cell>
                                            <Table.Cell>{department}</Table.Cell>
                                            <Table.Cell>{sbu}</Table.Cell>
                                            {/* <Table.Cell>{emailAddress}</Table.Cell> */}
                                            <Table.Cell>{lineManagerStatus}</Table.Cell>
                                            <Table.Cell>{superLineManagerStatus}</Table.Cell>
                                            <Table.Cell>{submissionDate === "01/01/0001 12:00 AM" ? "Yet to Submit" : submissionDate}</Table.Cell>
                                            <Table.Cell><Button content="Begin One-on-One" disabled={false} compact onClick={() => this.gotoDetail(staffId, emailAddress, fullName)}/></Table.Cell>
                                        </Table.Row>
                                    ))
                                }
                                {lmStaffAppraisals
                                    .filter(this.searchFilterFunction)
                                    .length < 1
                                    &&
                                    <Table.Row textAlign="center">
                                        <Table.Cell colSpan={9}>
                                        <br/>
                                            <Header as='h2' content="Nothing To Display"/>
                                        <br/>
                                        </Table.Cell>
                                    </Table.Row>
                                }
                            </Table.Body>
                        </Table>

                        <Pagination
                            activePage={activePage}
                            onPageChange={this.handlePaginationChange}
                            totalPages={Math.ceil(lmStaffAppraisals.filter(this.searchFilterFunction).length / itemsPerPage)}
                        />
                    </Grid.Row>
                </Grid>
            </Segment>
        )
    }
}

function mapStateToProps(state) {
    return {
        lmStaffAppraisals: state.requestingLineManagerStaffAppraisals,
        isFetchingStaffAppraisals: state.isFetchingStaffAppraisals,
    }
}

export default connect(mapStateToProps)(OneOnOneManager)