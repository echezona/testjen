import React from 'react';
import {Header, Button, Segment, Icon} from 'semantic-ui-react'
import { CREATE_STAFF_APPRAISAL, FETCH_SBU_OBJECTIVES } from '../../../constants';

const ExceptionHandler = ({
    staffOkrPeriod, userKeyResults, appraisalCycle, staffAppraisal
}) => {

    //&&userkyresults is a check to make sure there's no network error

    //no internet / all apis are failing
    if (!staffOkrPeriod && !userKeyResults && !appraisalCycle.id && !staffAppraisal.id) {
        return (
            <div className='general-comments' >
                <Segment placeholder textAlign='center'>
                    <Header icon>
                        <Icon name='warning sign' color='yellow' />
                        Something went wrong. Please check your internet connection and try again.
                    </Header>
                </Segment>
            </div>
        )
    }

    //key results have not been approved
    if (staffAppraisal.lineManagerTwoApproved !== 'approved' && appraisalCycle.id) {
        return (
            <div className='general-comments' >
                <Segment placeholder textAlign='center'>
                    <Header icon>
                        <Icon name='info circle' color='blue' />
                        This staff is yet to complete their OKR setting process.
                    </Header>
                </Segment>
            </div>
        )
    }

    //no period exists but period is locked
    if (appraisalCycle.monthlyOneOnOnActiveLock) {
        return (
            <div className='general-comments' >
                <Segment placeholder textAlign='center'>
                    <Header icon>
                        <Icon name='info circle' color='blue' />
                        The one-on-one period has not been activated by People-Ops.
                    </Header>
                </Segment>
            </div>
        )
    }

    //The staffOkrPeriod searched for could not be retrieved
    if (!staffOkrPeriod && userKeyResults) {
        return (
            <div className='general-comments' >
                <Segment placeholder textAlign='center'>
                    <Header icon>
                        <Icon name='info circle' color='blue' />
                        This staff is yet to begin their one-on-one for this period.
                    </Header>
                </Segment>
            </div>
        )
    }
    
    return (
        <div className='general-comments' >
            <Segment placeholder textAlign='center'>
                <Header icon>
                    <Icon name='warning sign' color='yellow' />
                    Something went wrong.
                </Header>
            </Segment>
        </div>
    )
}

export default ExceptionHandler;