//Components
import React, { Fragment, useEffect, useState } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import {  Button, Label, Loader, Dropdown, Header, Divider, Segment, TextArea, Form, Grid, Checkbox, Icon, Message } from 'semantic-ui-react';

// Css Files
import './index.css';
import { FETCH_LINE_MANAGER_OKRS, FETCH_LINE_MANAGER_STAFF_APPRAISALS, FETCH_PEOPLE_OPS_STAFF_APPRAISALS, FETCH_PEOPLE_OPS_COUNT, SUBMIT_PEOPLE_OPS_OKRS, FETCH_ALL_STAFF, FETCH_APPRAISAL_OBJECTIVES, FETCH_OBJECTIVES_AND_KEY_RESULTS, FETCH_STAFF_OKR_PERIOD, CREATE_STAFF_OKR_PERIOD, UPDATE_STAFF_OKR_PERIOD, FETCH_ALL_STAFF_OKR_PERIOD, FETCH_STAFF_APPRAISAL, FETCH_ALL_LM_STAFF_OKR_PERIOD, FETCH_LMPO_OKRS } from '../../../constants';
import { poStaffAppraisalsSelector, userIDsSelector, userObjectivesSelector, userKeyResultsSelector, userCategoriesSelector, sbuObjectivesSelector, staffIdSelector, lmIDsSelector, lmObjectivesSelector, lmKeyResultsSelector, lmCategoriesSelector, lmStaffAppraisalsSelector } from '../../../reducer';
import { ToastContainer } from "react-toastify";
import ReactModal from 'react-modal';
import ExceptionHandler from './ExceptionHandler';
import queryString from 'query-string';



const date = new Date();
const getCurrentMonth = () => {
    const monthArray = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    return monthArray[date.getMonth()];
}
const getCurrentYear = () => date.getFullYear();
const wizardSteps = [
    { step: 'STEP 1', name: 'OKR Progress' },  
    { step: 'STEP 2', name: 'General Comments' },  
    { step: 'STEP 3', name: 'Conclusion' }  
]

const OneOnOneManagerDetails = ({ 
    userIDs, userObjectives, userKeyResults, userCategories, isFetchingOKRs,
    staffOkrPeriod, isCreatingStaffOkrPeriod, staffId, 
    allStaffOkrPeriod, isFetchingStaffAppraisal,

    dispatch, location, allLmStaffOkrPeriod, lmIDs, lmKeyResults, lmObjectives, lmStaffAppraisals, isFetchingLMPOOKRs,
    isFetchingStaffAppraisals, isUpdatingStaffOkrPeriod, sbuObjectives, appraisalCycle, isFetchingAppraisalCycle, 
    staffAppraisal, 
}) => {

    const [objOneOnOne, setObjOneOnOne] = useState([]);
    const [period, setPeriod] = useState(null);
    // const [oneOnOneStatusOptions] = useState([
    //     { key: 'On track', text: 'On track', value: 'on track' },
    //     { key: 'Off track', text: 'Off track', value: 'off track' },
    //     { key: 'Paused', text: 'Paused', value: 'paused' },
    //     { key: 'Postponed', text: 'Postponed', value: 'postponed' },
    //     { key: 'Completed', text: 'Completed', value: 'completed' },
    //     { key: 'Late', text: 'Late', value: 'late' },
    // ]);
    const [pageStep, setPageStep] = useState(wizardSteps[0]);
    const [modal, setModal] = useState({ open: false, mode: null, krId: null, objId: null });
    const [selectedPeriod, setSelectedPeriod] = useState(`${getCurrentMonth()}, ${getCurrentYear()}`);
    const [isComplete, setIsComplete] = useState(false);

    // const onPeriodSelectChange = (e, {value}) => {
    //     //if dropdown value does not change, then do nothing.
    //     if (selectedPeriod === value) {
    //         return;
    //     }

    //     setSelectedPeriod(value);

    //     const month = value.substring(0, value.indexOf(','))
    //     const year = parseInt(value.substring(value.indexOf(',') + 2))

    //     dispatch({ type: FETCH_STAFF_OKR_PERIOD, payload: {month, year} });
    // }

    const nextWizardClick = () => {
        switch (pageStep.step) {
            case 'STEP 1':
                setPageStep(wizardSteps[1]);
                return;
            case 'STEP 2':
                setPageStep(wizardSteps[2]);
                return;
            default:
                return;
        }
    }

    const prevWizardClick = () => {
        switch (pageStep.step) {
            case 'STEP 3':
                setPageStep(wizardSteps[1]);
                return;
            case 'STEP 2':
                setPageStep(wizardSteps[0]);
                return;
            default:
                return;
        }
    }

    useEffect(() => {
        if( allLmStaffOkrPeriod.data && lmKeyResults) {
            const objectives = lmIDs.map(id => lmObjectives[id])
            const okrs = objectives.reduce((acc, curr) => {
                curr = {
                    ...curr,
                    keyResults: curr.keyResults.map(id => ({
                        ...lmKeyResults[id], 
                        oneOnOne: allLmStaffOkrPeriod
                            .data
                            // .find(period => console.log('per', period.month, period.year))
                            .find(period => period.month == getCurrentMonth() && period.year == getCurrentYear())
                            .oneOnOne
                            .find(oneOnOne => oneOnOne.keyResultId === id)
                    }))
                }
                acc.push(curr)
                return acc;
            }, [])


            setObjOneOnOne(okrs);

            setPeriod({ ...allLmStaffOkrPeriod.data?.find(period => period.month == getCurrentMonth() && period.year == getCurrentYear()), oneOnOne: null, staffId: queryString.parse(location.search).staffId })

            setIsComplete(allLmStaffOkrPeriod?.data?.find(period => period.month == getCurrentMonth() && period.year == getCurrentYear())?.periodStatusCompletion)

            console.log('okra', okrs)
        }

    }, [isFetchingLMPOOKRs, allLmStaffOkrPeriod.loading])

    useEffect(() => {
        dispatch({ type: FETCH_ALL_LM_STAFF_OKR_PERIOD, payload: { staffId: queryString.parse(location.search).staffId } });
        dispatch({ type: FETCH_LINE_MANAGER_STAFF_APPRAISALS })
        dispatch({ type: FETCH_LMPO_OKRS, staffId: `?staffId=${queryString.parse(location.search).staffId }` })

        // dispatch({ type: FETCH_OBJECTIVES_AND_KEY_RESULTS });
        // dispatch({ type: FETCH_ALL_STAFF_OKR_PERIOD });
        // dispatch({ type: FETCH_STAFF_OKR_PERIOD, payload: {month: getCurrentMonth(), year: getCurrentYear()} });
    }, [])

    // const selectStatus = (e, {value}, krId) => {
    //     console.log('val', value, krId)

    //     const newObjOneOnOne = objOneOnOne.reduce((acc, curr) => {
    //         curr = {
    //             ...curr,
    //             keyResults: curr.keyResults.map(krItem => {
    //                 if (krItem.id == krId) {
    //                     return {
    //                         ...krItem,
    //                         oneOnOne: {
    //                             ...krItem.oneOnOne,
    //                             oneOnOneStatus: value
    //                         }
    //                     }
    //                 }
    //                 return krItem
    //             })
    //         }
    //         acc.push(curr);
    //         return acc
    //     }, [])

    //     setObjOneOnOne(newObjOneOnOne)
    // }

    const onComplete = () => {
        const allOneOnOnes = objOneOnOne.reduce((acc, curr) => {
            curr = [...curr.keyResults].map(kr => kr.oneOnOne)
            acc = acc.concat(curr)
            return acc
        }, [])
        // const periodz = { ...period, oneOnOne: allOneOnOnes }
        // console.log('boo', periodz, allOneOnOnes)

        dispatch({ type: UPDATE_STAFF_OKR_PERIOD, payload: { data: { ...period, oneOnOne: allOneOnOnes, periodStatusCompletion: true }, refetchPeriod: true } });
    }

    const onSave = () => {
        const allOneOnOnes = objOneOnOne.reduce((acc, curr) => {
            curr = [...curr.keyResults].map(kr => kr.oneOnOne)
            acc = acc.concat(curr)
            return acc
        }, [])
        // const periodz = { ...period, oneOnOne: allOneOnOnes }
        // console.log('boo', allOneOnOnes, period)

        dispatch({ type: UPDATE_STAFF_OKR_PERIOD, payload: { data: { ...period, oneOnOne: allOneOnOnes } } });
    }

    const closeModal = () => setModal({ ...modal, open: false, mode: null });

    const onKrMenuChange = (e, {value}, krId, objId) => {
        //if dropdown value is not valid, then do nothing.
        if (!value) {
            return;
        }
        setModal({ open: true, mode: value, krId,  objId })
    }

    const onCommentChange = (e, krId) => {
        // console.log('val', value, krId)

        const newObjOneOnOne = objOneOnOne.reduce((acc, curr) => {
            curr = {
                ...curr,
                keyResults: curr.keyResults.map(krItem => {
                    if (krItem.id == krId) {
                        return {
                            ...krItem,
                            oneOnOne: {
                                ...krItem.oneOnOne,
                                lineManagerComment: e.target.value
                            }
                        }
                    }
                    return krItem
                })
            }
            acc.push(curr);
            return acc
        }, [])

        setObjOneOnOne(newObjOneOnOne)
    }

    
    const conditions = () => ({
        // IS_VIEWING_PAST_PERIOD: `${staffOkrPeriod.data?.month}, ${staffOkrPeriod.data?.year}` !== `${getCurrentMonth()}, ${getCurrentYear()}`,
        ONE_ON_ONE_PERIOD_DISABLED: appraisalCycle?.monthlyOneOnOnActiveLock
    })
    
    console.log('logged', appraisalCycle, lmStaffAppraisals, allLmStaffOkrPeriod.data, lmObjectives);

    if (allLmStaffOkrPeriod.loading || isFetchingLMPOOKRs || isFetchingAppraisalCycle || isFetchingStaffAppraisals) {
        return <Loader active />
    }

    if (
        !lmKeyResults || !allLmStaffOkrPeriod.data?.find(period => period.month == getCurrentMonth() && period.year == getCurrentYear()) ||
        (appraisalCycle.monthlyOneOnOnActiveLock && !allLmStaffOkrPeriod.data?.find(period => period.month == getCurrentMonth() && period.year == getCurrentYear()) && lmKeyResults) || 
        lmStaffAppraisals?.find(app => app.staffId == queryString.parse(location.search).staffId)?.lineManagerTwoApproved !== 'approved'
    ) {
        return (
            <ExceptionHandler
                staffOkrPeriod={allLmStaffOkrPeriod.data?.find(period => period.month == getCurrentMonth() && period.year == getCurrentYear())}
                userKeyResults={lmKeyResults}
                appraisalCycle={appraisalCycle}
                staffAppraisal={staffAppraisal}
            />
        )
    }

    // if (!staffOkrPeriod.data && userKeyResults && !appraisalCycle.monthlyOneOnOnActiveLock) {
    //     return (
    //         // <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
    //         //     No One on One found for selected period!
    //         //     <Button color="blue" content="Start One on One" onClick={createPeriod} loading={isCreatingStaffOkrPeriod} disabled={isCreatingStaffOkrPeriod} />
    //         // </div>
    //         <div className='general-comments' >
    //             <Segment placeholder textAlign='center'>
    //                 <Header icon>
    //                     <Icon name='warning sign' color='yellow' />
    //                     No One on One found for current period!
    //                 </Header>
    //                 <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
    //                     <Button color="blue" content="Start One on One" onClick={createPeriod} loading={isCreatingStaffOkrPeriod} disabled={isCreatingStaffOkrPeriod} />
    //                 </div>
    //             </Segment>
    //         </div>
    //     )
    // }

    return (
        <div>
            {/* Toast Container */}
            <ToastContainer/>
            {/* Modal */}
            <ReactModal
                id='commentModal'
                isOpen={modal.open}
                onRequestClose={closeModal}
                style={{
                    overlay: {backgroundColor: 'rgba(0, 0, 0, 0.75)'},
                    content: {
                        top: '25vh',
                        left: '250px',
                        right: '250px',
                        bottom: '30vh',
                    },
                }}
            >
                <Header
                    content= {modal.mode === 'staffComment' ? 'Direct Report comment' : 'Enter your comment'}
                />
                { modal.mode === 'lineManagerComment' &&
                    <>
                        <Form>
                            <label>
                                {/* Enter your objective */}
                                <textarea rows={6} 
                                    value={objOneOnOne.find(objItem => objItem.id === modal.objId).keyResults.find(krItem => krItem.id === modal.krId).oneOnOne.lineManagerComment} 
                                    placeholder='Enter your comment' 
                                    onChange={(e) => onCommentChange(e, modal.krId)}
                                />
                            </label>
                        </Form>
                        { !(conditions().ONE_ON_ONE_PERIOD_DISABLED) &&
                            <div className='save-button'>
                                <Button
                                    content='Save'
                                    color='blue'
                                    onClick={() => {onSave(); closeModal()}}
                                    loading={isUpdatingStaffOkrPeriod}
                                    disabled={isUpdatingStaffOkrPeriod || !objOneOnOne.find(objItem => objItem.id === modal.objId).keyResults.find(krItem => krItem.id === modal.krId).oneOnOne.lineManagerComment.trim()}
                                />
                            </div>
                        }
                    </>
                }
                { modal.mode === 'staffComment' &&
                    <Form>
                        <textarea rows={6} 
                            value={objOneOnOne.find(objItem => objItem.id === modal.objId).keyResults.find(krItem => krItem.id === modal.krId).oneOnOne.staffComment} 
                            readOnly
                        />
                    </Form>
                }

            </ReactModal>
            {/* Period Bar */}
            <div className='period-bar-lm'>
                <div className='period-bar__status'>
                    {/* Direct Report Name */}
                    <Message compact>
                        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                            <div style={{ marginRight: '10px' }}>
                                <Icon name='user circle' size='large' />
                            </div>
                            <Header>{location.state.reportName}</Header>
                        </div>
                    </Message>
                </div>
                <div style={{ display: 'flex' }}>
                    <div className='period-bar__status'>
                        <Label
                            content={ selectedPeriod }
                            size='big'
                        />
                    </div>
                    <div className='period-bar__status'>
                        <Label
                            content={ allLmStaffOkrPeriod.data?.find(period => period.month == getCurrentMonth() && period.year == getCurrentYear())?.periodStatusCompletion ? 'Complete' : 'Incomplete' }
                            color={ allLmStaffOkrPeriod.data?.find(period => period.month == getCurrentMonth() && period.year == getCurrentYear())?.periodStatusCompletion ? 'green' : 'red' }
                            size='big'
                        />
                    </div>
                </div>
            </div>
            {/* Exception display message */}
            { conditions().ONE_ON_ONE_PERIOD_DISABLED
                ? (
                    <Message icon>
                        <Icon name='info circle'/>
                        <Message.Content>
                        <Message.Header>Attention</Message.Header>
                            The One-on-one period has been disabled by People-Ops so you'll be <b>UNABLE</b> to save any changes.
                        </Message.Content>
                    </Message>
                )
                : null
            }
            {/* Wizard Switcher */}
            <div className='wizard-control'>
                <Header
                    as='h3'
                    content={pageStep.step}
                    subheader={pageStep.name}
                />
                <div className='wizard-control__buttons'>
                    <Button.Group>
                        <Button content='Prev' icon='arrow left' labelPosition='left' color='blue' onClick={prevWizardClick} disabled={pageStep.step === 'STEP 1'} />
                        <Button.Or text='' />
                        <Button content='Next' icon='arrow right' labelPosition='right' color='blue' onClick={nextWizardClick} disabled={pageStep.step === 'STEP 3'} />
                    </Button.Group>
                </div>
            </div>
            { pageStep.step === 'STEP 1' &&
                <>
                    {objOneOnOne.map((objItem, index) => (
                        <div className='objective'>
                            <Segment>
                                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                    <div style={{ display: 'inline-block' }}>
                                        <Header 
                                            as='h2' 
                                            content={objItem.order=='Personal' ? 'Personal Development Objective' : `Business Objective ${objItem.order}`}
                                            style={{ display: 'inline-block', margin: '0 10px 0 0' }}    
                                        />
                                    </div>
                                </div>
                                <Divider/>
                                <p>
                                    { objItem.description }
                                </p>
                                { objItem.order !== 'Personal' &&
                                    <>
                                        <Header as='h5' content='SBU Objective'/>
                                        <p>{sbuObjectives && sbuObjectives.length > 0 ? sbuObjectives?.find(sbuObj => sbuObj.id === objItem.sbuObjectiveId)?.description : null}</p>
                                    </>
                                }
                                <div>
                                    {objItem.keyResults.map(krItem => (
                                        <Segment>
                                            <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                                                <Header 
                                                    as='h4' 
                                                    content={`Key Result ${krItem.order}`}
                                                    style={{ display: 'inline-block', margin: '0 10px 0 0' }}    
                                                />
                                                <Dropdown 
                                                    id='ellipsis-dropdown'
                                                    icon={null}
                                                    direction='left'
                                                    trigger = {(
                                                        <Button circular icon='ellipsis vertical' />
                                                    )}
                                                    inline
                                                    header='OPTIONS'
                                                    options={[
                                                        {text: "Select an action", value:''},
                                                        {text: `Comment`, value: "lineManagerComment", icon:'comment'},
                                                        {text: `Direct Report Comment`, value: 'staffComment', icon:'comment', disabled: krItem.oneOnOne.staffComment ? false : true },
                                                    ]}
                                                    onChange={(e, titleProps) => onKrMenuChange(e, titleProps, krItem.id, objItem.id)}
                                                    value={modal.mode}
                                                    // loading={deleteLoadingStatus}
                                                    // disabled={deleteLoadingStatus}
                                                />
                                            </div>
                                            <Divider style={{marginTop: '4px'}} />
                                            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                                <p>
                                                    {krItem.description}
                                                </p>
                                                {/* <Dropdown 
                                                    placeholder='Status' 
                                                    selection 
                                                    options={oneOnOneStatusOptions} 
                                                    value={krItem.oneOnOne.oneOnOneStatus}
                                                    onChange={(e, titleProps) => selectStatus(e, titleProps, krItem.id)}
                                                    disabled
                                                /> */}
                                                {/* <label>
                                                    Status: 
                                                    {krItem.oneOnOne.oneOnOneStatus ? krItem.oneOnOne.oneOnOneStatus : 'pending'}
                                                </label> */}
                                                <div>
                                                    <Label
                                                        content={ krItem.oneOnOne.oneOnOneStatus ? krItem.oneOnOne.oneOnOneStatus : 'pending' }
                                                        color='blue'
                                                    />
                                                </div>
                                            </div>
                                        </Segment>
                                    ))}
                                </div>
                            </Segment>
                        </div>
                    ))}
                    { !(conditions().ONE_ON_ONE_PERIOD_DISABLED) &&
                        <div className='save-button'>
                            <Button
                                content='Save'
                                color='blue'
                                onClick={onSave}
                                loading={isUpdatingStaffOkrPeriod}
                                disabled={isUpdatingStaffOkrPeriod}
                            />
                        </div>
                    }
                </>
            }
            { pageStep.step === 'STEP 2' &&
               
                <div className='general-comments' >
                    <Segment placeholder>
                        <Grid columns={2} stackable textAlign='center'>
                            <Divider vertical></Divider>

                            <Grid.Row verticalAlign='middle'>
                                <Grid.Column>
                                    <Form>
                                        Your Comment
                                        <TextArea 
                                            rows={6}
                                            placeholder='Tell us more' 
                                            value={period.generalLineManagerComment}    
                                            onChange={(e, {value}) =>  setPeriod({ ...period, generalLineManagerComment: value })}
                                        />
                                    </Form>
                                </Grid.Column>

                                <Grid.Column>
                                    <Form>
                                        Your Direct Report's Comment
                                        <TextArea 
                                            rows={6}
                                            placeholder='No comment yet from your direct report' 
                                            value={ period.generalStaffComment }   
                                            readOnly 
                                        />
                                    </Form>
                                </Grid.Column>
                        </Grid.Row>
                        </Grid>
                    </Segment>
                    { !(conditions().ONE_ON_ONE_PERIOD_DISABLED) &&
                        <div className='save-button'>
                            <Button
                                content='Save'
                                color='blue'
                                onClick={onSave}
                                loading={isUpdatingStaffOkrPeriod}
                                disabled={isUpdatingStaffOkrPeriod || !period.generalLineManagerComment.trim()}
                            />
                        </div>
                    }
                </div>
            }
            { pageStep.step === 'STEP 3' &&
                <>
                    { !allLmStaffOkrPeriod.data?.find(period => period.month == getCurrentMonth() && period.year == getCurrentYear())?.periodStatusCompletion &&
                        <div className='general-comments' >
                            <Segment>
                                <Form>
                                    <Checkbox 
                                        label={`I acknowledge that I have had my one-on-one session with my direct report and that we've captured progress here.`}
                                        checked={isComplete}
                                        onChange={() => setIsComplete(!isComplete)}
                                    />
                                    <div style={{ display: 'flex', justifyContent: 'center', marginTop: '30px' }}>
                                        <Button
                                            color='blue'
                                            content='Complete Session'
                                            onClick={onComplete}
                                            loading={isUpdatingStaffOkrPeriod}
                                            disabled={isUpdatingStaffOkrPeriod || !isComplete}
                                        />
                                    </div>
                                </Form>
                            </Segment>
                        </div>
                    }

                    { allLmStaffOkrPeriod.data?.find(period => period.month == getCurrentMonth() && period.year == getCurrentYear())?.periodStatusCompletion &&
                        <div className='general-comments' >
                            <Segment placeholder textAlign='center'>
                                <Header icon>
                                    <Icon name='check circle outline' color='green' />
                                    You have completed your one-on-one with this direct report for this period.
                                </Header>
                            </Segment>
                        </div>
                    }
                
                </>
            }
        </div>
    )
}

function mapStateToProps(state) {
    return {
        userIDs: userIDsSelector(state),
        userObjectives: userObjectivesSelector(state),
        userKeyResults: userKeyResultsSelector(state),
        userCategories: userCategoriesSelector(state),
        isFetchingOKRs: state.fetchLoadingReducer.FETCH_OBJECTIVES_AND_KEY_RESULTS_LOADING_STATUS,
        staffOkrPeriod: state.staffOkrPeriodReducer,
        isCreatingStaffOkrPeriod: state.saveLoadingReducer.CREATE_STAFF_OKR_PERIOD_LOADING_STATUS,
        isUpdatingStaffOkrPeriod: state.saveLoadingReducer.UPDATE_STAFF_OKR_PERIOD_LOADING_STATUS,
        staffId: staffIdSelector(state),
        allStaffOkrPeriod: state.allStaffOkrPeriodReducer,
        isFetchingAppraisalCycle: state.fetchLoadingReducer.FETCH_APPRAISAL_CYCLE_LOADING_STATUS,
        appraisalCycle: state.requestingAppraisalCycle,
        isFetchingStaffAppraisal: state.fetchLoadingReducer.FETCH_STAFF_APPRAISAL_LOADING_STATUS,
        staffAppraisal: state.requestingStaffAppraisal,

        sbuObjectives: sbuObjectivesSelector(state),
        allLmStaffOkrPeriod: state.allLmStaffOkrPeriodReducer,
        lmIDs: lmIDsSelector(state),
        lmObjectives: lmObjectivesSelector(state),
        lmKeyResults: lmKeyResultsSelector(state),
        lmCategories: lmCategoriesSelector(state),
        lmStaffAppraisals: lmStaffAppraisalsSelector(state),
        isFetchingLMPOOKRs: state.isFetchingLMPOOKRs,
        isFetchingStaffAppraisals: state.isFetchingStaffAppraisals,
    }
}

export default connect(mapStateToProps)(OneOnOneManagerDetails)