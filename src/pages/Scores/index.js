//Components
import React, {Fragment} from 'react';
import { Loader, Card , Segment, Header, Icon} from 'semantic-ui-react';
// import Staff from './Staff';
// import Results from './results';
import { connect } from "react-redux"
import {withRouter} from 'react-router-dom'
// import NavList from './navList'
import {userObjectivesSelector, userIDsSelector, userKeyResultsSelector, userCategoriesSelector} from '../../reducer'

// Css Files
// import './index.css';
import { FETCH_OBJECTIVES_AND_KEY_RESULTS, FETCH_CATEGORY, FETCH_SBU_OBJECTIVES, FETCH_LINE_MANAGER_STAFF_APPRAISALS, FETCH_BEHAVIORAL_INDICATORS, FETCH_STAFF_APPRAISAL, FETCH_APPRAISAL_CYCLE } from '../../constants';
import ExceptionHandler from './ExceptionHandler';
import SummaryTable from '../../components/SummaryTable';
import PageInfo from '../../components/PageInfo';

// const submittedStyle = {
//     fontSize: "30px",
//     textAlign: "center",
// }

class Scores extends React.Component {
 
    componentDidMount() {
        const { dispatch } = this.props
        dispatch({ type: FETCH_OBJECTIVES_AND_KEY_RESULTS })
        dispatch({ type: FETCH_CATEGORY })
        dispatch({ type: FETCH_SBU_OBJECTIVES})
        dispatch({ type: FETCH_BEHAVIORAL_INDICATORS })
        dispatch({ type: FETCH_STAFF_APPRAISAL })
        dispatch({ type: FETCH_APPRAISAL_CYCLE })
        
        document.title = "PMS - Performance Assessment" // set the document title in the browser
    }

    // iUpdated = () => {
    //     this.forceUpdate();
    // }

    render() {
        const { IS_REQUESTING_OKRs, match, userIDs, userObjectives, userKeyResults, sbuObjectives, category, okrLoadingStatus, categoryLoadingStatus, sbuObjectivesLoadingStatus, staffAppraisal, appraisalCycle, behavioralIndicators, isSubmittingOKRs } = this.props
        console.log("staffAppraisal bla bal", staffAppraisal)

        
        let staffKeyResults, staffPerformanceSum, staffPerformanceScore, staffBehaviorSum, staffBehaviorScore, staffTotalScore,
        lineManagerPerformanceSum, lineManagerPerformanceScore, lineManagerBehaviorSum, lineManagerBehaviorScore, lineManagerTotalScore
        
        if (
            // (Object.prototype.toString.call(appraisalCycle) === '[object Object]' && appraisalCycle.message) || 
            (Object.prototype.toString.call(behavioralIndicators) === '[object Array]' && behavioralIndicators.length > 0) || 
            (userIDs.length > 0 && typeof userIDs[0] != "string")
        ) {
            staffKeyResults = userIDs.map(item => userObjectives[item].keyResults)
                                    .reduce((acc, item) => acc.concat(item), [])
                                    .map(item => userKeyResults[item])

            staffPerformanceSum = staffKeyResults.reduce((acc, curr) => acc + curr.staffRating, 0)

            staffPerformanceScore = ((staffPerformanceSum / (staffKeyResults.length * 5)) * 100)

            staffBehaviorSum = behavioralIndicators.reduce((acc, curr) => acc + curr.staffRating,  0)

            staffBehaviorScore = ((staffBehaviorSum / (behavioralIndicators.length * 5)) * 100)

            staffTotalScore = ((staffPerformanceScore / 10) * 7) + ((staffBehaviorScore / 10) * 3)

            lineManagerPerformanceSum = staffKeyResults.reduce((acc, curr) => acc + curr.lineManagerRating, 0)

            lineManagerPerformanceScore = ((lineManagerPerformanceSum / (staffKeyResults.length * 5)) * 100)

            lineManagerBehaviorSum = behavioralIndicators.reduce((acc, curr) => acc + curr.lineManagerRating,  0)

            lineManagerBehaviorScore = ((lineManagerBehaviorSum / (behavioralIndicators.length * 5)) * 100)

            lineManagerTotalScore = ((lineManagerPerformanceScore / 10) * 7) + ((lineManagerBehaviorScore / 10) * 3)
        }



        if ( okrLoadingStatus || categoryLoadingStatus || sbuObjectivesLoadingStatus ) {
            return <Loader active/>
        } 

        console.log("appraisalCycle", appraisalCycle, userIDs, staffAppraisal)

        if (appraisalCycle.okrAssessmentLock === true || 
            (Object.prototype.toString.call(appraisalCycle) === '[object Object]' && appraisalCycle.message) || 
            (Object.prototype.toString.call(behavioralIndicators) !== '[object Array]') ||
            userIDs.length < 1
        ) {
            return (
                <ExceptionHandler {...this.props}/>
            )
        }

        
        return (
            <Fragment>
                {/* <p style={submittedStyle}>Congratulations! You've completed your staff assessment exercise</p> */}

                <Segment basic style={{ display: 'flex', justifyContent: 'center'}}>
                    <Card style={{ padding: '10px', margin: '20px' }}>
                        <Header content='Self Rating' subheader="Here's how you rated yourself"  dividing/>
                        <div>Performance: {staffPerformanceScore.toFixed(1) + '%'}</div>
                        <div>Behavior: {staffBehaviorScore.toFixed(1) + '%'}</div>
                        <div style={{ fontSize: '1.2em', marginTop: '10px', fontWeight: 'bold' }}>Total: {staffTotalScore.toFixed(1) + '%'}</div>
                    </Card>

                    <Card style={{ padding: '10px', margin: '20px' }}>
                        <Header content='Final Rating' subheader={"Here's how your line manager rated you"}  dividing/>
                        { staffAppraisal.lineManagerTwoApproved === 'approved' &&
                            <>
                                <div>Performance: {lineManagerPerformanceScore.toFixed(1) + '%'}</div>
                                <div>Behavior: {lineManagerBehaviorScore.toFixed(1) + '%'}</div>
                                <div style={{ fontSize: '1.2em', marginTop: '10px', fontWeight: 'bold' }}>Total: {lineManagerTotalScore.toFixed(1) + '%'}</div>
                            </>
                        }
                        { staffAppraisal.lineManagerTwoApproved !== 'approved' &&
                            <div> <Icon name='info circle'/> Your final rating is not yet available </div>
                        }
                    </Card>
                </Segment>

                <Segment basic>
                    <PageInfo 
                        title='Click to View/Hide Performance Score Summary' 
                        content={
                            <SummaryTable objectives={userIDs.map(item => userObjectives[item])} keyResults={staffKeyResults} variant={'performance'}/>
                        } 
                    />
                </Segment>

                <Segment basic>
                    <PageInfo 
                        title='Click to View/Hide Behaviour Score Summary' 
                        content={
                            <SummaryTable indicators={behavioralIndicators} variant={'behavior'}/>
                        }
                    />
                </Segment>

            </Fragment>
        )
        

        // return (
        //     <Fragment>
        //         {/* <PageInfo info={[
        //             <p>Use the accordion on the left to navigate between objectives.</p>,
        //             <p>Use the menu at the right of each key result to navigate between providing the rating, the comment and a link to your supporting documents.</p>,
        //             <p>To complete your task on this screen, all key results for all objectives must have been rated with a comment provided.</p>,
        //             <p>Providing a link to your supporting documents on One Drive is not compulsory</p>,
        //             <p>Make sure to save your progress for EACH OBJECTIVE before navigating to another</p>,
        //         ]}/> */}
        //         <Grid padded>
        //             <Grid.Column largeScreen={6} wideScreen={6} computer={6} tablet={6} mobile={16}>
        //                 {/* <NavList userIDs={userIDs} userObjectives={userObjectives}/> */}
        //                 <Staff userIDs={userIDs} userObjectives={userObjectives} userCategories={category} sbuObjectives={sbuObjectives} {...this.props}/>
        //             </Grid.Column>
        //             <Grid.Column largeScreen={10} wideScreen={10} computer={10} tablet={10} mobile={16}>
        //                 <Switch>
        //                     {userIDs.map((item, index) => <Route key={item} path={`${match.path}/${item}`} render={(props) => <Results objIndex={index} keyResults={userKeyResults} id={item} objective={userObjectives[item]} iUpdated={this.iUpdated} {...props} {...this.props}/>}/>)}
        //                 </Switch>
        //             </Grid.Column>
        //         </Grid>
        //     </Fragment>
        // )
    }
}

function mapStateToProps(state) {
    return {
        appraisalCycle: state.requestingAppraisalCycle,
        IS_REQUESTING_OKRs: state.requestingOKRs,
        userIDs: userIDsSelector(state),
        userObjectives: userObjectivesSelector(state),
        userKeyResults: userKeyResultsSelector(state),
        // userCategories: userCategoriesSelector(state),
        sbuObjectives: state.sbuObjectives,
        category: state.category,
        okrLoadingStatus: state.fetchLoadingReducer.FETCH_OBJECTIVES_AND_KEY_RESULTS_LOADING_STATUS,
        categoryLoadingStatus: state.fetchLoadingReducer.FETCH_CATEGORY_LOADING_STATUS,
        sbuObjectivesLoadingStatus: state.fetchLoadingReducer.FETCH_SBU_OBJECTIVES_LOADING_STATUS,
        staffAppraisal: state.requestingStaffAppraisal,
        //for results.js
        buttonLoadingStatus: state.saveLoadingReducer.SAVE_KEY_RESULTS_LOADING_STATUS,
        REQUESTING_SAVE_KEY_RESULTS: state.REQUESTING_SAVE_KEY_RESULTS,
        SAVING_OBJECTIVE_STATUS: state.SAVING_OBJECTIVE_STATUS,
        behavioralIndicators: state.requestingBehavioralIndicators,
        isSubmittingOKRs: state.isSubmittingOKRs,
        SUBMIT_OKRS_STATUS: state.SUBMIT_OKRS_STATUS,
    }
}

export default withRouter(connect(mapStateToProps)(Scores))
