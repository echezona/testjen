import React, {Component, Fragment} from 'react';
import { Segment,Header,Button,Label } from 'semantic-ui-react';
import Indicator from './Indicator'
import { SAVE_BEHAVIORAL_INDICATORS, SUBMIT_OKRS } from '../../constants';


class Results extends Component {
    constructor(props){
        super(props)
        this.state = {
            clicked: "",
            comment: props.superLineManagerComment,
            rating: props.superLineManagerRating,
            objIndex: props.index,
            status: props.superLineManagerStatus
        }
    }

    handleRatingClick = (e, titleProps) => {
        const {name} = e.target
        const {content} = titleProps
        this.setState({
            clicked: name,
            rating: parseInt(content),
        })
    }

    handleApproveClick = ({target: {name}}) => {
        this.setState({ status: name })
    }

    onComment = (e) => {
        const {value} = e.target
        this.setState({
            comment: value,
        })
    }


    handleSaveClick = () => {
        const {comment, status, objIndex} = this.state;
        const { dispatch, behaviourId, staffAppraisalId, peer1Rating, 
            peer2Rating, staffRating, peer1Comment, 
            peer2Comment, staffComment, id, lineManagerRating, lineManagerComment } = this.props
        
        dispatch({  type: SAVE_BEHAVIORAL_INDICATORS, 
                    data: { id, behaviourId, staffAppraisalId, peer1Rating, 
                        peer2Rating, staffRating, peer1Comment, 
                        peer2Comment, staffComment, lineManagerRating, lineManagerComment,
                        superLineManagerStatus: status, superLineManagerComment: comment,
                    },
                    objIndex
        })

        console.log("saving this guy", { id, behaviourId, staffAppraisalId, peer1Rating, 
            peer2Rating, staffRating, peer1Comment, 
            peer2Comment, staffComment, lineManagerRating, lineManagerComment,
            superLineManagerStatus: status, superLineManagerComment: comment,
        })
    }

    // componentDidUpdate(){
    //     console.log("see the state", this.state)
    // }

    render(){
        const {SAVE_BEHAVIOUR_STATUS, userIDs, userObjectives, userKeyResults, behavioralIndicators, behaviourSaveLoadingStatus} = this.props
        const { comment, status } = this.state;
        console.log("status", this.state.status)
        let commentApproved = true
        if ( status === 'disapproved' && !comment) {
            commentApproved = false;
        }
        // const SUBMIT_ACTIVE = userIDs.map(item => userObjectives[item].keyResults).reduce((acc, item) => acc.concat(...item), []).map((item) => userKeyResults[item]).every((item) => item.staffRating > 0 && item.staffComment !== null && item.staffComment !== "")
        // const CONTINUE_TO_PERFORMANCE_ASSESSMENT_ACTIVE = behavioralIndicators.every((item) => item.staffRating > 0 && item.staffComment !== null && item.staffComment !== "")
        return(
            <Fragment>
                <Header as="h2" textAlign="center" content="Behavioral Assessment"/>
                <Segment>
                    <Header as="h4" content="Behavioral Score Card" dividing/>
                    <Indicator {...this.props} {...this.state} handleRatingClick={this.handleRatingClick} onComment={this.onComment} handleApproveClick={this.handleApproveClick}/>
                    <br/>
                    <Segment basic textAlign="right">
                        { SAVE_BEHAVIOUR_STATUS === true && 
                            <Label pointing='right' color="green" >Save Successful</Label>
                        }
                        { SAVE_BEHAVIOUR_STATUS === false && 
                            <Label pointing='right' color="red" >Save Failed. Please try again or refresh page.</Label>
                        }
                        <Button content="Save" color="blue" onClick={this.handleSaveClick} loading={behaviourSaveLoadingStatus} disabled={!commentApproved || !status || behaviourSaveLoadingStatus}/> 
                        {/* { !SUBMIT_ACTIVE &&
                            <Button content="Continue to Behavioral Objectives" disabled={!CONTINUE_TO_PERFORMANCE_ASSESSMENT_ACTIVE} onClick={this.goToBehavioral}/>
                        }
                        { SUBMIT_ACTIVE &&
                            <Button content="Submit" disabled={!CONTINUE_TO_PERFORMANCE_ASSESSMENT_ACTIVE} loading={!SUBMIT_ACTIVE} onClick={this.handleSubmit}/>
                        }  */}
                    </Segment>
                </Segment>
            </Fragment>
        )
    }
}


export default Results