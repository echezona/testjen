import React,{Component, Fragment} from 'react'
import { Segment,Grid,Header,Button,Form,Input,Container, Tab } from 'semantic-ui-react'
import "./index.css"


class Indicator extends Component {
    // constructor(props){
    //     super(props);
    //     this.state = {
    //         clicked: "",
    //     }
    // }

    // handleRatingClick = (e) => {
    //     const {name} = e.target
    //     this.setState({
    //         clicked: name,
    //     })
    // }

    panes = [
        {
            menuItem: 'Learner(2)',
            render: () => {
                return (
                    <p style={{overflow: 'auto', maxHeight: 200}}><pre>{this.props.behaviour.learnerDescription}</pre></p>
                )
            }
        },
        {
            menuItem: 'Emerging Performer(3)',
            render: () => {
                return (
                    <p style={{overflow: 'auto', maxHeight: 200}}><pre>{this.props.behaviour.emergingPerformerDescription}</pre></p>
                )
            }
        },
        {
            menuItem: 'Performer(4)',
            render: () => {
                return (
                    <p style={{overflow: 'auto', maxHeight: 200}}><pre>{this.props.behaviour.performerDescription}</pre></p>
                )
            }
        },
        {
            menuItem: 'Influencer(5)',
            render: () => {
                return (
                    <p style={{overflow: 'auto', maxHeight: 200}}><pre>{this.props.behaviour.influencerDescription}</pre></p>
                )
            }
        },
    ]

    render(){
        // const {clicked} = this.state
        const {behavioralIndicator, name, description, handleRatingClick, onComment, rating, comment, staffComment, staffRating, lineManagerRating, lineManagerComment, superLineManagerRating, superLineManagerComment, status, handleApproveClick} = this.props
         return(
            <Fragment>
                <Segment>
                    <Header as="h5" content={name}/>
                    <Segment basic textAlign="right">
                        <Header as='h4' content={`Line Manager Rating: ${lineManagerRating}`} subheader={`Staff Rating: ${staffRating}`} />
                    </Segment>
                    {description}
                    <br/>   
                    <Tab menu={{ secondary: true, pointing: true }} panes={this.panes} />
                    <br/>
                    {/* <Segment textAlign="right">
                        <Button content="2" compact color={rating===2 ? "red" : "green"} onClick={handleRatingClick} name="two"/> 
                        <Button content="3" compact color={rating===3 ? "red" : "green"} onClick={handleRatingClick} name="three"/> 
                        <Button content="4" compact color={rating===4 ? "red" : "green"} onClick={handleRatingClick} name="four"/> 
                        <Button content="5" compact color={rating===5 ? "red" : "green"} onClick={handleRatingClick} name="five"/> 
                    </Segment> */}
                    <Segment textAlign='right'>
                        <Button.Group>
                            <Button 
                                content="Approve"
                                name='approved'
                                color="green"
                                basic={status === 'approved' ? false : true}
                                onClick={handleApproveClick}
                            />

                            <Button 
                                content="Disapprove"
                                name='disapproved'
                                color="red"
                                basic={status === 'disapproved' ? false : true}
                                onClick={handleApproveClick}
                            />
                        </Button.Group>
                    </Segment>
                </Segment>
                <Form>
                    <Header as='h4' style={{marginTop: '20px'}}>
                        <span style={{color:"red"}}>Line Manager Comment: </span>
                        <span >{lineManagerComment}</span>
                    </Header>
                    <Header as='h4'>
                        <span style={{color:"red"}}>Staff Comment: </span>
                        <span >{staffComment ? staffComment : "Staff did not make any comment"}</span>
                    </Header>
                    <Form.TextArea 
                        placeholder={status == 'disapproved' ? "Please give reasons for disagreeing with the line manager's rating" : 'Enter your comments if any'} 
                        onChange={(e)=>onComment(e)} value={comment}
                        style={ status == 'disapproved' ? { borderColor: 'red' } : null}    
                    />
                </Form>
            </Fragment>
        )
    }
}


export default Indicator