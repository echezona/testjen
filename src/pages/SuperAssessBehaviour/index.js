//Components
import React, {Component} from 'react';
import { Grid, Loader, Header } from 'semantic-ui-react';
import Staff from './Staff';
import Results from './results';
import { connect } from "react-redux"
import {withRouter,Switch,Route} from 'react-router-dom'
import { FETCH_BEHAVIORAL_INDICATORS, FETCH_OBJECTIVES_AND_KEY_RESULTS, FETCH_STAFF_APPRAISAL } from '../../constants';


// Css Files
import './index.css';
import ExceptionHandler from './exceptionHandler';
import { userIDsSelector, userObjectivesSelector, userKeyResultsSelector } from '../../reducer';
import PageInfo from '../../components/PageInfo';


const submittedStyle = {
    fontSize: "50px",
    textAlign: "center",
}

class SuperAssessBehaviour extends Component {

    render(){
        const { match, behavioralIndicators, behavioralLoadingStatus, staffAppraisal, lmIDs, lmObjectives, lmKeyResults } = this.props
        // if(Object.prototype.toString.call(behavioralIndicators) !== '[object Array]'){
        //     return(
        //         <ExceptionHandler BI={behavioralIndicators}/>
        //     )
        // }

        //PageInfo Values start here
        const staffKeyResults = lmIDs.map(item => lmObjectives[item].keyResults)
                                    .reduce((acc, item) => acc.concat(item), [])
                                    .map(item => lmKeyResults[item])

        const staffPerformanceSum = staffKeyResults.reduce((acc, curr) => acc + curr.staffRating, 0)

        const staffPerformanceScore = ((staffPerformanceSum / (staffKeyResults.length * 5)) * 100)

        const staffBehaviorSum = behavioralIndicators.reduce((acc, curr) => acc + curr.staffRating,  0)

        const staffBehaviorScore = ((staffBehaviorSum / (behavioralIndicators.length * 5)) * 100)

        const staffTotalScore = ((staffPerformanceScore / 10) * 7) + ((staffBehaviorScore / 10) * 3)

        const lineManagerPerformanceSum = staffKeyResults.reduce((acc, curr) => acc + curr.lineManagerRating, 0)

        const lineManagerPerformanceScore = ((lineManagerPerformanceSum / (staffKeyResults.length * 5)) * 100)

        const lineManagerBehaviorSum = behavioralIndicators.reduce((acc, curr) => acc + curr.lineManagerRating,  0)

        const lineManagerBehaviorScore = ((lineManagerBehaviorSum / (behavioralIndicators.length * 5)) * 100)

        const lineManagerTotalScore = ((lineManagerPerformanceScore / 10) * 7) + ((lineManagerBehaviorScore / 10) * 3)


        if(behavioralLoadingStatus){
            return (
                <Loader active/>
            )
        }


        return(
            <Grid padded>
                <Grid.Row>
                    <PageInfo
                        title='Click To View/Hide Assessment Scores'  
                        content={
                            <Grid divided columns={2}>
                                <Grid.Column width={8}>
                                    <Header as='h4' content="Staff"/>
                                    <div>Performance: <span style={{fontWeight: "bold"}}>{staffPerformanceScore.toFixed(1) + '%'}</span></div>
                                    <div>Behavior: <span style={{fontWeight: "bold"}}>{staffBehaviorScore.toFixed(1) + '%'}</span></div>
                                    <div style={{marginTop: "10px", fontSize: "1.3em"}}>Total: <span style={{fontWeight: "bold"}}>{staffTotalScore.toFixed(1) + '%'}</span></div>
                                </Grid.Column>
                                <Grid.Column width={8}>
                                    <Header as='h4' content="Line Manager"/>
                                    <div >Performance: <span style={{fontWeight: "bold"}}>{lineManagerPerformanceScore.toFixed(1) + '%'}</span></div>
                                    <div>Behavior: <span style={{fontWeight: "bold"}}>{lineManagerBehaviorScore.toFixed(1) + '%'}</span></div>
                                    <div style={{marginTop: "10px", fontSize: "1.3em"}}>Total: <span style={{fontWeight: "bold"}}>{lineManagerTotalScore.toFixed(1) + '%'}</span></div>
                                </Grid.Column>
                            </Grid>
                        }
                    />
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column computer={6} tablet={6} mobile={16}>
                        <Staff {...this.props}/>
                    </Grid.Column>
                    <Grid.Column computer={10} tablet={10} mobile={16}>
                        <Switch>
                            {behavioralIndicators.map((item, index) => <Route key={item.behaviour.id} path={`${match.path}/superassessbehaviour/${item.behaviour.id}`} render={(props) => <Results 
                                                                                                                                                                name={item.behaviour.description.substr(0, item.behaviour.description.indexOf(':'))} 
                                                                                                                                                                description={item.behaviour.description.substr(item.behaviour.description.indexOf(':')+2)} 
                                                                                                                                                                behaviour={item.behaviour} staffComment={item.staffComment} staffRating={item.staffRating} 
                                                                                                                                                                behaviourId={item.behaviourId} staffAppraisalId={item.staffAppraisalId} id={item.id}
                                                                                                                                                                peer1Rating={item.peer1Rating} peer2Rating={item.peer2Rating} lineManagerRating={item.lineManagerRating}
                                                                                                                                                                peer1Comment={item.peer1Comment} peer2Comment={item.peer2Comment} lineManagerComment={item.lineManagerComment}
                                                                                                                                                                superLineManagerRating={item.superLineManagerRating} superLineManagerComment={item.superLineManagerComment}
                                                                                                                                                                superLineManagerStatus={item.superLineManagerStatus}
                                                                                                                                                                index={index}
                                                                                                                                                                {...this.props} {...props}/>}/>)}
                        </Switch>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        )
    }
}

export default SuperAssessBehaviour
