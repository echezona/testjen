import React from "react";
import { Dropdown } from "semantic-ui-react";
import { Link } from "react-router-dom";

const staffList = [
  {
    key: "Upload Line Manager",
    text: "Upload Line Manager",
    value: "Upload Line Manager",
    as: Link,
    to: "/upload-line-manager"
  },
  {
    key: "Upload Staff",
    text: "Upload Staff",
    value: "Upload Staff",
    as: Link,
    to: "/upload-staff"
  },
  {
    key: "Upload Business Unit",
    text: "Upload Business Unit",
    value: "Upload Business Unit",
    as: Link,
    to: "/upload-business-unit"
  },
  {
    key: "Upload Department",
    text: "Upload Department",
    value: "Upload Department",
    as: Link,
    to: "/upload-department"
  }
];

const DropdownSelection = () => (
  <Dropdown placeholder="Upload" fluid selection options={staffList} />
);

export default DropdownSelection;
